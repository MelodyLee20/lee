# Install the python packages
pipenv install

# Generate the executable under Ubuntu
# The executable needs the main.kv to display the UI
pyinstaller main_ubuntu.spec

# Generate the executable under Windows
# The executable needs the main.kv to display the UI
pyinstaller main_win.spec

