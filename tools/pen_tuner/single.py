"""
Pen tuner.
"""

import sys
import os
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from ui_single import Ui_MainWindow
import logging
sys.path.append("../../")
import touch.fwio2 as fwio
import numpy as np

# mode       0x1:"Train mode", 0x2:"Test mode"
# train_type 0x1:"abswt", 0x2:"total_negative_wt", 0x3:"tx_freq"

class PenTuner(QMainWindow, Ui_MainWindow):

    def __init__(self):
        # Explaining super is out of the scope of this article
        # So please google it if you're not familar with it
        # Simple reason why we use it here is that it allows us to
        # access variables, methods etc in the design.py file
        super().__init__()
        self.setupUi(self)  # This is defined in design.py file automatically

        self.touch_dev = fwio.core(logging.DEBUG, ui="pyqt")
        if self.touch_dev.search() != True:
            self.touch_dev = None
            sys.exit()
        self.touch_dev.open()
        self.touch_dev.set_callback(self.callback)
        self.touch_dev.start_server()

        self.abs_weight = 0
        self.total_negative = 0
        self.tx_freq = 0
        self.start_test = False
        self.hidraw_buf = []
        self.hidraw_buf_size = 1000

        #初始化一个定时器
        self.timer=QTimer()
        #定时器结束，触发showTime方法
        self.timer.timeout.connect(self.check_hidraw)
        self.timer.start(50)
        self.le_abs_weight.setText(str(self.abs_weight))
        self.le_abs_weight.editingFinished.connect(self.set_abs_weight)
        self.le_total_negative.setText(str(self.total_negative))
        self.le_total_negative.editingFinished.connect(self.set_total_negative)
        self.le_tx_freq.setText(str(self.tx_freq))
        self.le_tx_freq.editingFinished.connect(self.set_tx_freq)

        self.btn_execute.clicked.connect(self.execute)

    def check_hidraw(self):
        if self.start_test == True:
            tmp = self.touch_dev.get_hidraw()
            if not tmp:
                return
            if len(tmp) > 0:
                self.hidraw_buf.extend(tmp)
            if len(self.hidraw_buf) > self.hidraw_buf_size:
                self.start_test = False
                
                tmp = np.array(self.hidraw_buf[:self.hidraw_buf_size])
                pen_data = tmp[:,[2,9]]
                #'[{}]'.format(', '.join(hex(x) for x in a_list))  
                pen_id = tmp[:,2] 
                j = np.bincount(pen_id)
                jj = np.nonzero(j)[0]
                scores = np.vstack((jj,j[jj]))
                number_of_data = np.sum(scores[1], axis=0)
                result = ""
                for i in range(len(scores[0])):
                    result = result + "id {} appears {} times ({}%)\n".format(scores[0][i],scores[1][i],int((scores[1][i]*100)/number_of_data))
                    
                self.update_score(result)
                self.hidraw_buf.clear() 
                

    def callback(self, ws, status, msg, data=[]):
        self.update_status(msg) 
        print([status, msg, data])

    def set_abs_weight(self):

        text = self.le_abs_weight.text()
        value = to_number(text, 'int')
        if (value):
            self.abs_weight = value
            self.le_total_negative.setText("0")
            self.le_tx_freq.setText("0")
        else:
            message = "Invalid value!"
            QMessageBox.information(self, "Error", message, QMessageBox.Close)

    def set_total_negative(self):

        text = self.le_total_negative.text()
        value = to_number(text, 'int')
        if (value):
            self.total_negative = value
            self.le_tx_freq.setText("0")
            self.le_abs_weight.setText("0")
        else:
            message = "Invalid value!"
            QMessageBox.information(self, "Error", message, QMessageBox.Close)


    def set_tx_freq(self):

        text = self.le_tx_freq.text()
        value = to_number(text, 'int')
        if (value):
            self.tx_freq = value
            self.le_total_negative.setText("0")
            self.le_abs_weight.setText("0")
        else:
            message = "Invalid value!"
            QMessageBox.information(self, "Error", message, QMessageBox.Close)


    def update_status(self, value):
        self.lb_status.setText("Status: {}".format(value))

    def update_score(self, value):
        self.lb_score.setText("Score: {}".format(value))

    def execute(self):
        if self.abs_weight > 0:
            self.touch_dev.online_train_mode(train_type=1, abswt=self.abs_weight)
        elif self.total_negative > 0:
            self.touch_dev.online_train_mode(train_type=2, total_negative_wt=self.total_negative)
        elif self.tx_freq > 0:
            self.touch_dev.online_train_mode(train_type=3, tx_freq=self.tx_freq)
        self.start_test = True
        self.update_score("")

def to_number(string, type, int_type=10):
    # Convert a string into a number

    if type == 'float':
        func = float
        try:
            value = func(string)
        except:
            value = None
    if type == 'int':
        func = int
        try:
            value = func(string, int_type)
        except:
            value = None

    return value


def main():
    app = QApplication(sys.argv)  # A new instance of QApplication
    tuner = PenTuner()
    tuner.show()
    app.exec_()  # and execute the app


if __name__ == '__main__':  # if we're running file directly and not importing it
    main()  # run the main function
