"""
Pen tuner.
"""
import threading
import math
import platform
import sys
import os
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from ui_main import Ui_MainWindow
import logging
sys.path.append("../../../touch")
import touch.fwio2 as fwio
import numpy as np
import time

# mode       0x1:"Train mode", 0x2:"Test mode"
# train_type 0x1:"abswt", 0x2:"total_negative_wt", 0x3:"tx_freq"

class PenTuner(QMainWindow, Ui_MainWindow):

    def __init__(self):
        # Explaining super is out of the scope of this article
        # So please google it if you're not familar with it
        # Simple reason why we use it here is that it allows us to
        # access variables, methods etc in the design.py file
        super().__init__()
        self.setupUi(self)  # This is defined in design.py file automatically

        self.touch_dev = fwio.core(logging.ERROR, ui="pyqt")
        if self.touch_dev.search() != True:
            self.touch_dev = None
            sys.exit()
        self.touch_dev.start_server(cb=self.callback)
        self.touch_dev.open()

        self.start_tunning = 0
        self.start_test = False
        self.hidraw_buf = []
        self.score_buf = []
        self.pen_id = 0
        self.testing_abs_weight = 0
        self.testing_total_negative = 0
        self.testing_tx_freq = 0
        self.numbers_of_collection = 0
        self.best_score = 0
        self.total_combination_no = 0
        self.testing_counter = 0
        self.cancel_tunning = 0
        self.first_data_skipped = False

        self.timer = QTimer()
        self.timer.timeout.connect(self.check_hidraw)
        self.timer.start(50)
        self.le_abs_weight.setText(str(0))
        self.le_total_negative.setText(str(0))
        self.le_tx_freq.setText(str(0))
        self.le_abs_weight_max.setText(str(0))
        self.le_total_negative_max.setText(str(0))
        self.le_tx_freq_max.setText(str(0))
        self.le_pen_id.setText(str(85))
        self.le_abs_weight_step.setText(str(1))
        self.le_total_negative_step.setText(str(1))
        self.le_tx_freq_step.setText(str(1))
        self.le_numbers_of_collection.setText(str(100))

        self.btn_execute.clicked.connect(self.execute)
        self.btn_stop.clicked.connect(self.stop)
        self.btn_save.clicked.connect(self.save)

    def check_hidraw(self):
        if self.cancel_tunning == 1:
            self.start_test = False
            
        if self.start_test == True:
            tmp = self.touch_dev.get_hidraw()
            if not tmp:
                return
            if len(tmp) > 0:
                # skip first hid data
                if len(self.hidraw_buf) == 0 and self.first_data_skipped == False:
                    if len(tmp) > 1:
                        self.hidraw_buf.extend(tmp[1:])
                    self.first_data_skipped = True
                else:
                    self.hidraw_buf.extend(tmp)
            if len(self.hidraw_buf) > self.numbers_of_collection:
                tmp = np.array(self.hidraw_buf[:self.numbers_of_collection])
                print("hidraw: {}".format(tmp))
                pen_data = tmp[:,[2,9]]
                
                pen_id = tmp[:,2] 
                j = np.bincount(pen_id)
                jj = np.nonzero(j)[0]
                scores = np.vstack((jj,j[jj]))
                number_of_data = np.sum(scores[1], axis=0)
                get_score = False
                result = ""
                for i in range(len(scores[0])):
                    if scores[0][i]  == self.pen_id: 
                        get_score = True
                        score = scores[1][i] / number_of_data
                        if self.best_score < score:
                            self.best_score = score
                            self.update_best("abs_wt={}, total_negative_wt={}, tx_freq={}, score={}\n".format(self.testing_abs_weight, self.testing_total_negative, self.testing_tx_freq, round(score,4)))
                        self.score_buf.append([self.testing_abs_weight, self.testing_total_negative, self.testing_tx_freq, round(score,4)])
                if get_score == False:
                        self.score_buf.append([self.testing_abs_weight, self.testing_total_negative, self.testing_tx_freq, 0.0])
                        #self.result = self.result + "abs_wt={}, total_negative_wt={}, tx_freq={}, score={}\n".format(self.testing_abs_weight, self.testing_total_negative, self.testing_tx_freq, 0)
                tmp = np.array(self.score_buf)
                tmp.view('i8,i8,i8,f8').sort(order=['f3'], axis=0)
                for i in reversed(tmp):
                        result = result + "abs_wt={}, total_negative_wt={}, tx_freq={}, score={}\n".format(i[0],i[1],i[2],i[3])
                self.update_score(result)
                self.update_progressive("{} / {}".format(self.testing_counter, self.total_combination_no))
                self.hidraw_buf.clear() 
                self.start_test = False
                self.first_data_skipped = False
                
                

    def callback(self, ws, status, id, msg, data=[]):
        self.update_status(msg) 
        print([status, msg, data])

    def update_progressive(self, value):
        self.lb_progressive.setText("progressive: {}".format(value))

    def update_best(self, value):
        self.lb_best.setText("best: {}".format(value))

    def update_status(self, value):
        self.lb_status.setText("status: {}".format(value))

    def update_score(self, value):
        self.lb_score.setText("{}".format(value))

    def stop(self):
        self.cancel_tunning = 1
        self.update_status("stop tunning") 


    def save(self):
        filename = "report_{}.txt".format(int(time.time()))
        with open(filename, "w") as fo:
            result = self.lb_score.text() 
            fo.write(result)
            fo.close()
            self.update_status("save file to {}".format(filename)) 

    def execute(self):
        if self.start_tunning == 0:
            self.start_tunning = 1
            self.update_score("")
            self.update_best("")
            self.update_progressive("")

            self.read_thread = threading.Thread(target=self.execute_thread)
            self.read_thread.daemon = True
            self.read_thread.start()

    def execute_thread(self):
        abs_weight = to_number(self.le_abs_weight.text(), 'int')
        abs_weight_max = to_number(self.le_abs_weight_max.text(), 'int')
        total_negative = to_number(self.le_total_negative.text(), 'int')
        total_negative_max = to_number(self.le_total_negative_max.text(), 'int')
        tx_freq = to_number(self.le_tx_freq.text(), 'int')
        tx_freq_max = to_number(self.le_tx_freq_max.text(), 'int')
        abs_weight_step = to_number(self.le_abs_weight_step.text(), 'int')
        total_negative_step = to_number(self.le_total_negative_step.text(), 'int')
        tx_freq_step = to_number(self.le_tx_freq_step.text(), 'int')
        self.pen_id = to_number(self.le_pen_id.text(), 'int')
        self.numbers_of_collection = to_number(self.le_numbers_of_collection.text(), 'int')
        abs_weight_loops = math.ceil((abs_weight_max+1-abs_weight)/abs_weight_step)
        total_negative_loops = math.ceil((total_negative_max+1-total_negative)/total_negative_step)
        tx_freq_loops = math.ceil((tx_freq_max+1-tx_freq)/tx_freq_step)
        if abs_weight_loops == 0:
            abs_weight_loops = 1
        if total_negative_loops == 0:
            total_negative_loops = 1
        if tx_freq_loops == 0:
            tx_freq_loops = 1
        self.total_combination_no = abs_weight_loops * total_negative_loops * tx_freq_loops
        self.score_buf.clear() 
        self.best_score = 0
        self.testing_counter = 0
        for i in range(abs_weight, abs_weight_max+1, abs_weight_step):
            for j in range(total_negative, total_negative_max+1, total_negative_step):
                for k in range(tx_freq, tx_freq_max+1, tx_freq_step):
                    if self.cancel_tunning == 1:
                        self.cancel_tunning = 0
                        self.start_tunning = 0
                        return
                    self.testing_abs_weight = i
                    self.testing_total_negative = j
                    self.testing_tx_freq = k
                    self.testing_counter += 1
                    self.touch_dev.online_train_mode(train_type=7, abswt=self.testing_abs_weight, total_negative_wt=self.testing_total_negative, tx_freq=self.testing_tx_freq)
                    self.start_test = True
                    while self.start_test == True:
                        time.sleep(0.1)
                print("abs:{}, total_negative:{}, tx_freq:{} finished ".format(i, j, k))
                self.start_tunning = 0

def to_number(string, type, int_type=10):
    # Convert a string into a number

    if type == 'float':
        func = float
        try:
            value = func(string)
        except:
            value = None
    if type == 'int':
        func = int
        try:
            value = func(string, int_type)
        except:
            value = None

    return value


def main():
    app = QApplication(sys.argv)  # A new instance of QApplication
    tuner = PenTuner()
    tuner.show()
    app.exec_()  # and execute the app


if __name__ == '__main__':  # if we're running file directly and not importing it
    main()  # run the main function
