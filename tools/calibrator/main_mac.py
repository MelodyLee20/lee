# -*- coding: utf-8 -*-
import sys
import os
import time
import numpy as np
import json
import logging
sys.path.append("../../")
import touch.fwio2 as fwio
import platform
import subprocess

# Version
__version__ = '0.2.0'

# Model fitting
from fitting import fit_model

import threading

class StopThread(StopIteration): pass

threading.SystemExit = SystemExit, StopThread

class thread_stoppable(threading.Thread):

    def stop(self):
        self.__stop = True

    def _bootstrap(self):
        if threading._trace_hook is not None:
            raise ValueError('Cannot run thread with tracing!')
        self.__stop = False
        sys.settrace(self.__trace)
        super()._bootstrap()

    def __trace(self, frame, event, arg):
        if self.__stop:
            raise StopThread()
        return self.__trace


def convert_fitting_params(params):
    for i in range(6):
        if params[i] < 0 and params[i] > -1:
            params[i] -= 32768
            print(params[i])

    print(params)
    numerator = np.modf(np.array([params[0],params[1],params[3],params[4],params[2],params[5]]))
    denominator = np.modf(np.array([params[6],params[7]]))
    numerator_fra = (np.absolute(numerator[0])* 65536).astype(int)
    numerator_int = numerator[1].astype(int)
    denominator_fra = (denominator[0] * (1<<27)).astype(int)
    n_fra = []
    n_int = []
    d_fra = []
    data = []
    for i in range(len(numerator_int)):
        n_fra.append([(numerator_fra[i]>>8) & 0xff, numerator_fra[i] & 0xff])
        n_int.append([(numerator_int[i]>>8) & 0xff, numerator_int[i] & 0xff])
    for i in range(len(denominator)):
        d_fra.append([(denominator_fra[i]>>24) & 0xff, (denominator_fra[i]>>16) & 0xff, (denominator_fra[i]>>8) & 0xff, denominator_fra[i] & 0xff])
    for i in range(0,len(numerator_int),2):
        data.append([n_int[i][0], n_int[i][1], n_fra[i][0], n_fra[i][1]])
        data.append([n_int[i+1][0], n_int[i+1][1], n_fra[i+1][0], n_fra[i+1][1]])
    data.append([d_fra[0][0], d_fra[0][1], d_fra[0][2], d_fra[0][3]])
    data.append([d_fra[1][0], d_fra[1][1], d_fra[1][2], d_fra[1][3]])
    return data


if __name__ == '__main__':
    proc = subprocess.Popen("/Users/sis_test/anaconda3/bin/python3.7 reset.py&", shell=True)
    #proc = subprocess.call("/Users/sis_test/anaconda3/bin/python3.7 reset.py", shell=True)
    proc.wait()
    time.sleep(10)
    proc = subprocess.Popen("killall -9 python3.7", shell=True)
    proc.wait()
    print("reset finished")
    touch_dev = fwio.core(logging.DEBUG)
    cmd_thread = None 
    if touch_dev.search() == True:
        print("Find Device")

        def callback(ws, status, msg, data=[]):
            print("get callback")
            print([status, msg, [hex(item).upper() for item in data[1:]]])
            ws_data = {"type":"firmwareUpdated", "errCode": status}
            ws.send_updates(ws_data)
            if cmd_thread is not None:
                try:
                    cmd_thread.stop()
                except:
                    print("command thread exited")

        def update_firmware(params):
            # Update the firmware
            print('params: {}'.format(params))
            data = convert_fitting_params(params)
            print(data)
            cmd_thread = thread_stoppable(target=touch_dev.calibrate_coordinates, args=(data,))
            cmd_thread.start()
            err_code = 0
            return err_code

        def handler(data):

            if data['cmd'] == "requestResetFitting":
                # Reset touch calibrate data
                pass

            elif data['cmd'] == "requestPoint":
                pass
                # Prepare the TP point

                # To be corrected !!!
                #tp_point = [1, 1]

                #wsData = {"type":"tpPoint","data": tp_point}
                #wsData = json.dumps(wsData)
                #self.send_updates(wsData)

            elif data['cmd'] == "requestFitting":
                # Obtain the target amd touch points in the screen coordinates
                target_points = np.array(data['value']['targetPoints']);
                touch_points = np.array(data['value']['touchPoints']);

                # Screen resolution
                nx_sc = data['value']['screenWidth'];
                ny_sc = data['value']['screenHeight'];

                # TP resolution (4096x4096 is assumed)
                nx_tp = 4096
                ny_tp = 4096

                # Resolution ratio
                ratio_x = 1.0*nx_sc/nx_tp
                ratio_y = 1.0*ny_sc/ny_tp

                # Fit the model
                score, params = fit_model(touch_points, target_points, ratio_x, ratio_y)

                # Send out the training score
                ws_data = {"type":"fittingFinished", "score":score}
                ws_data = json.dumps(ws_data)
                print("wsData: {}".format(ws_data))
                touch_dev.ws_handler.send_updates(ws_data)

                # Update the firmware
                err_code = update_firmware(params)

            elif data['cmd'] == "requestFinalization":
                # Finalize the run.
                print('close touch device.')
                touch_dev.close()

        print("Open Device")
        touch_dev.open()
        touch_dev.set_callback(callback)
        touch_dev.start_server(handler)

    print("close")
    touch_dev.close()
