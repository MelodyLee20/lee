#include <stdio.h>
#include <string.h>
#include <math.h>


void PrintBuffer(unsigned char *id, unsigned int byteLen )
{
    unsigned int i;
    for(i = 0; i < byteLen; i++ )
    {
        if(i%16==0)
            printf("\n");
        printf( "%02X ", id[i] );
    }
    printf( "\n" );
}



int separateDoubleToShortShort( double * _doubles, unsigned char * _bytes, const int nDoubles, const int nByte)
{
    /*
     *  Spearate a double into two short. The fractional part is always positive and multi by 65525.
     *  This double only uses integer to represent positive or negitive.
     *  So integer part has range between 127~-127, -128 represented -0 ( ex. -0.12 )
     */

    const double _65535 = 65535.0f;

    /* for perform float << 30 bits */
    //const double _2exp30 = 1000000000.0f;
//    const double _2exp30 = 1073741824.0f;
    /* for perform float << 24 bits */
//    const double _2exp24 = 16777216.0f;
    const double _2exp27 = 134217728.0f;

//    const double _230 = 1073741824.0f;
//    const double _208 = 256.0f;

//    const double _2exp8 = 100000000.0f;

        printf("nByte:%d, nDoubles:%d\n", nByte, nDoubles);
    if ( nByte < (nDoubles * 4) ) { 
        return -1; 
    }
    for ( int i = 0, j = 0; i < nDoubles; i++, j++ )
    {
             printf("_doubles[%d]: %f \n", i, _doubles[i]);
        if ( ( _doubles[i] > 32767.0f ) || ( _doubles[i] < -32767.0f ) ) {
             return -1; 
        }
        double integer = 0.0f;
        double fractional = 0.0f;
        fractional = modf(_doubles[i], &integer);

        if ( 6 == i || 7 == i )
        {
            // R1, R2 only need fractional part, and these 2 need shift 30 bits

            //fractional = fractional * _2exp30;
            fractional = fractional * _2exp27;
            printf("the %d raw is %lf\n", i, fractional);
            //fractional = fractional * _2exp24;
            //fractional = fractional * _230;
            //printf("_230 is %lf\n", fractional);
            //fractional = fractional * _208;
            //printf("_208 is %lf\n", fractional);

            // for translate into integer
            //fractional = fractional * _2exp8;

            int sh_fra = (int)fractional;
            printf("the %d int is %d\n", i, sh_fra);

            //printf("the int is %d\n", sh_fra);

            //printf("the shift int is %d\n", sh_fra >> 8);

            _bytes[j*4+0] = (unsigned char)(( sh_fra >> 24 ) & 0xff);
            _bytes[j*4+1] = (unsigned char)(( sh_fra >> 16 ) & 0xff);
            _bytes[j*4+2] = (unsigned char)(( sh_fra >> 8 ) & 0xff);
            _bytes[j*4+3] = (unsigned char)(sh_fra & 0xff);
        }
        else
        {
            // the fractional must be positive
            if ( fractional < 0 )
            {
                fractional = fractional * -1.0f;
            }
            //printf("the f is %lf\n", fractional);

            fractional = fractional * _65535;

            //printf("the _65535 f is %lf\n", fractional);


            short sh_int = (short)integer;
            short sh_fra = (short)fractional;

            if ( _doubles[i] < 0 && integer == 0 )
            {
                sh_int = -32768;
            }

            _bytes[j*4+0] = ( sh_int >> 8 )& 0xff;
            _bytes[j*4+1] = sh_int & 0xff;
            _bytes[j*4+2] = ( sh_fra >> 8 )& 0xff;
            _bytes[j*4+3] = sh_fra & 0xff;
        }

//        if(g_SiSCoreWrapper.getFwGeneration() == "819")
//        {
//            unsigned char temp[4] = {0};
//            for(int ti = 0; ti < 4; ti++)
//                temp[ti] =  _bytes[j*4+ti];
//            for(int ti = 0; ti < 4; ti++)
//                _bytes[j*4+ti] =  temp[3-ti];

//        }
    }

    return 0;
}

int SIS_setCoordinateParam( const double Ax, const double Bx, const double Ay, const double By, const double Dx, const double Dy, const int type, const double R1, const double R2 )
{
    const unsigned int nAllByte = 10 * 4;
    const unsigned int nCurrentParam = 8;

    unsigned char params[nAllByte];
    memset(params, 0, sizeof(char) * nAllByte);

    double currentParams[nCurrentParam];
    memset(currentParams, 0, sizeof(double) * nCurrentParam);

    currentParams[0] = Ax;
    currentParams[1] = Ay;
    currentParams[2] = Bx;
    currentParams[3] = By;
    currentParams[4] = Dx;
    currentParams[5] = Dy;
    currentParams[6] = R1;
    currentParams[7] = R2;

    if ( separateDoubleToShortShort(currentParams, params, nCurrentParam, nAllByte) == -1 )
    {
        printf("convert error\n");
        return -1;
    }

    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 10; j++){
            char paramsByte[10] = "";
            sprintf(paramsByte, "%02x", params[i*10+j]);
            printf("%s", paramsByte);
        }
        printf("\n");
    }
    return 0;
}

int main()
{
 //pdict: {'Ax': -28.005943683329946, 'Bx': 44.252979542140579, 'Cx': 2715.5416738258191, 'Dx': -0.014026602209779743, 'Ex': 0.023076494515745106, 'Ay': 0.096593529937993305, 'By': -0.049589602147817519, 'Cy': -0.013017942860144939, 'Dy': -0.00093689349680366055, 'Ey': -0.00031719680312600836}
    double Ax= -28.005943683329946, Bx= 44.252979542140579, Cx= 2715.5416738258191, Dx= -0.014026602209779743, Ex= 0.023076494515745106, Ay= 0.096593529937993305, By= -0.049589602147817519, Cy= -0.013017942860144939, Dy= -0.00093689349680366055, Ey= -0.00031719680312600836;

    SIS_setCoordinateParam( 1, 0, 0, 1, 0, 0, 2, 0, 0);
    SIS_setCoordinateParam( Ax, Bx, Ay, By, Dx, Dy, 2, Ex, Ey);

    return 0;
}

