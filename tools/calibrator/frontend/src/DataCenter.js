
class DataCenter { 
	// Data Center which takes care the data exchange via websocket.

	constructor(wsUri) {
    this.data = [];

		// Websocket
		this.wsUri = wsUri;
		this.websocket = null;
		this.connected = false;
		this.dataSent = null;

		// Handlers
		this.pointArriveHandler = null;

	}

  connect() {
		this.websocket = new WebSocket(this.wsUri);

		// callbacks
	  this.websocket.onopen = (e) => { 
			console.log('Connected');
		  this.connected = true;
		};
	  this.websocket.onclose = (e) => { 
			console.log('Disconnected');
		  this.connected = false;
		};
	  this.websocket.onmessage = (e) => {
			this.dataReceived = JSON.parse(e.data);

			if (this.dataReceived.type === "fittingFinished") {
        this.handleFittingFinished(this.dataReceived);
			} else if (this.dataReceived.type === "firmwareUpdated") {
        this.handleFirmwareUpdated(this.dataReceived);
			}	
		};

	  this.websocket.onerror = (e) => { 
			console.log('Connection failed');
		};
	}

	disconnect() {
		console.log('user Disconnected');
		this.websocket.close();
	  this.connected = false;
	}

  send(data) {
		this.dataSent = data;
		this.websocket.send(this.dataSent);
	}	

  setPointArriveHandler(handler) {
		this.pointArriveHandler = handler;
	}

  setHandleFittingFinished(handler) {
		this.handleFittingFinished = handler;
	}

  setHandleFirmwareUpdated(handler) {
		this.handleFirmwareUpdated = handler;
	}

}

export {DataCenter}
