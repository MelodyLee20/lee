import React from 'react';
import {Button} from 'react-bootstrap';

class Fitting extends React.Component {
  constructor(props) {
    super(props);
		this.state = {
		  fittingFinished: false,
		  firmwareUpdated: false,
    }
		this.score = null;

	  this.handleRecalibrateClicked = this.handleRecalibrateClicked.bind(this);
	  this.handleFittingFinished = this.handleFittingFinished.bind(this);
	  this.handleFirmwareUpdated = this.handleFirmwareUpdated.bind(this);
  }

  componentDidMount() {
    this.props.dataCenter.setHandleFittingFinished(this.handleFittingFinished);
    this.props.dataCenter.setHandleFirmwareUpdated(this.handleFirmwareUpdated);

    // Request fitting
		this.requestFitting();

	}	

	handleRecalibrateClicked() {
		this.props.onRecalibrate();
	}	

	handleFittingFinished(data) {
		this.score = data.score;
		this.setState({fittingFinished: true});
	}	

	handleFirmwareUpdated(data) {
		let errCode = data.errCode;
		let sucessful;
		console.log(errCode);

		if (errCode === 0) {
        sucessful = true
		} else {	
        sucessful = false
		}	
		this.setState({firmwareUpdated: sucessful});
	}	

    
  requestFitting() {
    
    const dc = this.props.dataCenter;
    const targetPoints = this.props.targetPoints;
    const touchPoints = this.props.touchPoints;
 
    let fittingData = {targetPoints: targetPoints, touchPoints: touchPoints, screenHeight: window.screen.height, screenWidth: window.screen.width}
		let data = {cmd: "requestFitting", value: fittingData};
		data = JSON.stringify(data);
		dc.send(data);

	}	

  get_score() {
    return this.score.toFixed(2)
	}	

  render() {
    return (
		  <div>
		    <div>
			    {this.state.fittingFinished ? `Fitting finished. (score: ${this.get_score()})` : 'Fitting the model...'}
		    </div>
		    <div>
			    {this.state.firmwareUpdated ? 'Firmware is updated.' : 'Updating the firmware...'}
		    </div>
			  <Button variant="outline-primary" onClick={this.handleRecalibrateClicked} > Recalibrate </Button>
	    </div>
    );
  }
}


// Export modules
export {Fitting}
