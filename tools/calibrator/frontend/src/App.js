import React from 'react';
import { CalibrationType } from './CalibrationType';
import { TargetInput } from './TargetInput';
import { Fitting } from './Fitting';
import { DataCenter } from './DataCenter';
import { finalizeBeforeClosingTab, disablePinZoom } from './CommonUtils';
import './App.css';

// Websocket address
const wsUri = "ws://127.0.0.1:8050/ws";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			type: '2x2', // rows x cols
			flowStep: 0,
		};

		this.rows = 2;
		this.cols = 2;

    this.touchPoints = null;
		this.targetPoints = null;

    // Handlers
    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.handleTypeConfirm = this.handleTypeConfirm.bind(this);
    this.handlePointsCollected = this.handlePointsCollected.bind(this);
    this.recalibrate = this.recalibrate.bind(this);

		// Data Center
    this.dc = new DataCenter(wsUri);
		this.dc.connect();
	}

  componentDidMount() {
		// Disable pin-zoom
		disablePinZoom();
		// Finalize before closing the browser tab.
    finalizeBeforeClosingTab(this.dc);
	}	

  showConnectStatus(connected) {
		return connected? 'Connected': 'Disconnected';
	}	

  updateFlowStep() {
    let flowStep = this.state.flowStep;

		flowStep++;
    this.setState({flowStep: flowStep});
	}	

  calibrate(touchPoints, targetPoints) {
		console.log('Calibrating.')

	}	

  calibrationFinished() {
		console.log('Calibration finished.')

	}	


  handleTypeChange(type) {
    this.setState({type: type});

    this.rows = parseInt(type[0])
    this.cols = parseInt(type[2])
  }

  handleTypeConfirm() {
		let data = {cmd: "requestResetFitting"};
		data = JSON.stringify(data);
		this.dc.send(data);
		this.updateFlowStep();
  }

    
  rearrangePoints(points) {
    // Rearrange the sequence of the points from column major to row major.
    
    let rows = this.rows;
		let cols = this.cols;

    let out = []
    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < cols; j++) {
        let indexIn = j*rows + i;
        let indexOut = i*cols + j;
        out[indexOut] = points[indexIn];
			}	
		}
    return out;
	}	

  handlePointsCollected(touchPoints, targetPoints) {
		this.touchPoints = this.rearrangePoints(touchPoints)
		this.targetPoints = this.rearrangePoints(targetPoints);

		this.updateFlowStep();
  }

  recalibrate() {
		this.touchPoints = null;
		this.targetPoints = null;
    this.setState({flowStep: 0});
  }

	renderContent() {

		const rows = this.rows;
		const cols = this.cols;
    
    //let content = [];
    let content;
		let calibrationType = <CalibrationType type={this.state.type} onTypeChange={this.handleTypeChange} onTypeConfirm={this.handleTypeConfirm} /> 
	  let targetInput = <TargetInput rows={rows} cols={cols} onPointsCollected={this.handlePointsCollected} dataCenter={this.dc} />;
	  let fitting = <Fitting onRecalibrate={this.recalibrate} dataCenter={this.dc} touchPoints={this.touchPoints} targetPoints={this.targetPoints} />;

    switch(this.state.flowStep) {
    case 0:
			content = calibrationType;
      break;
    case 1:
			content = targetInput;
      break;
    case 2:
			content = fitting;
      break;
		default:
				// Do nothing
		}	
		return content;
	}	


  render() {
    return (
      <div className="App">
			  {this.renderContent()}
      </div>
    );
  }
}

export default App;
