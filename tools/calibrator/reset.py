# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 16:12:27 2019

@author: carey
"""

import os
import sys
import time
import logging
sys.path.append("../../../touch")
import touch.fwio2 as fwio

def callback(ws, status, msg, data=[]):
    print("get callback")
    f = open("test","w")
    f.write(msg)
    f.close()
    print([status, msg, [hex(item).upper() for item in data[1:]]])
    if ws:
        ws.send_updates(msg)

if __name__ == '__main__':
    touch_dev = fwio.core(logging.DEBUG, ui="none")
    if touch_dev.search() == True:
        touch_dev.open()
        touch_dev.set_callback(callback)
        touch_dev.start_server()
        ret = touch_dev.calibrate_coordinates([],True)
    touch_dev.close()

