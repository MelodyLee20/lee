import numpy as np
import sys
from touch.fitting import SimpleModel

def to_tp_coordinates(points, ratio_x, ratio_y):

    points_out = np.zeros_like(points)
    points_out[:, 0] = points[:, 0] / ratio_x
    points_out[:, 1] = points[:, 1] / ratio_y

    return points_out


def fit_model(touch_points, target_points, ratio_x, ratio_y):
    # Fit the model

    # Convert into the TP coordinates
    touch_points = to_tp_coordinates(touch_points, ratio_x, ratio_y)
    target_points = to_tp_coordinates(target_points, ratio_x, ratio_y)

    # Fitting
    model = SimpleModel()
    score, popt = model.fit(touch_points, target_points)

    return score, popt

