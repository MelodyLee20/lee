import sys
import os
import numpy as np
import cv2 as cv
import face_recognition
from time import perf_counter

from touch.utils.file import to_str_digits
from touch.utils.image import make_square
from touch.utils.opencv import Webcam, wait_key, load_image_files

from object_tracker import ObjectTracker
from lib import *

# Load the cascade
# For Ubuntu
#face_cascade = cv.CascadeClassifier('/home/andrew/.local/lib/python3.6/site-packages/cv2/data/haarcascade_frontalface_default.xml')
#eye_cascade = cv.CascadeClassifier('/home/andrew/.local/lib/python3.6/site-packages/cv2/data/haarcascade_eye.xml')

# For windows
face_cascade = cv.CascadeClassifier('C:\Python37\Lib\site-packages\cv2\data\haarcascade_frontalface_default.xml')
eye_cascade = cv.CascadeClassifier('C:\Python37\Lib\site-packages\cv2\data\haarcascade_eye.xml')

#eye_cascade = cv.CascadeClassifier('C:\Python37\Lib\site-packages\cv2\data\haarcascade_eye.xml')

def detect_faces(frame, engine_type=0):

    if engine_type == 0: # OpenCV

        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.1, 4)

    else:

        frame_rgb = cv.cvtColor(frame, cv.COLOR_BGR2RGB)
        # Detect the faces
        face_locations = face_recognition.face_locations(frame_rgb)
        faces = to_locations(face_locations)

    return faces

def detect_eyes(frame, engine_type=0):

    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    eyes = eye_cascade.detectMultiScale(gray, 1.1, 4)

    return eyes


if __name__ == '__main__':


    webcam = Webcam()
    win_face = "face"

    if webcam.is_open():
        webcam.release()

    device = 0 # Device ID
    frame_width = 600
    frame_height = 300

    webcam.open(device, width=frame_width, height=frame_height)

    # Object tracker
    tracker = ObjectTracker(width=frame_width, height=frame_height)

    # Read and process the frame
    frame_count = 0
    while True:

        time_start = perf_counter()

        frame_count += 1
        print("Frame: {}".format(frame_count))

        frame = webcam.read() # Read the frame

        # Detect faces
        # 0: OpenCV, 1: dLib
        #faces = detect_faces(frame, engine_type=0)
        eyes = detect_eyes(frame, engine_type=0)

        # Tracking the target's face
        #target = tracker.get_target(faces)
        #(xc, yc) = tracker.get_position()

        image_out = draw_faces(frame, eyes, (0, 255, 0))
        #image_out = draw_faces(frame, faces, (0, 255, 0))

        '''
        if target:
            print("xc is {}".format(xc))
            image_out = draw_faces(image_out, [target], (255, 0, 0))

        '''

        # Show image
        #image_out = cv.resize(image_out, (frame_width, frame_height))
        cv.imshow(win_face, image_out)

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)
        print("fps is {}".format(fps))

        # Exit while 'q' is pressed
        key = wait_key(1)
        if key == ord("q"): break

    # Finalization
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
