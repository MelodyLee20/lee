import sys
import os
import numpy as np
import cv2 as cv
import face_recognition
from time import perf_counter

from touch.utils.file import to_str_digits
from touch.utils.image import make_square
from touch.utils.opencv import Webcam, wait_key, load_image_files

#from object_tracker import ObjectTracker
from centroid_tracker import CentroidTracker
from lib import *


if __name__ == '__main__':


    webcam = Webcam()

    if webcam.is_open():
        webcam.release()

    device = 0 # Device ID
    frame_width = 600
    frame_height = 300

    webcam.open(device, width=frame_width, height=frame_height)

    # Object tracker
    #tracker = ObjectTracker(width=frame_width, height=frame_height)
    tracker = CentroidTracker()

    # Read and process the frame
    frame_count = 0
    while True:

        time_start = perf_counter()

        frame_count += 1
        print("Frame: {}".format(frame_count))

        frame = webcam.read() # Read the frame

        # Detect faces
        faces = detect_faces(frame)
        #faces = detect_faces_opencv(frame)

        # Tracking the target's face
        #target = tracker.get_target(faces)
        #(xc, yc) = tracker.get_position()

        # update our centroid tracker using the computed set of bounding box
        tracker.update(faces)
        target_position = tracker.get_target_position()

        if target_position is not None:
            target_x = target_position[0]
            target_y = target_position[1]

            # Normalized target position
            target_x_norm = float(target_x) / frame_width
            target_y_norm = float(target_y) / frame_height

            print("target_x_norm is {}".format(target_x_norm))

            # Draw the target position
            cv.putText(frame, "target", (target_x - 10, target_y - 10),
            cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            cv.circle(frame, (target_x, target_y), 4, (0, 255, 0), -1)


        # Draw all faces
        image_out = draw_faces(frame, faces, (0, 255, 0))

        # Show image
        cv.imshow("face", image_out)

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)
        print("fps is {}".format(fps))

        # Exit while 'q' is pressed
        key = wait_key(1)
        if key == ord("q"): break

    # Finalization
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
