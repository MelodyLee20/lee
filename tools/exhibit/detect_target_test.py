import sys
import os
import numpy as np
from time import perf_counter
import cv2 as cv
from touch.utils.opencv import Webcam, wait_key, load_image_files
from touch.trackers import TargetTracker


if __name__ == '__main__':


    webcam = Webcam()

    if webcam.is_open():
        webcam.release()

    device = 1 # Device ID
    frame_width = 600
    frame_height = 300

    webcam.open(device, width=frame_width, height=frame_height)

    # Target tracker
    tracker = TargetTracker(frame_width=frame_width, frame_height=frame_height)

    # Read and process the frame
    frame_count = 0
    while True:

        time_start = perf_counter()

        frame_count += 1
        print("Frame: {}".format(frame_count))

        frame = webcam.read() # Read the frame

        # Update the tracker
        tracker.update(frame)
        position = tracker.get_position()
        position_norm = tracker.get_position_norm()

        print("position_norm is {}".format(position_norm))

        if position_norm:
            (x, y) = position_norm
            print("target x is {}".format(x))

        if position is not None:
            (x, y) = position

            # Draw the target position
            cv.putText(frame, "target", (x - 10, y - 10),
            cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            cv.circle(frame, (x, y), 4, (0, 255, 0), -1)


        # Show image
        cv.imshow("target", frame)

        # Frame rate
        time_end = perf_counter()
        time_duration = time_end - time_start
        fps = int(1.0/time_duration)
        print("fps is {}".format(fps))

        # Exit while 'q' is pressed
        key = wait_key(1)
        if key == ord("q"): break

    # Finalization
    if webcam.is_open():
        webcam.release()

    cv.destroyAllWindows()
