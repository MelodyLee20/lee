from touch.input_device import get_device_list, InputDevice

if __name__ == '__main__':

    device_list = get_device_list()
    for item in device_list:
        print(item)

    path = '/dev/input/event6'
    device = InputDevice(path)

    print("Device: {}, {}, {}".format(device.path, device.name, device.phys))

    for event in device.read_loop():
        print("event: ", event)
