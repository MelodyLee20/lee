# -*- coding: utf-8 -*-
import sys
import os
import time
import optparse
import tornado.web
import tornado.websocket
import tornado.httpserver
import tornado.process
import tornado.ioloop
import json

sys.path.append("../../../touch")
from touch.input_device import get_device_list
from touch_panel import TouchPanel

# Version
__version__ = '0.8.0'

# Global variables
touch_dev = None
debug_mode = True
NET_PORT = 8050

# Touch panel
panel = TouchPanel()

def parse_arguments():
  parser = optparse.OptionParser()

  parser.add_option('-a', '--addr', dest='addr', default=None,
                    help=('The address of the DUT'))

  parser.add_option('--debug', dest='debug',
                    default=False, action='store_true',
                    help=('Debug mode.'))

  (options, args) = parser.parse_args()

  return options, args


class WsHandler(tornado.websocket.WebSocketHandler):
    waiters = set()
    counter = 0
    begin = 0
    touch_util = None
    touch_dev = None


    def check_origin(self, origin):
        return True

    def open(self):
        WsHandler.waiters.add(self)
        print("client IP:" + self.request.remote_ip)


    def on_message(self, message):

        print(message)
        data = json.loads(message)
        print("received data: {}".format(data))

        if data['cmd'] == "requestDeviceList":
            device_list = get_device_list()

            ws_data = {"type":"deviceList", "data": device_list}
            ws_data = json.dumps(ws_data)
            WsHandler.send_updates(ws_data)

        if data['cmd'] == "requestTouchPoints":

            device_name = data['deviceName']

            # Set the device
            panel.set_device(device_name)

            # Set the websocket handler
            panel.set_ws_handler(WsHandler)

            # Set the ioloop instance
            ioloop_instance = tornado.ioloop.IOLoop.instance()
            panel.set_ioloop_instance(ioloop_instance)

            # Read and send points
            panel.read_and_send_points()

        if data['cmd'] == "requestFinalization":
            # Finalize the run.
            print('Perform finalization.')

            # Save file
            panel.save_file()

            # Stop the IOLoop
            tornado.ioloop.IOLoop.instance().stop()

    def on_close(self):
        WsHandler.waiters.remove(self)
        print("close ws")

    @classmethod
    def send_updates(cls, msg ):
        for waiter in cls.waiters:
            if cls.counter == 0:
                cls.begin = time.time()
            cls.counter += 1
            #print(msg)

            waiter.write_message( msg )


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        return self.render('index.html')

class PageNotFoundHandler(tornado.web.RequestHandler):
    def get(self):
        return self.write_error(404)

class Application(tornado.web.Application):
    def __init__(self):

        handlers = [
            (r"/ws", WsHandler),
            (r"/", IndexHandler),
            (r".*", PageNotFoundHandler),
        ]

        if getattr(sys, 'frozen', False): # For deployment
            static_folder = os.path.join(sys._MEIPASS, 'static')
            template_folder = os.path.join(sys._MEIPASS, 'templates')
        else: # For development
            static_folder = os.path.join(os.path.dirname(__file__), "static")
            template_folder = os.path.join(os.path.dirname(__file__), "templates")

        settings = dict(
            static_path=static_folder,
            template_path=template_folder,
        )
        tornado.web.Application.__init__(self, handlers, **settings)


def main():

    application = Application()
    server = tornado.httpserver.HTTPServer(application, xheaders=True)
    server.listen(NET_PORT)
    ioLoop = tornado.ioloop.IOLoop.instance()
    options, args = parse_arguments()

    print ("Web address: http://localhost:{}".format(NET_PORT))
    print ("websocket server start")
    ioLoop.start()
    print ("websocket server stop")


if __name__ == '__main__':

    main()
