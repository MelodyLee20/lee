
class DataCenter { 
	// Data Center which takes care the data exchange via websocket.

	constructor(wsUri) {
    this.data = [];

		// Websocket
		this.wsUri = wsUri;
		this.websocket = null;
		this.connected = false;
		this.dataSent = null;
	}

  setDeviceListArrivedHandler(handler) {
		this.handleDeviceListArrived = handler;
	}

  setPointsArrivedHandler(handler) {
		this.handlePointsArrived = handler;
	}

  connect() {
		this.websocket = new WebSocket(this.wsUri);

		// callbacks
	  this.websocket.onopen = (e) => { 
			console.log('Connected');
		  this.connected = true;
		};
	  this.websocket.onclose = (e) => { 
			console.log('Disconnected');
		  this.connected = false;
		};
	  this.websocket.onmessage = (e) => {
			this.dataReceived = JSON.parse(e.data);

			//console.log("Received data: ");
      //console.log(this.dataReceived);

			// Event List
			if (this.dataReceived.type === "deviceList") {
        this.handleDeviceListArrived(this.dataReceived.data);
			}
       
			if (this.dataReceived.type === "touchPoints") {
        this.handlePointsArrived(this.dataReceived.data);
			}	

		};

	  this.websocket.onerror = (e) => { 
			console.log('Connection failed');
		};
	}

	disconnect() {
		console.log('user Disconnected');
		this.websocket.close();
	  this.connected = false;
	}

  send(data) {
		this.dataSent = data;
		this.websocket.send(this.dataSent);
	}	


}

export {DataCenter}
