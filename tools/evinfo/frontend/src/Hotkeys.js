
function setHotkeys() {

  document.addEventListener("keyup", function(e) {
      switch(e.which) {
      // Press the 'ESC' key
      case 27:
        //ctx.clearRect(0, 0, canvas.width, canvas.height);
        break;

      // Press the 'p' key to come back to the previous point.
      case 80:
        //pointMode = !pointMode;
        break;

      // Press the 'r' key for recalibration
      case 82:
        //pressureMode = !pressureMode;
        break;

      // Press the 'up' key to increase the line width
      case 38:
        //if (lineWidth < 30) { lineWidth += 1; }
        //console.info('lineWidth: ' + lineWidth);
        break;

      // Press the 'down' key to decrease the line width
      case 40:
        //if (lineWidth > 1) { lineWidth -= 1; }	
        //console.info('lineWidth: ' + lineWidth);
        break;


      // Press the 'f' or 'Enter' key
      case 70:
      //case 13:
        if (document.documentElement.webkitRequestFullscreen) {
            if (document.webkitFullscreenElement)
                document.webkitCancelFullScreen();
            else
                document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
        break;
		
			default:
					// Do nothing
      }


  });

	return false;
}	

export {setHotkeys}
