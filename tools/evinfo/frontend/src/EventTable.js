import React from 'react';
import ReactTable from "react-table";
import 'react-table/react-table.css';
//import {Button} from 'react-bootstrap';

function drop_points(points, numMax) {
// Drop points.

  let num = points.length;
  let ia, iz;
  let out;

  if (num > numMax) {
     iz = num-1;
     ia = iz - numMax + 1;
     out = points.slice(ia, iz);
  } else {
    out= points;
  }	

  return out;
}

function renderTable(events, numEventsMax) {

  const columns = prepareColumns();
	let data = events.reverse(); // Reverse the order

  return <ReactTable data={data} columns={columns} sortable={false} pageSize={100} pageSizeOptions={[100, 200]} />
}


function makeColumn(header) {

  let col;

  col = {
		Header: () => (
			<span>
				{header}
			</span>
		),
    accessor: header.toUpperCase(), 
    Cell: props => ( 
      <div> 
			  {props.value}
      </div>
    ),
    minWidth: 100, // Minimum cell width 
  };

	return col;

}	


function prepareColumns() {

  let columns = [];
  let col;

	// Timestamp
	col = makeColumn('Timestamp') 
  columns.push(col);

	// X
	col = makeColumn('x') 
  columns.push(col);

	// Y
	col = makeColumn('y') 
  columns.push(col);

	// Pressure 
	col = makeColumn('Pressure') 
  columns.push(col);

	return columns;
}

class EventTable extends React.Component {
  constructor(props) {
    super(props);
		this.state = {
			events: []
		};	

		this.numPointsMax = 2000;
		this.points = []
    

		// Handlers 
		this.handlePointsArrived = this.handlePointsArrived.bind(this);
		this.dc = this.props.dc;
	}

	componentDidMount() {
		this.dc.setPointsArrivedHandler(this.handlePointsArrived);
		// Refresh the events  after a short interval of 1 sec
		setInterval( () => this.refreshEvents(), 1000)
	}	


  handlePointsArrived(newPoints) {

		this.points = this.points.concat(newPoints);

    // Drop the previous points when the number exceeds the maximun value
		this.points = drop_points(this.points, this.numPointsMax);

	}

	refreshEvents() {
		// Clone points as events
		let events = Object.assign([], this.points);
		// Refresh the touch events
		this.setState({events: events});
	}	

	render() {
		const events = this.state.events;
    return (
      <div>
			  {renderTable(events)}
	    </div>
		);	
	}
}	

export {EventTable}

