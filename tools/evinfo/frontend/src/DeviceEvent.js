import React from 'react';
import {Button} from 'react-bootstrap';

class DeviceEvent extends React.Component {
  constructor(props) {
    super(props);
		this.state = {
			deviceList: [],
      deviceName: null
		};	

		this.deviceList = []
		this.numDevices = null;

		// Handlers 
		this.handleDeviceListArrived = this.handleDeviceListArrived.bind(this);
		this.requestDeviceList = this.requestDeviceList.bind(this);
		this.cleanDeviceList = this.cleanDeviceList.bind(this);
		this.setDeviceName = this.setDeviceName.bind(this);
		this.requestTouchPoints = this.requestTouchPoints.bind(this);
		this.dc = this.props.dc;
	}

	componentDidMount() {
		this.dc.setDeviceListArrivedHandler(this.handleDeviceListArrived)
	}	

  requestDeviceList(e) {
    let wsdata = {"cmd":"requestDeviceList", "value":1};
		wsdata = JSON.stringify(wsdata);
    this.dc.send(wsdata);
	}

  requestTouchPoints(deviceName) {
    let wsdata = {"cmd":"requestTouchPoints", "deviceName": deviceName};
		wsdata = JSON.stringify(wsdata);
    this.dc.send(wsdata);
	}

  handleDeviceListArrived(data) {
		this.deviceList = data;
	  this.setState({deviceList: this.deviceList});
	}

  cleanDeviceList(e) {
		this.setState({deviceList: []});
	}

  displayDevices() {
	  return (
      <ul>
        {this.state.deviceList.map((item) => {
          return <li key={item}>{item}</li>;
        })}
      </ul>
		);
	}	

  setDeviceName(e) {
		const index = e.target.value
		const deviceName = 'event' + index;
	  this.setState({deviceName: deviceName});
		// Requst touch points
		this.requestTouchPoints(deviceName);
	}	

	displayDeviceChoice() {

		const numDevices = this.deviceList.length;
		let indexArray = [...Array(numDevices).keys()];
		let options = indexArray.map((i) => { return <option key={i} value={i}>{i}</option>});

		return (
			<div>
        Select the event number: &nbsp; 
        <select onChange={this.setDeviceName}>
			   {options}
			  </select>
			</div>
		)	
	}	


	render() {
    return (
      <div>
			  <Button variant="outline-primary" onClick={this.requestDeviceList}> Show Devices </Button>
			  <Button variant="outline-primary" onClick={this.cleanDeviceList}> Clean </Button>
			  {this.displayDevices()}
			  {this.displayDeviceChoice()}
	    </div>
		);	
	}
}	

export {DeviceEvent}

