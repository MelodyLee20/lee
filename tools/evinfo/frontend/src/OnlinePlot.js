import React from 'react';
import Plot from 'react-plotly.js';

function parseLimits(vmin, vmax) {

  var zmin = vmin
  var zmax = vmax
  if (isNaN(zmin) || zmin === '') {
    zmin = 0;
  }	
  if (isNaN(zmax) || zmax === '') {
    zmax = -1;
  }
  zmin = parseFloat(zmin);
  zmax = parseFloat(zmax);

  return [zmin, zmax]

}


class ValueLimits extends React.Component {
  constructor(props) {
    super(props);

		this.handleVminChange = this.handleVminChange.bind(this);
		this.handleVmaxChange = this.handleVmaxChange.bind(this);
	}

  handleVminChange(e) {
	  this.props.onVminChange(e.target.value);
  }		

  handleVmaxChange(e) {
	  this.props.onVmaxChange(e.target.value);
  }

	render() {
		const vmin = this.props.vmin;
		const vmax = this.props.vmax;

    return (
      <div>
        <label> vmin/vmax: </label>
 	      <input type="text" value={vmin} onChange={this.handleVminChange} />
		    <input type="text" value={vmax} onChange={this.handleVmaxChange} />
		  </div>
  	);	
	}
}	

class Line extends React.Component {

  jsonFactory(x, y, ymin=0, ymax=-1) {

    const width = 800;
    const height = 800;

    if (ymin > ymax) {
      ymin = 0;
	    ymax = 10;
	  }	 

		var json = {
			data: [
	  		{
					x: x,
          y: y,
          type: 'scatter',
          mode: 'lines+points',
	  			name: 'Freq-Voltage'
		    },
	  	],
		  layout: {
				xaxis: {
          title: 'Frequency (Hz)',
				},	
				yaxis: {
          title: 'Voltage',
          range: [ymin, ymax]
				},	
				width: width,
				height: height 
			}
		}

		return json
	}

	render() {
		const x = this.props.x;
		const y = this.props.y;
		const ymin = this.props.ymin
		const ymax = this.props.ymax
		const json = this.jsonFactory(x, y, ymin, ymax);
		const config = {staticPlot: false, displayModeBar: true};

    return (
			  <Plot data={json.data} layout={json.layout} config={config} />
  	);	
	}
}	

class SpectrumPlot extends React.Component {
  constructor(props) {
    super(props);
		this.state = {
			plotType: 'lines',
		};

    // Handlers
		this.handleTypeChange = this.handleTypeChange.bind(this);
	}

  componentDidMount() {
  }

  handleTypeChange(e) {
	  this.setState({plotType: e.target.value});
  }		

	render() {
		const x = this.props.data.x;
		const y = this.props.data.y;
		const ymin = this.props.vmin;
		const ymax = this.props.vmax;

		var plot;
    plot = <Line x={x} y={y} ymin={ymin} ymax={ymax}/>

    return (
      <div>
		    {plot}
	    </div>
  	);	
	}
}	


class OnlinePlot extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
			timestamp: null,
			vmin: 0,
			vmax: -1,
		};

		// Handlers 
		this.handleDataArrive = this.handleDataArrive.bind(this);
		// Limits
		this.handleVminChange = this.handleVminChange.bind(this);
		this.handleVmaxChange = this.handleVmaxChange.bind(this);
		this.handleSetLimits = this.handleSetLimits.bind(this);
	}	

  componentDidMount() {
		this.props.dataCenter.setDataArriveHandler(this.handleDataArrive)
  }

  handleDataArrive(timestamp) {
    this.setState({timestamp: timestamp});
	}	

  handleVminChange(value) {
	  this.setState({vmin: value});
  }		

  handleVmaxChange(value) {
	  this.setState({vmax: value});
  }

  handleSetLimits(e) {
		const dc = this.props.dataCenter;
		const vmin = dc.min.toFixed(0); 
		const vmax = dc.max.toFixed(0); 
	  this.setState({vmin: vmin, vmax: vmax});
  }

  renderPlot() {
   
		const device = this.props.device;
		const dataType = this.props.dataType;
		const dc = this.props.dataCenter;
		var data = dc.fetchData(device, dataType);

		// Convert to the numerical values
    var [vmin, vmax] = parseLimits(this.state.vmin, this.state.vmax);

		var plot;
    plot = <SpectrumPlot data={data} vmin={vmin} vmax={vmax}/>
    
		return plot

	}	

	render() {
		const dc = this.props.dataCenter;
		// Limits
    var [vmin, vmax] = [this.state.vmin, this.state.vmax];

    return (
      <div>
			  <ValueLimits 
			    vmin={vmin} vmax={vmax} 
			    onVminChange={this.handleVminChange} onVmaxChange={this.handleVmaxChange} />
			  {this.renderPlot()}
	    </div>
  	);	
	}
}	

export {OnlinePlot}
