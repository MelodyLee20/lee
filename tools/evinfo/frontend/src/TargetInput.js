import React from 'react';

var touchPoint = null;
var touchPointPrevious = null;


class TouchCollection {

	constructor() {
		this.targetPoints = []; // Target points
		this.touchPoints = []; // Touch points
		this.rows = null;
		this.cols = null;
    this.numTargets = null;
		this.dataCenter = null;
		this.onPointsCollected = null;

	}	


  init(props) {
   
		this.rows = props.rows;
		this.cols = props.cols;
		this.numTargets = props.rows * props.cols;
    this.dataCenter = props.dataCenter;
    this.onPointsCollected = props.onPointsCollected;

		// Set the target points
		this.targetPoints = this.getTargetPoints(this.rows, this.cols);
    // touchPoints should be re-initialized for recalibration. 
    this.touchPoints = [];
	}	

  setHandleTargetSet(f) {
		this.handleTargetSet = f;
	}	

  updateMarkerColor(i, color) {
		// Update the marker color. 

	  let circle = document.getElementById('circle_'+i);
	  let line1 = document.getElementById('line1_'+i);
	  let line2 = document.getElementById('line2_'+i);

		circle.style.stroke = color;
		line1.style.stroke = color;
		line2.style.stroke = color;
	}	

  updateMarkerColors() {

		let numTouch = this.touchPoints.length;
    let color;

    for (let i= 0; i < this.numTargets; i++) {

		  if (i < numTouch) {
        color = 'green';
		  }	else {
				if (i === numTouch) {
		      color = 'blue';
				} else {
		      color = 'black';
				}	
			}
      // Update the color
      this.updateMarkerColor(i, color);
		}	

	}	

  isValidTouch(point, pointPrevious, minDistance) {
    // Judge if it is a valid touch point
	
    if (touchPointPrevious == null) {
      return true;
		}	

		let x0 = point[0];
		let y0 = point[1];
		let x1 = pointPrevious[0];
		let y1 = pointPrevious[1];
    let d = Math.sqrt((x1-x0)**2 + (y1-y0)**2);
		let out = d > minDistance ? true : false;

		return out
	}	

  handlePointArrive(point) {

    // Save the target points
		this.touchPoints.push(point);

    // Set the color of makers
    let numPoints = this.touchPoints.length;
	  this.updateMarkerColors(numPoints);

	  // Perform calibration when all points are collected.
    if (numPoints === this.numTargets) {
      console.log('Perform calibration.');
		
	   	this.onPointsCollected(this.touchPoints, this.targetPoints);
		}	

	}	

  onTouch() {
    // On touch the screen.

		let minDistance = 100; // Minimum distance in unit of px.
    let isValid = this.isValidTouch(touchPoint, touchPointPrevious, minDistance);
		if (isValid) {
      this.handlePointArrive(touchPoint)
			// Update the previous touch point
			touchPointPrevious = touchPoint;
		}	
  }		

  getTargetPoints(rows, cols) {
		// Return the points.

		let dx = window.screen.width/cols;
		let dy = window.screen.height/rows;
    let points = [];
		let x, y;

    for (let i = 0; i < cols; i++) {
	      x = Math.round((0.5+i)*dx);
      for (let j = 0; j < rows; j++) {
			  y = Math.round((0.5+j)*dy);
        points.push([x, y]);
			}	
		}	
    
		return points;
	}	

  makeMarker(key, x, y, color, radius=30) {

    let marker = [];
    marker.push(<circle key={'circle_'+key} id={'circle_'+key} cx={x} cy={y} r={radius} stroke={color} strokeWidth="3" fill="none"/>)
    marker.push(<line key={'line1_'+key} id={'line1_'+key} x1={x-radius} y1={y} x2={x+radius} y2={y} stroke={color} strokeWidth="3"/>)
    marker.push(<line key={'line2_'+key} id={'line2_'+key} x1={x} y1={y-radius} x2={x} y2={y+radius} stroke={color} strokeWidth="3"/>)
		return marker;	
	}	

  getMarkers() {
		// Return the markers

		// Prepare the markers
		let points = this.targetPoints;
		let markers = [];
		let marker;
		let x, y;
		let style;
		let color;

		for (let i=0; i < points.length; i++) {
			[x, y] = points[i];
			color = (i === 0)? 'blue': 'black'; 
	    marker = this.makeMarker(i, x, y, color);
			markers.push(marker);
		}	

		return markers
	}

  reverse() {
		// Reverse to the previous point.
    
		// Remove the last point
		this.touchPoints.pop();

		// Update the color of markers
    this.updateMarkerColors()
	}	

  cleanAll() {
		// Clean all points.
		let num = this.touchPoints.length;

		for (let i=0; i < num; i++) {
      this.reverse();
		}
	}	

}	

var tc = new TouchCollection();

function updateTouchPoint(ev) {
  ev.preventDefault();
	touchPoint = [ev.screenX, ev.screenY] // Screen coordinates
	tc.onTouch(); // Get new touch
} 

function initHotkeys(evt) {

  switch(evt.which) {
  // Press the 'p' key to come back to the previous point.
  case 80:
	  tc.reverse();		
    break;

  // Press the 'c' key to clear all points.
  case 67:
    tc.cleanAll();
    break;

  // Press the 'r' key for recalibration
  case 82:
    //pressureMode = !pressureMode;
    break;

  // Press the 'f' or 'Enter' key
  case 70:
    if (document.documentElement.webkitRequestFullscreen) {
      if (document.webkitFullscreenElement)
         document.webkitCancelFullScreen();
      else
        document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
    }
    break;
		
	default:
		// Do nothing
  }
}

class TargetInput extends React.Component {
  constructor(props) {
    super(props);
		this.state = {
		}	

		this.tc = tc
	}

  componentWillMount() {
		tc.init(this.props);

		// Add listeners
    document.addEventListener("pointerdown", updateTouchPoint, false);
    document.addEventListener("keyup", initHotkeys, false);
  }

  componentWillUnmount() {
    document.removeEventListener("pointerdown", updateTouchPoint, false);
    document.removeEventListener("keyup", initHotkeys, false);
	}	

	render() {
		const screenWidth = window.screen.width;
		const screenHeight = window.screen.height;
    return (
      <div>
			  <svg width={screenWidth} height={screenHeight}>
			    <text x="0" y="20" fill="black"> Resolution: ({screenWidth}x{screenHeight}) </text>
	        <text x="0" y="40" fill="black"> Hotkeys: </text>
	        <text x="0" y="60" fill="black"> f => Full screen mode </text>
	        <text x="0" y="80" fill="black"> p => Previous point </text>
	        text = <text x="0" y="100" fill="black"> c => Clear all points </text>

			    // Set the markers
			    {tc.getMarkers()}
			  </svg>
	    </div>
		);	
	}
}	

export {TargetInput}

