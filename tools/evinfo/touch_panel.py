from touch.input_device import PenPoint, get_pen_point, InputDevice
from touch.writers import csv_dict_writer
import threading
import json


class TouchPanel(object):

    def __init__(self):

        self.device = None
        self.ws_handler = None
        self.ioloop_instance = None
        self.num_points_max = 10000000
        self.points = []
        self.num_buffer = 50
        self.point_buffer = []
        self.thread = None

    def set_device(self, device_name):

        device_path = "/dev/input/{}".format(device_name)
        self.device = InputDevice(device_path)

    def set_ws_handler(self, handler):

        self.ws_handler = handler

    def set_ioloop_instance(self, instance):

        self.ioloop_instance = instance

    def drop_old_points(self):

        num = len(self.points)
        num_max = self.num_points_max

        iz = num - 1 if num > num_max else num_max - 1

        if num > num_max:
            iz = num
            ia = num-num_max
        else:
            iz = num_max
            ia = 0

        self.points = self.points[ia:iz]

    def save_point(self, point):

        self.points.append(point)

        # Drop the old points
        self.drop_old_points()

    def send_points(self, points):

        ws_data = {"type": "touchPoints", "data": points}
        self.ws_handler.send_updates(json.dumps(ws_data))


    def read_and_send(self):

        pen_point = PenPoint()
        for event in self.device.read_loop():
            point = get_pen_point(event, pen_point)

            if point:
                self.save_point(point)
                self.point_buffer.append(point)

                # Send the point buffer
                if len(self.point_buffer) % self.num_buffer == 0:
                    self.ioloop_instance.add_callback(self.send_points, self.point_buffer)

                    # Clear the buffer
                    self.buffer = []


    def read_and_send_points(self):

         def run():
             self.read_and_send()

         self.thread = threading.Thread(target=run)
         self.thread.daemon = True # It's important for finalization
         self.thread.start()

    def save_file(self, filename='output.csv'):
        # Save file.

        points = self.points

        # Write data to a file
        if len(points) > 0:
            print("Write out file: {}".format(filename))
            csv_dict_writer(filename, points)
        else:
            print("There is no data to write out!")


