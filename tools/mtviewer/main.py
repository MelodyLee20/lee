import os
import sys
import evdev
import argparse

def check_root():
    uid = os.getuid()
    print(uid)
    if uid != 0:
        print('Error: You need root privileges to use this tool!')

def show_devices():
    devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
    print("devices: ", devices)
    for device in devices:
        print(device.path, device.name, device.phys)


if __name__ == '__main__':
    #check_root()
    show_devices()

