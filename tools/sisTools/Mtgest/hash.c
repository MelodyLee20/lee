/*!
\file hash.c
\brief To provide general HashTable-related API
\author nf
 */
#include <stdio.h>

#include "hash.h"

/**
 *\brief To Construct a Hashtable with size slot 
 *\param size
 *\date 2008/04/03
 *\return a pointer to the Hashtable
 */

tableList *tables = NULL;

hash_table *hashConstructTable(size_t size)
{
    size_t i;
    bucket **temp;
    hash_table *table;

    table = malloc(sizeof(hash_table));
    if (table == NULL)
        return NULL;

    table->size = size;
    table->table = (bucket * *)malloc(sizeof(bucket *) * size);
    temp = table->table;

    if (temp == NULL) {
        free(table);
        return NULL;
    }

    for (i = 0; i < size; i++)
        temp[i] = NULL;

    return table;
}

void freeHashTables(hash_table * table)
{
    tableList *ptr, *lastPtr, *nextPtr;
    keyList *keyPtr, *nextKeyPtr;

    for (ptr = tables; NULL != ptr; lastPtr = ptr, ptr = ptr->next) {
        if (ptr->tableAddr == table) {    
            for (keyPtr = ptr->keyHead; NULL != keyPtr; keyPtr = nextKeyPtr) {
                nextKeyPtr = keyPtr->next;
                free(keyPtr);
            }
                        
            nextPtr = ptr->next;
            if (ptr != tables)
            {
                lastPtr->next = nextPtr;
            } 
            else
            {
                tables = nextPtr;
            }
            free(ptr);
            
            break;
        }
    }
}

unsigned hashHash(hash_table * table, char *string)
{    
    tableList *ptr, *lastPtr;
    keyList *keyPtr, *lastKeyPtr;
    unsigned i;

    if (NULL == tables) {
        tables = (tableList *) malloc(sizeof(tableList));
        if (NULL == tables)
            return 0;
        tables->tableAddr = table;
        tables->stringCount = 0;
        tables->next = NULL;

        tables->keyHead = (keyList *) malloc(sizeof(keyList));
        if (NULL == tables->keyHead)
            return 0;
        sprintf(tables->keyHead->key, string);
        tables->keyHead->next = NULL;
        return (tables->stringCount)++;
    }

    for (ptr = tables; NULL != ptr; lastPtr = ptr, ptr = ptr->next) {
        if (ptr->tableAddr == table) {
            for ( i = 0, keyPtr = ptr->keyHead; i < ptr->stringCount; i++, lastKeyPtr = keyPtr, keyPtr = keyPtr->next )
            {
                if (stricmp(string, keyPtr->key) == 0)
                {
                    return i;
                }
            }
            keyPtr = (keyList *) malloc(sizeof(keyList));
            if (NULL == keyPtr)
                return 0;
            lastKeyPtr->next = keyPtr;
            sprintf(keyPtr->key, string);
            keyPtr->next = NULL;
            return (ptr->stringCount)++;
        }
    }

    ptr = (tableList *) malloc(sizeof(tableList));
    if (NULL == ptr)
        return 0;

    lastPtr->next = ptr;
    ptr->tableAddr = table;
    ptr->stringCount = 0;
    ptr->next = NULL;

    ptr->keyHead = (keyList *) malloc(sizeof(keyList));
    if (NULL == ptr->keyHead)
        return 0;
    sprintf(ptr->keyHead->key, string);  
    ptr->keyHead->next = NULL;
    return (ptr->stringCount)++;
    
}

/*
 * \brief Hashes a string to produce an unsigned short, which should be
 * sufficient for most purposes.
 *
 *\param string
 *\return the hash value
*/
//unsigned hashHash(char *string)
//{
//    /*static unsigned totalString = 0;
//    static char symbolTable[512][32];
//    int i;
//
//    for ( i = 0; i < totalString; i++ )
//    {
//        if (stricmp(string, symbolTable[i]) == 0)
//        {
//            return i;
//        }        
//    }
//
//    sprintf(symbolTable[totalString], string);   
//    return totalString++;*/
//    
//    
//    unsigned ret_val = 0;
//    int i;
//
//    while (*string) {
//        i = tolower(*string);
//        ret_val ^= i;
//        ret_val <<= 1;
//        string++;
//    }
//    
//    return ret_val;
//    
//}


/**
 *\brief Insert 'key' into hash table.
 *\param key
 *\param data
 *\param table
 *\date 2008/04/03
 *\return  Returns pointer to old data associated with the key, if any, or
 * NULL if the key wasn't in the table previously.
*/
void *hashInsert(char *key, void *data, hash_table * table)
{
    unsigned val = hashHash(table, key) % table->size;
    bucket *ptr;
    bucket *lastPtr;
    /*
       ** NULL means this bucket hasn't been used yet.  We'll simply
       ** allocate space for our new bucket and put our data there, with
       ** the table pointing at it.
     */
    if (NULL == (table->table)[val]) {
        (table->table)[val] = (bucket *) malloc(sizeof(bucket));
        if (NULL == (table->table)[val])
            return NULL;
        (table->table)[val]->key = strdup(key);
        (table->table)[val]->next = NULL;
        (table->table)[val]->data = data;
        return (table->table)[val]->data;
    }

    /*
       ** This spot in the table is already in use.  See if the current string
       ** has already been inserted, and if so, increment its count.
     */    

    for (ptr = (table->table)[val]; NULL != ptr; lastPtr = ptr, ptr = ptr->next){
        if (0 == stricmp(key, ptr->key)) {
            void *old_data;
            old_data = ptr->data;
            ptr->data = data;
            return old_data;
        }
    }
    /*
       ** This key must not be in the table yet.  We'll add it to the head of
       ** the list at this spot in the hash table.  Speed would be
       ** slightly improved if the list was kept sorted instead.  In this case,
       ** this code would be moved into the loop above, and the insertion would
       ** take place as soon as it was determined that the present key in the
       ** list was larger than this one.
     */
    ptr = (bucket *) malloc(sizeof(bucket));
    if (NULL == ptr)
        return 0;

    ptr->key = strdup(key);
    ptr->data = data;
    ptr->next = NULL;

    lastPtr->next = ptr;
    /*
    ptr->next = (table->table)[val];
    (table->table)[val] = ptr;
    */
    return data;

}


/**
 *\brief  Look up a key and return the associated data.  
 *\param key
 *\param table
 *\date 2008/04/03
 *\return Returns NULL if the key is not in the table.
*/
void *hashLookup(char *key, hash_table * table)
{
    unsigned val = hashHash(table, key) % table->size;
    bucket *ptr;

    if (NULL == (table->table)[val])
        return NULL;
    for (ptr = (table->table)[val]; NULL != ptr; ptr = ptr->next) {
        if (0 == stricmp(key, ptr->key))
            return ptr->data;
    }
    return NULL;
}


/**
 *\brief  Delete a key from the hash table 
 *\param key
 *\param table
 *\date 2008/04/03
 *\return associated data, or NULL if not present.
*/
void *hashDel(char *key, hash_table * table)
{
    unsigned val = hashHash(table, key) % table->size;
    void *data;
    bucket *ptr, *last = NULL;

    if (NULL == (table->table)[val])
        return NULL;

    /*
       ** Traverse the list, keeping track of the previous node in the list.
       ** When we find the node to delete, we set the previous node's next
       ** pointer to point to the node after ourself instead.  We then delete
       ** the key from the present node, and return a pointer to the data it
       ** contains.
     */
    for (last = NULL, ptr = (table->table)[val]; NULL != ptr; last = ptr, ptr = ptr->next) {
        if (0 == strcmp(key, ptr->key)) {
            if (last != NULL) {
                data = ptr->data;
                last->next = ptr->next;
                free(ptr->key);
                free(ptr);
                return data;
            }
            /*
               ** If 'last' still equals NULL, it means that we need to
               ** delete the first node in the list. This simply consists
               ** of putting our own 'next' pointer in the array holding
               ** the head of the list.  We then dispose of the current
               ** node as above.
             */
            else {
                data = ptr->data;
                (table->table)[val] = ptr->next;
                free(ptr->key);
                free(ptr);
                
                return data;
            }
        }
    }

    /*
       ** If we get here, it means we didn't find the item in the table.
       ** Signal this by returning NULL.
     */

    return NULL;
}



/*
*/

/**
 *\brief Frees a complete table by iterating over it and freeing each node.
 * the second parameter is the address of a function it will call with a
 * pointer to the data associated with each node.  This function is
 * responsible for freeing the data, or doing whatever is needed with
 * it. Pass "NULL" if you don't need to free anything.
 *
 *\param table
 *\param func : a callback function pointer
 *\date 2008/04/03
 *\return void
 */
void hashFreeTable(hash_table * table, void (*func) (void *))
{
    /* Changed
     * enumerate( table, hashFreeNode);
     * here I expand the enumerate function into this function so I can
     * avoid the dodgy globals which prevent me from freeing nested hash
     * tables. 
     */
    unsigned i;
    bucket *temp;
    void *data;
    for (i = 0; i < table->size; i++) {
        if ((table->table)[i] != NULL) {
            while (temp = (table->table)[i]) {
                data = hashDel(temp->key, table);
                //assert(data);
                if (func)
                    func(data);
            }
        }
    }
    free(table->table);
    table->table = NULL;
    table->size = 0;
    free(table);
}



/**
 *\brief Simply invokes the function given as the second parameter for each
 * node in the table, passing it the key and the associated data.
 *
 *\param table
 *\param func : a callback function pointer
 *\date 2008/04/03
 *\return void
*/
void hashEnumerate(hash_table * table, void (*func) (char *, void *))
{
    unsigned i;
    bucket *temp;
    for (i = 0; i < table->size; i++) {
        if ((table->table)[i] != NULL) {
            for (temp = (table->table)[i]; NULL != temp; temp = temp->next) {
                func(temp->key, temp->data);
            }
        }
    }
}


/**
 *\brief compared to hashEnumerate,hashEnumerate2 just add a parameter fp which is useful to file operation
 *\param table
 *\param func
 *\param fp
 *\date 2008/04/03
 *\return void
 */
void hashEnumerate2(hash_table * table, void (*func) (char *, void *, FILE *), FILE * fp)
{
    unsigned i;
    bucket *temp;
    for (i = 0; i < table->size; i++) {
        if ((table->table)[i] != NULL) {
            for (temp = (table->table)[i]; NULL != temp; temp = temp->next) {
                func(temp->key, temp->data, fp);
            }
        }
    }
}
