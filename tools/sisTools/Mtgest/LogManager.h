#ifndef LOGMANAGER_H
#define LOGMANAGER_H

#include <fstream>
#include <string>

class LogManager
{
public:
    /*BEGIN & END*/

    static LogManager& instance()
    {
        static LogManager instance;
        return instance;
    }  

    ~LogManager();

    /*GET & SET*/
	
	void setLog();

    /*OPERATION*/

	void append(std::string message);

protected:
    /*ATTRIBUTE*/

	std::fstream m_filePtr;
	std::string m_oldMessage;
	int m_count;

private:
    /*BEGIN & END*/

    LogManager();
};

#define LOGMGR LogManager::instance()

#endif //LOGMANAGER_H