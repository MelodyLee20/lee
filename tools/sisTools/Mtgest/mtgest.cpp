// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved.

// MTGest application
// Description:
//  This application demonstrate how to handle gesture commands. This application
//  will initially draw rectangle in the middle of client window. By using gestures
//  end user can manipulate with rectangle. The availlable commands are:
//      - rectangle stratch 
//          By putting two fingers on the screen and modifying distance between them
//          by moving fingers in the opposite directions or toward each other user
//          can zoom out/in this rectangle.
//      - panning
//          By touching screen with two fingers and moving them into same direction
//          user can move rectangle.
//      - rotate
//          By putting one finger in the center of rotation and then rotation the other
//          finger around it user can rotate rectangle
//      - two finger tap
//          By tapping into screen with two fingers user can toggle drawing of diagonals
//      - finger rollover
//          This gesture consists of movements by two fingers. It's similar as two finger tap
//          but it's little bit different. First put one finger down. Then put down second finger.
//          Bring up second finger and finally bring up second finger. This gesture will 
//          change color of rectangle.
//
// Purpose:
//  This sample demonstrates handling of the multi-touch gestures. 
//
// MTGest.cpp : Defines the entry point for the application.


// Windows Header Files:
#include <windows.h>
#include <process.h>
//#include <winuser.h>    // included for Windows Touch
//#include <windowsx.h>   // included for point conversion

#include      <tchar.h>


// C RunTime Header Files
#include <assert.h>

#include <strsafe.h>
#include "resource.h"
#include "DrawingObject.h"
#include "MyGestureEngine.h"
#include <iostream>
using std::cerr;
using std::cin;
using std::cout;
using std::endl;
using std::ios;
#include <fstream> // file stream
using std::ofstream; // output file stream
using std::ifstream; // output file stream
#include "config.h"
#include "atlimage.h"
#include <string>
using std::string;
#include <sstream>
#include <math.h>
#include "LogManager.h"

#pragma warning(disable:4996) // disable _CRT_SECURE_NO_WARNINGS warning

#define ASSERT assert

#define MAX_LOADSTRING 100

#define TABLET_ROTATE_GESTURE_ENABLE 0x02000000
#define WM_TABLET_QUERYSYSTEMGESTURESTATUS 716
#define CONFIGFILE "MTGesture.ini"
#define TOUCHLOG "touch.log"
#define CHINESE "Chinese"
#define TOUCHDIR "TestLogs"
#define TOUCHCSVLOG "touch.csv"

#include "LogManager.h"//kid
#include <sstream> 


CDrawingObject g_cRect;                        // this is a class that manipulates with a rectangle
CMyGestureEngine g_cGestureEngine(&g_cRect);   // class that reads and process multi touch gesture commands

///////////////////////////////////////////////////////////////////////////////
// Application framework

//save picture
void CaptureScreen(HWND window, const char* filename);
//read ini
bool ReadIni();

//change font
RECT rc;
//void TextOutWithFont(HDC hdc, LPRECT lprect, LPSTR lpstr);   //claire0306 hide
long fnWndProc_OnPaint(HDC hDC,RECT rc);

//void PaintRoutine (HWND, HDC, int, int) ;
//HFONT EzCreateFont (HDC hdc, TCHAR * szFaceName, int iDeciPtHeight,
//					int iDeciPtWidth, int iAttributes, BOOL fLogRes) ;
//
//#define EZ_ATTR_BOLD          1
//#define EZ_ATTR_ITALIC        2
//#define EZ_ATTR_UNDERLINE     4
//#define EZ_ATTR_STRIKEOUT     8
#define LINE_WIDTH_WITH_DOT     1
#define LINE_WIDTH_WITHOUT_DOT  5

//Fullscreen
void ChangeToFullScreen(HWND hWnd);
RECT WndRect;
HMENU WndMenu;

//Function before TranslateMessage(), we may check key down in this function
void PreTranslateMessage( MSG* msg );

//Load Bitmap info
void LoadBitmapFile( char *fileName, BITMAPINFOHEADER *bitmapInfoHeader);

//Draw background
void DrawBackground( char *fileName );

// Global Variables:
HINSTANCE g_hInst;                      // current instance
WCHAR g_wszTitle[MAX_LOADSTRING];       // The title bar text
WCHAR g_wszWindowClass[MAX_LOADSTRING]; // the main window class name
bool crossMode = false;					// Cross on/off ( true/false ) in Line Mode
HWND m_hwnd;
bool gTouchLog = true;
bool gInitState = true;
bool fLogRes = true;

ofstream mOutFile;
ofstream mOutCsvFile;


//Touch
//#define MAXPOINTS 12
#define MAXPOINTS 40
int points[MAXPOINTS][2];
int gNumContacts;

configFile *m_cfgfile;
TCHAR t_cfgFileName[FILENAME_MAX];
char  cfgFileName[FILENAME_MAX];

TCHAR t_logFileName[FILENAME_MAX];
char  logFileName[FILENAME_MAX];

TCHAR t_logCsvFileName[FILENAME_MAX];
char  logCsvFileName[FILENAME_MAX];

int ReportRate;
int TestTime;
int Counter;
char *Language;
char temp[32];
int PassFinger;
bool DetectFingerID;
bool g_AutoMode;
bool FlagTimer;
bool ShowBackground;
int  DefaultTest;
bool LineWithDot;
int LineWidth;
bool FullScreenInit;
unsigned int touch_start_time;
unsigned int touch_last_time;

unsigned int Font_Size;
unsigned int Line_Width;//kid
bool Test_Enabled;//kid
static TCHAR szBuffer[] = TEXT ("十指测试") ;



bool ReadIni()
{
	if(strcmp(cfgFileName,"") == 0)
	{
		strcpy_s(cfgFileName, FILENAME_MAX, CONFIGFILE);
	}


	m_cfgfile = readConfigFile(cfgFileName);

	if(!m_cfgfile)
		return FALSE;

	ReportRate = getConfigInt(m_cfgfile, "Touch", "ReportRate");
	Language = getConfigString(m_cfgfile, "Support", "Language");
	sprintf(temp,"%s",Language);

	TestTime = getConfigInt(m_cfgfile, "Touch", "TestTime");
	PassFinger = getConfigInt(m_cfgfile, "Touch", "PassFinger");
    DetectFingerID = getConfigInt(m_cfgfile, "Touch", "DetectFingerID");
	FlagTimer = getConfigInt(m_cfgfile, "Touch", "EnableTimer");
	DefaultTest = getConfigInt(m_cfgfile, "TestMode", "Default");
	ShowBackground = getConfigInt(m_cfgfile, "Touch", "ShowBackground");
	LineWithDot = getConfigInt(m_cfgfile, "TestMode", "LineWithDot");
	FullScreenInit = getConfigInt(m_cfgfile, "TestMode", "FullScreenInit");

	Font_Size = getConfigInt(m_cfgfile, "Font", "Size");
	Line_Width = getConfigInt(m_cfgfile, "Line", "Size");//kid

	LOGMGR.setLog();//kid

	if ( LineWithDot )
	{
		LineWidth = Line_Width;
	} 
	else
	{
		LineWidth = Line_Width;
	}

	if(strcmp(temp,CHINESE) == 0)
	{
		switch(PassFinger)
		{
		case 1:
			wsprintf(szBuffer, TEXT("一指测试"));
			break;
		case 2:
			wsprintf(szBuffer, TEXT("二指测试"));
			break;
		case 3:
			wsprintf(szBuffer, TEXT("三指测试"));
			break;
		case 4:
			wsprintf(szBuffer, TEXT("四指测试"));
			break;
		case 5:
			wsprintf(szBuffer, TEXT("五指测试"));
			break;
		case 6:
			wsprintf(szBuffer, TEXT("六指测试"));
			break;
		case 7:
			wsprintf(szBuffer, TEXT("七指测试"));
			break;
		case 8:
			wsprintf(szBuffer, TEXT("八指测试"));
			break;
		case 9:
			wsprintf(szBuffer, TEXT("九指测试"));
			break;
		case 10:
			wsprintf(szBuffer, TEXT("十指测试"));
			break;
		default:
			break;
		}
	}
	else
	{
		switch(PassFinger)
		{
		case 1:
			wsprintf(szBuffer, TEXT("One Finger Test"));
			break;
		case 2:
			wsprintf(szBuffer, TEXT("Two Finger Test"));
			break;
		case 3:
			wsprintf(szBuffer, TEXT("Three Finger Test"));
			break;
		case 4:
			wsprintf(szBuffer, TEXT("Four Finger Test"));
			break;
		case 5:
			wsprintf(szBuffer, TEXT("Five Finger Test"));
			break;
		case 6:
			wsprintf(szBuffer, TEXT("Six Finger Test"));
			break;
		case 7:
			wsprintf(szBuffer, TEXT("Seven Finger Test"));
			break;
		case 8:
			wsprintf(szBuffer, TEXT("Eight Finger Test"));
			break;
		case 9:
			wsprintf(szBuffer, TEXT("Nine Finger Test"));
			break;
		case 10:
			wsprintf(szBuffer, TEXT("Ten Finger Test"));
			break;
		default:
			break;
		}
	}

	//saveConfigFile(m_cfgfile, cfgFileName);
	unloadConfigFile(m_cfgfile);
	
	return TRUE;
}



// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
//INT_PTR CALLBACK    DlgProc(HWND, UINT, WPARAM, LPARAM);
//VOID				AdapterListBox(HWND hWnd);

//unsigned icounter=0;  
//unsigned icounterMax = 100000;  
//unsigned  __stdcall secondThreadFunc(void* argu) {  
//    printf("In second thread...\n");  
//	
//    while(icounter < icounterMax)  
//        icounter++;  
//    _endthreadex(0);          
//
//	
//    return 0;  
//}

//void CreateThread(HWND hWnd)
//{
//    HANDLE hThread;   
//    printf("Create second thread...\n");  
//    //createThreadEx(&hThread, (void*) &secondThreadFunc, NULL, 0, 0);  
//    unsigned threadID;  
//    hThread = (HANDLE)_beginthreadex(NULL, 0, &secondThreadFunc, NULL, 0, &threadID);  
//    WaitForSingleObject(hThread, INFINITE);  
//    printf("Counter should be %d, it is %d now!\n", icounterMax, icounter);  
//    CloseHandle(hThread); 
//}

//INT_PTR CALLBACK DlgProc( HWND hDlg, UINT Message, WPARAM wParam,LPARAM lParam )
//{
//	HTOUCHINPUT hInput;
//
//	switch( Message )
//	{
//			case WM_INITDIALOG:
//			{
//				AdapterListBox( hDlg );
//				return ( INT_PTR ) TRUE;
//			}
//
//			case WM_TOUCH:
//				gNumContacts = LOWORD(wParam);
//				hInput = (HTOUCHINPUT)lParam;
//				break;
//
//			case WM_COMMAND:
//			{
//				// Control Changed
//				switch( LOWORD( wParam ) )
//				{
//					// OK Button => Read Selections
//					case IDOK:
//					{
//						EndDialog( hDlg, IDOK );
//						return TRUE;
//					} break;
//
//					case IDCANCEL:
//					{
//						// Cancel Button
//						EndDialog( hDlg, IDCANCEL );
//						return TRUE;
//					} break;
//
//				}
//
//			}break;
//
//		  case WM_CLOSE:
//
//			if(MessageBox(hDlg, TEXT("Close the program?"), TEXT("Close"),  MB_ICONQUESTION | MB_YESNO) == IDYES)
//			{
//			  DestroyWindow(hDlg);
//			}
//			return TRUE;
//
//		  case WM_DESTROY:
//			PostQuitMessage(0);
//			return TRUE;
//		  
//
//	}
//
//	
//
//}

//---------------------------------------------------------------

// Name: AdapterListBox()

// Desc: Populate the Adapter List Box

//---------------------------------------------------------------

//VOID AdapterListBox( HWND hWnd )
//
//{
	// Put some text in the list box

	//HWND hListBox = ::GetDlgItem(hWnd, IDC_MODES);

	//for (int i = 0 ; i < 8 ; i++)
	//	:endMessage( hListBox, LB_ADDSTRING, 0, (LPARAM) _T("Modes List Box"));
//}

// Win32 application main entry point function.
// in:
//      hInstance       handle of the application instance
//      hPrevInstance   not used, always NULL
//      lpCmdLine       command line for the application, null-terminated string
//      nCmdShow        how to show the window
int APIENTRY wWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPWSTR    lpCmdLine,
                     int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

#ifdef UNICODE
	DWORD dwNum = WideCharToMultiByte(CP_ACP,NULL,lpCmdLine,-1,NULL,0,NULL,FALSE);
	WideCharToMultiByte (CP_ACP,NULL,lpCmdLine,-1,cfgFileName,dwNum,NULL,FALSE);
#else
    strcpy(t_cfgFileName, cfgFileName);
#endif
     // TODO: Place code here.
    MSG msg;
    HACCEL hAccelTable;

    // Initialize global strings
    LoadString(hInstance, IDS_APP_TITLE, g_wszTitle, MAX_LOADSTRING);
    LoadString(hInstance, IDC_MTGEST, g_wszWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }
	//Load hotkey table
    hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MTGEST));

    // Main message loop:
    while (GetMessage(&msg, NULL, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
			PreTranslateMessage( &msg );
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}

//Convert int to std::string
string int2str( int &i ) 
{
	string s;
	std::stringstream ss( s );
	ss << i;
	return ss.str();
}

//Get std::wstring from std::string
std::wstring StringToWString( const std::string &s )
{
	std::wstring wsTmp(s.begin(), s.end());
	return wsTmp;
}

//Print resolution
void PrintResolution( string msg, COLORREF msgColor )
{
	LONG fontSize = 12;
	string cbuf;
	std::wstring wbuf;
	RECT rect;
	rect.left = 50;
	rect.top = 100;
	int width;
	int height;

	HDC wdc = GetWindowDC( m_hwnd );
	SetTextColor( wdc, 0xff6f83 );
	SetBkMode( wdc, TRANSPARENT );

	width  = GetSystemMetrics( SM_CXSCREEN );
	height = GetSystemMetrics( SM_CYSCREEN );
	cbuf = int2str( width );
	cbuf = "Screen Resolution: " + cbuf + "x" + int2str( height );
	wbuf = StringToWString( cbuf );
	DrawText( wdc, wbuf.c_str(), -1, &rect, DT_SINGLELINE | DT_NOCLIP ) ;

	width  = GetSystemMetrics( SM_CXFULLSCREEN );
	height = GetSystemMetrics( SM_CYFULLSCREEN );
	rect.top = rect.top + fontSize;
	cbuf = int2str( width );
	cbuf = "Resolution without Taskbar: " + cbuf + "x" + int2str( height );
	wbuf = StringToWString( cbuf );
	DrawText( wdc, wbuf.c_str(), -1, &rect, DT_SINGLELINE | DT_NOCLIP ) ;

	//Print extra message
	rect.top = rect.top + fontSize;
	SetTextColor( wdc, msgColor );
	wbuf = StringToWString( msg );
	DrawText( wdc, wbuf.c_str(), -1, &rect, DT_SINGLELINE | DT_NOCLIP ) ;

	DeleteDC( wdc );
}

//Process message before TranslateMessage()
HMENU savedMenu = NULL;
bool noneMode = false;
RECT savedRect;
LONG savedStyle;
LONG savedExStyle;
LONG bitmapWidth;
LONG bitmapHeight;
void PreTranslateMessage( MSG* msg )
{
	BITMAPINFOHEADER bitmapInfoHeader;
	char bitmapLocation[] = ".\\bg\\bgn.bmp";
	char *bitmapNumPtr = bitmapLocation;

	if( WM_KEYDOWN == msg->message )
	{		
		switch( msg->wParam )
		{
			case ( '0' ): case ( '1' ): case ( '2' ): case ( '3' ): case ( '4' ):
			case ( '5' ): case ( '6' ): case ( '7' ): case ( '8' ): case ( '9' ):
				for ( int cnt = 0; *(bitmapNumPtr + cnt) != '\0'; cnt ++ )
				{
					if ( *(bitmapNumPtr + cnt) == 'n')
					{
						bitmapLocation[cnt] = msg->wParam;
						break;
					}
				}
				DrawBackground( bitmapNumPtr );
				break;
			/*case ( 'C' ):
				{
					DialogBoxParam( g_hInst, MAKEINTRESOURCE( IDD_INPUTSIZE ), m_hwnd, ( DLGPROC )InputWidthHeightProc, 0 );
					if( ( g_panelHeight != NULL ) && ( g_panelWidth != NULL ) )
					{
						std::wstring textLine;
						RECT rect;
						HDC wdc = GetWindowDC( m_hwnd );
						HFONT hFont = CreateFont(48,0,0,0,FW_DONTCARE,FALSE,TRUE,FALSE,DEFAULT_CHARSET,OUT_OUTLINE_PRECIS,
												 CLIP_DEFAULT_PRECIS,CLEARTYPE_QUALITY, VARIABLE_PITCH,TEXT("Impact"));
						SelectObject( wdc, hFont );
						SetTextColor( wdc, 0xff00ff );
						SetBkMode( wdc, OPAQUE );
						rect.left = 50;
						rect.top = 100;

						textLine = StringToWString( string( "Width: ") ) + *g_panelWidth + StringToWString( string( ", Height: " ) ) + *g_panelHeight;
						DrawText( wdc, textLine.c_str() , -1, &rect, DT_SINGLELINE | DT_NOCLIP ) ;
						DeleteDC( wdc ); 
					}
				}
				break;*/
			case ( 'M' ):
				if( savedMenu != NULL )
				{
					SetMenu( m_hwnd, savedMenu );
				}
				break;
			case ( 'R' ):
				PrintResolution( string( "Resolution Print Out" ), 0xff0000 );
				break;
			case ( 'N' ):
				if( false == noneMode )
				{
					noneMode = true;
					GetWindowRect( m_hwnd, &savedRect );

					int width  = GetSystemMetrics( SM_CXSCREEN );
					int height = GetSystemMetrics( SM_CYSCREEN );
					SetWindowPos( m_hwnd, HWND_TOP, 0, 0, width, height, SWP_SHOWWINDOW );

					LONG lStyle = GetWindowLong( m_hwnd, GWL_STYLE );
					savedStyle = lStyle;
					lStyle &= ~( WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU );
					SetWindowLong( m_hwnd, GWL_STYLE, lStyle );
					LONG lExStyle = GetWindowLong( m_hwnd, GWL_EXSTYLE );
					savedExStyle = lExStyle;
					lExStyle &= ~( WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE );
					SetWindowLong( m_hwnd, GWL_EXSTYLE, lExStyle );
					if( NULL != GetMenu( m_hwnd ) )
					{
						savedMenu = GetMenu( m_hwnd );
					}
					SetMenu( m_hwnd, NULL ); //Set no menu
					SetWindowPos( m_hwnd, HWND_TOP, 0,0,0,0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER );
				}
				else
				{
					noneMode = false;
					SetWindowLong( m_hwnd, GWL_STYLE, savedStyle );
					SetWindowLong( m_hwnd, GWL_EXSTYLE, savedExStyle );
					SetWindowPos( m_hwnd, HWND_TOP, savedRect.left, savedRect.top, savedRect.right - savedRect.left, savedRect.bottom - savedRect.top, SWP_SHOWWINDOW );
					if( savedMenu != NULL )
					{
						SetMenu( m_hwnd, savedMenu );
					}
				}
				PrintResolution( string( "Thin Mode Switch" ), 0x00ff00 );
				break;
			case ( 'X' ):
				if( crossMode == false )
				{
					crossMode = true;
				}
				else // crossMode == true
				{
					crossMode = false;
				}
				break;
			case ( 'E' ):
				//PostQuitMessage(0);
				SendMessage(m_hwnd, WM_CLOSE, 0, 0);
				break;
			case ( VK_SPACE ):
				ChangeToFullScreen( m_hwnd );
				PrintResolution( string( "Full Screen Mode Switch" ), 0x0000ff );
				break;
			default:
				break;
		}
	}
}

//Load Bitmap info
void LoadBitmapFile( char *fileName, BITMAPINFOHEADER *bitmapInfoHeader)
{
	FILE *filePtr;
	BITMAPFILEHEADER bitmapFileHeader;

	//Open filename in read binary mode
	filePtr = fopen( fileName, "rb" );
	if ( filePtr != NULL )
	{
		//Read the bitmap file header and verify by checking id
		fread( &bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, filePtr );
		if ( bitmapFileHeader.bfType != 0x4D42 )
		{
			fclose( filePtr );
			return;
		}

		//Read the bitmap info header
		fread( bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, filePtr);

		bitmapWidth = bitmapInfoHeader->biWidth;
		bitmapHeight = bitmapInfoHeader->biHeight;

		fclose( filePtr );		
	}	
	else
	{
		bitmapWidth = 2560;
		bitmapHeight = 1440;
	}
}

//Draw background from .bmp file
void DrawBackground( char *fileName )
{
	BITMAPINFOHEADER bitmapInfoHeader;
	CA2W BmpPath( fileName );

	InvalidateRect(m_hwnd, NULL, TRUE);
	HBITMAP buffer = (HBITMAP)::LoadImage(
				NULL,
				BmpPath,
				IMAGE_BITMAP,
				0,
				0,
				LR_LOADFROMFILE|LR_CREATEDIBSECTION
				);
	if( buffer != NULL )
	{
		PAINTSTRUCT ps;
		HDC deviceContext = GetDC(m_hwnd);
		HDC bufferContext  = CreateCompatibleDC(deviceContext);
		ReleaseDC(m_hwnd,deviceContext);
		
		//Draw in Memory
		SelectObject(bufferContext, buffer);

		//Get the pixel of .bmp file
		LoadBitmapFile( fileName, &bitmapInfoHeader );

		//Begin Paint
		deviceContext = BeginPaint(m_hwnd, &ps);
		BitBlt(deviceContext, 0, 0, bitmapWidth, bitmapHeight, bufferContext, 0, 0, SRCCOPY);
		DeleteDC(deviceContext);
		DeleteDC(bufferContext);
		EndPaint(m_hwnd, &ps);
	}
}

// Registers the window class of the application.
// This function and its usage are only necessary if you want this code
// to be compatible with Win32 systems prior to the 'RegisterClassEx'
// function that was added to Windows 95. It is important to call this function
// so that the application will get 'well formed' small icons associated
// with it.
// in:
//      hInstance       handle to the instance of the application
// returns:
//      class atom that uniquely identifies the window class
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEX wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style            = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc      = WndProc;
    wcex.cbClsExtra       = 0;
    wcex.cbWndExtra       = 0;
    wcex.hInstance        = hInstance;
    wcex.hIcon            = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MTGEST));
    wcex.hCursor          = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground    = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName     = MAKEINTRESOURCE(IDC_MTGEST);
    wcex.lpszClassName    = g_wszWindowClass;
    wcex.hIconSm          = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassEx(&wcex);
}

// Saves instance handle and creates main window
// In this function, we save the instance handle in a global variable and
// create and display the main program window.
// in:
//      hInstance       handle to the instance of the application
//      nCmdShow        how to show the window
// returns:
//      flag, succeeded or failed to create the window

bool gTouchEnable = true;   //default  for vendor
bool gLineEnable = false;   //default

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

   g_hInst = hInstance; // Store instance handle in our global variable

   m_hwnd = hWnd = CreateWindow(g_wszWindowClass, g_wszTitle, WS_OVERLAPPEDWINDOW | WS_MAXIMIZE,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

    //if ( __argc >= 2 )
    //{
    //    g_AutoMode = true;    
    //}

   if(ReadIni() == FALSE)
	   return FALSE;

	if(gTouchLog == true)
	{
	//	GetCurrentDirectory(FILENAME_MAX, t_logFileName);

	//#ifdef UNICODE
	//	DWORD dwNum = WideCharToMultiByte(CP_ACP,NULL,t_logFileName,-1,NULL,0,NULL,FALSE);
	//	WideCharToMultiByte (CP_ACP,NULL,t_logFileName,-1,logFileName,dwNum,NULL,FALSE);
	//#else
	//	strcpy(t_logFileName, logFileName);
	//#endif

	//	strcat(logFileName, "\\");
	//	strcat(logFileName, TOUCHLOG);

		strcpy_s(logFileName, FILENAME_MAX, TOUCHLOG);

		mOutFile.open(logFileName,ios::out | ios::trunc );
		mOutFile << "(LimReportRate=" << ReportRate << ")"<<endl;

		strcpy_s(logCsvFileName, FILENAME_MAX, TOUCHCSVLOG);

		mOutCsvFile.open(logCsvFileName,ios::out | ios::trunc );
	}
   //mOutFile << "test";

#if 1
   	GESTURECONFIG config = { 0 };
    config.dwWant = GC_ROTATE;
    config.dwID = GID_ROTATE;
    config.dwBlock = 0;

	BOOL result = SetGestureConfig(
        hWnd,
        0,
        1,
        &config,
        sizeof(GESTURECONFIG)
        );

	config.dwID = GID_PAN;
    config.dwWant = GC_PAN;
//Exercise2-Task2-Step2
    config.dwBlock = GC_PAN_WITH_GUTTER;
	config.dwBlock |= GC_PAN_WITH_SINGLE_FINGER_VERTICALLY;
    config.dwBlock |= GC_PAN_WITH_SINGLE_FINGER_HORIZONTALLY;

	result = SetGestureConfig(
        hWnd,
        0,
        1,
        &config,
        sizeof(GESTURECONFIG)
        );

	
	// the following code initializes the points
    //for (int i=0; i< MAXPOINTS; i++){
    //    points[i][0] = -1;
    //    points[i][1] = -1;
    //}

	RegisterTouchWindow(hWnd, 0);
#endif   

    //ShowWindow(hWnd, SW_SHOWMAXIMIZED);
    //UpdateWindow(hWnd);

    HMENU hMenu = GetMenu(hWnd);
    EnableMenuItem(hMenu, IDM_LOG_STOP, MF_ENABLED);
    EnableMenuItem(hMenu, IDM_LOG_START,MF_DISABLED );

	switch(DefaultTest)
	{
	case 1: //touch
		CheckMenuItem(hMenu, IDM_TOUCH, MF_CHECKED );   //default for vendor
		CheckMenuItem(hMenu, IDM_GESTURE, MF_UNCHECKED);
		CheckMenuItem(hMenu, IDM_LINE, MF_UNCHECKED);   //default 	

		gTouchEnable = true;   //default  for vendor
		gLineEnable = false;   //default
		break;
	case 2: //line
		CheckMenuItem(hMenu, IDM_TOUCH,  MF_UNCHECKED);   //default 
		CheckMenuItem(hMenu, IDM_GESTURE, MF_UNCHECKED);
		CheckMenuItem(hMenu, IDM_LINE, MF_CHECKED);   //default for sis	

		gTouchEnable = false;   //default  
		gLineEnable = true;   //default  for sis	
		break;
	default:
		break;
	}

	ShowWindow(hWnd, SW_SHOWMAXIMIZED);
	UpdateWindow(hWnd);
	if ( FullScreenInit )
	{
		ChangeToFullScreen(hWnd);
	}
	
	RegisterHotKey(hWnd, 1, NULL, VK_ESCAPE);
	//RegisterHotKey(hWnd, 1, NULL, VK_SPACE);
	RegisterHotKey(hWnd, 1, MOD_CONTROL, 0x53/*S key*/);
	

   return TRUE;
}


FILE *fh = NULL;
char buf[256];

int id_color[MAXPOINTS];

const COLORREF pt_colors[MAXPOINTS] = 
{
	//color for 12 finger touch
	//RGB(255,0,0),	// red
	//RGB(0,255,0),	// lime
	//RGB(0,0,255),	// blue
	//RGB(255,255,0), // yellow
	//RGB(255,0,255), // magenta
	//RGB(0, 255, 255),	// aqua
	//RGB(128,0,0),	// maroon
	//RGB(0,128,0), // green
	//RGB(0, 0,128), // navy
	//RGB(128,128,0), // olive
	//RGB(128,0,128), // purple
	//RGB(0,128,128), //  teal 

	//original color
	//RGB(0, 0, 128),	// 0-navy
	//RGB(255, 0, 0),	// 1-red
	//RGB(0, 255, 0),	// 2-green
	//RGB(0, 0, 255),	// 3-blue
    //RGB(255,255,0), // 4-yellow
	//RGB(255,0,255), // 5-magenta
	//RGB(0, 255,255), // 6-cyan
	//RGB(190,190,190), // 7-grey
	//RGB(107,142,35), // 8-oliveDrab
	//RGB(255,165,0), // 9-Orange 
////RGB(160,32,240), // 10-purple
////	RGB(160,32,240), // 11
////	RGB(160,32,240), // 12
////	RGB(160,32,240), // 13
////	RGB(160,32,240), // 14

	//new color for 40 finger touch
	RGB(128,0,128), //0-purple
	RGB(255,0,0), //1-red
	RGB(255,165,0), //2-orange
	RGB(0,255,0), //3-lime
	RGB(127,255,212), //4-aquamarine
	RGB(0,255,255), //5-cyan
	RGB(176,196,222), //6-lightsteelblue
	RGB(255,218,185), //7-peachpuff

	RGB(238,130,238), //8-violet
	RGB(240,128,128), //9-lightcoral
	RGB(255,255,0), //10-yellow
	RGB(173,255,47), //11-greenyellow	
	RGB(0,128,0), //12-green
	RGB(135,206,235), //13-skyblue
	RGB(192,192,192), //14-silver
	RGB(255,235,205), //15-blanchedalmond
	
	RGB(75,0,130), //16-indigo
	RGB(255,20,147), //17-deeppink
	RGB(184,134,11), //18-darkgoldenrod
	RGB(189,183,107), //19-darkkhaki
	RGB(144,238,144), //20-lightgreen	
	RGB(95,158,160), //21-cadetblue
	RGB(65,105,225), //22-royalblue
	RGB(47,79,79), //23-darkslategray
			
	RGB(123,104,238), //24-mediumslateblue
	RGB(255,192,203), //25-pink
	RGB(128,0,0), //26-maroon
	RGB(238,232,170), //27-palegoldenrod
	RGB(0,255,127), //28-springgreen
	RGB(46,139,87), //29-seagreen
	RGB(0,0,128), //30-navy
	RGB(128,128,128), //31-gray	

	RGB(221,160,221),//32-plum
	RGB(255,99,71),//33-tomato
	RGB(222,184,135),//34-burlywood
	RGB(124,252,0),//35-lawngreen
	RGB(152,251,152),//36-palegreen
	RGB(70,130,180),//37-steelblue
	RGB(105,105,105),//38-dimgray
	RGB(255,228,225),//39-mistyrose	
	
};


TCHAR gTouchStatue[64];//for logdump.txt
TCHAR gTouchStr[MAXPOINTS][256];  //for logdump.txt

TCHAR gMouseState[128];//for display 
TCHAR gStatusStr[128];
TCHAR gShowTouchStr[MAXPOINTS][256]; //for display 


TCHAR gTestStr[64];
int gtouchNum = -1;
int gstrLength;
RECT gStatusRect, gDrawRect;

TOUCHINPUT gTouchInputs[MAXPOINTS], gPreTouchInputs[MAXPOINTS];



bool gLineClear = false;
bool gFullScreen = false;


#define ID_TIMER 1
bool gTimer = false;
int Rate[MAXPOINTS], preRate[MAXPOINTS];
bool TouchResult[MAXPOINTS];
int PassCounter;

// Processes messages for the main window:
//      WM_COMMAND        - process the application menu
//      WM_SIZE           - process resizing of client window
//      WM_PAINT          - paint the main window
//      WM_DESTROY        - post a quit message and return
//      WM_GESTURE		  - gesture command messages
//      WM_TABLET_QUERYSYSTEMGESTURESTATUS - processed to implement GCI_COMMAND_ROTATE gesture
// in:
//      hWnd        window handle
//      message     message code
//      wParam      message parameter (message-specific)
//      lParam      message parameter (message-specific)
// returns:
//      the result of the message processing and depends on the message sent
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    int wmId, wmEvent;
    PAINTSTRUCT ps;
    HDC hdc;
    static int cxChar, cyChar;
	TEXTMETRIC tm;
	RECT rect;
	static int wndWidth, wndHeigth;

	PTOUCHINPUT pTouchInput;
    HTOUCHINPUT hInput;
    //int iNumContacts;
    POINT pt;
	HMENU hMenu;

	int strLength;

	HBRUSH hBrush, o_hBrush;
	HPEN hPen, o_hPen;

	int fsModifiers, vk;

	int ids; //sample rate

    switch (message)
    {
	case WM_MOUSEMOVE:
		if(!gLineEnable)
		{
			gstrLength = wsprintf(gMouseState, TEXT("WM_MOUSEMOVE ( %d, %d)"), LOWORD( lParam), HIWORD( lParam));
			//InvalidateRect(hWnd, &gStatusRect, TRUE);
			InvalidateRect(hWnd, NULL, TRUE);
		}
		return 0;

	case WM_LBUTTONDOWN:
		if(!gLineEnable)
		{
			gstrLength = wsprintf(gMouseState, TEXT("WM_LBUTTONDOWN ( %d, %d)"), LOWORD( lParam), HIWORD( lParam));
			InvalidateRect(hWnd, &gStatusRect, TRUE);
		}
		return 0;
    case WM_LBUTTONUP:
		if(!gLineEnable)
		{
			//gtouchNum = 0;
			gstrLength = wsprintf(gMouseState, TEXT("WM_LBUTTONUP   ( %d, %d)"), LOWORD( lParam), HIWORD( lParam));
			InvalidateRect(hWnd, &gStatusRect, TRUE);
		}
		return 0;
    case WM_TOUCH:
        gNumContacts = LOWORD(wParam);
        hInput = (HTOUCHINPUT)lParam;
        //pInputs = new TOUCHINPUT[iNumContacts];

		// Get each touch input info and feed each tagTOUCHINPUT into the process input handler

        //if(pInputs != NULL)
		{
            if(GetTouchInputInfo(hInput, gNumContacts, gTouchInputs, sizeof(TOUCHINPUT)))
            {
				gInitState = false;
				gtouchNum = gNumContacts;

				if(gTouchLog == true)
					mOutFile << "Touch count = "<< gNumContacts <<endl;

				wsprintf(gTouchStatue, TEXT("WM_TOUCH Count = %d"),gNumContacts);
				if(fh)
				{
					WideCharToMultiByte(CP_ACP, 0, gTouchStatue, wcslen(gTouchStatue)+1, buf, 256, NULL, NULL);
					strcat(buf, "\n");
					fprintf(fh, buf);
				}

				hdc = GetDC(hWnd);
				//hdc = BeginPaint(hWnd, &ps); 

				for(int i = 0; i < gNumContacts; i++)
                {
                    // Bring touch input info into client coordinates
					pTouchInput = &gTouchInputs[i];

                    pt.x = pTouchInput->x/100;	
	                pt.y = pTouchInput->y/100;
                    ScreenToClient(hWnd, &pt);
                   
                    pTouchInput->x = pt.x;
                    pTouchInput->y = pt.y;

                    //new finger touch, ids increase 1
					//old finger touch, ids equal idx j
					// 1st finger, ids = 0, id_coror[0] = 440
					// 2nd finger, ids = 1, id_coror[1] = 441	
					ids = -1;
					for(int j=0; j<MAXPOINTS; j++)
					{
						if(ids == -1 && id_color[j] == -1)
							ids = j;

						if(id_color[j] == pTouchInput->dwID)
						{
							ids = j;
							break;
						}
					}

                    if ( gTouchEnable && DetectFingerID && ids == PassFinger-1 )
                    {
                        CaptureScreen(hWnd, "Touch.bmp");
                        mOutFile << PassFinger << " Fingers Test Pass"<<endl;
                        PostQuitMessage(0);
                    }

                    //g_ctDriver->ProcessInputEvent(pInputs[i]);					
					if( pTouchInput->dwFlags & TOUCHEVENTF_DOWN)
					{
						id_color[ids] = pTouchInput->dwID;

						if(gLineEnable)
						{
							gPreTouchInputs[ids].x = pTouchInput->x;
							gPreTouchInputs[ids].y = pTouchInput->y;
							gPreTouchInputs[ids].dwID = pTouchInput->dwID;

							hPen = CreatePen(PS_SOLID, LineWidth, pt_colors[ids]);
							o_hPen = (HPEN)SelectObject(hdc, hPen);

							if ( LineWithDot )
							{
								hBrush = CreateSolidBrush(pt_colors[ids]);					
								o_hBrush = (HBRUSH)SelectObject(hdc, hBrush);
								Ellipse(hdc, gTouchInputs[i].x - 2, gTouchInputs[i].y - 2, gTouchInputs[i].x + 2, gTouchInputs[i].y + 2);
								SelectObject(hdc, o_hBrush);
								DeleteObject(hBrush);
							}
							
							SelectObject(hdc, o_hPen);
							DeleteObject(hPen);
						}

						//sample rate
						if(gTimer == false)
						{
							SetTimer(hWnd, ID_TIMER, 1000, NULL);
							gTimer = true;
						}
						if ( FlagTimer )
						{
							Rate[ids] = 1;
						}
						else   //if ( !FlagTimer )
						{
							for(int i=0; i<MAXPOINTS; i++)
							{
								Rate[i] = 1;
							}
							touch_start_time = touch_last_time = ::GetTickCount();		
						}					

						if(gTouchLog == true)
							mOutFile << "DOWN id ="<<  pTouchInput->dwID <<", x = "<<pTouchInput->x<<", y ="<<  pTouchInput->y<<", Rate = "<<Rate[ids]<<endl;
						string s;
						std::stringstream ss(s);
						ss << pTouchInput->dwID;	
						string s2;
						std::stringstream ss2(s2);
						ss2 << pTouchInput->x;
						string s3;
						std::stringstream ss3(s3);
						ss3 << pTouchInput->y;

						LOGMGR.append(std::string("id =") + ss.str() + std::string(",x =") + ss2.str() + std::string(",y =") + ss3.str());//kid2
						if(preRate[ids] < ReportRate)
						{
							wsprintf(gTouchStr[i], TEXT("WM_TOUCH%d_FAIL (DOWN id=%d %d, %d)(Rate=%d) Less Than (LimReportRate=%d)"), i,  pTouchInput->dwID,  pTouchInput->x,  pTouchInput->y, preRate[ids],ReportRate);
							mOutFile << "WM_TOUCH" << i << "_FAIL(DOWN id=" <<  pTouchInput->dwID << " " << pTouchInput->x <<"," << pTouchInput->y << ")(Rate=" << preRate[ids] <<") Less Than (LimReportRate=" << ReportRate << ")"<<endl;
							TouchResult[i] = false;
						}
						else
						{
							wsprintf(gTouchStr[i], TEXT("WM_TOUCH%d_PASS (DOWN id=%d %d, %d)(Rate=%d)"), i,  pTouchInput->dwID,  pTouchInput->x,  pTouchInput->y, preRate[ids]);
							mOutFile << "WM_TOUCH" << i << "_PASS(DOWN id=" <<  pTouchInput->dwID << " " << pTouchInput->x <<"," << pTouchInput->y << ")(Rate=" << preRate[ids] <<")"<<endl;
							TouchResult[i] = true;
						}

						wsprintf(gShowTouchStr[i], TEXT("TOUCH%d (Report Rate=%d)"), i, preRate[ids]);

						//mOutCsvFile<<  pTouchInput->x <<"," << pTouchInput->y <<endl;
					}
					else if( pTouchInput->dwFlags & TOUCHEVENTF_MOVE)
					{						
						
						if(gLineEnable)
						{		
							hPen = CreatePen(PS_SOLID, LineWidth, pt_colors[ids]);
							o_hPen = (HPEN)SelectObject(hdc, hPen);
							MoveToEx(hdc, gPreTouchInputs[ids].x, gPreTouchInputs[ids].y, NULL);
							LineTo(hdc, pTouchInput->x, pTouchInput->y);
							
							//Tik
							if( crossMode == true )
							{
								MoveToEx(hdc, gPreTouchInputs[ids].x-1000, gPreTouchInputs[ids].y, NULL);
								LineTo(hdc, gPreTouchInputs[ids].x +1000, gPreTouchInputs[ids].y);							
								MoveToEx(hdc, gPreTouchInputs[ids].x, gPreTouchInputs[ids].y-1000, NULL);
								LineTo(hdc, gPreTouchInputs[ids].x, gPreTouchInputs[ids].y+1000);	
							}

							if ( LineWithDot )
							{
								hBrush = CreateSolidBrush(pt_colors[ids]);					
								o_hBrush = (HBRUSH)SelectObject(hdc, hBrush);									
								Ellipse(hdc, gTouchInputs[i].x - 2, gTouchInputs[i].y - 2, gTouchInputs[i].x + 2, gTouchInputs[i].y + 2);
								SelectObject(hdc, o_hBrush);
								DeleteObject(hBrush);
							}
														
							SelectObject(hdc, o_hPen);
							DeleteObject(hPen);

							gPreTouchInputs[ids].x = pTouchInput->x;
							gPreTouchInputs[ids].y = pTouchInput->y;	
						}

						//sample rate
						Rate[ids]++;
						if ( !FlagTimer )
						{
							touch_last_time = ::GetTickCount();
							if((touch_last_time - touch_start_time) * 0.001 >= 1)
							{
								mOutFile << "touch_start_time: " <<  touch_start_time <<"touch_last_time: "<< touch_last_time <<endl;
								touch_start_time = touch_last_time;
								for(int i=0; i<MAXPOINTS; i++)
								{
									preRate[i] = Rate[i];
									Rate[i] = 0;
									//mOutFile << " i ="<< i <<", preRate[i] = "<<preRate[i]<<endl;
								}

								if(!gLineEnable)
								{
									InvalidateRect(hWnd, NULL, TRUE);
									for(int i=0; i<MAXPOINTS; i++)
									{
										if(TouchResult[i] == true)
											PassCounter++;
									}

									if(PassCounter >= PassFinger)
									{
										CaptureScreen(hWnd, "Touch.bmp");
										mOutFile << PassFinger << " Fingers Test Pass"<<endl;
										PostQuitMessage(0);
									}
									else
										PassCounter = 0;

									Counter+= 1000;	

									if(TestTime && Counter >= TestTime)//kid
									{
										CaptureScreen(hWnd, "Touch.bmp");

										if(PassCounter >= PassFinger)
											mOutFile << PassFinger << " Fingers Test Pass"<<endl;
										else
											mOutFile << PassFinger << " Fingers Test Fail"<<endl;

										PostQuitMessage(0);
									}
								}
							}
						}
						
						if(gTouchLog == true)
							mOutFile << "MOVE id ="<<  pTouchInput->dwID <<", x = "<<pTouchInput->x<<", y ="<<  pTouchInput->y<<", Rate = "<<Rate[ids]<<endl;
						string s;
						std::stringstream ss(s);
						ss << pTouchInput->dwID;	
						string s2;
						std::stringstream ss2(s2);
						ss2 << pTouchInput->x;
						string s3;
						std::stringstream ss3(s3);
						ss3 << pTouchInput->y;

						LOGMGR.append(std::string("id =") + ss.str() + std::string(",x =") + ss2.str() + std::string(",y =") + ss3.str());//kid2
						if(preRate[ids] < ReportRate)
						{
							wsprintf(gTouchStr[i], TEXT("WM_TOUCH%d_FAIL (MOVE id=%d %d, %d)(Rate=%d) Less Than (LimReportRate=%d)"), i,  pTouchInput->dwID,  pTouchInput->x,  pTouchInput->y, preRate[ids],ReportRate);
							mOutFile << "WM_TOUCH" << i << "_FAIL(MOVE id=" <<  pTouchInput->dwID << " " << pTouchInput->x <<"," << pTouchInput->y << ")(Rate=" << preRate[ids] <<") Less Than (LimReportRate=" << ReportRate << ")"<<endl;
							TouchResult[i] = false;
						}
						else
						{
							wsprintf(gTouchStr[i], TEXT("WM_TOUCH%d_PASS (MOVE id=%d %d, %d)(Rate=%d)"), i,  pTouchInput->dwID,  pTouchInput->x,  pTouchInput->y, preRate[ids]);
							mOutFile << "WM_TOUCH" << i << "_PASS(MOVE id=" <<  pTouchInput->dwID << " " << pTouchInput->x <<"," << pTouchInput->y << ")(Rate=" << preRate[ids] <<")"<<endl;
							TouchResult[i] = true;
						}

						wsprintf(gShowTouchStr[i], TEXT("TOUCH%d (Report Rate=%d)"), i, preRate[ids]);
					}
					else if( pTouchInput->dwFlags & TOUCHEVENTF_UP)
					{
						if(gNumContacts == 1)
						{
							gTimer = false;
							KillTimer(hWnd, ID_TIMER);
						}

						id_color[ids] = -1;
								
						if(gLineEnable)
						{
							hPen = CreatePen(PS_SOLID, LineWidth, pt_colors[ids]);
							o_hPen = (HPEN)SelectObject(hdc, hPen);
							MoveToEx(hdc, gPreTouchInputs[ids].x, gPreTouchInputs[ids].y, NULL);
							LineTo(hdc, pTouchInput->x, pTouchInput->y);
						
							if ( LineWithDot )
							{
								hBrush = CreateSolidBrush(pt_colors[ids]);					
								o_hBrush = (HBRUSH)SelectObject(hdc, hBrush);
								Ellipse(hdc, gTouchInputs[i].x - 2, gTouchInputs[i].y - 2, gTouchInputs[i].x + 2, gTouchInputs[i].y + 2);
								SelectObject(hdc, o_hBrush);
								DeleteObject(hBrush);
							}
							
							SelectObject(hdc, o_hPen);
							DeleteObject(hPen);

							gPreTouchInputs[ids].x = 0;
							gPreTouchInputs[ids].y = 0;
							gPreTouchInputs[ids].dwID = -1;
						}
						if(gTouchLog == true)
							mOutFile << "UP   id ="<<  pTouchInput->dwID <<", x = "<<pTouchInput->x<<", y ="<<  pTouchInput->y<<endl;
						string s;
						std::stringstream ss(s);
						ss << pTouchInput->dwID;	
						string s2;
						std::stringstream ss2(s2);
						ss2 << pTouchInput->x;
						string s3;
						std::stringstream ss3(s3);
						ss3 << pTouchInput->y;

						LOGMGR.append(std::string("id =") + ss.str() + std::string(",x =") + ss2.str() + std::string(",y =") + ss3.str());//kid2
						if(preRate[ids] < ReportRate)
						{
							wsprintf(gTouchStr[i], TEXT("WM_TOUCH%d_FAIL (UP id=%d %d, %d)(Rate=%d) Less Than (LimReportRate=%d)"), i,  pTouchInput->dwID,  pTouchInput->x,  pTouchInput->y, preRate[ids],ReportRate);
							mOutFile << "WM_TOUCH" << i << "_FAIL(UP id=" <<  pTouchInput->dwID << " " << pTouchInput->x <<"," << pTouchInput->y << ")(Rate=" << preRate[ids] <<") Less Than (LimReportRate=" << ReportRate << ")"<<endl;
							TouchResult[i] = false;
						}
						else
						{
							wsprintf(gTouchStr[i], TEXT("WM_TOUCH%d_PASS (UP id=%d %d, %d)(Rate=%d)"), i,  pTouchInput->dwID,  pTouchInput->x,  pTouchInput->y, preRate[ids]);
							mOutFile << "WM_TOUCH" << i << "_PASS(UP id=" <<  pTouchInput->dwID << " " << pTouchInput->x <<"," << pTouchInput->y << ")(Rate=" << preRate[ids] <<")"<<endl;
							TouchResult[i] = true;
						}

						wsprintf(gShowTouchStr[i], TEXT("TOUCH%d (Report Rate=%d)"), i, preRate[ids]);

						if(!gLineEnable)
						{
							InvalidateRect(hWnd, NULL, TRUE);
						}

					}
					if(fh)
					{
						WideCharToMultiByte(CP_ACP, 0, gTouchStr[i], wcslen(gTouchStr[i])+1, buf, 256, NULL, NULL);
						char *ptr;
						ptr = strstr(buf, "(Rate");
						*ptr = '\0';
						strcat(buf, "\n");
						fprintf(fh, buf);
					}
                }

				ReleaseDC(hWnd, hdc);
				//EndPaint(hWnd, &ps);//kid
				//DeleteObject(hdc);
            }
        }
        
	    //delete [] pInputs;		
		//InvalidateRect(hWnd, &gStatusRect, TRUE);
		//if(!gLineEnable)
		//	InvalidateRect(hWnd, NULL, TRUE);
		//else
			InvalidateRect(hWnd, NULL, FALSE);
		return 0;
	case WM_CREATE:

//		CreateThread(hWnd);

		hdc = GetDC(hWnd);
        SelectObject(hdc, GetStockObject(SYSTEM_FIXED_FONT));
		GetTextMetrics(hdc, &tm);
		//::GetWindowRect(hWnd, &rect);
		::GetClientRect(hWnd, &rect);
		gStatusRect.top = 0;
		gStatusRect.left = 0;
		gStatusRect.bottom = rect.bottom / 2;
		gStatusRect.right = rect.right / 2;

		gDrawRect.top = 0;
		gDrawRect.left = 0;
		gDrawRect.bottom = rect.bottom;
		gDrawRect.top = rect.top;

		wndWidth  = rect.right - rect.left;
		wndHeigth =  rect.bottom - rect.top;

		gNumContacts = 0;
		//cxChar = tm.tmAveCharWidth;
		//cyChar = tm.tmHeight + tm.tmExternalLeading;
		Counter = 0;
		PassCounter = 0;
		//PassFinger = 10;

		for(int i=0; i<MAXPOINTS; i++)
		{
			id_color[i] = -1;
			gPreTouchInputs[i].x = 0;
			gPreTouchInputs[i].y = 0;
			gPreTouchInputs[i].dwID = -1;
			TouchResult[i]=false;
		}

		ReleaseDC(hWnd, hdc);	

		//sample rate
		//if(gTimer == false)
		//{
		//	SetTimer(hWnd, ID_TIMER, 1000, NULL);
		//	gTimer = true;
		//}

		return 0;
    case WM_GESTURE:
        // call gesture engine WndProc function to process gestures
//Exercise2-Task1-Step2 
#if 1
		gtouchNum = -1;//Gesture Function
		//memset(gStatusStr, 0, 128);
		//wsprintf(gStatusStr, TEXT("WM_GESTURE\n"));
		return g_cGestureEngine.WndProc(hWnd,wParam,lParam);
#else
		return 0;
#endif
		break;
    case WM_SIZE:
        // resize rectangle and place it in the middle of the new client window
        g_cRect.ResetObject(LOWORD(lParam),HIWORD(lParam));
		return 0;

        break;

    case WM_COMMAND:
        wmId    = LOWORD(wParam);
        wmEvent = HIWORD(wParam);
		hMenu = GetMenu(hWnd);
        // Parse the menu selections:
        switch (wmId)
        {
        case IDM_ABOUT:
            DialogBox(g_hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
            break;
        case IDM_EXIT:
            DestroyWindow(hWnd);
            break;
		case IDM_TOUCH:
			gTouchEnable = true;
			gLineEnable = false;
            RegisterTouchWindow(hWnd, 0);
			CheckMenuItem(hMenu, IDM_TOUCH, MF_CHECKED);
			CheckMenuItem(hMenu, IDM_GESTURE, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_LINE, MF_UNCHECKED);
            break;
		case IDM_GESTURE:
			gTouchEnable = false;
			gLineEnable = false;
            UnregisterTouchWindow(hWnd);
			CheckMenuItem(hMenu, IDM_TOUCH, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_GESTURE, MF_CHECKED);
			CheckMenuItem(hMenu, IDM_LINE, MF_UNCHECKED);
            break;
		case IDM_LINE:
			gTouchEnable = false;
			gLineEnable = true;
			gLineClear = true;
            RegisterTouchWindow(hWnd, 0);
			CheckMenuItem(hMenu, IDM_TOUCH, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_GESTURE, MF_UNCHECKED);
			CheckMenuItem(hMenu, IDM_LINE, MF_CHECKED);
			InvalidateRect(hWnd, NULL, TRUE);
            break;
		case IDM_LOG_START:
			gTouchLog = true;
			fh = fopen("logdump.txt", "w+");
			if(fh)
			{
				EnableMenuItem(hMenu, IDM_LOG_START, MF_DISABLED);
				EnableMenuItem(hMenu, IDM_LOG_STOP, MF_ENABLED);
			}
			break;
		case IDM_LOG_STOP:
			gTouchLog = false;
			if(fh)
			{
				fclose(fh);
				fh = NULL;
			}
			EnableMenuItem(hMenu, IDM_LOG_STOP, MF_DISABLED);
			EnableMenuItem(hMenu, IDM_LOG_START, MF_ENABLED);
			break;
		case IDM_FULLSCREEN:
			//SetWindowPos(hWnd, HWND_TOP, 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN), SWP_FRAMECHANGED);			
			ChangeToFullScreen(hWnd);
			break;
        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
        break;

	case WM_HOTKEY:
		fsModifiers = LOWORD(lParam);
		vk = HIWORD(lParam);
		if(fsModifiers == 0 && vk == VK_ESCAPE)
		{
			LOGMGR.setLog();
			EndMenu();
			gLineClear = true;
			InvalidateRect(hWnd, NULL, TRUE);
			//SendMessage(hWnd, WM_CLOSE, 0, 0);   //hide for sis internal
			//return TRUE;
		}
		//else if(fsModifiers == 0 && vk == VK_SPACE)
		//{
		//	ChangeToFullScreen(hWnd);
		//	//DialogBox(g_hInst, MAKEINTRESOURCE(IDD_DIALOG_RPT), hWnd, DlgProc);
		//}
		else if(fsModifiers == MOD_CONTROL && vk == 0x53/*S key*/)
		{
			CaptureScreen(hWnd, "Touch.bmp");
		}

		
		break;
    case WM_PAINT:
		
		if(gLineEnable)
			break;

		hdc = BeginPaint(hWnd, &ps); 

		GetClientRect(hWnd,&rc);

		if(gTouchEnable)
		{
			for(int i = 0; i < gNumContacts; i++)
			{
				if( gTouchInputs[i].dwFlags & TOUCHEVENTF_MOVE)
				{
					int j;
					for(j=0; j<MAXPOINTS; j++)
					{
						if(id_color[j] == gTouchInputs[i].dwID)
						{
							//hPen = CreatePen(PS_SOLID, 1, pt_colors[j]);
							//o_hPen = (HPEN)SelectObject(hdc, hPen);
							hBrush = CreateSolidBrush(pt_colors[j]);
							break;
						}
					}
					if(j == MAXPOINTS)
					{
						//hPen = CreatePen(PS_SOLID, 1, pt_colors[0]);
						//o_hPen = (HPEN)SelectObject(hdc, hPen);
						hBrush = CreateSolidBrush(pt_colors[0]);
					}
					
					o_hBrush = (HBRUSH)SelectObject(hdc, hBrush);
				    Ellipse(hdc, gTouchInputs[i].x - 25, gTouchInputs[i].y - 25, gTouchInputs[i].x + 25, gTouchInputs[i].y + 25);
					SelectObject(hdc, o_hBrush);
					DeleteObject(hBrush);

					//SelectObject(hdc, o_hPen);
					//DeleteObject(hPen);
				}
			}
		}
		else
		{
            // Full redraw: background + rectangle
            g_cRect.Paint(hdc);
		}
        
		//
		SelectObject(hdc, GetStockObject(SYSTEM_FIXED_FONT));
		SetMapMode(hdc, MM_ANISOTROPIC);
		SetWindowExtEx(hdc, 1, 1, NULL);
		SetViewportExtEx(hdc, cxChar, cyChar,NULL);
		SetTextColor(hdc,RGB(0,0,0));


		if(gtouchNum == -1)
		{
			if(gTouchEnable == true)
			{
				StringCchLength(gMouseState, 128, (size_t*)&gstrLength);
				TextOut(hdc, 0,  0, gMouseState, /*128*/gstrLength);

				//StringCchLength(gStatusStr, 128, (size_t*)&gstrLength);
				//TextOut(hdc, 0, 20, gStatusStr, /*128*/gstrLength);
			}
			else
			{
				StringCchLength(gMouseState, 128, (size_t*)&gstrLength);
				TextOut(hdc, 0,  0, gMouseState, /*128*/gstrLength);

				//StringCchLength(gStatusStr, 128, (size_t*)&gstrLength);
				//TextOut(hdc, 0, 20, gStatusStr, /*128*/gstrLength);			
			}

		}
		else if(gtouchNum > 0)
		{			
			StringCchLength(gTouchStatue, 64, (size_t*)&gstrLength);
			TextOut(hdc, 0, 0, gTouchStatue, /*64*/gstrLength);

			for(int i = 0; i< gtouchNum;i++)
			{
				if(preRate[i] < ReportRate)
					SetTextColor(hdc,RGB(255, 0, 0));
				else
					SetTextColor(hdc,RGB(0, 0, 0));

				StringCchLength(gShowTouchStr[i], 256, (size_t*)&gstrLength);
				TextOut(hdc, 0, 20 * (i+1) , gShowTouchStr[i], /*256*/gstrLength);
			}
		}
		
		if(ShowBackground && gInitState)
			fnWndProc_OnPaint(hdc,rc);   //claire0311
			//PaintRoutine (hWnd, hdc, cxClient, cyClient);   //claire0311 hide


        EndPaint(hWnd, &ps);
		//DeleteObject(hdc);
        break;

	case WM_TIMER:

		//sample rate
		if ( FlagTimer )
		{
			for(int i=0; i<MAXPOINTS; i++)
			{
				preRate[i] = Rate[i];
				Rate[i] = 0;
				//mOutFile << " i ="<< i <<", preRate[i] = "<<preRate[i]<<endl;
			}
		}
		
		if(!gLineEnable)
		{
			InvalidateRect(hWnd, NULL, TRUE);
			for(int i=0; i<MAXPOINTS; i++)
			{
				if(TouchResult[i] == true)
					PassCounter++;
			}

			if(PassCounter >= PassFinger)
			{
				CaptureScreen(hWnd, "Touch.bmp");
				mOutFile << PassFinger << " Fingers Test Pass"<<endl;
				PostQuitMessage(0);
			}
			else
				PassCounter = 0;

			Counter+= 1000;	

			if(TestTime && Counter >= TestTime)//kid
			{
				CaptureScreen(hWnd, "Touch.bmp");

				if(PassCounter >= PassFinger)
					mOutFile << PassFinger << " Fingers Test Pass"<<endl;
				else
					mOutFile << PassFinger << " Fingers Test Fail"<<endl;

				PostQuitMessage(0);
			}

		}
		break;


	  case WM_CLOSE:

		//if(MessageBox(hWnd, TEXT("Close the program?"), TEXT("Close"),  MB_ICONQUESTION | MB_YESNO) == IDYES)
		//{
		  DestroyWindow(hWnd);
		//}
		  
		break;


    case WM_DESTROY:
		if(fh)
			fclose(fh);
        mOutFile.close();
		mOutCsvFile.close();
        PostQuitMessage(0);
		
        break;
	case WM_ERASEBKGND:
		if(gLineClear)
		{
			gLineClear = false;
		}
		//break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

long fnWndProc_OnPaint(HDC hDC,RECT rc)
{
	FLOAT cxDpi, cyDpi;
	POINT pt;
	HFONT hFont,hTmp;
	//int fullscreenWidth;
	int fontSize;

	if (fLogRes)
	{
		cxDpi = (FLOAT) GetDeviceCaps (hDC, LOGPIXELSX) ;
		cyDpi = (FLOAT) GetDeviceCaps (hDC, LOGPIXELSY) ;
	}
	else
	{
		cxDpi = (FLOAT) (25.4 * GetDeviceCaps (hDC, HORZRES) /
			GetDeviceCaps (hDC, HORZSIZE)) ;

		cyDpi = (FLOAT) (25.4 * GetDeviceCaps (hDC, VERTRES) /
			GetDeviceCaps (hDC, VERTSIZE)) ;
	}

	 pt.x = 0;
	 pt.y = (int) (Font_Size * cyDpi / 72);

	 DPtoLP (hDC, &pt, 1) ;
	 fontSize  = - (int) (fabs ((float)pt.y) / 10.0 + 0.5);

	 //fullscreenWidth = GetDeviceCaps(hDC, HORZRES);
	 //fontSize = fullscreenWidth/10;
	 SetTextColor(hDC,RGB(230,230,230));
	 hFont=CreateFont(fontSize,0,0,0,FW_BOLD,0,0,0,0,0,0,2,0,_T("SYSTEM_FIXED_FONT"));
	 hTmp=(HFONT)SelectObject(hDC,hFont);
	 SetBkMode(hDC,TRANSPARENT);
	 DrawText(hDC,szBuffer,-1,&rc,DT_SINGLELINE|DT_CENTER|DT_VCENTER);
		
	 //DeleteObject(SelectObject(hDC,hTmp));
	 return 0;
}

//claire0311 hide----------
//void PaintRoutine (HWND hwnd, HDC hdc, int cxArea, int cyArea)
//{
//	//static TCHAR szString [] = TEXT ("十指测试") ;
//
//
//
//	HFONT        hFont ;
//	int          i ;
//	LOGFONT      lf ;
//
//	SetTextColor(hdc,RGB(230,230,230));
//	hFont = EzCreateFont (hdc, TEXT ("Times New Roman"), Font_Size, 0, 0, TRUE) ;
//	GetObject (hFont, sizeof (LOGFONT), &lf) ;
//	DeleteObject (hFont) ;
//
//	SetBkMode (hdc, TRANSPARENT) ;
//	SetTextAlign (hdc, TA_BASELINE) ;
//	SetViewportOrgEx (hdc, Font_X, cyArea / 2, NULL) ;
//
//
//	SelectObject (hdc, CreateFontIndirect (&lf)) ;
//
//	//TextOut (hdc, 0, 0, szString, lstrlen (szString)) ;
//
//	TextOut (hdc, 0, 0, szBuffer, lstrlen (szBuffer)) ;
//
//	DeleteObject (SelectObject (hdc, GetStockObject (SYSTEM_FONT))) ;
//
//}
//
//HFONT EzCreateFont (HDC hdc, TCHAR * szFaceName, int iDeciPtHeight,
//					int iDeciPtWidth, int iAttributes, BOOL fLogRes)
//{
//	FLOAT      cxDpi, cyDpi ;
//	HFONT      hFont ;
//	LOGFONT    lf ;
//	POINT      pt ;
//	TEXTMETRIC tm ;
//
//	SaveDC (hdc) ;
//
//	SetGraphicsMode (hdc, GM_ADVANCED) ;
//	ModifyWorldTransform (hdc, NULL, MWT_IDENTITY) ;
//	SetViewportOrgEx (hdc, 0, 0, NULL) ;
//	SetWindowOrgEx   (hdc, 0, 0, NULL) ;
//
//	if (fLogRes)
//	{
//		cxDpi = (FLOAT) GetDeviceCaps (hdc, LOGPIXELSX) ;
//		cyDpi = (FLOAT) GetDeviceCaps (hdc, LOGPIXELSY) ;
//	}
//	else
//	{
//		cxDpi = (FLOAT) (25.4 * GetDeviceCaps (hdc, HORZRES) /
//			GetDeviceCaps (hdc, HORZSIZE)) ;
//
//		cyDpi = (FLOAT) (25.4 * GetDeviceCaps (hdc, VERTRES) /
//			GetDeviceCaps (hdc, VERTSIZE)) ;
//	}
//
//	pt.x = (int) (iDeciPtWidth  * cxDpi / 72) ;
//	pt.y = (int) (iDeciPtHeight * cyDpi / 72) ;
//
//	DPtoLP (hdc, &pt, 1) ;
//	lf.lfHeight         = - (int) (fabs ((float)pt.y) / 10.0 + 0.5) ;
//	lf.lfWidth          = 0 ;
//	lf.lfEscapement     = 0 ;
//	lf.lfOrientation    = 0 ;
//	lf.lfWeight         = iAttributes & EZ_ATTR_BOLD      ? 700 : 0 ;
//	lf.lfItalic         = iAttributes & EZ_ATTR_ITALIC    ?   1 : 0 ;
//	lf.lfUnderline      = iAttributes & EZ_ATTR_UNDERLINE ?   1 : 0 ;
//	lf.lfStrikeOut      = iAttributes & EZ_ATTR_STRIKEOUT ?   1 : 0 ;
//	lf.lfCharSet        = DEFAULT_CHARSET ;
//	lf.lfOutPrecision   = 0 ;
//	lf.lfClipPrecision  = 0 ;
//	lf.lfQuality        = 0 ;
//	lf.lfPitchAndFamily = 0 ;
//
//	lstrcpy (lf.lfFaceName, szFaceName) ;
//
//	hFont = CreateFontIndirect (&lf) ;
//
//	if (iDeciPtWidth != 0)
//	{
//		hFont = (HFONT) SelectObject (hdc, hFont) ;
//
//		GetTextMetrics (hdc, &tm) ;
//
//		DeleteObject (SelectObject (hdc, hFont)) ;
//
//		lf.lfWidth = (int) (tm.tmAveCharWidth *
//			fabs ((float)pt.x) / fabs ((float)pt.y) + 0.5) ;
//
//		hFont = CreateFontIndirect (&lf) ;
//	}
//
//	RestoreDC (hdc, -1) ;
//	return hFont ;
//}
//
//void TextOutWithFont(HDC hdc, LPRECT lprect, LPSTR lpstr)
//{
//  // Gets current font
//  HFONT hFont = (HFONT) GetCurrentObject(hdc, OBJ_FONT);
//
//  // Gets LOGFONT structure corresponding to current font
//  LOGFONT LogFont;
//  if (GetObject(hFont, sizeof(LOGFONT), &LogFont) == 0)
//    return;
//
//  // Calculates span of the string sets rough font width and height
//  int Len = strlen(lpstr);
//  int Width = lprect->right - lprect->left;
//  LogFont.lfWidth = -MulDiv(Width / Len, GetDeviceCaps(hdc, LOGPIXELSX), 72);
//  int Height = lprect->bottom - lprect->top;
//  LogFont.lfHeight = -MulDiv(Height, GetDeviceCaps(hdc, LOGPIXELSY), 72);
//
//  // Creates and sets font to device context
//  hFont = CreateFontIndirect(&LogFont);
//  HFONT hOldFont = (HFONT) SelectObject(hdc, hFont);
//
//  // Gets the string span and text metrics with current font
//  SIZE Size;
//  GetTextExtentExPoint(hdc,(LPCWSTR) lpstr, strlen(lpstr), Width, NULL, NULL, &Size);
//  TEXTMETRIC TextMetric;
//  GetTextMetrics(hdc, &TextMetric);
//  int RowSpace = TextMetric.tmExternalLeading;
//
//  // Deselects and deletes rough font
//  SelectObject(hdc, hOldFont);
//  DeleteObject(hFont);
//
//  // Updates font width and height with new information of string span
//  LogFont.lfWidth = MulDiv(LogFont.lfWidth, Width, Size.cx);
//  LogFont.lfHeight = MulDiv(LogFont.lfHeight, Height, Size.cy - RowSpace);
//
//  // Creates and selects font of actual span filling the rectangle
//  hFont = CreateFontIndirect(&LogFont);
//  SelectObject(hdc, hFont);
//
//  // Performs displaying text
//  TextOut(hdc, lprect->left, lprect->top - RowSpace, (LPCWSTR)lpstr, strlen(lpstr));
//
//  // Select the original font and deletes the new font
//  SelectObject(hdc, hOldFont);
//  DeleteObject(hFont);
//}
//claire0311 hide----------

// Message handler for about box.
// in:
//      hDlg        window handle
//      message     message code
//      wParam      message parameter (message-specific)
//      lParam      message parameter (message-specific)
// returns:
//      the result of the message processing and depends on the message sent
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}


bool enterFullscreen(HWND hwnd, int fullscreenWidth, int fullscreenHeight, int colourBits, int refreshRate) 
{     
	DEVMODE fullscreenSettings;
	bool isChangeSuccessful; 
	RECT windowBoundary; 

	EnumDisplaySettings(NULL, 0, &fullscreenSettings); 
	fullscreenSettings.dmPelsWidth        = fullscreenWidth;
	fullscreenSettings.dmPelsHeight       = fullscreenHeight; 
	fullscreenSettings.dmBitsPerPel       = colourBits;     
	fullscreenSettings.dmDisplayFrequency = refreshRate; 
	fullscreenSettings.dmFields           = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL | DM_DISPLAYFREQUENCY;
	
	SetWindowLongPtr(hwnd, GWL_EXSTYLE, WS_EX_APPWINDOW | WS_EX_TOPMOST);     
	SetWindowLongPtr(hwnd, GWL_STYLE, WS_POPUP | WS_VISIBLE);   
	SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, fullscreenWidth, fullscreenHeight, SWP_SHOWWINDOW);    
	
	isChangeSuccessful = ChangeDisplaySettings(&fullscreenSettings, CDS_FULLSCREEN) == DISP_CHANGE_SUCCESSFUL; 

	ShowWindow(hwnd, SW_MAXIMIZE);      
	
	return isChangeSuccessful; 
}

bool exitFullscreen(HWND hwnd, int windowX, int windowY, int windowedWidth, int windowedHeight, int windowedPaddingX, int windowedPaddingY) 
{     
	bool isChangeSuccessful;      

	SetWindowLongPtr(hwnd, GWL_EXSTYLE, WS_EX_LEFT);     
	SetWindowLongPtr(hwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW | WS_VISIBLE);     

	isChangeSuccessful = ChangeDisplaySettings(NULL, CDS_RESET) == DISP_CHANGE_SUCCESSFUL;     

	SetWindowPos(hwnd, HWND_NOTOPMOST, windowX, windowY, windowedWidth + windowedPaddingX, windowedHeight + windowedPaddingY, SWP_SHOWWINDOW);
	ShowWindow(hwnd, SW_RESTORE);      

	return isChangeSuccessful; 
} 

void ChangeToFullScreen(HWND hWnd)
{
	if(!gFullScreen)
	{
		HDC windowHDC;
		int fullscreenWidth;
		int fullscreenHeight;
		int colourBits;
		int refreshRate;

		GetWindowRect(hWnd, &WndRect);				

		windowHDC = GetDC(hWnd); 
		fullscreenWidth  = GetDeviceCaps(windowHDC, HORZRES); 
		fullscreenHeight = GetDeviceCaps(windowHDC, VERTRES); 
		colourBits       = GetDeviceCaps(windowHDC, BITSPIXEL); 
		refreshRate      = GetDeviceCaps(windowHDC, VREFRESH); 
		if(enterFullscreen(hWnd, fullscreenWidth, fullscreenHeight, colourBits, refreshRate))
		{
			WndMenu = GetMenu(hWnd);
			CheckMenuItem(WndMenu, IDM_FULLSCREEN, MF_CHECKED);

			SetMenu(hWnd, NULL);

			gFullScreen = true;
		}
	}
	else
	{
		if(exitFullscreen(hWnd, WndRect.left, WndRect.top, WndRect.right-WndRect.left, WndRect.bottom-WndRect.top, 0, 0))
		{
			gFullScreen = false;
		}
		
		CheckMenuItem(WndMenu, IDM_FULLSCREEN, MF_UNCHECKED);
		SetMenu(hWnd, WndMenu);
	}
}


bool FileExist( LPCTSTR filename ) 
{ 
    // Data structure for FindFirstFile 
    WIN32_FIND_DATA findData; 

    // Clear find structure 
    ZeroMemory(&findData, sizeof(findData)); 


    // Search the file 
    HANDLE hFind = FindFirstFile( filename, &findData ); 
    if ( hFind == INVALID_HANDLE_VALUE ) 
    { 
        // File not found 
        return false; 
    } 

    // File found 


    // Release find handle 
    FindClose( hFind ); 
    hFind = NULL; 

    // The file exists 
    return true; 

} 

void CaptureScreen(HWND window, const char* filename) 
{ 
	// get screen rectangle 
	RECT windowRect; 
	GetWindowRect(window, &windowRect); 

	// bitmap dimensions 
	int bitmap_dx = windowRect.right - windowRect.left; 
	int bitmap_dy = windowRect.bottom - windowRect.top; 

	// create file 
	TCHAR t_BMPFileName[FILENAME_MAX];
	char BMPFileName[FILENAME_MAX];

	//GetCurrentDirectory(FILENAME_MAX, t_BMPFileName);

	//#ifdef UNICODE
	//	DWORD dwNum = WideCharToMultiByte(CP_ACP,NULL,t_BMPFileName,-1,NULL,0,NULL,FALSE);
	//	WideCharToMultiByte (CP_ACP,NULL,t_BMPFileName,-1,BMPFileName,dwNum,NULL,FALSE);
	//#else
	//	strcpy(t_BMPFileName, BMPFileName);
	//#endif

	//strcat(BMPFileName, "\\");
	//strcat(BMPFileName, TOUCHDIR);
	//strcat(BMPFileName, "\\");
	//strcat(BMPFileName, filename);

	
	strcpy_s(BMPFileName, FILENAME_MAX, TOUCHDIR);
	strcat_s(BMPFileName, FILENAME_MAX, "\\");
	strcat_s(BMPFileName, FILENAME_MAX, filename);

	int nIndex = MultiByteToWideChar(CP_ACP, 0, BMPFileName , -1, NULL, 0);
	MultiByteToWideChar(CP_ACP, 0, BMPFileName , -1, t_BMPFileName, nIndex); 

	mOutFile << BMPFileName <<endl;

	if(FileExist(t_BMPFileName) == true)
		 DeleteFile(t_BMPFileName);


	//ofstream file(filename, ios::binary); 
	ofstream file(BMPFileName, ios::binary); 
	if(!file) return; 

	// save bitmap file headers 
	BITMAPFILEHEADER fileHeader; 
	BITMAPINFOHEADER infoHeader; 

	fileHeader.bfType      = 0x4d42; 
	fileHeader.bfSize      = 0; 
	fileHeader.bfReserved1 = 0; 
	fileHeader.bfReserved2 = 0; 
	fileHeader.bfOffBits   = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER); 

	infoHeader.biSize          = sizeof(infoHeader); 
	infoHeader.biWidth         = bitmap_dx; 
	infoHeader.biHeight        = bitmap_dy; 
	infoHeader.biPlanes        = 1; 
	infoHeader.biBitCount      = 24; 
	infoHeader.biCompression   = BI_RGB; 
	infoHeader.biSizeImage     = 0; 
	infoHeader.biXPelsPerMeter = 0; 
	infoHeader.biYPelsPerMeter = 0; 
	infoHeader.biClrUsed       = 0; 
	infoHeader.biClrImportant  = 0; 

	file.write((char*)&fileHeader, sizeof(fileHeader)); 
	file.write((char*)&infoHeader, sizeof(infoHeader)); 

	// dibsection information 
	BITMAPINFO info; 
	info.bmiHeader = infoHeader;  

	// create a dibsection and blit the window contents to the bitmap 
	HDC winDC = GetWindowDC(window); 
	HDC memDC = CreateCompatibleDC(winDC); 
	/*BYTE* */ char* memory = 0; 
	HBITMAP bitmap = CreateDIBSection(winDC, &info, DIB_RGB_COLORS, (void**)&memory, 0, 0); 
	SelectObject(memDC, bitmap); 
	BitBlt(memDC, 0, 0, bitmap_dx, bitmap_dy, winDC, 0, 0, SRCCOPY); 
	DeleteDC(memDC); 
	ReleaseDC(window, winDC); 

	// save dibsection data 
	int bytes = (((24*bitmap_dx + 31) & (~31))/8)*bitmap_dy; 
	file.write(memory, bytes); 

	DeleteObject(bitmap); 

	mOutFile << "saved bmp" <<endl;
}


