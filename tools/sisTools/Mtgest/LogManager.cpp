#include "LogManager.h"
#include <iostream>
#include <sstream> 

LogManager::LogManager()
{
	m_count = 0;
}

LogManager::~LogManager()
{
    
}

void LogManager::setLog()
{
	if(m_filePtr.is_open()) m_filePtr.close();
	std::string s;
	std::stringstream ss(s);
	ss << m_count;
	std::string t = std::string("test") + ss.str() +  std::string(".txt");
	m_filePtr.open(t.c_str(), std::ios_base::out);
	m_oldMessage = " ";
	++m_count;
}

void LogManager::append(std::string message)
{
	if(m_filePtr.is_open())
	{
		if(m_oldMessage != message)
		m_filePtr.write(std::string(message + "\n").c_str(), message.size() + 1);
	}
	m_oldMessage = message;
}