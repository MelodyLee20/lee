#ifndef DRAWMANAGER_H
#define DRAWMANAGER_H

#include "ipainttest.h"

class drawmanager
{
public:
    drawmanager();
    ~drawmanager();
    void setFunction(int functionID);
    void reset();
    void paint(QPainter *painter, QPaintEvent *event, int elapsed);

    IpaintTest* m_paintTest;
};

#endif // DRAWMANAGER_H
