#ifndef DIALOG_H
#define DIALOG_H

#include <QMainWindow>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include "FreeConsole.h"
#include <QProgressBar>
#include <QProgressDialog>
#include <QTimer>
#include "mainwindow.h"

//#include "runfwcmdthread.h"

//#define beta_num 2
#define precise 128
#define mrPOS 0x0225	//Read multiChip position
#define srPOS 0x0201	//Read singleChip position

#define rLEN 0x3000	//Read length
#define ALLROMSIZE 0x20000
#define rADDR 0xa0000000+0xc000	 //ROM address
#define bADDR 0xc000
#define rADDR819 0xa0000000+0x4000
#define bADDR819 0x4000

#define MAX_810_SMB_READ_BYTES 15
#define wBlockAddr 0xc000
#define wPOS 0x024f//0x0125
#define wLEN 0x1000

//#define WRITE_BIN

namespace Ui {
class dialog;
}

enum SIS_STATUS
{
    SIS_STATUS_PASS = 0,
    SIS_STATUS_FAIL = -1,
    SIS_STATUS_PARAMETER_ERROR = -2,
    SIS_STATUS_FW_FILE_ERROR = -3,
    SIS_STATUS_FW_IO_ERROR = -4,
    SIS_STATUS_FW_ROM_ERROR = -5,
    SIS_STATUS_USER_STOP = -6,
    SIS_STATUS_CRC_MISMATCH = -7,
};


class dialog : public QMainWindow
{
    Q_OBJECT

public:
    explicit dialog(QWidget *parent = 0);
    ~dialog();

    QTimer *timer;
    int betaNum,err;
    void exitUpdateFW();

signals:
    void functionChoice(int id);

private slots:
    void on_outside_clicked();

    void on_parseOutside_clicked();

    void on_Exit_clicked();

//public slots:
//    void processStart();
//    void processEnd();

public:

    void PrintBuffer( unsigned char *, unsigned int);

//    bool ReadFirmwareMem( HANDLE , unsigned int , const UINT ,
//        unsigned short *, PUCHAR , BOOLEAN );
    bool ReadFirmwareMem( unsigned int , const unsigned int ,
        unsigned short *, unsigned char* , bool );

//    bool Sub_BlockWriteMemBuffTo810( HANDLE , unsigned int , unsigned char* ,
//        unsigned int , bool check_ack = true , bool isRecal = true);
    bool Sub_BlockWriteMemBuffTo810( unsigned int , unsigned char* ,
        unsigned int , bool check_ack = true , bool isRecal = true);

    int GaussianUpper (int , int );
    void showLeave();
    void InitialP();

    int i;
    int j;
    int k;
    int sum;
    int beta_count;
    int isSwap;
    int isInvx;
    int isInvy;
    int slvNum;
    int slvID[8];
    int selectChiptype;

    double a[10][10];
    double d;
    double tmp;

    double tmp_FWtouch[2];
    double tmp_A[2];
    double PL[2];
    double ITO_DEF[2];
    double OSscreen_DEF[2];
    double target[2];
    double touch[2];
    double data[100];
    double result[9];
    QFile myfile;

    QString context;
    QString preContext;
    QFile m_file;

    QString bFileName;
    QFile fp;

    unsigned short nByteCntRead;
    unsigned char DeviceOutput[rLEN];
    //UINT recieve_err;

    bool bOK;
    bool bSuc;
    //HANDLE Device;
    int recieve_err;

    //bool call_88_for_coordinate(HANDLE hDevice, unsigned char * uWriteData, const int writeDataSize, const int type );
    bool call_88_for_coordinate(unsigned char * uWriteData, const int writeDataSize, const int type );
    //bool call_88_for_swap_inverse(HANDLE hDevice, unsigned char thirdByte, unsigned char swap, unsigned char inverseX, unsigned char inverseY, const int type );
    bool call_88_for_swap_inverse(unsigned char thirdByte, unsigned char swap, unsigned char inverseX, unsigned char inverseY, const int type );
    bool separateDoubleToShortShort( double * _doubles, unsigned char * _bytes, const int nDoubles, const int nByte);   
    SIS_STATUS SIS_setCoordinateParam( const double Ax, const double Bx, const double Ay, const double By,
                            const double Dx, const double Dy, const int type, double R1 = 0.0f, double R2 = 0.0f);


    //RunFwCmdThread* updateStepThread;

private:
    Ui::dialog *ui;

    //QProgressDialog m_progressDialog;
    int convertToFunctionID(QString text);

};

#endif // DIALOG_H
