﻿#include "paintbackground.h"
#include <QPen>
#include "parameter.h"
#include <QDebug>
#include "touchdata.h"
#include <QTouchEvent>
#include <QPushButton>
#include <QMessageBox>
#ifdef INNER_ALG
    //#include "algorithm.h"
    #include "algononoutside.h"
#endif
#include "pathmanager.h"

paintBackground::paintBackground()
    :m_lineDx(10), m_dir(VERTICAL), m_solidLineWidth(1), m_dashLineWidth(1), m_speed(4)
{
    c_x = -200;
    c_y = -200;
    m_x = -200;
    m_y = -200;
    m_endX = -200;
    m_endY = -200;
    m_solidLinePen = QPen(Qt::green,m_solidLineWidth,Qt::SolidLine);
    m_dashLinePen = QPen(Qt::black,m_dashLineWidth,Qt::DashLine);
    m_arrowLinePen = QPen(Qt::red,12,Qt::SolidLine);
    m_emptyLinePen = QPen(QColor(128,128,0,0xa0),12,Qt::SolidLine);
    reset();
}

paintBackground::~paintBackground()
{

}

void paintBackground::reset()
{
    //m_TState = INIT;
    //m_TDraw = MID;
    m_TState = STOP;
    m_TDraw = LT;

    if(PARA.getPointMode() == 1)
    {
        countMax = 10;
    }
    else if(PARA.getPointMode() == 2)
    {
        countMax = 5;
        //countMax = 4;
    }
    m_TCount = countMax;

    exitbtnR = 50;
    flag = 0;
    preCountx = 0;
    preCounty = 0;
    preTouchTestX = 0;
    preTouchTestY = 0;

    QFile::remove(PathManager::getFilePath("pattern/trainTargetX.txt"));
    QFile::remove(PathManager::getFilePath("pattern/trainTargetY.txt"));
    QFile::remove(PathManager::getFilePath("pattern/trainTouchX.txt"));
    QFile::remove(PathManager::getFilePath("pattern/trainTouchY.txt"));


    m_file1.setFileName(PathManager::getFilePath("pattern/trainTargetX.txt"));
    m_file1.open(QIODevice::WriteOnly | QIODevice::Text);

    m_file2.setFileName(PathManager::getFilePath("pattern/trainTargetY.txt"));
    m_file2.open(QIODevice::WriteOnly | QIODevice::Text);

    m_file3.setFileName(PathManager::getFilePath("pattern/trainTouchX.txt"));
    m_file3.open(QIODevice::WriteOnly | QIODevice::Text);

    m_file4.setFileName(PathManager::getFilePath("pattern/trainTouchY.txt"));
    m_file4.open(QIODevice::WriteOnly | QIODevice::Text);
    m_TCount--;
}

void paintBackground::retry()
{
    m_TState = INIT;
    m_TDraw = MID;

    if(PARA.getPointMode() == 1)
    {
        countMax = 10;
    }
    else if(PARA.getPointMode() == 2)
    {
        countMax = 5;
    }
    m_TCount = countMax;

    exitbtnR = 50;
    flag = 0;
}

void paintBackground::setPosition(float x, float y, float endX, float endY)
{
    m_x = x;
    m_y = y;
    m_endX = endX;
    m_endY = endY;
    setslope(m_x, m_y, m_endX, m_endY);
}

void paintBackground::setCirclePosition(float x, float y)
{
    c_x = x;
    c_y = y;
}

void paintBackground::setRectPosition(float x, float y)
{
    r_x = x;
    r_y = y;
}

void paintBackground::setslope(float x, float y, float x2, float y2)
{
    if(x == x2)
        x2 = x + 1;
    m_slope = (y2 - y)/(x2 - x);
}

void paintBackground::setDir(LineDir dir)
{
    m_dir = dir;
}

void paintBackground::setID(int id)
{
    m_id = id;
}

void paintBackground::setLineDx(int lineDx)
{
    m_lineDx = lineDx;
}

void paintBackground::setSpeed(int speed)
{
    m_speed = speed;
}

void paintBackground::setSolidLine(int solidLine)
{
    m_solidLineWidth = solidLine;
    m_solidLinePen.setWidth(solidLine);
}

void paintBackground::setDashLine(int DashLine)
{
    m_dashLineWidth = DashLine;
    m_dashLinePen.setWidth(DashLine);
}

int paintBackground::getmarginalLineShift()
{
    return (m_lineDx)/2;
}

void paintBackground::paint(QPainter *painter, QPaintEvent *event)
{
    painter->save();

    double screenHeight = PARA.getScreenH();
    double screenWidth = PARA.getScreenW();
    double circleDm = screenHeight/40;

    double correctRegionH = screenHeight;
    double correctRegionW = screenWidth;

    painter->setPen(m_dashLinePen);
    painter->drawLine(c_x, c_y, screenWidth-c_x, c_y);
    painter->drawLine(screenWidth-c_x, c_y, screenWidth-c_x, screenHeight-c_y);
    painter->drawLine(screenWidth-c_x, screenHeight-c_y, c_x, screenHeight-c_y);
    painter->drawLine(c_x, screenHeight-c_y, c_x, c_y);

    QBrush transGreen(QColor(51, 102, 102, 0x0a));
    painter->setBrush(transGreen);
    painter->drawRect(c_x, c_y, screenWidth-2*c_x, screenHeight-2*c_y);

    font = painter->font();
    font.setPointSize(30);
    font.setWeight(QFont::DemiBold);
    painter->setFont(font);

    if(m_TCount == countMax)
        painter->drawText(QPoint(5*screenWidth/13, 4*screenWidth/30), QObject::tr("Please touch 'any place' to initial..."));

    if(m_TCount==(countMax*8/10+1) && PARA.getPointMode() == 1){
        QPixmap arrow;
        painter->setPen(m_arrowLinePen);

        TargetX=c_x;
        TargetY=c_y;
        painter->drawEllipse(TargetX-circleDm,TargetY-circleDm,circleDm*2,circleDm*2);

        if(m_TCount==(countMax*8/10+1))
        {
            for(int j=0;j<1000;j++)
               for(int i=0;i<1000;i++)
               {}
        }

        painter->setPen(m_dashLinePen);
        painter->drawLine(TargetX-circleDm,TargetY,TargetX+circleDm,TargetY);
        painter->drawLine(TargetX,TargetY-circleDm,TargetX,TargetY+circleDm);
#ifdef MAXELL
        arrow = QPixmap(PathManager::getFilePath("picture", "UpperLeft.png"));
        painter->drawPixmap(QPoint(TargetX, TargetY), arrow);
#else
        font=painter->font() ;
        font.setPointSize ( 20 );
        font.setWeight(QFont::DemiBold);
        painter->setFont(font);
        txt1="Touch 'red' circle center";
        painter->drawText(QPoint(TargetX+3*circleDm, TargetY+3*circleDm),txt1);
        txt1="Touch : "+QString::number(rectTP.center().x())+" , "+QString::number(rectTP.center().y());
        painter->drawText(QPoint(TargetX+3*circleDm, TargetY+5*circleDm),txt1);
        txt1="Target : ";
        txt1+=QString::number(TargetX)+" , "+QString::number(TargetY);
        painter->drawText(QPoint(TargetX+3*circleDm, TargetY+7*circleDm),txt1);
#endif
    }

    if(m_TCount==(countMax*3/5+1) && PARA.getPointMode() == 2)
    {
        QPixmap arrow;
        painter->setPen(m_arrowLinePen);

        TargetX=c_x;
        TargetY=c_y;
        painter->drawEllipse(TargetX-circleDm,TargetY-circleDm,circleDm*2,circleDm*2);

        if(m_TCount==(countMax*3/5+1))
        {
            for(int j=0;j<1000;j++)
               for(int i=0;i<1000;i++)
               {}
        }

        painter->setPen(m_dashLinePen);
        painter->drawLine(TargetX-circleDm,TargetY,TargetX+circleDm,TargetY);
        painter->drawLine(TargetX,TargetY-circleDm,TargetX,TargetY+circleDm);
#ifdef MAXELL
        arrow = QPixmap(PathManager::getFilePath("picture", "UpperLeft.png"));
        painter->drawPixmap(QPoint(TargetX, TargetY), arrow);
#else
        font=painter->font() ;
        font.setPointSize ( 20 );
        font.setWeight(QFont::DemiBold);
        painter->setFont(font);
        txt1="Touch 'red' circle center";
        painter->drawText(QPoint(TargetX+3*circleDm, TargetY+3*circleDm),txt1);
        txt1="Touch : "+QString::number(rectTP.center().x())+" , "+QString::number(rectTP.center().y());
        painter->drawText(QPoint(TargetX+3*circleDm, TargetY+5*circleDm),txt1);
        txt1="Target : ";
        txt1+=QString::number(TargetX)+" , "+QString::number(TargetY);
        painter->drawText(QPoint(TargetX+3*circleDm, TargetY+7*circleDm),txt1);
#endif
    }

    if(m_TCount == 0)
    {
        font = painter->font() ;
        font.setPointSize(30);
        font.setWeight(QFont::DemiBold);
        painter->setFont(font);
        painter->drawText(QPoint(4*screenWidth/12, 2*screenWidth/30), QObject::tr("Please touch BUTTON to 'EXIT' or 'RETRY'"));

        double TT1[2];
        double TT2[2];
        double tar1[50];
        double tar2[50];
        double tar3[50];
        double tar4[50];

        myfile.setFileName(PathManager::getFilePath("pattern/trainTargetX.txt"));
        if(myfile.open(QIODevice::ReadOnly))
        {
            i = 0;
            betaNum = 0;
            QTextStream stream(&myfile);
            while(!stream.atEnd())
            {
                stream>>tar1[i++];
                betaNum++;
            }
            myfile.close();
        }

        myfile.setFileName(PathManager::getFilePath("pattern/trainTargetY.txt"));
        if(myfile.open(QIODevice::ReadOnly))
        {
            i = 0;
            QTextStream stream(&myfile);
            while(!stream.atEnd())
            {
                stream>>tar2[i++];
            }
            myfile.close();
        }

        myfile.setFileName(PathManager::getFilePath("pattern/trainTouchX.txt"));
        if(myfile.open(QIODevice::ReadOnly))
        {
            i = 0;
            QTextStream stream(&myfile);
            while(!stream.atEnd())
            {
                stream>>tar3[i++];
            }
            myfile.close();
        }

        myfile.setFileName(PathManager::getFilePath("pattern/trainTouchY.txt"));
        if(myfile.open(QIODevice::ReadOnly))
        {
            i = 0;
            QTextStream stream(&myfile);
            while(!stream.atEnd())
            {
                stream>>tar4[i++];
            }
            myfile.close();
        }

        for(int j=0;j<betaNum-1;j++)
        {
            TT1[0] = tar1[j];
            TT1[1] = tar2[j];
            TT2[0] = tar3[j];
            TT2[1] = tar4[j];
//                qDebug()<<"point"<<j<<": "<<TT1[0]<<TT1[1]<<TT2[0]<<TT2[1]<<endl;

            font = painter->font() ;
            font.setPointSize(15);
            font.setWeight(QFont::DemiBold);
            painter->setFont(font);
            txt1 = "Point[" + QString::number(j+1) + "] : target(" + QString::number(tar1[j]) + " , " + QString::number(tar2[j])
                    + ")  Touch("+QString::number(tar3[j]) + " , "+QString::number(tar4[j]) + ")";
            painter->drawText(QPoint(4*screenWidth/12, (3+j)*screenWidth/30), txt1);
        }
        m_TState = END;
    }

    REGION_XMIN = r_x-screenWidth/6;
    REGION_YMIN = r_y-screenHeight/2;
    REGION_XADD = 0;
    REGION_YADD = 0;
    REGION_XMAX = r_x + screenWidth/6;
    REGION_YMAX = r_y + screenHeight/2;
    painter->setPen(m_solidLinePen);

    QEvent::Type touchState = TouchData::instance().getTouchState();

    QList<QTouchEvent::TouchPoint>touchP = TouchData::instance().getCTPs();
    if(!touchP.empty())
    {
        foreach(const QTouchEvent::TouchPoint &touchPoint, touchP)
        {
            QRectF rectTP = touchPoint.rect();
//                qDebug()<<"TPX POS:"<<rectTP.center().x();
//                qDebug()<<"TPY POS:"<<rectTP.center().y();
            if(m_TCount == 0)
            {
                if((touchPoint.rect().center().x()> 3*screenWidth/4-exitbtnR ) && (touchPoint.rect().center().x()< 3*screenWidth/4+exitbtnR)
                    && (touchPoint.rect().center().y()> screenHeight/2-exitbtnR) && (touchPoint.rect().center().y()< screenHeight/2+exitbtnR))
                {
                    paintBackground::instance().flag = 0;
                    paintBackground::instance().retryflag = 1;
                    retry();
                }
            }

            if(PARA.getPointMode() == 1)
            {
                bool tooNearWithPreTouch = false;
                switch(m_TState)
                {
                case INIT:
                {
                    if(m_TCount<=countMax && m_TCount>(countMax*9/10))
                    {
                        m_file1.setFileName(PathManager::getFilePath("pattern/trainTargetX.txt"));
                        m_file1.open(QIODevice::WriteOnly | QIODevice::Text);

                        m_file2.setFileName(PathManager::getFilePath("pattern/trainTargetY.txt"));
                        m_file2.open(QIODevice::WriteOnly | QIODevice::Text);

                        m_file3.setFileName(PathManager::getFilePath("pattern/trainTouchX.txt"));
                        m_file3.open(QIODevice::WriteOnly | QIODevice::Text);

                        m_file4.setFileName(PathManager::getFilePath("pattern/trainTouchY.txt"));
                        m_file4.open(QIODevice::WriteOnly | QIODevice::Text);

                        painter->drawEllipse(screenWidth/2-circleDm, screenHeight/2-circleDm, circleDm*2, circleDm*2);
                        m_TState = MID;
                    }

                    if(m_TCount<=(countMax*9/10) && m_TCount>(countMax*8/10))
                    {
                        m_TState = LT;
                    }
                    else if(m_TCount<=(countMax*8/10) && m_TCount>(countMax*7/10))
                    {
                        m_TState = LM;
                    }
                    else if(m_TCount<=(countMax*7/10) && m_TCount>(countMax*6/10))
                    {
                        m_TState = LB;
                    }
                    else if(m_TCount<=(countMax*6/10) && m_TCount>(countMax*5/10))
                    {
                        m_TState = MT;
                    }
                    else if(m_TCount<=(countMax*5/10) && m_TCount>(countMax*4/10))
                    {
                        m_TState = MM;
                    }
                    else if(m_TCount<=(countMax*4/10) && m_TCount>(countMax*3/10))
                    {
                        m_TState = MB;
                    }
                    else if(m_TCount<=(countMax*3/10) && m_TCount>(countMax*2/10))
                    {
                        m_TState = RT;
                    }
                    else if(m_TCount<=(countMax*2/10) && m_TCount>(countMax/10))
                    {
                        m_TState = RM;
                    }
                    else if(m_TCount<=(countMax/10) && m_TCount>0)
                    {
                        m_TState = RB;
                    }
                    break;
                }
                case MID:
                {
                    m_TDraw = MID;
                    if(touchState == QEvent::TouchEnd)
                    {
                        if(!(rectTP.center().x()==preCountx && rectTP.center().y()==preCounty))
                            m_TCount--;
                        preCountx = rectTP.center().x();
                        preCounty = rectTP.center().y();
                    }
                    m_TState = INIT;
                    break;
                }
                case LT:
                {
//                        qDebug()<<"LT:"<<rectTP.center().x()<<" , "<<rectTP.center().y();
                    m_TDraw = LT;
                    m_TState = STOP;
                    break;
                }
                case LM:
                {
                    m_TDraw = LM;
                    m_TState = STOP;
                    break;
                }
                case LB:
                {
                    m_TDraw = LB;
                    m_TState = STOP;
                    break;
                }
                case MT:
                {
                    m_TDraw = MT;
                    m_TState = STOP;
                    break;
                }
                case MM:
                {
                    m_TDraw = MM;
                    m_TState = STOP;
                    break;
                }
                case MB:
                {
                    m_TDraw = MB;
                    m_TState = STOP;
                    break;
                }
                case RT:
                {
                    m_TDraw = RT;
                    m_TState = STOP;
                    break;
                }
                case RM:
                {
                    m_TDraw = RM;
                    m_TState = STOP;
                    break;
                }
                case RB:
                {
                    m_TDraw = RB;
                    m_TState = STOP;
                    break;
                }
                case STOP:
                {
                    if(touchState == QEvent::TouchEnd)
                    {
                        if(TargetX>=0 && TargetX<=screenWidth && (rectTP.center().x()>(TargetX-correctRegionW)) && (rectTP.center().x()<(TargetX+correctRegionW))
                                && TargetY>=0 && TargetY<=screenHeight && (rectTP.center().y()>(TargetY-correctRegionH)) && (rectTP.center().y()<(TargetY+correctRegionH)))
                        {
                            if(!(rectTP.center().x()==preCountx && rectTP.center().y()==preCounty))
                            {

                                if ( ( m_TCount < 8 ) && (
                                    (rectTP.center().x()-preTouchTestX) * (rectTP.center().x()-preTouchTestX) +
                                    (rectTP.center().y()-preTouchTestY) * (rectTP.center().y()-preTouchTestY) <
                                    (screenHeight/10) * (screenHeight/10) )
                                    )
                                {
                                    tooNearWithPreTouch = true;
                                }

                                if(tooNearWithPreTouch == false){
#ifndef INNER_ALG
                                    context1 = QString::number((int)(((TargetX)*100)/100.0));
                                    QTextStream out1(&m_file1);
                                    out1<<context1<<" ";

                                    context2 = QString::number((int)(((TargetY)*100)/100.0));
                                    QTextStream out2(&m_file2);
                                    out2<<context2<<" ";

                                    context3 = QString::number((int)((rectTP.center().x()*100)/100.0));
                                    QTextStream out3(&m_file3);
                                    out3<<context3<<" ";

                                    context4 = QString::number((int)((rectTP.center().y()*100)/100.0));
                                    QTextStream out4(&m_file4);
                                    out4<<context4<<" ";
#else
                                    ALG.addTargetX((int)(((TargetX)*100)/100.0));
                                    ALG.addTargetY((int)(((TargetY)*100)/100.0));
                                    ALG.addTouchX((int)((rectTP.center().x()*100)/100.0));
                                    ALG.addTouchY((int)((rectTP.center().y()*100)/100.0));
#endif
                                    m_TCount--;
                                }
                            }
                            preCountx = rectTP.center().x();
                            preCounty = rectTP.center().y();
                        }

                        m_TState = INIT;

                        if(m_TCount==0)
                        {
                            m_TState = END;
                        }
/*
                            if(TargetX> 0 && TargetX<screenWidth-1
                               && TargetY> 0 && TargetY<screenHeight-1)
                            qDebug()<<"TarX : "<<TargetX;
                            qDebug()<<"TX : "<<(int)((rectTP.center().x()*100)/100.0);
                            if(TargetX> 0 && TargetX<screenWidth-1
                               && TargetY> 0 && TargetY<screenHeight-1)
                            qDebug()<<"TarY : "<<TargetY;
                            qDebug()<<"TY : "<<(int)((rectTP.center().y()*100)/100.0)<<endl;
*/
                    }

                    break;
                }
                case END:
                {
                    m_TDraw = END;
                    m_file1.close();
                    m_file2.close();
                    m_file3.close();
                    m_file4.close();
                    paintBackground::instance().flag = 1;
                    break;
                }
                default:
                    break;
                }
#ifdef MAXELL
                QPixmap arrow;
#endif
                switch(m_TDraw)
                {
                case MID:
                    break;
                case LT:
                    if(m_TCount==(countMax*8/10+1))
                        painter->setPen(m_arrowLinePen);

                    TargetX = c_x;
                    TargetY = c_y;
                    painter->drawEllipse(TargetX-circleDm, TargetY-circleDm, circleDm*2, circleDm*2);

                    if(m_TCount==(countMax*8/10+1))
                    {
                        for(int j=0;j<1000;j++)
                           for(int i=0;i<1000;i++)
                           {}
                    }


                    painter->setPen(m_dashLinePen);
                    painter->drawLine(TargetX-circleDm, TargetY, TargetX + circleDm, TargetY);
                    painter->drawLine(TargetX, TargetY - circleDm, TargetX, TargetY + circleDm);
#ifdef MAXELL
                    arrow = QPixmap(PathManager::getFilePath("picture", "UpperLeft.png"));
                    painter->drawPixmap(QPoint(TargetX, TargetY), arrow);
#else

                    font = painter->font() ;
                    font.setPointSize(20);
                    font.setWeight(QFont::DemiBold);
                    painter->setFont(font);
                    txt1 = "Touch 'red' circle center";
                    painter->drawText(QPoint(TargetX + 3*circleDm, TargetY + 3*circleDm), txt1);
                    txt1 = "Touch : " + QString::number(rectTP.center().x()) + " , " + QString::number(rectTP.center().y());
                    painter->drawText(QPoint(TargetX + 3*circleDm, TargetY + 5*circleDm), txt1);
                    txt1 = "Target : ";
                    txt1 += QString::number(TargetX) + " , " + QString::number(TargetY);
                    painter->drawText(QPoint(TargetX + 3*circleDm, TargetY + 7*circleDm), txt1);
#endif
                    break;
                case LM:
                    if(m_TCount==(countMax*7/10+1))
                        painter->setPen(m_arrowLinePen);

                    TargetX = c_x;
                    TargetY = screenHeight/2;
                    painter->drawEllipse(TargetX - circleDm, TargetY - circleDm, circleDm*2, circleDm*2);

                    if(m_TCount==(countMax*7/10+1))
                    {
                        for(int j=0;j<1000;j++)
                           for(int i=0;i<1000;i++)
                           {}
                    }

                    painter->setPen(m_dashLinePen);
                    painter->drawLine(TargetX - circleDm, TargetY, TargetX + circleDm, TargetY);
                    painter->drawLine(TargetX, TargetY - circleDm, TargetX, TargetY + circleDm);
#ifdef MAXELL
                    arrow = QPixmap(PathManager::getFilePath("picture", "Left.png"));
                    painter->drawPixmap(QPoint(TargetX+arrow.width()/2, TargetY-arrow.height()/2), arrow);
#else
                    font=painter->font() ;
                    font.setPointSize(20);
                    font.setWeight(QFont::DemiBold);
                    painter->setFont(font);
                    txt1 = "Touch 'red' circle center";
                    painter->drawText(QPoint(TargetX + 3*circleDm, TargetY + 3*circleDm), txt1);
                    txt1 = "Touch : " + QString::number(rectTP.center().x())+" , " + QString::number(rectTP.center().y());
                    painter->drawText(QPoint(TargetX + 3*circleDm, TargetY + 5*circleDm), txt1);
                    txt1 = "Target : ";
                    txt1 += QString::number(TargetX) + " , " + QString::number(TargetY);
                    painter->drawText(QPoint(TargetX + 3*circleDm, TargetY + 7*circleDm), txt1);
#endif
                    break;
                case LB:
                    if(m_TCount==(countMax*6/10+1))
                        painter->setPen(m_arrowLinePen);

                    TargetX=c_x;
                    TargetY=screenHeight-c_y;
                    painter->drawEllipse(TargetX-circleDm,TargetY-circleDm,circleDm*2,circleDm*2);

                    if(m_TCount==(countMax*6/10+1))
                    {
                        for(int j=0;j<1000;j++)
                           for(int i=0;i<1000;i++)
                           {}
                    }

                    painter->setPen(m_dashLinePen);
                    painter->drawLine(TargetX-circleDm,TargetY,TargetX+circleDm,TargetY);
                    painter->drawLine(TargetX,TargetY-circleDm,TargetX,TargetY+circleDm);
#ifdef MAXELL
                    arrow = QPixmap(PathManager::getFilePath("picture", "LowerLeft.png"));
                    painter->drawPixmap(QPoint(TargetX, TargetY-arrow.height()), arrow);
#else

                    font=painter->font() ;
                    font.setPointSize ( 20 );
                    font.setWeight(QFont::DemiBold);
                    painter->setFont(font);
                    txt1="Touch 'red' circle center";
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY-7*circleDm),txt1);
                    txt1="Touch : "+QString::number(rectTP.center().x())+" , "+QString::number(rectTP.center().y());
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY-5*circleDm),txt1);
                    txt1="Target : ";
                    txt1+=QString::number(TargetX)+" , "+QString::number(TargetY);
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY-3*circleDm),txt1);
#endif
                    break;
                case MT:
                    if(m_TCount==(countMax*5/10+1))
                        painter->setPen(m_arrowLinePen);

                    TargetX=screenWidth/2;
                    TargetY=c_y;
                    painter->drawEllipse(TargetX-circleDm,TargetY-circleDm,circleDm*2,circleDm*2);

                    if(m_TCount==(countMax*5/10+1))
                    {
                        for(int j=0;j<1000;j++)
                           for(int i=0;i<1000;i++)
                           {}
                    }

                    painter->setPen(m_dashLinePen);
                    painter->drawLine(TargetX-circleDm,TargetY,TargetX+circleDm,TargetY);
                    painter->drawLine(TargetX,TargetY-circleDm,TargetX,TargetY+circleDm);
#ifdef MAXELL
                    arrow = QPixmap(PathManager::getFilePath("picture", "Up.png"));
                    painter->drawPixmap(QPoint(TargetX-arrow.width()/2, TargetY+arrow.height()/2), arrow);
#else
                    font=painter->font() ;
                    font.setPointSize ( 20 );
                    font.setWeight(QFont::DemiBold);
                    painter->setFont(font);
                    txt1="Touch 'red' circle center";
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY+3*circleDm),txt1);
                    txt1="Touch : "+QString::number(rectTP.center().x())+" , "+QString::number(rectTP.center().y());
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY+5*circleDm),txt1);
                    txt1="Target : ";
                    txt1+=QString::number(TargetX)+" , "+QString::number(TargetY);
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY+7*circleDm),txt1);
#endif
                    break;
                case MM:
                    if(m_TCount==(countMax*4/10+1))
                        painter->setPen(m_arrowLinePen);

                    TargetX=screenWidth/2;
                    TargetY=screenHeight/2;
                    painter->drawEllipse(TargetX-circleDm,TargetY-circleDm,circleDm*2,circleDm*2);

                    if(m_TCount==(countMax*4/10+1))
                    {
                        for(int j=0;j<1000;j++)
                           for(int i=0;i<1000;i++)
                           {}
                    }

                    painter->setPen(m_dashLinePen);
                    painter->drawLine(TargetX-circleDm,TargetY,TargetX+circleDm,TargetY);
                    painter->drawLine(TargetX,TargetY-circleDm,TargetX,TargetY+circleDm);
#ifdef MAXELL
                    arrow = QPixmap(PathManager::getFilePath("picture", "UpperLeft.png"));
                    painter->drawPixmap(QPoint(TargetX, TargetY), arrow);
#else
                    font=painter->font() ;
                    font.setPointSize ( 20 );
                    font.setWeight(QFont::DemiBold);
                    painter->setFont(font);
                    txt1="Touch 'red' circle center";
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY+3*circleDm),txt1);
                    txt1="Touch : "+QString::number(rectTP.center().x())+" , "+QString::number(rectTP.center().y());
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY+5*circleDm),txt1);
                    txt1="Target : ";
                    txt1+=QString::number(TargetX)+" , "+QString::number(TargetY);
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY+7*circleDm),txt1);
#endif
                    break;
                case MB:
                    if(m_TCount==countMax*3/10+1)
                        painter->setPen(m_arrowLinePen);

                    TargetX=screenWidth/2;
                    TargetY=screenHeight-c_y;
                    painter->drawEllipse(TargetX-circleDm,TargetY-circleDm,circleDm*2,circleDm*2);

                    if(m_TCount==countMax*3/10+1)
                    {
                        for(int j=0;j<1000;j++)
                           for(int i=0;i<1000;i++)
                           {}
                    }

                    painter->setPen(m_dashLinePen);
                    painter->drawLine(TargetX-circleDm,TargetY,TargetX+circleDm,TargetY);
                    painter->drawLine(TargetX,TargetY-circleDm,TargetX,TargetY+circleDm);
#ifdef MAXELL
                    arrow = QPixmap(PathManager::getFilePath("picture", "Down.png"));
                    painter->drawPixmap(QPoint(TargetX-arrow.width()/2, TargetY-3.0/2.0*arrow.height()), arrow);
#else
                    font=painter->font() ;
                    font.setPointSize ( 20 );
                    font.setWeight(QFont::DemiBold);
                    painter->setFont(font);
                    txt1="Touch 'red' circle center";
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY-7*circleDm),txt1);
                    txt1="Touch : "+QString::number(rectTP.center().x())+" , "+QString::number(rectTP.center().y());
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY-5*circleDm),txt1);
                    txt1="Target : ";
                    txt1+=QString::number(TargetX)+" , "+QString::number(TargetY);
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY-3*circleDm),txt1);
#endif
                    break;
                case RT:
                    if(m_TCount==(countMax*2/10+1))
                        painter->setPen(m_arrowLinePen);

                    TargetX=screenWidth-c_x;
                    TargetY=c_y;
                    painter->drawEllipse(TargetX-circleDm,TargetY-circleDm,circleDm*2,circleDm*2);

                    if(m_TCount==(countMax*2/10+1))
                    {
                        for(int j=0;j<1000;j++)
                           for(int i=0;i<1000;i++)
                           {}
                    }

                    painter->setPen(m_dashLinePen);
                    painter->drawLine(TargetX-circleDm,TargetY,TargetX+circleDm,TargetY);
                    painter->drawLine(TargetX,TargetY-circleDm,TargetX,TargetY+circleDm);
#ifdef MAXELL
                    arrow = QPixmap(PathManager::getFilePath("picture", "UpperRight.png"));
                    painter->drawPixmap(QPoint(TargetX-arrow.width(), TargetY), arrow);
#else
                    font=painter->font() ;
                    font.setPointSize ( 20 );
                    font.setWeight(QFont::DemiBold);
                    painter->setFont(font);
                    txt1="Touch 'red' circle center";
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY+3*circleDm),txt1);
                    txt1="Touch : "+QString::number(rectTP.center().x())+" , "+QString::number(rectTP.center().y());
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY+5*circleDm),txt1);
                    txt1="Target : ";
                    txt1+=QString::number(TargetX)+" , "+QString::number(TargetY);
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY+7*circleDm),txt1);
#endif
                    break;
                case RM:
                    if(m_TCount==(countMax/10+1))
                        painter->setPen(m_arrowLinePen);

                    TargetX=screenWidth-c_x;
                    TargetY=screenHeight/2;
                    painter->drawEllipse(TargetX-circleDm,TargetY-circleDm,circleDm*2,circleDm*2);

                    if(m_TCount==(countMax/10+1))
                    {
                        for(int j=0;j<1000;j++)
                           for(int i=0;i<1000;i++)
                           {}
                    }

                    painter->setPen(m_dashLinePen);
                    painter->drawLine(TargetX-circleDm,TargetY,TargetX+circleDm,TargetY);
                    painter->drawLine(TargetX,TargetY-circleDm,TargetX,TargetY+circleDm);
#ifdef MAXELL
                    arrow = QPixmap(PathManager::getFilePath("picture", "Right.png"));
                    painter->drawPixmap(QPoint(TargetX-3.0/2.0*arrow.width(), TargetY-arrow.height()/2), arrow);
#else
                    font=painter->font() ;
                    font.setPointSize ( 20 );
                    font.setWeight(QFont::DemiBold);
                    painter->setFont(font);
                    txt1="Touch 'red' circle center";
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY+3*circleDm),txt1);
                    txt1="Touch : "+QString::number(rectTP.center().x())+" , "+QString::number(rectTP.center().y());
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY+5*circleDm),txt1);
                    txt1="Target : ";
                    txt1+=QString::number(TargetX)+" , "+QString::number(TargetY);
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY+7*circleDm),txt1);
#endif
                    break;
                case RB:
                    if(m_TCount==(1))
                        painter->setPen(m_arrowLinePen);

                    TargetX=screenWidth-c_x;
                    TargetY=screenHeight-c_y;
                    painter->drawEllipse(TargetX-circleDm,TargetY-circleDm,circleDm*2,circleDm*2);

                    if(m_TCount==(1))
                    {
                        for(int j=0;j<1000;j++)
                           for(int i=0;i<1000;i++)
                           {}
                    }

                    painter->setPen(m_dashLinePen);
                    painter->drawLine(TargetX-circleDm,TargetY,TargetX+circleDm,TargetY);
                    painter->drawLine(TargetX,TargetY-circleDm,TargetX,TargetY+circleDm);
#ifdef MAXELL
                    arrow = QPixmap(PathManager::getFilePath("picture", "LowerRight.png"));
                    painter->drawPixmap(QPoint(TargetX-arrow.width(), TargetY-arrow.height()), arrow);
#else
                    font=painter->font() ;
                    font.setPointSize ( 20 );
                    font.setWeight(QFont::DemiBold);
                    painter->setFont(font);
                    txt1="Touch 'red' circle center";
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY-7*circleDm),txt1);
                    txt1="Touch : "+QString::number(rectTP.center().x())+" , "+QString::number(rectTP.center().y());
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY-5*circleDm),txt1);
                    txt1="Target : ";
                    txt1+=QString::number(TargetX)+" , "+QString::number(TargetY);
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY-3*circleDm),txt1);
#endif
                    break;
                case END:
                    break;
                default:
                    break;
                }
            }
            else if(PARA.getPointMode() == 2)
            {
                bool tooNearWithPreTouch = false;
                switch(m_TState)
                {
                case INIT:
                {                   
                    if(m_TCount<=countMax && m_TCount>(countMax*4/5))
                    //if(m_TCount<=countMax && m_TCount>(countMax*3/4))
                    {
                        m_file1.setFileName(PathManager::getFilePath("pattern/trainTargetX.txt"));
                        m_file1.open(QIODevice::WriteOnly | QIODevice::Text);

                        m_file2.setFileName(PathManager::getFilePath("pattern/trainTargetY.txt"));
                        m_file2.open(QIODevice::WriteOnly | QIODevice::Text);

                        m_file3.setFileName(PathManager::getFilePath("pattern/trainTouchX.txt"));
                        m_file3.open(QIODevice::WriteOnly | QIODevice::Text);

                        m_file4.setFileName(PathManager::getFilePath("pattern/trainTouchY.txt"));
                        m_file4.open(QIODevice::WriteOnly | QIODevice::Text);

                        painter->drawEllipse(screenWidth/2-circleDm,screenHeight/2-circleDm,circleDm*2,circleDm*2);
                        m_TState=MID;
                    }

                    if(m_TCount<=(countMax*4/5) && m_TCount>(countMax*3/5))
                    {
                        m_TState=LT;
                    }
                    else if(m_TCount<=(countMax*3/5) && m_TCount>(countMax*2/5))
                    {
                        m_TState=LB;
                    }
                    else if(m_TCount<=(countMax*2/5) && m_TCount>(countMax/5))
                    {
                        m_TState=RT;
                    }
                    else if(m_TCount<=(countMax/5) && m_TCount>0)
                    {
                        m_TState=RB;
                    }
                    break;
                }
                case MID:
                {
                    m_TDraw=MID;
                    if(touchState == QEvent::TouchEnd)
                    {
                        if(!(rectTP.center().x()==preCountx && rectTP.center().y()==preCounty))
                            m_TCount--;
                        preCountx=rectTP.center().x();
                        preCounty=rectTP.center().y();
                    }
                    m_TState=INIT;
                    break;
                }
                case LT:
                {
//                        qDebug()<<"LT:"<<rectTP.center().x()<<" , "<<rectTP.center().y();
                    m_TDraw=LT;
                    m_TState=STOP;

                    break;
                }
                case LB:
                {
                    m_TDraw=LB;
                    m_TState=STOP;
                    break;
                }
                case RT:
                {
                    m_TDraw=RT;
                    m_TState=STOP;
                    break;
                }
                case RB:
                {
                    m_TDraw=RB;
                    m_TState=STOP;
                    break;
                }
                case STOP:
                {
                    if(touchState == QEvent::TouchEnd)
                    {
                        if(TargetX>=0 && TargetX<=screenWidth && (rectTP.center().x()>(TargetX-correctRegionW)) && (rectTP.center().x()<(TargetX+correctRegionW))
                                && TargetY>=0 && TargetY<=screenHeight && (rectTP.center().y()>(TargetY-correctRegionH)) && (rectTP.center().y()<(TargetY+correctRegionH)))
                        {
                            if(!(rectTP.center().x()==preCountx && rectTP.center().y()==preCounty))
                            {
                                if( ( m_TCount < 4 ) && (
//                                        (rectTP.center().x()-preTouchTestX > 0 && rectTP.center().x()-preTouchTestX < screenWidth/8) ||
//                                        (rectTP.center().x()-preTouchTestX < 0 && preTouchTestX -rectTP.center().x() < screenWidth/8) ||
//                                        (rectTP.center().y()-preTouchTestY > 0 && rectTP.center().y()-preTouchTestY < screenHeight/8) ||
//                                        (rectTP.center().y()-preTouchTestY < 0 && preTouchTestY-rectTP.center().y() < screenHeight/8) )
                                        ( (rectTP.center().x()-preTouchTestX) * (rectTP.center().x()-preTouchTestX) +
                                        (rectTP.center().y()-preTouchTestY) * (rectTP.center().y()-preTouchTestY) ) <
                                        ( (screenHeight/10) * (screenHeight/10) ) )
                                        )
                                {
                                    tooNearWithPreTouch = true;
                                }

                                if(tooNearWithPreTouch == false){
#ifndef INNER_ALG
                                    context1=QString::number((int)(((TargetX)*100)/100.0));
                                    QTextStream out1(&m_file1);
                                    out1<<context1<<" ";

                                    context2=QString::number((int)(((TargetY)*100)/100.0));
                                    QTextStream out2(&m_file2);
                                    out2<<context2<<" ";

                                    context3=QString::number((int)((rectTP.center().x()*100)/100.0));
                                    QTextStream out3(&m_file3);
                                    out3<<context3<<" ";

                                    context4=QString::number((int)((rectTP.center().y()*100)/100.0));
                                    QTextStream out4(&m_file4);
                                    out4<<context4<<" ";
#else
                                    ALG.addTargetX((int)(((TargetX)*100)/100.0));
                                    ALG.addTargetY((int)(((TargetY)*100)/100.0));
                                    ALG.addTouchX((int)((rectTP.center().x()*100)/100.0));
                                    ALG.addTouchY((int)((rectTP.center().y()*100)/100.0));
#endif
                                    m_TCount--;
                                    preTouchTestX = rectTP.center().x();
                                    preTouchTestY = rectTP.center().y();
                                    qDebug() << "preTestX:" << preTouchTestX << " preTestY:" << preTouchTestY;
                                }
                            }
                            preCountx=rectTP.center().x();
                            preCounty=rectTP.center().y();

                        }

                        m_TState = INIT;

                        if(m_TCount==0)
                        {
                            m_TState = END;
                        }
/*
                            if(TargetX> 0 && TargetX<screenWidth-1
                               && TargetY> 0 && TargetY<screenHeight-1)
                            qDebug()<<"TarX : "<<TargetX;
                            qDebug()<<"TX : "<<(int)((rectTP.center().x()*100)/100.0);
                            if(TargetX> 0 && TargetX<screenWidth-1
                               && TargetY> 0 && TargetY<screenHeight-1)
                            qDebug()<<"TarY : "<<TargetY;
                            qDebug()<<"TY : "<<(int)((rectTP.center().y()*100)/100.0)<<endl;
*/
                    }

                    break;
                }
                case END:
                {
                    m_TDraw=END;
                    m_file1.close();
                    m_file2.close();
                    m_file3.close();
                    m_file4.close();
                    paintBackground::instance().flag=1;

                    break;
                }
                default:
                    break;
                }
#ifdef MAXELL
                QPixmap arrow;
//                if(tooNearWithPreTouch == true){
//                    QMessageBox msgBox;
//                    msgBox.setText("Too near with previous one !!");
//                    msgBox.exec();
//                }
#endif
                switch(m_TDraw)
                {
                case MID:
                    break;
                case LT:
                    if(m_TCount==(countMax*3/5+1))
                        painter->setPen(m_arrowLinePen);

                    TargetX=c_x;
                    TargetY=c_y;
                    painter->drawEllipse(TargetX-circleDm,TargetY-circleDm,circleDm*2,circleDm*2);

                    if(m_TCount==(countMax*3/5+1))
                    {
                        for(int j=0;j<1000;j++)
                           for(int i=0;i<1000;i++)
                           {}
                    }

                    painter->setPen(m_dashLinePen);
                    painter->drawLine(TargetX-circleDm,TargetY,TargetX+circleDm,TargetY);
                    painter->drawLine(TargetX,TargetY-circleDm,TargetX,TargetY+circleDm);
#ifdef MAXELL
                    arrow = QPixmap(PathManager::getFilePath("picture", "UpperLeft.png"));
                    painter->drawPixmap(QPoint(TargetX, TargetY), arrow);
#else
                    font=painter->font() ;
                    font.setPointSize ( 20 );
                    font.setWeight(QFont::DemiBold);
                    painter->setFont(font);
                    txt1="Touch 'red' circle center";
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY+3*circleDm),txt1);
                    txt1="Touch : "+QString::number(rectTP.center().x())+" , "+QString::number(rectTP.center().y());
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY+5*circleDm),txt1);
                    txt1="Target : ";
                    txt1+=QString::number(TargetX)+" , "+QString::number(TargetY);
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY+7*circleDm),txt1);
#endif
                    break;
                case LB:
                    if(m_TCount==(countMax*2/5+1))
                        painter->setPen(m_arrowLinePen);

                    TargetX=c_x;
                    TargetY=screenHeight-c_y;
                    painter->drawEllipse(TargetX-circleDm,TargetY-circleDm,circleDm*2,circleDm*2);

                    if(m_TCount==(countMax*2/5+1))
                    {
                        for(int j=0;j<1000;j++)
                           for(int i=0;i<1000;i++)
                           {}
                    }

                    painter->setPen(m_dashLinePen);
                    painter->drawLine(TargetX-circleDm,TargetY,TargetX+circleDm,TargetY);
                    painter->drawLine(TargetX,TargetY-circleDm,TargetX,TargetY+circleDm);
#ifdef MAXELL
                    arrow = QPixmap(PathManager::getFilePath("picture", "LowerLeft.png"));
                    painter->drawPixmap(QPoint(TargetX, TargetY-arrow.height()), arrow);
#else
                    font=painter->font() ;
                    font.setPointSize ( 20 );
                    font.setWeight(QFont::DemiBold);
                    painter->setFont(font);
                    txt1="Touch 'red' circle center";
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY-7*circleDm),txt1);
                    txt1="Touch : "+QString::number(rectTP.center().x())+" , "+QString::number(rectTP.center().y());
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY-5*circleDm),txt1);
                    txt1="Target : ";
                    txt1+=QString::number(TargetX)+" , "+QString::number(TargetY);
                    painter->drawText(QPoint(TargetX+3*circleDm, TargetY-3*circleDm),txt1);
#endif
                    break;
                case RT:
                    if(m_TCount==(countMax/5+1))
                        painter->setPen(m_arrowLinePen);

                    TargetX=screenWidth-c_x;
                    TargetY=c_y;
                    painter->drawEllipse(TargetX-circleDm,TargetY-circleDm,circleDm*2,circleDm*2);

                    if(m_TCount==(countMax/5+1))
                    {
                        for(int j=0;j<1000;j++)
                           for(int i=0;i<1000;i++)
                           {}
                    }

                    painter->setPen(m_dashLinePen);
                    painter->drawLine(TargetX-circleDm,TargetY,TargetX+circleDm,TargetY);
                    painter->drawLine(TargetX,TargetY-circleDm,TargetX,TargetY+circleDm);
#ifdef MAXELL
                    arrow = QPixmap(PathManager::getFilePath("picture", "UpperRight.png"));
                    painter->drawPixmap(QPoint(TargetX-arrow.width(), TargetY), arrow);
#else
                    font=painter->font() ;
                    font.setPointSize ( 20 );
                    font.setWeight(QFont::DemiBold);
                    painter->setFont(font);                   
                    txt1="Touch 'red' circle center";
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY+3*circleDm),txt1);
                    txt1="Touch : "+QString::number(rectTP.center().x())+" , "+QString::number(rectTP.center().y());
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY+5*circleDm),txt1);
                    txt1="Target : ";
                    txt1+=QString::number(TargetX)+" , "+QString::number(TargetY);
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY+7*circleDm),txt1);
#endif
                    break;
                case RB:
                    if(m_TCount==(1))
                        painter->setPen(m_arrowLinePen);

                    TargetX=screenWidth-c_x;
                    TargetY=screenHeight-c_y;
                    painter->drawEllipse(TargetX-circleDm,TargetY-circleDm,circleDm*2,circleDm*2);

                    if(m_TCount==(1))
                    {
                        for(int j=0;j<1000;j++)
                           for(int i=0;i<1000;i++)
                           {}
                    }

                    painter->setPen(m_dashLinePen);
                    painter->drawLine(TargetX-circleDm,TargetY,TargetX+circleDm,TargetY);
                    painter->drawLine(TargetX,TargetY-circleDm,TargetX,TargetY+circleDm);
#ifdef MAXELL
                    arrow = QPixmap(PathManager::getFilePath("picture", "LowerRight.png"));
                    painter->drawPixmap(QPoint(TargetX-arrow.width(), TargetY-arrow.height()), arrow);
#else
                    font=painter->font() ;
                    font.setPointSize ( 20 );
                    font.setWeight(QFont::DemiBold);
                    painter->setFont(font);                    
                    txt1="Touch 'red' circle center";
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY-7*circleDm),txt1);
                    txt1="Touch : "+QString::number(rectTP.center().x())+" , "+QString::number(rectTP.center().y());
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY-5*circleDm),txt1);
                    txt1="Target : ";
                    txt1+=QString::number(TargetX)+" , "+QString::number(TargetY);
                    painter->drawText(QPoint(TargetX-19*circleDm, TargetY-3*circleDm),txt1);
#endif
                    break;
                case END:
                    break;
                default:
                    break;
                }
            }

        }
    }

//        qDebug()<<"m_TCount="<<m_TCount;
    font=painter->font() ;
    font.setPointSize ( 120 );
    font.setWeight(QFont::DemiBold);
    painter->setFont(font);
    if(PARA.getPointMode() == 1)
    {
        if(m_TCount!=(countMax*9/10))
        {
#ifndef MAXELL
            painter->drawText(QPoint(6*screenWidth/13, 6*screenWidth/30),QString::number(m_TCount));
#endif            
        }
    }
    else if(PARA.getPointMode() == 2)
    {
        if(m_TCount!=(countMax*4/5))
        {
#ifndef MAXELL
            painter->drawText(QPoint(6*screenWidth/13, 6*screenWidth/30),QString::number(m_TCount));
#endif
        }
    }

    painter->restore();
}

