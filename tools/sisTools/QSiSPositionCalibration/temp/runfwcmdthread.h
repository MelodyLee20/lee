#ifndef RUNFWCMDTHREAD_H
#define RUNFWCMDTHREAD_H

#include"dialog.h"
#include<QThread>
#include<QObject>

class RunFwCmdThread : public QThread
{
Q_OBJECT

public:
    RunFwCmdThread();
    //RunFwCmdThread(dialog* dia);
    ~RunFwCmdThread();

protected:
    void run();

signals:
    void processStart();
    void processEnd();

private:
    //dialog* m_dia;
};

#endif // RUNFWCMDTHREAD_H
