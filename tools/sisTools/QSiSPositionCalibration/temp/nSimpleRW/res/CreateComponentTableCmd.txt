CREATE TABLE `CP_CRC` (
	`CommandID`	TEXT NOT NULL,
	`Interface`	TEXT NOT NULL,
	`OS`	TEXT NOT NULL,
	`StartAddr`	INTEGER NOT NULL,
	`ComponentLength`	INTEGER NOT NULL,
	`OccupierLevel`	INTEGER NOT NULL,
	`DefaultValue`	INTEGER NOT NULL,
	`Func`	TEXT NOT NULL,
	PRIMARY KEY(CommandID,Interface,OS)
);