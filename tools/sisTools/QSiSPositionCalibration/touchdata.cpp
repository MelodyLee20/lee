#include "touchdata.h"

TouchData::TouchData()
{

}

TouchData::~TouchData()
{

}

void TouchData::handleTouchEvent(QEvent *event)
{
    QTouchEvent* touchEvent=static_cast<QTouchEvent*>(event);

    switch(event->type())
    {
        case QEvent::TouchBegin:
            break;
        case QEvent::TouchUpdate:
            break;
        case QEvent::TouchEnd:
            break;
    }
    setCTPs(touchEvent->touchPoints());
    setTouchState(event->type());
}

void TouchData::handleMouseEvent(QEvent *event)
{
    QMouseEvent* mouseEvent=static_cast<QMouseEvent*>(event);


    QEvent::Type eType = QEvent::TouchBegin;
    if(event->type() == QEvent::MouseMove)
        eType = QEvent::TouchUpdate;
    else if(event->type() == QEvent::MouseButtonRelease)
        eType = QEvent::TouchEnd;

    QTouchEvent::TouchPoint touchPoint;
    touchPoint.setId(0);
    touchPoint.setRect(QRectF(mouseEvent->x(),mouseEvent->y(),1,1));
    QList<QTouchEvent::TouchPoint> touchPointList;
    touchPointList.append(touchPoint);
    setCTPs(touchPointList);
    setTouchState(eType);
}

void TouchData::setCTPs(const QList<QTouchEvent::TouchPoint> &CTPs)
{
    m_CTPs = CTPs;
}

void TouchData::setTouchState(QEvent::Type type)
{
    m_touchState = type;
}

QList<QTouchEvent::TouchPoint> TouchData::getCTPs() const
{
    return m_CTPs;
}

QEvent::Type TouchData::getTouchState()
{
    return m_touchState;
}

void TouchData::clearTP()
{
    m_CTPs.clear();
}
