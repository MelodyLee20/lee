#include "paintobjectbuilder.h"
#include "config.h"
#include "layoutbuilder.h"
#include "paintbackground.h"
#include <QDebug>
#include "math.h"

paintObjectBuilder::paintObjectBuilder()
{

}

paintObjectBuilder::~paintObjectBuilder()
{

}

QList<IpaintTable*> paintObjectBuilder::buildPaintObject(int functionID)
{
    switch(functionID)
    {
    case FUNCTION_OUTSIDE:
        return buildOutside();
        break;
    default:
        return QList<IpaintTable*>();
        break;
    }
}

QList<IpaintTable*> paintObjectBuilder::buildOutside()
{
    QList<LineData*> datalist=layoutBuilder::buildLine();
    QList<IpaintTable*> list;
    for(int i=0; i<datalist.size() ; ++i)
    {
        paintBackground* line = new paintBackground();
        line->setPosition(datalist[i]->x,datalist[i]->y,datalist[i]->endX,datalist[i]->endY);
        line->setDir(datalist[i]->direct);
        line->setID(datalist[i]->id);
        line->setLineDx(datalist[i]->getDx());
        line->setDashLine(datalist[i]->Diameter);
        line->setSpeed(floor((double)datalist[i]->Speed/100));
        list.append(line);
    }

    QList<CircleData*> datalistC=layoutBuilder::buildCircle_outside();
    for(int i=0; i<datalistC.size() ; ++i)
    {
        paintBackground* circle = new paintBackground();
        circle->setCirclePosition(datalistC[i]->centerX,datalistC[i]->centerY);
        circle->setID(datalistC[i]->id);
        circle->setLineDx(datalistC[i]->getDx());
        circle->setSolidLine(datalistC[i]->diameter);
        list.append(circle);
    }

    QList<RectData*> datalistR=layoutBuilder::buildRect();
    for(int i=0; i<datalistR.size() ; ++i)
    {
        paintBackground* rect = new paintBackground();
        rect->setRectPosition(datalistR[i]->PosX,datalistR[i]->PosY);
        list.append(rect);
    }

    return list;
}
