#include "touchdatadrawing.h"
#include <QTouchEvent>
#include "touchdata.h"
#include <QPainter>
#include <QVector>
#include <QPaintEvent>
#include <QDebug>

TouchDataDrawing::TouchDataDrawing()
{
    preRect.resize(40);
    m_width=10;
    TPcolors
            <<QColor("black")
            <<QColor("red")
            <<QColor("green")
            <<QColor("blue")
            <<QColor("yellow")
            <<QColor("pink")
            <<QColor("orange")
            <<QColor("grey")
            <<QColor("brown")
            <<QColor("purple");
}

TouchDataDrawing::~TouchDataDrawing()
{

}

void TouchDataDrawing::paint(QPainter *painter, QPaintEvent *event, int elapsed)
{
    drawTPs(painter,event);
}

void TouchDataDrawing::drawTPs(QPainter *painter, QPaintEvent *event)
{
    QList<QTouchEvent::TouchPoint> drawTouchPoints=TouchData::instance().getCTPs();

    if(!drawTouchPoints.empty())
    {
        foreach(const QTouchEvent::TouchPoint &touchPoint , drawTouchPoints)
        {
            switch(touchPoint.state())
            {
            case Qt::TouchPointStationary:
                continue;
            case Qt::TouchPointReleased:
            {
                preRect[touchPoint.id()].setX(0);
                preRect[touchPoint.id()].setY(0);
                break;
            }
            case Qt::TouchPointPressed:
            {
                preRect[touchPoint.id()].setX(0);
                preRect[touchPoint.id()].setY(0);
            }
            default:
                QRectF rect=touchPoint.rect();

                if(rect.isEmpty())
                {
                    qreal diameter=qreal(m_width)*touchPoint.pressure();
                    rect.setSize(QSizeF(diameter,diameter));
                }

                painter->setPen(QPen(TPcolors.at(touchPoint.id()%TPcolors.count()),m_width,Qt::DashDotLine,Qt::RoundCap));
                if(preRect[touchPoint.id()].x()==0 && preRect[touchPoint.id()].y()==0)
                {
                    painter->drawPoint(rect.center().x()-m_width/2,rect.center().y()-m_width/2);
                }
                else
                {
                    if(abs(rect.center().x()-m_width/2-preRect[touchPoint.id()].x())<300 && abs(rect.center().y()-m_width/2-preRect[touchPoint.id()].y())<300)
                    {
                        double xUnit=(rect.center().x()-m_width/2-preRect[touchPoint.id()].x())/100;
                        double yUnit=(rect.center().y()-m_width/2-preRect[touchPoint.id()].y())/100;

                        for(int i=0;i<100;++i)
                        {
                            painter->drawPoint(preRect[touchPoint.id()].x()+xUnit*i,preRect[touchPoint.id()].y()+yUnit*i);
                        }
                    }
                    else
                    {
                        painter->drawPoint(rect.center().x()-m_width/2,rect.center().y()-m_width/2);
                    }
                }

                preRect[touchPoint.id()].setX(rect.center().x()-m_width/2);
                preRect[touchPoint.id()].setY(rect.center().y()-m_width/2);
                break;
            }
        }
    }
}
