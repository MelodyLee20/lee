#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <math.h>
#include<fstream>
#include<vector>
using namespace std;

#define MAXN 10
#define RESOLUTION_FW 4095
#define AXIS 2
#define MODE 2

// CoordCali MODE:
// 1: Average for same target x-coord or y-coord   ,  this mode can't cali trapzoid or nonSquare
// 2: x'=(ax+by+c)/W & Y'=(dx+ey+f)/W  , W = (R1x+R2y+1) use min-square-err

#define debug_4point
//#define Print_coef

typedef struct{
    int datanum;
    double *OSscreen_DEF;
    unsigned int *OStargetX;
    unsigned int *OStargetY;
    unsigned int *OStouchX;
    unsigned int *OStouchY;
}s_InputData;

typedef struct{
    //X' = (Ax)X + (Bx)Y + (Dx)
    //Y' = (Ay)X + (By)Y + (Dy)
    //X' & Y' are new Coord
    double Ax;
    double Bx;
    double Dx;

    double Ay;
    double By;
    double Dy;

    double R1;
    double R2;

}s_OutputData;

class algorithm
{
public:
    algorithm();

    static algorithm& instance()
    {
        static algorithm instance;
        return instance;
    }

    int data_err;

    void matrixPrint(double* ,int,int);
    void transP(double* , double*,int,int);
    void matM(double*,double*,double* ,int,int,int);
    void Reg(double*,double*,double*,int,int);
    int MatInv(double* ,double* ,int );

    int CoordCali(s_InputData*,s_OutputData*,int); //Find Ax,	Bx, Dx,  Ay,  By, Dy
    // CoordCali mode:
    // 1: Average for same target x-coord or y-coord   ,  this mode can't cali trapzoid or nonSquare

    void CoordCaliAdj(s_InputData*);

    int run(int mode);

    double result[9];

    ~algorithm();

    vector<unsigned int> m_targetX;
    vector<unsigned int> m_targetY;
    vector<unsigned int> m_touchX;
    vector<unsigned int> m_touchY;

    void addTargetX(unsigned int x);
    void addTargetY(unsigned int y);
    void addTouchX(unsigned int x);
    void addTouchY(unsigned int y);

    int getBetaNum();
};

#define ALG_OLD algorithm::instance()

#endif // ALGORITHM_H
