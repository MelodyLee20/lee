#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H

#include <QProgressDialog>
#include <QTimer>

class ProgressBar : public QProgressDialog {
    Q_OBJECT

public:
    ProgressBar(QWidget *parent=0);

    ~ProgressBar();

    QTimer *timer;

public slots:
    void stepOne();

};

#endif // PROGRESSBAR_H
