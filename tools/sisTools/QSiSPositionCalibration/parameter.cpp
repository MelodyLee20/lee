#include "parameter.h"
#include <QApplication>
#include <QSettings>
#include <QDebug>
#include <QMessageBox>
#include "math.h"

parameter::parameter()
{

}

parameter::~parameter()
{

}

void parameter::initial()
{
    QSize desktopSize=QApplication::desktop()->size();
    setScreenH(desktopSize.height());
    setScreenW(desktopSize.width());

    qDebug()<<("screenH")<<m_screenH;
    qDebug()<<("screenW")<<m_screenW;
    initialD();
}

void parameter::initialD()
{
   m_LCDH = 737;
   m_LCDW = 478;

   m_pointDiameter = 3;
   m_pointTolerance = 0;

   m_lineDiameter = 0;
   m_lineTolerance = 0;
   m_lineSpeed = 100;
   m_lineLength = 50;

   m_timeout = 30;
   m_ngTolerance = 3;

   isInvX = 0;
   isInvY = 0;
   isSwap = 1;

   pointMode = 0;

   TxM = 1;
   TyM = 1;

   m_isCalWithDpi = true;

   QSettings settings(this->getININame(),QSettings::IniFormat);
//   QMessageBox msgBox;
//   msgBox.setText(this->getININame());
//   msgBox.exec();
   settings.beginGroup("TargetItem");
   TxD = settings.value("TARGET_X").toInt();
   TyD = settings.value("TARGET_Y").toInt();
   settings.endGroup();
   settings.beginGroup("COAItem");
   isSwap = settings.value("swap").toInt();
   isInvX = settings.value("invx").toInt();
   isInvY = settings.value("invy").toInt();
   settings.endGroup();
   settings.beginGroup("TargetPoint");
   pointMode = settings.value("mode").toInt();
   settings.endGroup();
}

double parameter::getScreenH() const
{
    return m_screenH;
}

void parameter::setScreenH(int value)
{
    m_screenH = value;
}

double parameter::getScreenW() const
{
    return m_screenW;
}

void parameter::setScreenW(int value)
{
    m_screenW = value;
}

QString parameter::getININame()
{
#ifdef ANDROID
    QString path = "/data/data/com.sis.QPJT/assets/" + QString(INI_NAME);
#else
    QString path = QApplication::applicationDirPath();
    path += QString("\\") + QString(INI_NAME);
#endif
    return path;
}

double parameter::convertMMtoPixel(int value)
{
    double pixel = -1;
    pixel = value*(m_screenW/m_LCDW);
    return floor(pixel);
}

int parameter::lineLength() const
{
    return m_lineLength;
}

int parameter::lineTolerance() const
{
    return m_lineTolerance;
}

int parameter::getNGTolerance() const
{
    return m_ngTolerance;
}

int parameter::pointDiameter() const
{
    return m_pointDiameter;
}

int parameter::pointTolerance() const
{
    return m_pointTolerance;
}

int parameter::getTimeout() const
{
    return m_ngTolerance;
}

int parameter::paraInvx()
{
    return isInvX;
}

int parameter::paraInvy()
{
    return isInvY;
}

int parameter::paraSwap()
{
    return isSwap;
}

volatile int parameter::paraTxM()
{
    return TxM;
}

volatile int parameter::paraTxD()
{
    return TxD;
}

volatile int parameter::paraTyM()
{
    return TyM;
}

volatile int parameter::paraTyD()
{
    return TyD;
}

void parameter::setInvx(int txtInvx)
{
    isInvX = txtInvx;
}

void parameter::setInvy(int txtInvy)
{
    isInvY = txtInvy;
}

void parameter::setSwap(int txtSwap)
{
    isSwap = txtSwap;
}

void parameter::setCount(int txtCount)
{
    Count = txtCount;
}

void parameter::setTxM(int txtTxM)
{
    TxM = txtTxM;
}

void parameter::setTxD(int txtTxD)
{
    TxD = txtTxD;
}

void parameter::setTyM(int txtTyM)
{
    TyM = txtTyM;
}

void parameter::setTyD(int txtTyD)
{
    TyD = txtTyD;
}

int parameter::getPointMode()
{
    return pointMode;
}
