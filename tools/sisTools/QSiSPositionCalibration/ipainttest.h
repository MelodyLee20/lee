#ifndef IPAINTTEST_H
#define IPAINTTEST_H

#include "ipainttable.h"

class IpaintTest:public IpaintTable
{
public:
    IpaintTest();
    virtual ~IpaintTest();

    int m_functionID;
    QList<IpaintTable*> m_paintObject;


    void setfunctionID(int functionID);
    void setpaintObject(QList<IpaintTable*> paintOject);

    virtual void paint(QPainter *painter, QPaintEvent *event)=0;
};

#endif // IPAINTTEST_H
