#include "updatebar.h"

UpdateBar::UpdateBar()
{
    progressBar = new ProgressBar();
    progressBar->resize(250, 20);

    progressBar->setMaximum(100);
    progressBar->setMinimum(0);
    progressBar->setValue(0);

    timer = new QTimer;
    timer->start(500);

    //QThread* timerThread = new QThread;
    //timer->moveToThread(timerThread);
    //connect(timer, SIGNAL(timeout()), progressBar, SLOT(stepOne()), Qt::DirectConnection);
    //timerThread->start();

    connect(timer, SIGNAL(timeout()), progressBar, SLOT(stepOne()));
    progressBar->show();
}

UpdateBar::~UpdateBar()
{
    delete progressBar;
}

void UpdateBar::run()
{
    QThread::msleep(500);
}

void UpdateBar::setMessage(QString str)
{
    progressBar->setWindowTitle(str);
}

void UpdateBar::setWindowSize(int h, int w)
{
    progressBar->resize(h, w);
}

