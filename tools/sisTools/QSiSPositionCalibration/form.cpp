#include "form.h"
#include "config.h"
#include <QCoreApplication>
#include "siscorewrapper.h"

form::form()
{
    connect(&m_dia,SIGNAL(functionChoice(int)),this,SLOT(functionChoiced(int)));
    connect(&m_mainwin,SIGNAL(backMenu()),this,SLOT(backMenu()));
    connect(&m_mainwin,SIGNAL(autoUpdate()),this,SLOT(autoUpdate()));

    char strFile[512];
    FILE* fp = fopen("path.log", "wb");
    fprintf(fp, "App path: %s\n", QCoreApplication::applicationDirPath().toStdString().c_str());
    fclose(fp);
}

form::~form()
{

}

void form::showMenu()
{
    int id = FUNCTION_OUTSIDE;
//    m_dia.show();
    sis_sleep(DELAY*6);
    m_dia.InitialP();
    m_mainwin.functionChoiced(id);
//    m_dia.hide();
    m_mainwin.show();
}

void form::functionChoiced(int id)
{
    m_mainwin.functionChoiced(id);
    m_dia.hide();
    m_mainwin.show();
}

void form::autoUpdate()
{
    m_mainwin.hide();
    m_dia.show();
    m_dia.exitUpdateFW();
    m_dia.showLeave();
}

void form::backMenu()
{
    m_mainwin.hide();
    m_dia.show();
}
