#include "siscorewrapper.h"
#ifdef ANDROID
    #include <OperationFormatReader.h>
    #include "simplerw.h"
    #include "mshell/mshell.h"
#endif
#include <QDebug>
#include <QMessageBox>
#include <QProcess>
#include "pathmanager.h"

SiSCoreWrapper::SiSCoreWrapper() :
    m_core(NULL)
{
    m_core = NULL;
}

SiSCoreWrapper::~SiSCoreWrapper()
{
    closeDevice();
}

SiSCoreWrapper& SiSCoreWrapper::instance()
{
    static SiSCoreWrapper wrapper;
    return wrapper;
}

SiSCore *SiSCoreWrapper::openDevice()
{
    try{
        if(m_core != NULL)
            closeDevice();

        m_core = new SiSCore();
        qDebug() << "SiSCoreWrapper::openDevice() new SiSCore(), m_core: " << m_core;

        SSetting setting;

#ifdef ANDROID
        OperationFormatReader reader1;
        //if(false == reader1.init("/data/data/com.sis.QPJT/assets/res/Command.db"))
        if(false == reader1.init(PathManager::getFilePath("res/Command.db").toStdString()))
        {
            QMessageBox msgBox;
            msgBox.setText("ProxyOpenDevice::run(Path Fail!)");
            msgBox.exec();
        }

        //setting[PROTOCOL] = HID_RAW;

        //QMessageBox msgBox;
        //msgBox.setText("SiSCoreWrapper::openDevice() m_core->boot(setting, &reader1)");
        //msgBox.exec();

        qDebug() << "SiSCoreWrapper::openDevice() m_core->boot(setting, &reader1)";
        m_core->boot(setting, &reader1);
#else
        m_core->boot(setting);
#endif

        m_fwGeneration = m_core->attribute(DEV_TYPE);
        qDebug() << "SiSCoreWrapper::openDevice() m_core->attribute(DEV_TYPE),  m_fwGeneration: " << m_fwGeneration.c_str();

        return m_core;

    }
    catch(SiSCmdModuleException & e )
    {
        //FAIL() << "Command Module exception : " << e.what();
        qDebug() << "Command Module exception : " << e.what();
        QMessageBox msgBox;
        msgBox.setText(e.what());
        msgBox.exec();
        return NULL;
    }
    catch(SiSCoreException & ce )
    {
        //FAIL() << "Core exception : " << ce.what();
        qDebug() << "Core exception : " << ce.what();
        QMessageBox msgBox;
        msgBox.setText(ce.what());
        msgBox.exec();
        return NULL;
    }
    catch(...)
    {
        //FAIL() << "other exception";
        qDebug() << "Other exception";
        QMessageBox msgBox;
        msgBox.setText("Other exception");
        msgBox.exec();
        return NULL;
    }
}

void SiSCoreWrapper::closeDevice()
{
    if(m_core != 0){
        qDebug() << "SiSCoreWrapper::closeDevice() delete m_core : " << m_core;
        delete m_core;
        m_core = 0;
    }
}

bool SiSCoreWrapper::reportToOS(bool enable)
{
    try{
        bool ret = true;
        if(!enable){
            ret = m_core->exe<int>("CTL_CHANGE_MODE", MODE(m_core, Hydra::ENABLE_DIAGNOSIS));
            sis_sleep(500);
        }
        else{
            ret = m_core->exe<int>("CTL_CHANGE_MODE", MODE(m_core, Hydra::DISABLE_DIAGNOSIS));
        }
        return ret;
    }
    catch(SiSCmdModuleException & e )
    {
        //FAIL() << "Command Module exception : " << e.what();
        qDebug() << "Command Module exception : " << e.what();
        return false;
    }
    catch(SiSCoreException & ce )
    {
        //FAIL() << "Core exception : " << ce.what();
        qDebug() << "Core exception : " << ce.what();
        return false;
    }
    catch(...)
    {
        //FAIL() << "Other exception";
        qDebug() << "Other exception";
        return false;
    }
}

bool SiSCoreWrapper::cmd88Write(unsigned int addr, BYTE* writeData)
{
    try{
        int len = 4;
        BYTE* readData = new BYTE[len];
        memset(readData, 0, len);
        Component * result = SERIAL_DATA(m_core, len);
        Component * cWriteData = new Component("SerialData", writeData, len);

        m_core->exe<int>(
            "READ_WRITE",
            ADDRESS(m_core, addr),
            cWriteData,
            DATA_MASK(m_core, 0, 0, 0, 0)
        );

        delete cWriteData;

        return true;
    }
    catch(SiSCmdModuleException & e )
    {
        //FAIL() << "Command Module exception : " << e.what();
        qDebug() << "Command Module exception : " << e.what();
        return false;
    }
    catch(SiSCoreException & ce )
    {
        //FAIL() << "Core exception : " << ce.what();
        qDebug() << "Core exception : " << ce.what();
        return false;
    }
    catch(...)
    {
        //FAIL() << "Other exception";
        qDebug() << "Other exception";
        return false;
    }
}

bool SiSCoreWrapper::cmd86Read(unsigned int addr, unsigned char* readData, unsigned int readLength)
{
    try{
        Component* result = SERIAL_DATA(m_core, readLength);

        bool ret = m_core->exe<int>(
            "BURST_READ",
            ADDRESS(m_core, addr),
            READ_LEN(m_core, readLength),
            result);

        for(int i = 0; i < readLength; i++)
            readData[i] = result->data(i);

        //memcpy (readData, result->data(), readLength);

        delete result;

        return ret;
    }
    catch(SiSCmdModuleException & e )
    {
        //FAIL() << "Command Module exception : " << e.what();
        qDebug() << "Command Module exception : " << e.what();
        return false;
    }
    catch(SiSCoreException & ce )
    {
        //FAIL() << "Core exception : " << ce.what();
        qDebug() << "Core exception : " << ce.what();
        return false;
    }
    catch(...)
    {
        //FAIL() << "Other exception";
        qDebug() << "Other exception";
        return false;
    }
}

bool SiSCoreWrapper::simpleRW88Write(unsigned char* reportBuffer, unsigned char size)
{

#ifdef ANDROID
//    char cmdFileName[128] = "cmd88WriteInput.txt";
//    char outputFileName[128] = "cmd88WriteOutput.txt";

//    FILE* fp = fopen(cmdFileName, "wb");

//    for(int i = 0; i < size; i++)
//        fprintf(fp, "%02x ", reportBuffer[i]);
//    fclose(fp);

//    char* argv[5]; //= {"SimpleRW\\SimpleRW", "-f", "", "-o", ""};
//    for(int i = 0; i < 5; i++){
//        argv[i] = new char[256];
//    }
//    strcpy(argv[0], PathManager::getFilePath("SimpleRW\\SimpleRW").toStdString().c_str());
//    strcpy(argv[1], "-f");
//    strcpy(argv[2], PathManager::getFilePath(cmdFileName).toStdString().c_str());
//    strcpy(argv[3], "-o");
//    strcpy(argv[4], PathManager::getFilePath(outputFileName).toStdString().c_str());

//    SimpleRW::run(5, argv, m_core);

    char** argv;
    argv = new char*[size+1];
    argv[0] = new char[256];
    strcpy(argv[0], PathManager::getFilePath("SimpleRW\\SimpleRW").toStdString().c_str());

//    memset(reportBuffer, 0, size);
//    reportBuffer[0] = 0x09;
//    reportBuffer[1] = 0x05;
//    reportBuffer[2] = 0x85;
//    reportBuffer[3] = 0x00;
//    reportBuffer[4] = 0x51;
//    reportBuffer[5] = 0x09;

    QString cmdStr = "";

    for(int i = 1; i < size+1; i++){
        argv[i] = new char[256];
        sprintf(argv[i], "%02x", reportBuffer[i-1]);
        cmdStr += QString("%1 ").arg(argv[i]);
    }

    QMessageBox msgBox;
    msgBox.setText(QString("SimpleRW::run %1 ").arg(cmdStr));
    //msgBox.exec();

    SimpleRW::run(size+1, argv, m_core);

    sis_sleep(1000);

//    MShell* shell = new MShell();

//    /* settings */
//    shell->setTimeout( 5000 ); // ms
//    shell->setWorkingDirectory( PathManager::getFilePath("nSimpleRW") );
//    shell->setExecFileName( PathManager::getFilePath("nSimpleRW/nSimpleRW") );

//    /* args */
//    QStringList args;
//    for(int i = 0; i < size; i++){
//        args.append(QString(argv[i+1]));
//    }
//    shell->setExecArgs( args );

//    QMessageBox msgBox;
//    QString cmd = "";
//    for(int i = 0; i < size; i++){
//        cmd = cmd + args[i] + " ";
//    }
//    //msgBox.setText(QString("MShell::startExec %1").arg(cmd));
//    //msgBox.exec();

//    /* exec */
//    shell->startExec();

//    /* result */
//    QString preExecInformation = shell->getPreExecInformation();
//    int exitCode = shell->getExitCode();
//    QString standardOutput = shell->getStandardOutput();

#else

    char cmdFileName[128] = "cmd88WriteInput.txt";
    char outputFileName[128] = "cmd88WriteOutput.txt";

    FILE* fp = fopen(cmdFileName, "wb");

    for(int i = 0; i < size; i++)
        fprintf(fp, "%02x ", reportBuffer[i]);
    fclose(fp);

    QString cmdStr = QString("SimpleRW\\cmd.exe");
    if(m_fwGeneration == "819")
        cmdStr = QString("nSimpleRW\\cmd.exe");

//    QString commandStr = QString("SimpleRW\\SimpleRW.exe -f %1 -o %2").arg(cmdFileName).arg(outputFileName);
//    if(m_fwGeneration == "819")
//        commandStr = QString("nSimpleRW\\nSimpleRW.exe -f %1 -o %2").arg(cmdFileName).arg(outputFileName);

    QString commandStr = QString("SimpleRW\\SimpleRW.exe");
    if(m_fwGeneration == "819")
        commandStr = QString("nSimpleRW\\nSimpleRW.exe");

    QStringList argumentStrList;
    //argumentStrList << QString("\"SimpleRW\\SimpleRW.exe -f %1 -o %2 > SimpleRW.log\"").arg(cmdFileName).arg(outputFileName);
    argumentStrList << "-f" << cmdFileName << "-o" << outputFileName;

    qDebug() << "Command is " << commandStr;

    QProcess *process = new QProcess();
    process->setProgram(commandStr);
    process->setArguments(argumentStrList);
    process->start();//Detached(commandStr);//, argumentStrList);

    sis_sleep(1000);

#endif
    return true;
}

bool SiSCoreWrapper::updateFw(unsigned int addr, BYTE* _updateData, int len)
{
    Component* updateData = new Component("SerialData", _updateData, 0x20000);

    try
    {
//        m_core->exe<int>(
//            "CTL_CHANGE_MODE",
//            MODE(m_core, Hydra::ENABLE_DIAGNOSIS)
//        );

//        m_core->exe<int>(
//            "CTL_CHANGE_MODE_S",
//            MODE(m_core, Hydra::BOOTLOADER),
//            CHIP(m_core, 0x11)
//        );
        qDebug() << "SiSCoreWrapper::updateFw() m_core->exe<int>(\"WRITE\", ...)";
        bool ret = m_core->exe<int>(
            "WRITE",
            ADDRESS(m_core, addr),
            WRITE_LEN(m_core, len),
            updateData
        );
        closeDevice();

        /* wait device */
        qDebug() << "SiSCoreWrapper::updateFw() sis_sleep(10000) ing...";
        sis_sleep(10000);

        if(!openDevice())
            ret = false;

        return ret;
    }
    catch(SiSCmdModuleException & e )
    {
        qDebug() << "Command Module exception : " << e.what();
        return false;
    }
    catch(SiSCoreException & ce )
    {
        qDebug() << "Core exception : " << ce.what();
        return false;
    }
    catch(std::invalid_argument const & ia)
    {
        qDebug() << "system exception : " << ia.what();
        return false;
    }
    catch(std::range_error const & re)
    {
        qDebug() << "system exception : " << re.what();
        return false;
    }
    catch(...)
    {
        qDebug() << "other exception";
        return false;
    }
}

std::string SiSCoreWrapper::getFwGeneration(void)
{
   return m_fwGeneration;
}
