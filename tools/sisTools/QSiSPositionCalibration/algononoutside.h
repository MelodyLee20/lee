#ifndef ALGONONOUTSIDE_H
#define ALGONONOUTSIDE_H

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <math.h>

#include<vector>
#include<fstream>
using namespace std;


#define RESOLUTION_FW 4095
#define AXIS 2
//#define MODE 3

// CoordCali MODE:
// 1: this mode can't cali trapzoid or nonSquare
//    x' = ax+by+c
//    y' = dx+ey+f
//
// 2: the mode is same with the mode 1, and add the average preprocess (Average for same target x-coord or y-coord), this mode can't cali rotation
//
// 3: Projective Calibration
//    X'= (ax+by+c)/W
//    Y'= (dx+ey+f)/W
//    W = (R1x+R2y+1)
// ------------------------------------------------------------------------
//	  X' and Y' are the coordinates  after calibration
//    x  and y  are the coordinates before calibration
//    a~f and R1 R2 are the cali coef


//=================== PRINTF ===================//
//#define debug_ShowEst  //show error
//#define Print_coef
//=================== PRINTF ===================//

//=================== txt IO ===================//
#define READ_FROM_TXT
#define WRITE_TO_TXT
//=================== txt IO ===================//


typedef struct{

    int datanum;
    double *OSscreen_DEF;

    //for OS resolution
    double *OStargetX;
    double *OStargetY;
    double *OStouchX;
    double *OStouchY;

}s_InputData;


typedef struct{

    //X' = ((Ax)X + (Bx)Y + (Dx)) / W
    //Y' = ((Ay)X + (By)Y + (Dy)) / W
    //W  = ((R1)X + (R2)Y + 1)
    double Ax;
    double Bx;
    double Dx;

    double Ay;
    double By;
    double Dy;

    double R1;
    double R2;

}s_OutputData;

class AlgoNonOutside
{
public:
    AlgoNonOutside();

    void transP(double *a, double *output, int x, int y);
    void matM(double *a, double *b, double *output, int x, int y, int z);
    int MatInv(double *input, double *output, int n);
    void Reg(double *y, double *X, double *b, int dataNum, int betaNum);
    int CoordCali(s_InputData *Input, s_OutputData *Output, int mode);
    void matrixPrint(double *a, int x, int y);
    void CoordCaliAdj(s_InputData *Input);
    int run(int mode);

    double result[9];

    vector<unsigned int> m_targetX;
    vector<unsigned int> m_targetY;
    vector<unsigned int> m_touchX;
    vector<unsigned int> m_touchY;

    void addTargetX(unsigned int x);
    void addTargetY(unsigned int y);
    void addTouchX(unsigned int x);
    void addTouchY(unsigned int y);

    int getBetaNum();

    static AlgoNonOutside& instance()
    {
        static AlgoNonOutside instance;
        return instance;
    }

};

#define ALG AlgoNonOutside::instance()

#endif // ALGONONOUTSIDE_H
