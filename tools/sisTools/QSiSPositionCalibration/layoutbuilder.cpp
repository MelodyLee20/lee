#include "layoutbuilder.h"
#include "parameter.h"
#include "touchdata.h"

layoutBuilder::layoutBuilder()
{
}

layoutBuilder::~layoutBuilder()
{
}


QList<LineData*> layoutBuilder::buildLine()
{
    QList<LineData*> list;

    double screenHeight=PARA.getScreenH();
    double screenWidth=PARA.getScreenW();

    LineData* line1;
    LineData* line2;

    double t=PARA.convertMMtoPixel(PARA.lineTolerance());
    double l=PARA.convertMMtoPixel(PARA.lineLength());

    line1=new LineData();
    line1->Tolerance=t;
    line1->Lenth=l;
    line1->id=0;
    line1->direct=HORIZONTAL;
    line1->x=0;
    line1->y=screenHeight/2;
    line1->endX=screenWidth;
    line1->endY=screenHeight/2;

    line2=new LineData();
    line2->Tolerance=t;
    line2->Lenth=l;
    line2->id=1;
    line2->direct=VERTICAL;
    line2->x=screenWidth/2;
    line2->y=0;
    line2->endX=screenWidth/2;
    line2->endY=(screenHeight-1);

    list.append(line1);
    list.append(line2);

    return list;
}

QList<CircleData*> layoutBuilder::buildCircle_outside()
{
    QList<CircleData*> list;

    double screenHeight=PARA.getScreenH();
    double screenWidth=PARA.getScreenW();
    double MultipleX=PARA.paraTxM();
    double DivisionX=PARA.paraTxD();
    double MultipleY=PARA.paraTyM();
    double DivisionY=PARA.paraTyD();

    double d=PARA.convertMMtoPixel(PARA.pointDiameter());
    double t=PARA.convertMMtoPixel(PARA.pointTolerance());

    CircleData* cir1;
    cir1=new CircleData();

    cir1->id=0;

    //cir1->centerX=screenWidth/20;
    //cir1->centerY=screenHeight/10;
    cir1->centerX=screenWidth*MultipleX/DivisionX;
    cir1->centerY=screenHeight*MultipleY/DivisionY;

    cir1->diameter=d;
    cir1->tolerance=t;

    list.append(cir1);

    return list;
}

QList<RectData*> layoutBuilder::buildRect()
{
    QList<RectData*> list;

    double screenHeight=PARA.getScreenH();
    double screenWidth=PARA.getScreenW();
    double t=PARA.convertMMtoPixel(PARA.lineTolerance());

    RectData* rec1;
    rec1=new RectData();

    rec1->id=0;
    rec1->PosX=(screenWidth/2);
    rec1->PosY=(screenHeight/2);
    rec1->tolerance=t;

    list.append(rec1);

    return list;
}

