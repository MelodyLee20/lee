#ifndef SISBASEEXCEPTION_H_
#define SISBASEEXCEPTION_H_

#include "exception"
#include "string"
//#include <Windows.h>

class SisBaseException:public std::exception
{
public:
    SisBaseException(const std::string& errMsg, const int errCode):std::exception(errMsg.c_str()), m_errCode(errCode){}
    SisBaseException(const std::string& errMsg):std::exception(errMsg.c_str()), m_errCode(0){}
    virtual int getErrCode() const
    {
        return m_errCode;
    }
    virtual ~SisBaseException(){}

protected:
    SisBaseException(){};
    int m_errCode;
};

#endif //SISBASEEXCEPTION_H_
