#ifndef FREECONSOLE_H
#define FREECONSOLE_H

#ifdef WIN32
    #include <Windows.h>
#endif

class ConsoleTool
{
public:
    static void releaseConsoleWindow(void)
    {
        #ifdef WIN32
            FreeConsole();
        #endif
    }
};

#endif //FREECONSOLE_H
