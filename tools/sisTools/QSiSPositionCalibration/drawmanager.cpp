#include "drawmanager.h"
#include "painttestbuilder.h"
#include <QDebug>

drawmanager::drawmanager()
{
    m_paintTest=0;
}

drawmanager::~drawmanager()
{
    reset();
}

void drawmanager::reset()
{
    if(m_paintTest)
        delete m_paintTest;
    m_paintTest = 0;
}

void drawmanager::setFunction(int functionID)
{
    reset();
    m_paintTest = paintTestBuilder::buildPaintTest(functionID);
}

void drawmanager::paint(QPainter *painter, QPaintEvent *event, int elapsed)
{
    if(m_paintTest != 0)
    {
        m_paintTest->paint(painter, event);
    }
}
