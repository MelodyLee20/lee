#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "parameter.h"
#include <QTimer>
#include "touchdata.h"
#include <QDebug>
#include <QMessageBox>
#include "touchdatadrawing.h"
#include <QTouchEvent>
#include "paintbackground.h"
#include <QPushButton>
#include "pathmanager.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    setAttribute(Qt::WA_AcceptTouchEvents);

    setWindowFlags(Qt::Window|Qt::FramelessWindowHint);

    showFullScreen();

    elapsed = 0;
    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(updateWindow()));
    timer->start(10);
}

MainWindow::~MainWindow()
{
    delete timer;
}

void MainWindow::functionChoiced(int id)
{
    m_id = id;
    draw.setFunction(id);
}

void MainWindow::updateWindow()
{
    elapsed = (elapsed + qobject_cast<QTimer*>(sender())->interval())% 1000;
    update();
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);

    draw.paint(&painter,event,elapsed);
    touchDataDraw.paint(&painter,event,elapsed);
    painter.end();

    m_fileptr.setFileName(PathManager::getFilePath("pattern/OSscreen_DEF.txt"));
    m_fileptr.open(QIODevice::WriteOnly | QIODevice::Text);
    context = QString::number(parameter::instance().getScreenW()) + QString(" ");
    context += QString::number(parameter::instance().getScreenH());

    if(context != preContext)
    {
        QTextStream out(&m_fileptr);
        out<<context<<endl;
    }
    context = preContext;
    m_fileptr.close();

    if(TouchData::instance().getTouchState())
    {
        double screenHeight = PARA.getScreenH();
        double screenWidth = PARA.getScreenW();
        double exitbtnR = 50;

        QList<QTouchEvent::TouchPoint> touchP = TouchData::instance().getCTPs();
        foreach(const QTouchEvent::TouchPoint &touchPoint , touchP)
        {
            if(paintBackground::instance().flag)
            {
#ifdef MAXELL
                emit autoUpdate();
#else
                if((touchPoint.rect().center().x()> screenWidth/4-exitbtnR ) && (touchPoint.rect().center().x()< screenWidth/4+exitbtnR)
                    && (touchPoint.rect().center().y()> screenHeight/2-exitbtnR) && (touchPoint.rect().center().y()< screenHeight/2+exitbtnR))
                {
                    TouchData::instance().clearTP();
                    emit autoUpdate();
                }
#endif
            }            
        }
    }
}

bool MainWindow::event(QEvent *event)
{
    switch (event->type()) {
    case QEvent::TouchBegin:
    case QEvent::TouchUpdate:
    case QEvent::TouchEnd:
    {
        TouchData::instance().handleTouchEvent(event);
        update();
        break;
    }
    case QEvent::KeyPress:
    {
        QKeyEvent* keyEvent=static_cast<QKeyEvent*>(event);
        if(keyEvent->key() == Qt::Key_Escape)
        {
            TouchData::instance().clearTP();
            emit backMenu();
        }
    }
    case QEvent::MouseButtonPress:
    case QEvent::MouseMove:
    case QEvent::MouseButtonRelease:
    {
        TouchData::instance().handleMouseEvent(event);
        update();
        break;
    }
    default:
        return QWidget::event(event);
    }

    return true;
}
