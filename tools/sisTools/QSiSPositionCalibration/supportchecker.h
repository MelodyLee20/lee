#ifndef SUPPORTCHECKER_H
#define SUPPORTCHECKER_H

#define DELAY 500

class SupportChecker
{
public:
    SupportChecker();
    ~SupportChecker();

    bool isFWSupport(void);
};

#endif // SUPPORTCHECKER_H
