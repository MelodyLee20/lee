#ifndef UPDATEBAR_H
#define UPDATEBAR_H

#include <QThread>
#include "progressbar.h"
#include <QTimer>

class UpdateBar : public QThread
{
    Q_OBJECT

public:
    UpdateBar();
    ~UpdateBar();

    ProgressBar* progressBar;
    QTimer *timer;

    void setMessage(QString str);
    void setWindowSize(int h, int w);

protected:
    void run();

};

#endif // UPDATEBAR_H
