#ifndef PAINTTESTBUILDER_H
#define PAINTTESTBUILDER_H

#include "ipainttest.h"

class paintTestBuilder
{
public:
    paintTestBuilder();
    ~paintTestBuilder();

    static IpaintTest* buildPaintTest(int functionID);
};

#endif // PAINTTESTBUILDER_H
