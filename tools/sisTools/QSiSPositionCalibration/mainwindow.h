#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include "drawmanager.h"
#include "touchdatadrawing.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void functionChoiced(int id);

    static MainWindow& instance()
    {
        static MainWindow instance;
        return instance;
    }

    drawmanager draw;
    TouchDataDrawing touchDataDraw;

    void paintEvent(QPaintEvent *event);
    bool event(QEvent *event);
    QFile m_fileptr;

signals:
    void backMenu();
    void autoUpdate();
public slots:
    void updateWindow();

private:
    Ui::MainWindow *ui;
    int elapsed;
    QTimer* timer;
    int m_id;
    QString context;
    QString preContext;

    QPen m_pen;
    QFont m_font;
    QPoint m_point;
    QColor m_color;
    int i;
};

#endif // MAINWINDOW_H
