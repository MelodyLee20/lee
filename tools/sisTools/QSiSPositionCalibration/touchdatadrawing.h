#ifndef TOUCHDATADRAWING_H
#define TOUCHDATADRAWING_H

#include <QWidget>

class TouchDataDrawing
{
public:
    TouchDataDrawing();
    ~TouchDataDrawing();

    static TouchDataDrawing &instance()
    {
        static TouchDataDrawing instance;
        return instance;
    }

    void paint(QPainter *painter, QPaintEvent *event, int elapsed);
    void drawTPs(QPainter *painter, QPaintEvent *event);

    QVector<QPoint> preRect;
    QVector<QColor> TPcolors;

    double m_width;
};

#endif // TOUCHDATADRAWING_H
