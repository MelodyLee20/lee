#ifndef PAINTBACKGROUND_H
#define PAINTBACKGROUND_H

#include "config.h"
#include <QPen>
#include "ipainttable.h"
#include <QTouchEvent>
#include "dialog.h"
#include "mainwindow.h"


class paintBackground:public IpaintTable
{

public:
    paintBackground();
    ~paintBackground();

    static paintBackground& instance()
    {
        static paintBackground instance;
        return instance;
    }

    enum
    {
        NOT_YET=12,
        INIT=11,
        MID,
        LT,
        LM,
        LB,
        MT,
        MM,
        MB,
        RT,
        RM,
        RB,
        STOP,
        END,
    };

    int m_TState;
    int m_TCount;
    int m_TDraw;
    int pointCount;
    int state;
    int countMax;
    double exitbtnR;
    double preCountx;
    double preCounty;
    double preTouchTestX;
    double preTouchTestY;
    int flag;
    int retryflag;

    int m_lineDx;
    QPen m_solidLinePen;
    QPen m_dashLinePen;
    QPen m_arrowLinePen;
    QPen m_emptyLinePen;
    LineDir m_dir;
    QPen m_pen;
    QFont m_font;
    QColor m_color;
    QPoint m_point;

    QString context;
    QString preContext;
    QString context1;

    QFile m_file1;
    QString context2;

    QFile m_file2;
    QString context3;

    QFile m_file3;
    QString context4;

    QFile m_file4;
    QFile myfile;
    int i,betaNum;
    QString txt1;

    QFont font;

    float TargetX;
    float TargetY;

    int m_solidLineWidth;
    int m_dashLineWidth;
    int m_id;
    int m_speed;
    float m_x;
    float m_y;
    float m_endX;
    float m_endY;
    double m_slope;

    volatile float c_x;
    volatile float c_y;

    float r_x;
    float r_y;

    float REGION_XMIN;
    float REGION_YMIN;
    float REGION_XMAX;
    float REGION_YMAX;
    float REGION_XADD;
    float REGION_YADD;

    float REGIONS_XMIN;
    float REGIONS_YMIN;
    float REGIONS_XMAX;
    float REGIONS_YMAX;

    bool swap;
    bool invx;
    bool invy;

    void reset();
    void retry();
    void setPosition(float x, float y, float endX, float endY);
    void setCirclePosition(float x, float y);
    void setRectPosition(float x, float y);
    void setDir(LineDir dir);
    void setID(int id);
    int getID();
    void setLineDx(int lineDx);
    void setSolidLine(int solidLine);
    void setDashLine(int DashLine);
    void setSpeed(int speed);
    void setslope(float x,float y,float x2, float y2);

    int getmarginalLineShift();

    void paint(QPainter *painter, QPaintEvent *event);

public slots:
    void updateWindow();
};

#endif // PAINTBACKGROUND_H
