#ifndef IPAINTTABLE
#define IPAINTTABLE

#include <QPaintEvent>
#include <QPainter>


class IpaintTable
{
public:
    IpaintTable() {}
    virtual ~IpaintTable() {}
    virtual void paint(QPainter* painter,QPaintEvent* event)=0;
};

#endif // IPAINTTABLE

