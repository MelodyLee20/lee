#include "algorithm.h"
#include "parameter.h"
#include "paintbackground.h"

#include <QMessageBox>

algorithm::algorithm()
{
    data_err=0;
}

algorithm::~algorithm()
{

}

int algorithm::getBetaNum()
{
    return m_targetX.size();
}

void algorithm::addTargetX(unsigned int x)
{
    m_targetX.push_back(x);
//    QMessageBox msgBox;
//    msgBox.setText(QString("TargetX:%1").arg(m_targetX.at(m_targetX.size()-1)));
//    msgBox.exec();
}

void algorithm::addTargetY(unsigned int y)
{
    m_targetY.push_back(y);
//    QMessageBox msgBox;
//    msgBox.setText(QString("TargetY:%1").arg(m_targetY.at(m_targetY.size()-1)));
//    msgBox.exec();
}

void algorithm::addTouchX(unsigned int x)
{
    m_touchX.push_back(x);
//    QMessageBox msgBox;
//    msgBox.setText(QString("TouchX:%1").arg(m_touchX.at(m_touchX.size()-1)));
//    msgBox.exec();
}

void algorithm::addTouchY(unsigned int y)
{
    m_touchY.push_back(y);
//    QMessageBox msgBox;
//    msgBox.setText(QString("TouchY:%1").arg(m_touchY.at(m_touchY.size()-1)));
//    msgBox.exec();
}

int algorithm::run(int mode)
{
    //paintBackground::instance().showLog();

    int i,j;
    double screen_DEF[AXIS];


    screen_DEF[0]=PARA.getScreenW();
    screen_DEF[1]=PARA.getScreenH();

    int dataNum;
    int count = 0;
    double tmp;

    count = getBetaNum();

    if(count<1)
    {
        ALG_OLD.data_err=1;
        printf("no data!!\n");
        //system("pause");
        QMessageBox msgBox;
        msgBox.setText(QString("betaNum: %1").arg(count));
        msgBox.exec();
        return 0;
    }

    unsigned int targetX[10] = {0};
    unsigned int targetY[10] = {0};
    unsigned int touchX[10] = {0};
    unsigned int touchY[10] = {0};

    for(int i = 0; i < m_targetX.size(); i++)
        targetX[i] = m_targetX.at(i);
    for(int i = 0; i < m_targetY.size(); i++)
        targetY[i] = m_targetY.at(i);
    for(int i = 0; i < m_touchX.size(); i++)
        touchX[i] = m_touchX.at(i);
    for(int i = 0; i < m_touchY.size(); i++)
        touchY[i] = m_touchY.at(i);

//    QMessageBox msgBox;
//    msgBox.setText(QString("(x:%1 y:%2) (x:%3 y:%4)").arg(targetX[0]).arg(targetY[0]).arg(touchX[0]).arg(touchY[0]));
//    msgBox.exec();
//    QMessageBox msgBox2;
//    msgBox2.setText(QString("(x:%1 y:%2) (x:%3 y:%4)").arg(targetX[1]).arg(targetY[1]).arg(touchX[1]).arg(touchY[1]));
//    msgBox2.exec();

//    unsigned int* targetX = new unsigned int;
//    unsigned int* targetY = new unsigned int;
//    unsigned int* touchX = new unsigned int;
//    unsigned int* touchY = new unsigned int;

//    targetX=paintBackground::instance().inputTarX;
//    targetY=paintBackground::instance().inputTarY;
//    touchX=paintBackground::instance().inputTouX;
//    touchY=paintBackground::instance().inputTouY;

    s_InputData* Input = new s_InputData;
    s_OutputData* Output = new s_OutputData;

//    for(int j=1;j<=count;j++)
//    {
//        qDebug()<<"algorithmmmmm "<<j<<"-- TarX : "<<*(targetX+j)<<", TarY : "
//               <<*(targetY+j)<<", TouX : "<<*(touchX+j)
//              <<", TouY : "<<*(touchY+j);
//    }

    Input->datanum = count;
    Input->OSscreen_DEF = screen_DEF;
    Input->OStargetX = targetX;
    Input->OStargetY = targetY;
    Input->OStouchX = touchX;
    Input->OStouchY = touchY;

    int isSucces = 0;//若沒data則回傳 isSucces=0

    //isSucces = CoordCali(Input,Output,MODE);
    isSucces = CoordCali(Input,Output,mode);

    if(isSucces)
        printf("succes!\n");
    else
        printf("fail!\n");


    qDebug()<<"result : "<<Output->Ax<<" "<<Output->Bx<<" "<<Output->Dx<<" "<<Output->Ay
           <<" "<<Output->By<<" "<<Output->Dy<<" "<<Output->R1<<" "<<Output->R2<<endl;

    QString resultStr = "";
    resultStr += QString("CoordCali Output: %1, %2, %3, %4, %5, %6, %7, %8")
            .arg(Output->Ax).arg(Output->Bx).arg(Output->Dx).arg(Output->Ay).arg(Output->By).arg(Output->Dy).arg(Output->R1).arg(Output->R2);

    QMessageBox msgBox;
    msgBox.setText(resultStr);
    msgBox.exec();

    *result=Output->Ax;
    *(result+1)=Output->Bx;
    *(result+2)=Output->Dx;
    *(result+3)=Output->Ay;
    *(result+4)=Output->By;
    *(result+5)=Output->Dy;
    *(result+6)=Output->R1;
    *(result+7)=Output->R2;

    for(int j=0;j<8;j++)
        qDebug()<<"result after : "<<*(result+j);

}

void algorithm::transP(double* a,double* output,int x,int y){
    int i = 0,j = 0;

    for(j=0;j<x;j++)
        for(i = 0;i<y;i++)
            output[i*x+j] = a[j*y+i];
}

void algorithm::matM(double* a,double* b,double* output ,int x,int y,int z){
    int i = 0,j=0,k=0,sum=0;

    for(i = 0;i<(x*z);i++)
        output[i] = 0.0;

    for (j = 0; j < x; j++)
      for (i = 0; i < z; i++)
        for (k = 0; k < y; k++)
          output[j*z+i]+= a[j*y+k]*b[k*z+i];

}

int algorithm::MatInv(double* input,double* output,int n)
{
    int i, j, k;
    double a[20][20] = {0},d;  // size matrix必須大於 (2*n)x(2*n)

    for (i = 1; i <= n; i++)
        for (j = 1; j <= n; j++)
            a[i][j] = input[(i-1)*n + (j-1)];

    for (i = 1; i <= n; i++)
        for (j = 1; j <= 2 * n; j++)
            if (j == (i + n))
                a[i][j] = 1;

    for (i = n; i > 1; i--)
    {
        if (a[i-1][1] < a[i][1])
        {
            for(j = 1; j <= n * 2; j++)
            {
                d = a[i][j];
                a[i][j] = a[i-1][j];
                a[i-1][j] = d;
            }
        }
    }

    for (i = 1; i <= n; i++)
    {
        for (j = 1; j <= n * 2; j++)
        {
            if (j != i)
            {
                d = a[j][i] / a[i][i];
                for (k = 1; k <= n * 2; k++)
                    a[j][k] = a[j][k] - (a[i][k] * d);
            }
        }
    }

    for (i = 1; i <= n; i++)
    {
        d=a[i][i];
        for (j = 1; j <= n * 2; j++)
            a[i][j] = a[i][j] / d;
    }
    for (i = 1; i <= n; i++)
        for (j = n + 1; j <= n * 2; j++)
           output[(i-1)*n + (j-n-1)] = a[i][j];

return 0;
}

void algorithm::Reg(double* y,double* X,double* b,int dataNum ,int betaNum ){

    double *Xt = new double[dataNum*betaNum];
    double *XtX = new double[dataNum*betaNum];
    double *XtX2 = new double[dataNum*betaNum];
    double *tmp = new double[dataNum*betaNum];


    transP(X,Xt,dataNum,betaNum);//Xt (betaNum*dataNum)
    matM(Xt,X,XtX,betaNum,dataNum,betaNum);//Xt*X (betaNum*betaNum)
    MatInv(XtX,XtX2,betaNum);//XtX = Inv(Xt*X)  (betaNum*betaNum)
    matM(XtX2,Xt,tmp,betaNum,betaNum,dataNum); // tmp = Inv(Xt*X)* Xt   (betaNum*dataNum)
    matM(tmp,y,b,betaNum,dataNum,1); //b = beta

    delete [] Xt;
    delete [] XtX;
    delete [] XtX2;
    delete [] tmp;
}

int algorithm::CoordCali(s_InputData* Input,s_OutputData* Output,int mode) //Find 	Ax,	Bx, Dx,  Ay,  By, Dy
{
    qDebug() << "algorithm::CoordCali Input->datanum=" << Input->datanum;
    if(Input->datanum<1){
        return 0;
    }

    if(mode == 2)
    //if(mode == 1)
    {
            CoordCaliAdj(Input);
            int BNum = 3;
            int i,j;
            double touch[AXIS],target[AXIS];
            double *B = new double[Input->datanum * BNum];
            double *y_x = new double[Input->datanum];
            double *y_y = new double[Input->datanum];
            double *beta_x = new double[BNum];
            double *beta_y = new double[BNum];

            for(j=0;j<Input->datanum;j++)
            {              

                for(i = 0;i < AXIS ;i++)
                    {
                        if(i==0)
                            y_x[j] = target[i];
                        else
                            y_y[j] = target[i];

                        B[j*BNum] = touch[0];
                        B[j*BNum+1] = touch[1];
                        B[j*BNum+2] = 1;
                    }
            }
            // Find coefficient Ax Bx Dx Ay...
            Reg(y_x,B,beta_x,Input->datanum ,BNum );
            Reg(y_y,B,beta_y,Input->datanum ,BNum );

            delete [] B ;
            delete [] y_x ;
            delete [] y_y ;

            //s_OutputData Output;
            Output->Ax = beta_x[0];
            Output->Bx = beta_x[1];
            Output->Dx = beta_x[2];

            Output->Ay = beta_y[0];
            Output->By = beta_y[1];
            Output->Dy = beta_y[2];
            delete [] beta_x;
            delete [] beta_y;

            return 1;
    }
    //else if(mode==2)
    else if(mode==1)
    {
        int i,j;
        int BNum = 8;
        double *B = new double[Input->datanum*2*BNum];
        double *y = new double[Input->datanum*2];
        double *beta = new double[BNum];

        double touch[AXIS],target[AXIS];

        QString coordStr = "";

        qDebug() << "Input->OSscreen_DEF[0]=" << Input->OSscreen_DEF[0] << ", Input->OSscreen_DEF[1]=" << Input->OSscreen_DEF[1];
        coordStr += QString("OSscreen_DEF[0]: %1, OSscreen_DEF[1]: %2\n").arg(Input->OSscreen_DEF[0]).arg(Input->OSscreen_DEF[1]);

        for(j=0;j<Input->datanum;j++)
        {
            touch[0] = Input->OStouchX[j];
            touch[1] = Input->OStouchY[j];
            target[0] = Input->OStargetX[j];
            target[1] = Input->OStargetY[j];

            qDebug()<<"coordinate "<<j<<"-- TarX : "<<target[0]<<", TarY : "
                   <<target[1]<<", TouX : "<<touch[0]
                  <<", TouY : "<<touch[1];

            coordStr += QString("coord %1-- TarX: %2, TarY: %3, TouX: %4, TouY: %5\n").arg(j).arg(target[0]).arg(target[1]).arg(touch[0]).arg(touch[1]);

            y[j] = Input->OStargetX[j] * RESOLUTION_FW/Input->OSscreen_DEF[0]  ;
            B[j*BNum  ] = Input->OStouchX[j] * RESOLUTION_FW/Input->OSscreen_DEF[0] ;
            B[j*BNum+1] = Input->OStouchY[j] * RESOLUTION_FW/Input->OSscreen_DEF[1] ;
            B[j*BNum+2] = 1.0;
            B[j*BNum+3] = 0.0;
            B[j*BNum+4] = 0.0;
            B[j*BNum+5] = 0.0;
            B[j*BNum+6] = (-1)*B[j*BNum  ] * y[j];
            B[j*BNum+7] = (-1)*B[j*BNum+1] * y[j];
        }

        QMessageBox msgBox;
        msgBox.setText(coordStr);
        msgBox.exec();

        for(j=Input->datanum;j<(Input->datanum*2);j++)
        {
            y[j] = Input->OStargetY[j-Input->datanum] * RESOLUTION_FW/Input->OSscreen_DEF[1] ;
            B[j*BNum  ] = 0.0;
            B[j*BNum+1] = 0.0;
            B[j*BNum+2] = 0.0;
            B[j*BNum+3] = Input->OStouchX[j-Input->datanum] * RESOLUTION_FW/Input->OSscreen_DEF[0] ;
            B[j*BNum+4] = Input->OStouchY[j-Input->datanum] * RESOLUTION_FW/Input->OSscreen_DEF[1] ;
            B[j*BNum+5] = 1.0  ;
            B[j*BNum+6] = (-1)*B[j*BNum+3] * y[j];
            B[j*BNum+7] = (-1)*B[j*BNum+4] * y[j];
        }

        Reg(y,B,beta,Input->datanum*2 ,BNum );

        //s_OutputData Output;
        Output->Ax = beta[0];
        Output->Bx = beta[1];
        Output->Dx = beta[2];
        Output->Ay = beta[3];
        Output->By = beta[4];
        Output->Dy = beta[5];
        Output->R1 = beta[6];
        Output->R2 = beta[7];

        return 1;
    }
    else if(mode==3)
    {
        int n=8, m, i, j, k, l, p, imax, x1, x2, x3, x0, y1, y2, y3, y0, u1, u2, u3, u0, v1, v2, v3, v0, U, V, z;
        double b, t, t1, a[MAXN][MAXN + 1];
        double x[4]={0};
        double y[4]={0};
        double u[4]={0};
        double v[4]={0};

//對於同一個target point座標，touch point取平均值
        for(i=0;i<Input->datanum;i++)
        {
            x[(int)(i*4/Input->datanum)]+=Input->OStouchX[i];
            y[(i*4/Input->datanum)]+=Input->OStouchY[i];
            u[(i*4/Input->datanum)]+=Input->OStargetX[i];
            v[(i*4/Input->datanum)]+=Input->OStargetY[i];
        }

        for(i=0;i<4;i++)
        {
            x[i]/=Input->datanum/4;
            y[i]/=Input->datanum/4;
            u[i]/=Input->datanum/4;
            v[i]/=Input->datanum/4;
        }
//對於同一個target point座標，touch point取平均值

/*
        for(i=0;i<4;i++)
        {
            x[i]=Input->OStouchX[i*Input->datanum/4];
            y[i]=Input->OStouchY[i*Input->datanum/4];
            u[i]=Input->OStargetX[i*Input->datanum/4];
            v[i]=Input->OStargetY[i*Input->datanum/4];
        }
        */
        #ifdef debug_4point
         matrixPrint(x,1,4);
         matrixPrint(y,1,4);
         matrixPrint(u,1,4);
         matrixPrint(v,1,4);
        #endif
        printf("%f\n\n\n",y[0]);
//高斯消去法找係數 - START
        a[0][0]=x[0];
        a[0][1]=y[0];
        a[0][2]=1;
        a[0][3]=0;
        a[0][4]=0;
        a[0][5]=0;
        a[0][6]=-x[0]*u[0];
        a[0][7]=-y[0]*u[0];
        a[0][8]=u[0];

        a[1][0]=x[1];
        a[1][1]=y[1];
        a[1][2]=1;
        a[1][3]=0;
        a[1][4]=0;
        a[1][5]=0;
        a[1][6]=-x[1]*u[1];
        a[1][7]=-y[1]*u[1];
        a[1][8]=u[1];

        a[2][0]=x[2];
        a[2][1]=y[2];
        a[2][2]=1;
        a[2][3]=0;
        a[2][4]=0;
        a[2][5]=0;
        a[2][6]=-x[2]*u[2];
        a[2][7]=-y[2]*u[2];
        a[2][8]=u[2];

        a[3][0]=x[3];
        a[3][1]=y[3];
        a[3][2]=1;
        a[3][3]=0;
        a[3][4]=0;
        a[3][5]=0;
        a[3][6]=-x[3]*u[3];
        a[3][7]=-y[3]*u[3];
        a[3][8]=u[3];

        a[4][0]=0;
        a[4][1]=0;
        a[4][2]=0;
        a[4][3]=x[0];
        a[4][4]=y[0];
        a[4][5]=1;
        a[4][6]=-x[0]*v[0];
        a[4][7]=-y[0]*v[0];
        a[4][8]=v[0];

        a[5][0]=0;
        a[5][1]=0;
        a[5][2]=0;
        a[5][3]=x[1];
        a[5][4]=y[1];
        a[5][5]=1;
        a[5][6]=-x[1]*v[1];
        a[5][7]=-y[1]*v[1];
        a[5][8]=v[1];

        a[6][0]=0;
        a[6][1]=0;
        a[6][2]=0;
        a[6][3]=x[2];
        a[6][4]=y[2];
        a[6][5]=1;
        a[6][6]=-x[2]*v[2];
        a[6][7]=-y[2]*v[2];
        a[6][8]=v[2];

        a[7][0]=0;
        a[7][1]=0;
        a[7][2]=0;
        a[7][3]=x[3];
        a[7][4]=y[3];
        a[7][5]=1;
        a[7][6]=-x[3]*v[3];
        a[7][7]=-y[3]*v[3];
        a[7][8]=v[3];

printf("%f\n\n\n",a[0][1]);
for(i=0;i<8;i++)
{
    for(j=0;j<9;j++)
        printf("%.0f\t",a[i][j]);
        printf("\n");
}

        for (k = 0; k < n; k++)
        {
            t = 0; imax = k;
            for (l = k; l < n; l++)
            {
                if (fabs(a[l][k]) > t)
                    {
                        imax = l;
                        t = a[l][k];
                    }
            }

            for (l = 0; l < n + 1; l++)
            {
                t = a[k][l];
                a[k][l] = a[imax][l];
                a[imax][l] = t;
            }

            b = a[k][k];
            for (j = 0; j < n + 1; j++) a[k][j] /= b;

            for (i = k + 1; i < n; i++)
            {
                b = -a[i][k];
                for (j = 0; j < n + 1; j++) a[i][j] += b * a[k][j];
            }
        }

        for (k = n - 2; k >= 0; k--)
            for (j = k + 1; j < n; j++)
                a[k][n] -= a[k][j] * a[j][n];
//高斯消去法找係數 - END

#ifdef Print_coef
    printf("============ Ax Bx Dx Ay By Dy R1 R2 ==============\n");
    for (i = 0; i < n; i++)
    {
     printf("   x[%d] = %lf\n", i, a[i][n]);
    }
    printf("============ Ax Bx Dx Ay By Dy R1 R2 ==============\n");
#endif

        //s_OutputData Output;
        Output->Ax = a[0][n];
        Output->Bx = a[1][n];
        Output->Dx = a[2][n];
        Output->Ay = a[3][n];
        Output->By = a[4][n];
        Output->Dy = a[5][n];
        Output->R1 = a[6][n];
        Output->R2 = a[7][n];

        return 1;
    }
}

void algorithm::matrixPrint(double* a,int x,int y){
    int i = 0,j = 0;
    for(j=0;j<x;j++)
    {
        for(i = 0;i<y;i++)
        {
            printf("%.10f ",a[j*y+i]);
        }
        printf("\n");
    }
}

void algorithm::CoordCaliAdj(s_InputData* Input)
{
    int i,j;
    int OnePointDataNum = Input->datanum/4;
    double sum_x[2] = {0};
    double sum_y[2] = {0};

    for(i=0;i<Input->datanum;i++)
    {
        if(i<OnePointDataNum)
        {
            sum_x[0]+=Input->OStouchX[i];
            sum_y[0]+=Input->OStouchY[i];

        }
        else if(i<2*OnePointDataNum)
        {
            sum_x[0]+=Input->OStouchX[i];
            sum_y[1]+=Input->OStouchY[i];
        }
        else if(i<3*OnePointDataNum)
        {
            sum_x[1]+=Input->OStouchX[i];
            sum_y[1]+=Input->OStouchY[i];
        }
        else
        {
            sum_x[1]+=Input->OStouchX[i];
            sum_y[0]+=Input->OStouchY[i];

        }
    }
    for(i=0;i<2;i++)
    {
        sum_x[i] = sum_x[i] / OnePointDataNum / 2;
        sum_y[i] = sum_y[i] / OnePointDataNum / 2;
    }

    for(i=0;i<Input->datanum;i++)
    {
        if(i<OnePointDataNum)
        {
            Input->OStouchX[i] = sum_x[0];
            Input->OStouchY[i] = sum_y[0];

        }
        else if(i<2*OnePointDataNum)
        {
            Input->OStouchX[i] = sum_x[0];
            Input->OStouchY[i] = sum_y[1];
        }
        else if(i<3*OnePointDataNum)
        {
            Input->OStouchX[i] = sum_x[1];
            Input->OStouchY[i] = sum_y[1];
        }
        else
        {
            Input->OStouchX[i] = sum_x[1];
            Input->OStouchY[i] = sum_y[0];
        }
    }
}
