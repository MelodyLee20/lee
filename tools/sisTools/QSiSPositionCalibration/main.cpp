#include <QApplication>
#include <QPainter>
#include <QMessageBox>
#include "form.h"
//#include "mainwindow.h"
//#include "dialog.h"
#include "parameter.h" //ini

#ifdef SISCORE_USE
    #include "siscorewrapper.h"
#else
    #include "USBIO.h"
    #include "SisBaseException.h"
#endif

#ifdef ANDROID
    #include "assetscopier.h"
#endif

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

#ifdef ANDROID
    if(!AssetsCopier::copyAssets("/data/data/com.sis.QPJT/assets/"))
    {
        QMessageBox msgBox;
        msgBox.setText("copyAssets failed !");
        msgBox.exec();
        return 0;
    }
#endif

#ifdef SISCORE_USE
    SiSCore* core = g_SiSCoreWrapper.openDevice();
    if(core == NULL)
    {
        QMessageBox msgBox;
        msgBox.setText("No SIS device detected!");
        msgBox.exec();
        return 0;
    }
    g_SiSCoreWrapper.closeDevice();
#else
    HANDLE deviceHandle = INVALID_HANDLE_VALUE;
    deviceHandle = IO_OpenDevice(NOT_SET);
    if(deviceHandle == INVALID_HANDLE_VALUE)
    {
        QMessageBox msgBox;
        msgBox.setText("No SIS device detected!");
        msgBox.exec();
        return 0;
    }
#endif

    PARA.initial();

    if(!(PARA.getPointMode() == 1 || PARA.getPointMode() == 2))
    {
        QMessageBox msgBox;
        msgBox.setText("No this target point mode!");
        msgBox.exec();
        return 0;
    }
    //MainWindow w;
    //w.show();

    //dialog d;
    //d.show();

    form fm;
    fm.showMenu();

    return a.exec();
}
