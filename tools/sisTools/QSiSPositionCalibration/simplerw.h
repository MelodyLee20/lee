#ifndef SIMPLERW_H
#define SIMPLERW_H

#include <QTime>

#include "SiSCore.h"
#include "CptFactoryFacade.h"
#include "SiSCmdModuleException.h"
//#include "SiSCoreException.h"
#include "SysDefine.h"
#include "HydraDefine.h"

#include <iostream>
#include <fstream>

#define CHAR_PER_BYTE 2
#define FWID_BYTE 14
#define BYTE_PER_LINE 16
#define TIMESTAMP_SHOW_BYTE BYTE_PER_LINE * 3
#define MAX_SHOW_BYTE BYTE_PER_LINE * 7
#define SHIFT_TO_FWID 40 //BYTE_PER_LINE*2+4

using namespace SiS::Model;
using namespace SiS;
using namespace SiS::Model::Hydra;

#define MAX_TRANSMISSION_SIZE 832
#define DEFAULT_TRANSMISSION_SIZE 64

enum
{
    MAX_LINE_SIZE = 256,
    MAX_FILENAME_LENGTH = 256,
};

class SimpleRW
{
public:
    SimpleRW();
    static int run(int argc, char *argv[], SiSCore *dummyHost);

private:
    static void processCommand(SiSCore *dummyHost, unsigned char *command, unsigned short size, FILE *output, int transmissionSize);
};

#endif // SIMPLERW_H
