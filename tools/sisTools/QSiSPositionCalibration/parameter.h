#ifndef PARAMETER_H
#define PARAMETER_H

#include <QTouchEvent>
#include <QDesktopWidget>

#define INI_NAME "PJT_Setting.ini"

class parameter
{
public:
    parameter();
    ~parameter();

    static parameter& instance()
    {
        static parameter instance;
        return instance;
    }

    void initialD();
    void initial();

    double getScreenW() const;
    void setScreenW(int value);
    double getScreenH() const;
    void setScreenH(int value);

    double convertMMtoPixel(int value);

    int lineTolerance() const;
    int lineLength() const;
    int getNGTolerance() const;
    int getTimeout() const;

    int pointDiameter() const;
    int pointTolerance() const;

    int paraInvx();
    int paraInvy();
    int paraSwap();
    volatile int paraTxM();
    volatile int paraTxD();
    volatile int paraTyM();
    volatile int paraTyD();
    void setInvx(int txtInvx);
    void setInvy(int txtInvy);
    void setSwap(int txtSwap);
    void setCount(int txtCount);
    void setTxM(volatile int txtTxM);
    void setTxD(volatile int txtTxD);
    void setTyM(volatile int txtTyM);
    void setTyD(volatile int txtTyD);
    int getPointMode();

private:
    QString getININame();

    double m_screenH;
    double m_screenW;
    int m_LCDH;
    int m_LCDW;

    int m_pointDiameter;
    int m_pointTolerance;

    int m_lineDiameter;
    int m_lineTolerance;
    int m_lineSpeed;
    int m_lineLength;

    int m_timeout;
    int m_ngTolerance;
    int m_isCalWithDpi;

    bool isInvX;
    bool isInvY;
    bool isSwap;

    int Count;
    volatile int TxM;
    volatile int TxD;
    volatile int TyM;
    volatile int TyD;
    int pointMode;
};

#define PARA parameter::instance()

#endif // PARAMETER_H
