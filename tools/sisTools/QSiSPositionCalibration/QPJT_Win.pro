#-------------------------------------------------
#
# Project created by QtCreator 2015-09-09T10:37:16
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QPJT
#CONFIG += console

TEMPLATE = app
DEFINES += "SISCORE_USE"
DEFINES += "MAXELL"
DEFINES += "INNER_ALG"

SOURCES += main.cpp\
    mainwindow.cpp \
    dialog.cpp \
    form.cpp \
    parameter.cpp \
    touchdata.cpp \
    ipainttest.cpp \
    painttestbuilder.cpp \
    paintobjectbuilder.cpp \
    layoutbuilder.cpp \
    paintbackground.cpp \
    painttest.cpp \
    touchdatadrawing.cpp \
    progressbar.cpp \
    updatebar.cpp \
    drawmanager.cpp \
    siscorewrapper.cpp \
    pathmanager.cpp \
    algorithm.cpp

HEADERS  += mainwindow.h \
    dialog.h \
    form.h \
    config.h \
    parameter.h \
    touchdata.h \
    ipainttest.h \
    ipainttable.h \
    painttestbuilder.h \
    paintobjectbuilder.h \
    layoutbuilder.h \
    paintbackground.h \
    painttest.h \
    touchdatadrawing.h \
    progressbar.h \
    updatebar.h \
    drawmanager.h \
    commonused.h \
    siscorewrapper.h \
    pathmanager.h \
    algorithm.h

FORMS    += mainwindow.ui \
    dialog.ui

RC_FILE += \
    myapp.rc


#INCLUDEPATH  += $$PWD/USBIO
#LIBS += -L$$PWD/ -lUSBIO64
#PRE_TARGETDEPS += $$PWD/USBIO64.lib

isEmpty(Lib_path) {
        Lib_path = $$PWD/../../../Lib
}

include($$Lib_path/include/qmake/utility/SiSUtility.pri)
include($$Lib_path/include/qmake/StaticSiSCore/StaticSiSCore.pri)

QMAKE_LFLAGS += /MANIFESTUAC:"level='requireAdministrator'uiAccess='false'"
