#include "simplerw.h"

#include <QMessageBox>
#include "pathmanager.h"
#ifdef ANDROID
    #include <OperationFormatReader.h>
#endif

#include "siscorewrapper.h"
#include <QDebug>

SimpleRW::SimpleRW()
{

}

void SimpleRW::processCommand( SiSCore *dummyHost, unsigned char* command, unsigned short size, FILE* output, int transmissionSize )
{
    bool bOK = false;

    if ( !size )
        return;

    fprintf( output, "Command: " );

    for ( int i = 0; i < size; i++ )
        fprintf( output, "%02x ", command[i] );

    fprintf( output, "\n" );

    BYTE *receiveBuf = new BYTE[transmissionSize];
    memset(receiveBuf, 0, transmissionSize);

//    receiveBuf[0] = 0x9;

    std::string fwGeneration = dummyHost->attribute(DEV_TYPE);

    QMessageBox msgBox2;
    msgBox2.setText(QString("fwGeneration %1").arg(fwGeneration.c_str()));
    //msgBox2.exec();

    if(fwGeneration == "817")
        receiveBuf[0] = 0x0a;
    if(fwGeneration == "819")
        receiveBuf[0] = 0x21;

    Component * receiveData = new Component("SerialData", receiveBuf, transmissionSize);
    if ( size > 1 )
    {
        //Component * sendData = new Component("SerialData", sendBuf, 64);
        try{
            QString cmd;
            for(int i = 0; i < size; i++){
                cmd = cmd + QString("%1").arg(command[i]) + " ";
            }
            QMessageBox msgBox;
            msgBox.setText(QString("SimpleRW::processCommand %1 \n transmissionSize: %2").arg(cmd).arg(transmissionSize));
            //msgBox.exec();
            qDebug() << "SimpleRW::processCommand " << cmd << " transmissionSize: " << transmissionSize;
            QMessageBox msgBox3;
            msgBox3.setText(QString("SimpleRW::processCommand %1").arg(dummyHost->attribute(DEV_PATH).c_str()));
            //msgBox3.exec();

            Component * sendData = new Component("SerialData", command, transmissionSize);

            QMessageBox msgBox1;
            msgBox1.setText(QString("SimpleRW::processCommand before SEND_CMD"));
            // msgBox1.exec();
            bOK = dummyHost->exe<int>("SEND_CMD", sendData);
            QMessageBox msgBox2;
            msgBox2.setText(QString("SimpleRW::processCommand after SEND_CMD"));
            //msgBox2.exec();
        }
        catch(SiSCmdModuleException & e )
        {
            //FAIL() << "Command Module exception : " << e.what();
            //qDebug() << "Command Module exception : " << e.what();
            QMessageBox msgBox;
            msgBox.setText(e.what());
            msgBox.exec();

        }
        catch(SiSCoreException & ce )
        {
            //FAIL() << "Core exception : " << ce.what();
            //qDebug() << "Core exception : " << ce.what();
            QMessageBox msgBox;
            msgBox.setText(ce.what());
            msgBox.exec();

        }
        catch(...)
        {
            //FAIL() << "other exception";
            //qDebug() << "other exception";
            QMessageBox msgBox;
            msgBox.setText("other exception");
            msgBox.exec();

        }


        if ( !bOK )
        {
            fprintf( output, "Write Command ERROR\n" );
            return;
        }

        QMessageBox msgBox;
        msgBox.setText(QString("SimpleRW::processCommand before RECEIVE_CMD"));
        //msgBox.exec();

        bOK = dummyHost->exe<int>("RECEIVE_CMD", receiveData);

        if ( !bOK )
        {
            fprintf( output, "Read Result ERROR\n" );
            return;
        }

        QMessageBox msgBox1;
        msgBox1.setText(QString("SimpleRW::processCommand after RECEIVE_CMD"));
        //msgBox1.exec();

        printf("receive successfully\n");
        for ( int i = 0; i < transmissionSize; i++ )
        {
            printf("%02x ", receiveData->data(i));
            receiveBuf[i] = receiveData->data(i);
            //fprintf(output, "%x ", receiveData->data(i));
            if ( ( (i +1) % 15 ) == 0 )
            {
                printf("\n");
            }
        }
        printf("\n");
        if ( 0 != receiveData )
        {
            delete receiveData;
            receiveData = 0;
        }
        else
        {
            printf("receive failingly\n");
        }
    }
    else if( size == 1 )
    {
        bOK = dummyHost->exe<int>("RECEIVE_CMD", receiveData);
        if ( !bOK )
        {
            fprintf( output, "Write Command ERROR\n" );
            return;
        }

    }
    fprintf( output, "Result: " );
    for ( int j = 0; j < transmissionSize; j++)
    {
        fprintf( output, "%02x ", receiveBuf[j] );
        //fprintf( output, "%02x ", buffer[j] );
    }
    fprintf( output, "\n" );

    g_SiSCoreWrapper.closeDevice();

    QString ackStr = "SimpleRW::processCommand receiveData: ";
    for ( int j = 0; j < transmissionSize; j++)
    {
        char ackByte[10] = "";
        sprintf( ackByte, "%02x ", receiveBuf[j] );
        ackStr += QString("%1 ").arg(ackByte);
    }
    QMessageBox msgBoxAck;
    msgBoxAck.setText(ackStr);
    //msgBoxAck.exec();

}

int SimpleRW::run(int argc, char *argv[], SiSCore *dummyHost  )
{
//    ParseLogLevel( argc, argv );
    bool inputSpecified = false;
    bool outputSpecified = false;
    bool hexDataSpecified = false;

    char* inputFilename = NULL;
    char* outputFilename = NULL;
    FILE* out = NULL;

    char line[MAX_LINE_SIZE];

    //unsigned char cmd_buffer[MAX_TRANSMISSION_SIZE] = {0};
    unsigned short cmd_idx = 0;
    int  transmissionSize = DEFAULT_TRANSMISSION_SIZE;

    printf( "\nUsage: SimpleRW [hex data] [-f <input file>] [-o <output file>] [-r] [/lNN] [/v]\n" );
    printf( "  hex data: USB command in hex\n" );
    printf( "  -f <input file >: the file which USB commands are described in\n" );
    printf( "  -o <output file>: the file where the result displays\n" );
    printf( "  -s transmission size: set the different transmission size. Default set as 64 bytes\n" );
//    printf( "  -r:	disable auto ReportToOS\n" );
//    printf( "  /v:    show version\n" );
    printf( "\nExample: nSimpleRW.exe 21 00 06 85 21 01\n" );
    printf( "Example: nSimpleRW.exe -f test.in\n" );
    printf( "Example: nSimpleRW.exe -f test.in -o test.out\n" );
    printf( "\n" );

    unsigned char *cmd_buffer = new unsigned char[transmissionSize];
    memset(cmd_buffer, 0, transmissionSize);

    for ( int i = 1; i < argc; i++ )
    {
        if ( argv[i][0] == '/' )
        {
//            if ( argv[i][1] == 'v' || argv[i][1] == 'V' )
//            {
//                PrintFileInfo( "USBIO.dll");
//                return 0;
//            }
        }
        else if ( argv[i][0] == '-' )
        {
            if ( argv[i][1] == 'f' || argv[i][1] == 'F' )
            {
                if ( ++i >= argc )
                {
                    printf( "argument error\n" );
                    return 1;
                }

                inputSpecified = true;
                inputFilename = argv[i];
                printf( "Input file set: %s\n", inputFilename );
            }
            else if ( argv[i][1] == 'o' || argv[i][1] == 'O' )
            {
                if ( ++i >= argc )
                {
                    printf( "argument error\n" );
                    return 1;
                }

                outputSpecified = true;
                outputFilename = argv[i];
                printf( "Output file set: %s\n", outputFilename );
            }
//            else if ( argv[i][1] == 'r' || argv[i][1] == 'R' )
//            {
//                useReportToOS = false;
//                printf( "Disable auto ReportToOS\n" );
//            }
            else if ( argv[i][1] == 's' || argv[i][1] == 'S' )
            {
                if ( ++i >= argc )
                {
                    printf( "argument error\n" );
                    return 1;
                }
                sscanf(argv[i], "%d", &transmissionSize );
                if( transmissionSize != 64 && transmissionSize != 320 && transmissionSize != 546 && transmissionSize != 832 )
                {
                    printf( "illegal transmission size. i.e. -s 64/320/546/832\n" );
                    return 1;
                }
                printf( "Specified transmission size is %d%\n", transmissionSize );
            }
            else
            {
                printf( "unknown option: %s\n", argv[i] );
            }

        }
        else
        {
            hexDataSpecified = true;

            if ( MAX_TRANSMISSION_SIZE == cmd_idx )
            {
                printf( "Command too long, only support %d byte\n", MAX_TRANSMISSION_SIZE );
            }

            if ( 0 == strcmp( argv[i], "crc" ) )
            {
                if ( cmd_idx < 2 )
                {
                    fprintf( out, "command syntax error\n" );
                    break;
                }
//                unsigned short crc = crc16_ccitt( &cmd_buffer[2], cmd_idx - 2 );
//                cmd_buffer[cmd_idx++] = ( unsigned char )( crc >> 8 );
//                cmd_buffer[cmd_idx++] = ( unsigned char )( crc & 0xff );
            }
            else
            {
                sscanf( argv[i], "%x", &cmd_buffer[cmd_idx++] );
            }
        }
    }

    if ( hexDataSpecified && ( inputSpecified || outputSpecified ) )
    {
        printf( "hex raw data and input file conflict, please choose only one input\n" );
        return 1;
    }

    if ( !hexDataSpecified && !inputSpecified )
        return 0;

    // main flow

    //unsigned char cmd_buffer[transmissionSize] = {0};
    if(dummyHost == NULL){
        QMessageBox msgBox;
        msgBox.setText(QString("SimpleRW::run dummyHost before open device %1").arg((int)dummyHost));
        //msgBox.exec();

        dummyHost = g_SiSCoreWrapper.openDevice();
//        dummyHost = new SiSCore();
//        SSetting setting;
//#ifdef ANDROID
//        OperationFormatReader reader1;
//        //if(false == reader1.init("/data/data/com.sis.QPJT/assets/res/Command.db"))
//        if(false == reader1.init(PathManager::getFilePath("res/Command.db").toStdString()))
//        {
//            QMessageBox msgBox;
//            msgBox.setText("ProxyOpenDevice::run(Path Fail!)");
//            msgBox.exec();
//        }
//        dummyHost->boot(setting, &reader1);
//#else
//        dummyHost->boot(setting);
//#endif

        QMessageBox msgBox1;
        msgBox1.setText(QString("SimpleRW::run dummyHost %1").arg((int)dummyHost));
       // msgBox1.exec();

    }

    if ( outputSpecified )
    {
        out = fopen( outputFilename, "w" );
        if ( out != NULL )
        {
            out = NULL; //open failed
        }
    }
    else
    {
        out = stdout;
    }


    if ( hexDataSpecified )
    {
        processCommand( dummyHost, cmd_buffer, cmd_idx, out, transmissionSize );
    }

    if ( inputSpecified )
    {
        FILE* in = NULL;

        in = fopen( inputFilename,"r" );
        if ( in != NULL )
        {
            in = NULL; //open failed
        }
        if ( NULL == in )
        {
            printf( "open input file failed.\n" );
        }
        else
        {
            char* tmp = NULL;

            while ( fgets( line, MAX_LINE_SIZE, in ) )
            {
                tmp = strchr( line, '#' );

                if ( tmp )
                {
                    *tmp = '\0';
                }
                cmd_idx = 0;

                char* tok = NULL;
                char* nextTok = NULL;
                tok = strtok( line, " \r\n\t");

                while ( tok )
                {
                    if ( 0 == strcmp( tok, "crc" ) )
                    {
                        if ( cmd_idx < 2 )
                        {
                            fprintf( out, "command syntax error\n" );
                            break;
                        }
//                        unsigned short crc = crc16_ccitt( &cmd_buffer[2], cmd_idx - 2 );
//                        cmd_buffer[cmd_idx++] = ( unsigned char )( crc >> 8 );
//                        cmd_buffer[cmd_idx++] = ( unsigned char )( crc & 0xff );
                    }
                    else
                    {
                        sscanf( tok, "%x", &cmd_buffer[cmd_idx++] );
                    }

                    tok = strtok( NULL, " \r\n\t");
                }

                processCommand( dummyHost, cmd_buffer, cmd_idx, out,transmissionSize );
            }

            fclose( in );

        }
    }

    if ( outputSpecified )
        fclose( out );

    return 0;
}
