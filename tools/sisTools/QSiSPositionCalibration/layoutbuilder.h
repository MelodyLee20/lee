#ifndef LAYOUTBUILDER_H
#define LAYOUTBUILDER_H

#include "config.h"
#include "QList"


class layoutBuilder
{    
public:
    layoutBuilder();
    ~layoutBuilder();


    static QList<LineData*> buildLine();
    static QList<CircleData*> buildCircle_outside();
    static QList<RectData*> buildRect();
};

#endif // LAYOUTBUILDER_H
