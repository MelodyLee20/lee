#ifndef CONFIG
#define CONFIG


#define FUNCTION_BACK_MENU -100
#define FUNCTION_NONE -1
#define FUNCTION_OUTSIDE 13

#define FUNCTION_POINT 3
#define FUNCTION_ARC 4
#define FUNCTION_JITTER 5
#define FUNCTION_LINE 6

#define DELAY 500

enum LineDir
{
    HORIZONTAL,
    VERTICAL,
    DIAGONAL
};

enum Chiptype
{
    Single,
    Multi
};

struct LineData
{
    LineData() {}
    ~LineData() {}
    double id;
    double x;
    double y;
    double endX;
    double endY;
    double Diameter;
    double Tolerance;
    double Speed;
    double Lenth;
    LineDir direct;
    double getDx()
    {
        return Diameter + 2*Tolerance;
    }
};


struct CircleData
{
    CircleData() {}
    CircleData(double id,volatile double x,volatile double y,double d,double t)
    {
        this->id=id;
        this->centerX=x;
        this->centerY=y;
        this->diameter=d;
        this->tolerance=t;
    }

    ~CircleData() {}
    double id;
    volatile double centerX;
    volatile double centerY;
    double diameter;
    double tolerance;
    double getDx()
    {
        return diameter + 2*tolerance;
    }
};

struct RectData
{
    RectData() {}
    RectData(double id,double x,double y,double d,double t)
    {
        this->id=id;
        this->PosX=x;
        this->PosY=y;
        this->diameter=d;
        this->tolerance=t;
    }

    ~RectData() {}
    double id;
    double PosX;
    double PosY;
    double diameter;
    double tolerance;
};

#endif // CONFIG

