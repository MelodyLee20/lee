#include "filer.h"

#include <QFile>
#include <QDir>
#include <QDebug>

Filer::Filer()
{

}

Filer::~Filer()
{

}

bool
Filer::mkDir(const QString dir)
{
    QDir newDir(dir);

    if( !newDir.exists() )
    {
        if( !newDir.mkdir(dir) )
        {
            return false; // mkdir fail
        }

        /* Set Permissions 777 */
        bool ret = QFile::setPermissions(dir,
                              QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ExeOwner |
                              QFileDevice::ReadUser | QFileDevice::WriteUser | QFileDevice::ExeUser |
                              QFileDevice::ReadGroup | QFileDevice::WriteGroup | QFileDevice::ExeGroup |
                              QFileDevice::ReadOther | QFileDevice::WriteOther | QFileDevice::ExeOther );

        if(!ret)
        {
            return false; // chmod 777 fail
        }
    }
    else
    {
        return true; // already exist
    }

    return true;
}

bool
Filer::copyDir(const QString from, const QString to)
{
    /* Check if to is exist, or mkdir */
    bool ret = Filer::mkDir(to);
    if(!ret)
    {
        return false; // to is not exist or mkdir fail
    }

    /* Get sub-files list of from */
    QDir fromDir(from);
    QStringList fromFileList = fromDir.entryList(QDir::Files);
    for(int i = 0; i < fromFileList.size(); i++)
    {
        QString subFrom = from + "/" + fromFileList.at(i);
        QString subTo = to + "/" + fromFileList.at(i);

        /* copy sub-file */
        if( !Filer::copyFile(subFrom, subTo) )
        {
            return false; // copy file fail
        }
    }

    /* Get sub-dir list of from */
    QStringList fromDirList = fromDir.entryList(QDir::Dirs);
    for(int i = 0; i < fromDirList.size(); i++)
    {
        QString subFrom = from + "/" + fromDirList.at(i);
        QString subTo = to + "/" + fromDirList.at(i);

        /* copy sub-dir */
        if( !Filer::copyDir(subFrom, subTo) )
        {
            return false; // copy dir fail
        }
    }

    return true;
}

bool
Filer::copyFile(const QString from, const QString to, bool force)
{
    /* Check if to is exist */
    QFile toFile(to);
    if(toFile.exists())
    {
        /* already exist */
        if( force )
        {
            /* force to cover */
            toFile.remove();
        }
        else
        {
            return true;
        }
    }

    /* copy file */
    if(!QFile::copy(from, to))
    {
        qDebug() << "copy file fail";
        return false; // copy file fail
    }
    else
    {
        /* Set Permissions 777 */
        bool ret = QFile::setPermissions(to,
                              QFileDevice::ReadOwner | QFileDevice::WriteOwner | QFileDevice::ExeOwner |
                              QFileDevice::ReadUser | QFileDevice::WriteUser | QFileDevice::ExeUser |
                              QFileDevice::ReadGroup | QFileDevice::WriteGroup | QFileDevice::ExeGroup |
                              QFileDevice::ReadOther | QFileDevice::WriteOther | QFileDevice::ExeOther );

        if(!ret)
        {
            qDebug() << "chmod 777 fail";
            //return false; // chmod 777 fail
        }
    }

    return true;
}
