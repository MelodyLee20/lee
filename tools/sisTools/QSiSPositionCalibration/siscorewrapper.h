#ifndef SISCOREWRAPPER_H
#define SISCOREWRAPPER_H

#include "SiSCore.h"
#include "CptFactoryFacade.h"
#include "SiSCmdModuleException.h"
#include "SiSCoreException.h"
#include "SysDefine.h"
#include "HydraDefine.h"

#include <string>

using namespace SiS::Model;
using namespace SiS;

//using namespace std;

#define g_SiSCoreWrapper SiSCoreWrapper::instance()

#if defined(WIN32) || defined(_WIN32)

#define sis_sleep(v) \
do \
{ \
    Sleep(v); \
}while(0)

#else
#include <unistd.h>
#define sis_sleep(v) \
do \
{ \
    usleep(v*1000); \
}while(0)

#endif


class SiSCoreWrapper
{
public:
    SiSCoreWrapper();
    ~SiSCoreWrapper();
    static SiSCoreWrapper& instance();
    SiSCore* openDevice();
    void closeDevice();
    bool reportToOS(bool enable);
    bool cmd88Write(unsigned int addr, BYTE* writeData);
    bool cmd86Read(unsigned int addr, unsigned char* readData, unsigned int readLength);
    bool updateFw(unsigned int addr, BYTE *_updateData, int len);
    bool simpleRW88Write(unsigned char *reportBuffer, unsigned char size);
    std::string getFwGeneration(void);
private:
    SiSCore* m_core;
    std::string m_fwGeneration;
};

#endif // SISCOREWRAPPER_H
