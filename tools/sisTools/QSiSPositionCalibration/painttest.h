#ifndef PAINTTEST_H
#define PAINTTEST_H

#include "ipainttest.h"

class PaintTimer;

class PaintTest:public IpaintTest
{
public:
    PaintTest();
    ~PaintTest();

    void paint(QPainter *painter, QPaintEvent *event);

    QBrush m_background;
    QList<QColor> m_penColor;
    PaintTimer* m_paintTimer;
    QFont font;
    QFile m_fileptr;
    int i;
};

#endif // PAINTTEST_H
