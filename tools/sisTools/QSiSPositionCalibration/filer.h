#ifndef FILER_H
#define FILER_H

#include <QString>

class Filer
{
public:
    Filer();
    ~Filer();

    static bool mkDir(const QString dir);
    static bool copyDir(const QString from, const QString to);
    static bool copyFile(const QString from, const QString to, bool force = false);
};

#endif // FILER_H
