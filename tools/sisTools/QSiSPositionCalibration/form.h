#ifndef FORM_H
#define FORM_H
#include <QObject>
#include "mainwindow.h"
#include "dialog.h"

class form : public QObject
{
    Q_OBJECT
public:
    form();
    ~form();

    void showMenu();
    dialog m_dia;
    MainWindow m_mainwin;

public slots:
    void backMenu();
    void autoUpdate();
    void functionChoiced(int id);

private:

};

#endif // FORM_H
