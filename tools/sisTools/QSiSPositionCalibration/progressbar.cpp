#include "progressbar.h"

ProgressBar::ProgressBar(QWidget *parent)
    : QProgressDialog(parent)
{

}

ProgressBar::~ProgressBar()
{

}

void ProgressBar::stepOne()
{
    if(this->value() + 1 <= this->maximum())
    {
        this->setValue(this->value() + 1);
    }
    else
    {
        this->setValue(this->minimum());
    }
}
