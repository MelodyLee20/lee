#ifndef ASSETSCOPIER_H
#define ASSETSCOPIER_H

#include <QString>

#ifdef ANDROID
    #define APP_HOME_PATH     ((QString) "/data/data/com.sis.QPJT/assets")
#else
    #define APP_HOME_PATH     qApp->applicationDirPath()
#endif


class AssetsCopier
{
public:
    AssetsCopier();
    ~AssetsCopier();

    static bool copyAssets(QString toPath);
};

#endif // ASSETSCOPIER_H
