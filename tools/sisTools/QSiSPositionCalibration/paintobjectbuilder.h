#ifndef PAINTOBJECTBUILDER_H
#define PAINTOBJECTBUILDER_H

#include "ipainttable.h"
#include <QList>

class paintObjectBuilder:public IpaintTable
{
public:
    paintObjectBuilder();
    ~paintObjectBuilder();

    static QList<IpaintTable*> buildPaintObject(int functionID);

    //static QList<IpaintTable*> buildPoint();
    //static QList<IpaintTable*> buildArc();
    //static QList<IpaintTable*> buildJitter();
    static QList<IpaintTable*> buildOutside();
    static QList<IpaintTable*> buildCircle();
    static QList<IpaintTable*> buildRect();
};

#endif // PAINTOBJECTBUILDER_H
