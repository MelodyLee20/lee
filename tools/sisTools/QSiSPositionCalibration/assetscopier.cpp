#include "assetscopier.h"
#include "filer.h"

#include <QDir>

AssetsCopier::AssetsCopier()
{

}

AssetsCopier::~AssetsCopier()
{

}

bool
AssetsCopier::copyAssets(QString toPath)
{
    /* check if toPath exist */
    if(!Filer::mkDir(toPath))
    {
        return false;
    }

    QString assetsDir = "assets:";
    QDir fromDir(assetsDir);

    /* Get sub-files list of assetsDir */
    QStringList fromFileList = fromDir.entryList(QDir::Files);

    for(int i = 0; i < fromFileList.size(); i++)
    {
        QString subFrom = assetsDir + "/" + fromFileList.at(i);
        QString subTo = toPath + "/" + fromFileList.at(i);

        /* copy sub-file */
        if(!Filer::copyFile(subFrom, subTo))
        {
            return false; // copy file fail
        }
    }

    /* Get sub-dir list of assetsDir */
    QStringList fromDirList = fromDir.entryList(QDir::Dirs);

    for(int i = 0; i < fromDirList.size(); i++)
    {
        QString subFrom = assetsDir + "/" + fromDirList.at(i);
        QString subTo = toPath + "/" + fromDirList.at(i);

        /* copy sub-dir */
        if(!Filer::copyDir(subFrom, subTo))
        {
            return false; // copy dir fail
        }
    }
    
    
    
    return true;
}
