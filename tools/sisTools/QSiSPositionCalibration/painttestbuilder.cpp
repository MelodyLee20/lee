#include "painttestbuilder.h"
#include "paintobjectbuilder.h"
#include "ipainttest.h"
#include <QDebug>
#include "painttest.h"

paintTestBuilder::paintTestBuilder()
{

}

paintTestBuilder::~paintTestBuilder()
{

}

IpaintTest* paintTestBuilder::buildPaintTest(int functionID)
{
    IpaintTest* paintTest = new PaintTest();
    paintTest->setfunctionID(functionID);
    paintTest->setpaintObject(paintObjectBuilder::buildPaintObject(functionID));

    return paintTest;
}
