#include "pathmanager.h"
#include <QApplication>

#define ASSETS_PATH "/data/data/com.sis.QPJT/assets"

PathManager::PathManager()
{

}

QString PathManager::getPath()
{
#ifdef ANDROID
    return ASSETS_PATH;
#endif
#ifdef WIN32
    return QApplication::applicationDirPath();
#endif
}

QString PathManager::getFilePath(QString fileName)
{
#ifdef ANDROID
    return getPath() + "/" + fileName;
#endif
#ifdef WIN32
    return getPath() + "\\" + fileName;
#endif
}

QString PathManager::getFilePath(QString subDirName, QString fileName)
{
#ifdef ANDROID
    return getPath() + "/" + subDirName + "/" + fileName;
#endif
#ifdef WIN32
    return getPath() + "\\" + subDirName + "\\" + fileName;;
#endif
}
