#ifndef TOUCHDATA_H
#define TOUCHDATA_H

#include <QTouchEvent>

class TouchData
{
public:
    TouchData();
    ~TouchData();

    static TouchData& instance()
    {
        static TouchData instance;
        return instance;
    }

    void handleTouchEvent(QEvent *event);

    void setCTPs(const QList<QTouchEvent::TouchPoint> &CTPs);
    void setTouchState(QEvent::Type type);
    QList<QTouchEvent::TouchPoint> getCTPs() const;
    QEvent::Type getTouchState();
    void clearTP();

    void handleMouseEvent(QEvent *event);
private:
    QList<QTouchEvent::TouchPoint> m_CTPs;
    QEvent::Type m_touchState;
};

#endif // TOUCHDATA_H
