#ifndef PATHMANAGER_H
#define PATHMANAGER_H

#include <QString>

class PathManager
{
public:
    PathManager();
    static QString getPath();
    static QString getFilePath(QString fileName);
    static QString getFilePath(QString subDirName, QString fileName);
};

#endif // PATHMANAGER_H
