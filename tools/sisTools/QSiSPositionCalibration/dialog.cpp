#include "dialog.h"
#include "ui_dialog.h"
#include <QDebug>
#include "config.h"
#include <QFile>
#include "parameter.h"
#include <QProcess>
#include <QMessageBox>
//#include "rwdata.h"
#include "updatebar.h"
#include "config.h"

//#include "SisBaseException.h"
#include "updatebar.h"

#include <stdexcept>

#include "SiSCore.h"
#include "CptFactoryFacade.h"
#include "SiSCmdModuleException.h"
#include "SiSCoreException.h"
#include "SysDefine.h"
#include "HydraDefine.h"

#include "siscorewrapper.h"
#ifdef INNER_ALG
    //#include "algorithm.h"
    #include "algononoutside.h"
#endif
#include "pathmanager.h"
#include "paintbackground.h"

#ifdef SISCORE_USE
    #define MAX_TRANSMISSION_SIZE 64
    enum ReturnValue
    {
        RetPass = 0,
        RetFail = -1,
        RET_PARAMETER_ERROR = -2,
        RET_FW_FILE_ERROR = -3,
        RET_FW_IO_ERROR = -4,
        RET_FW_ROM_ERROR = -5,
        RET_USER_STOP = -6,
        RET_CRC_MISMATCH = -7,
    };
    enum ERR_NO
    {
        OK = 0,
        ERR = -1,
        ERR_NACK = -2, // receive 0x0040
        ERR_CMD_NOT_SUPPORT = -3,
        ERR_84_FFFF = -4,
    };
#else
    #include "USBIO.h"
#endif

using namespace SiS::Model;
using namespace SiS;

dialog::dialog(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::dialog)
{
    ui->setupUi(this);
    this->setStyleSheet("background-color:rgb(236,255,255)");
    ui->outside->setStyleSheet("QPushButton{background-color:rgb(200,200,0);border: 3px solid rgb(200,200,0);border-radius:16px}");
    ui->parseOutside->setStyleSheet("QPushButton{background-color:rgb(255,80,40);border: 3px solid rgb(255,80,40);border-radius:16px}");
    ui->Exit->setStyleSheet("QPushButton{background-color:rgb(111,183,183);border: 3px solid rgb(111,183,183);border-radius:16px}");
#ifdef MAXELL
    ui->outside->setHidden(true);
    ui->parseOutside->setHidden(true);
    ui->groupBox_2->resize(ui->groupBox_2->width(), ui->groupBox_2->height()/3);
    this->resize(this->width() , this->height()/3);
    ui->Exit->setGeometry(ui->outside->geometry());
#endif
}

dialog::~dialog()
{
    delete ui;
}

int dialog::convertToFunctionID(QString text)
{
    int id = FUNCTION_NONE;

    if(text == "outside")
    {
        id = FUNCTION_OUTSIDE;
    }
    return id;
}

void dialog::on_outside_clicked()
{
    bOK = SIS_setCoordinateParam( 1, 0, 0, 1, 0, 0, 2, 0, 0);
    if ( bOK==0 )
    {
        printf( "update sucess!" );
        //PrintBuffer( DeviceOutput+rPOS, rLEN );
        printf("\n");
    }
    else
    {
        printf("update fail!\n\n");
    }

    QString text = qobject_cast<QPushButton*>(sender())->objectName();
    int id = convertToFunctionID(text);
    emit functionChoice(id);
}

void dialog::InitialP()
{
    UpdateBar* bar = new UpdateBar();
    bar->setMessage("Initial parameter , don't touch , thank you");
    bar->setWindowSize(500, 20);
    bar->start();

    bOK = SIS_setCoordinateParam( 1, 0, 0, 1, 0, 0, 2, 0, 0);
    if ( bOK==0 )
    {
        printf( "update sucess!" );
        //PrintBuffer( DeviceOutput+rPOS, rLEN );
        printf("\n");
    }
    else
    {
        printf("update fail!\n\n");
    }

#ifdef SISCORE_USE

//    if ( !g_SiSCoreWrapper.openDevice() )
//    {
//        qDebug() << "SiSCore_CoreBoot fail~!!";
//        //g_SiSCoreWrapper.closeDevice();
//    }

//    if ( !g_SiSCoreWrapper.reportToOS(false))
//    {
//        qDebug() << "SiSCore_ReportToOS(false) fail~!!";
//        //g_SiSCoreWrapper.closeDevice();
//    }

    //bool ret = g_SiSCoreWrapper.writeSwapAndInverse(0x0a, PARA.paraInvx(), PARA.paraInvy(), PARA.paraSwap(), 0xf0);
    //bool ret = call_88_for_swap_inverse(INVALID_HANDLE_VALUE, 0x0a, PARA.paraInvx(), PARA.paraInvy(), PARA.paraSwap(), 0xf0);
    bool ret = true;
#ifdef SWAP_INVERSE
    ret = call_88_for_swap_inverse(0x0a, PARA.paraInvx(), PARA.paraInvy(), PARA.paraSwap(), 0xf0);
#endif

    if ( !ret )
    {
        qDebug() << "call_88_for_swap_inverse fail~!!";
        //g_SiSCoreWrapper.closeDevice();
    }
    else
    {
        qDebug() << "call_88_for_swap_inverse success~!!";
    }

//    if ( !g_SiSCoreWrapper.reportToOS(true) )
//    {
//        qDebug() << "SiSCore_ReportToOS(true) fail~!!";
//        //g_SiSCoreWrapper.closeDevice();
//    }

//    g_SiSCoreWrapper.closeDevice();

#else
    HANDLE deviceHandle = INVALID_HANDLE_VALUE;
    deviceHandle = IO_OpenDevice(NOT_SET);

    if (!IO_ReportToOS(deviceHandle, false))
    {
        CloseHandle(deviceHandle);
        qDebug()<<"IO_ReportToOS fail~!!";
    }

    bool bSuc = call_88_for_swap_inverse(deviceHandle, 0x0a, PARA.paraInvx(), PARA.paraInvy(), PARA.paraSwap(), 0xf0);
//    bSuc = call_88_for_swap_inverse(deviceHandle, 0, 0, 0, 0, 0xf0);

    if ( !bSuc )
    {
        qDebug()<<"call_88_for_swap_inverse fail~!!";
    }
    else
    {
        qDebug()<<"call_88_for_swap_inverse success~!!";
    }

    if (!IO_ReportToOS(deviceHandle, true))
    {
        CloseHandle(deviceHandle);
    }

    CloseHandle(deviceHandle);
#endif

    delete bar;
}

void dialog::on_parseOutside_clicked()
{
    int recieve_err=0;
    double check[30];
    myfile.setFileName(PathManager::getFilePath("pattern/trainTargetX.txt"));
    if(myfile.open(QIODevice::ReadOnly))
    {
        i=0;
        betaNum=0;
        QTextStream stream(&myfile);
        while(!stream.atEnd())
        {
            stream>>check[i++];
            betaNum++;
        }
        myfile.close();
    }

    if(PARA.getPointMode() == 1)
    {
#ifdef ANDROID
        if(betaNum<10-1)
#else
        if(betaNum<10)
#endif
            recieve_err=2;
    }
    else if(PARA.getPointMode() == 2)
    {
#ifdef ANDROID
        if(betaNum<5-1)
#else
        if(betaNum<5)
#endif
            recieve_err=2;
    }

    if(!recieve_err)
    {
        char strFile[512];
        QString commandStr = "";
        QProcess *process = new QProcess();

        if(PARA.getPointMode() == 1)
        {
            sprintf(strFile, "CoordCali_NonOutside_1050516.exe\t%d", 3);
            //sprintf(strFile, "%s\\CoordCali_NonOutside_1050516.exe\t%d", QCoreApplication::applicationDirPath().toStdString().c_str(), 3);
            qDebug()<<"strFile is "<<strFile;
            commandStr = QString("CoordCali_NonOutside_1050516.exe\t%1").arg(3);
        }
        else if(PARA.getPointMode() == 2)
        {
            sprintf(strFile, "CoordCali_NonOutside_1050516.exe\t%d", 1);
            //sprintf(strFile, "%s\\CoordCali_NonOutside_1050516.exe\t%d", QCoreApplication::applicationDirPath().toStdString().c_str(), 1);
            qDebug()<<"strFile is "<<strFile;
            commandStr = QString("CoordCali_NonOutside_1050516.exe\t%1").arg(1);
        }

        qDebug()<<"command is "<<commandStr;
#ifdef INNER_ALG
        ALG.run(PARA.getPointMode());
#else
        process->execute(commandStr, QStringList());
        //process->start(commandStr, QStringList());
        //process->startDetached(commandStr, QStringList());
#endif
    }

    UpdateBar *bar=new UpdateBar();
    //bar->setMessage("Updating , don't worry~");
    bar->setMessage("Updating...");
    bar->start();
    sis_sleep(DELAY);

#ifdef WRITE_BIN
    Device = IO_OpenDevice(NOT_SET);

    if(Device == INVALID_HANDLE_VALUE)
    {
        throw SisBaseException("Can't open SiS device", RetFail);
    }

    if(false == IO_ReportToOS(Device, false))
    {
        CloseHandle(Device);
        throw SisBaseException("Stop report to OS fail", RET_FW_IO_ERROR);
    }

    bSus = false;
    bSus = ReadFirmwareMem( Device, rADDR , rLEN, &nByteCntRead, DeviceOutput, FALSE );
    if ( bSus )
    {
        printf( "ROM = sucess" );
        //PrintBuffer( DeviceOutput+rPOS, rLEN );
        printf("\n");
    }
    else
    {
        recieve_err=1;
        printf("ROM recieve fail\n\n");
    }

    bFileName = "ROM_BE_C000.bin";
    //fp.open(QIODevice::WriteOnly);
    fp.setFileName(bFileName);

    if (!fp.open(QIODevice::WriteOnly))
    {
        recieve_err=1;
        printf( "Bin file not found\n\n");
    }
    else
    {
        QDataStream stream(&fp);
        i=0;
        while(!(i>(rLEN-1)))
            stream<<*(DeviceOutput+(i++));
        fp.close();
    }
    if(false == IO_ReportToOS(Device, true))
    {
        CloseHandle(Device);
        throw SisBaseException("Start report to OS fail", RET_FW_IO_ERROR);
    }

    IO_Reset(Device);
    Sleep(DELAY*6);
    CloseHandle(Device);
#endif

    //-----------------------------WRITE-------------------------------//
#ifdef INNER_ALG
//    result[0] = *ALG.result;
//    result[1] = *(ALG.result+1);
//    result[2] = *(ALG.result+3);
//    result[3] = *(ALG.result+4);
//    result[4] = *(ALG.result+2);
//    result[5] = *(ALG.result+5);
//    result[6] = *(ALG.result+6);
//    result[7] = *(ALG.result+7);

    result[0] = *ALG.result;
    result[1] = *(ALG.result+1);
    result[2] = *(ALG.result+2);
    result[3] = *(ALG.result+3);
    result[4] = *(ALG.result+4);
    result[5] = *(ALG.result+5);
    result[6] = *(ALG.result+6);
    result[7] = *(ALG.result+7);
#else
    myfile.setFileName(PathManager::getFilePath("output/result.txt"));
    if(myfile.open(QIODevice::ReadOnly))
    {
        i=0;
        QTextStream stream(&myfile);
        while(!stream.atEnd())
        {
            stream>>*(result+(i++));
        }
        myfile.close();
    }

#endif

    qDebug()<<"result :"<<result[0]<<","<<result[1]<<","<<result[2]<<","<<result[3]<<","<<result[4]<<","<<result[5]<<","<<result[6]<<","<<result[7];

    bOK = SIS_setCoordinateParam( result[0], result[3] , result[1], result[4], result[2], result[5], 2, result[6], result[7]);
    if ( bOK==0 )
    {
        printf( "update sucess!" );
        //PrintBuffer( DeviceOutput+rPOS, rLEN );
        printf("\n");
    }
    else
    {
        recieve_err=1;
        printf("update fail!\n\n");
    }


#ifdef WRITE_BIN
    Device = IO_OpenDevice(NOT_SET);

    if(Device == INVALID_HANDLE_VALUE)
    {
        throw SisBaseException("Can't open SiS device", RetFail);
    }

    if(false == IO_ReportToOS(Device, false))
    {
        CloseHandle(Device);
        throw SisBaseException("Stop report to OS fail", RET_FW_IO_ERROR);
    }

    bSus = false;
    bSus = ReadFirmwareMem( Device, rADDR , rLEN, &nByteCntRead, DeviceOutput, FALSE );
    if ( bSus )
    {
        printf( "ROM = sucess" );
        //PrintBuffer( DeviceOutput+rPOS, rLEN );
        printf("\n");
    }
    else
    {
        recieve_err=1;
        printf("ROM recieve fail\n\n");
    }

    bFileName = "ROM_AF_C000.bin";
    //fp.open(QIODevice::WriteOnly);
    fp.setFileName(bFileName);

    if (!fp.open(QIODevice::WriteOnly))
    {
        recieve_err=1;
        printf( "Bin file not found\n\n");
    }
    else
    {
        QDataStream stream(&fp);
        i=0;
        while(!(i>(rLEN-1)))
            stream<<*(DeviceOutput+(i++));
        fp.close();
    }
    if(false == IO_ReportToOS(Device, true))
    {
        CloseHandle(Device);
        throw SisBaseException("Start report to OS fail", RET_FW_IO_ERROR);
    }

    IO_Reset(Device);
    Sleep(DELAY*6);
    CloseHandle(Device);
#endif

//-----------------------------INITIAL OUTSIDE-------------------------------//

#ifdef SISCORE_USE
    //QMessageBox::information(this , "Debug Info", "on_parseOutside_clicked()");

    if( !g_SiSCoreWrapper.openDevice() )
    {
        //throw SisBaseException("Can't open SiS device", RetFail);
        qDebug() << "Can't open SiS device !!";
    }

    //-----------------------------READ-------------------------------//

    if ( !g_SiSCoreWrapper.reportToOS(false))
    {
        //g_SiSCoreWrapper.closeDevice();
        //throw SisBaseException("Stop report to OS fail", RET_FW_IO_ERROR);
        qDebug() << "SiSCore_ReportToOS(false) fail !!";
    }

    unsigned int addr = rADDR;
    if(g_SiSCoreWrapper.getFwGeneration() == "819")
        addr = rADDR819;

    bOK = false;
    bOK = g_SiSCoreWrapper.cmd86Read(addr, DeviceOutput, rLEN);
    if ( bOK )
    {
        printf( "ROM = success" );
        printf("\n");
    }
    else
    {
        printf("initial ROM recieve fail\n\n");
    }

    unsigned char allRom[ALLROMSIZE];
    bOK = g_SiSCoreWrapper.cmd86Read(0xa0000000, allRom, ALLROMSIZE);
    if ( bOK )
    {
        printf( "ROM = success" );
        printf("\n");
    }
    else
    {
        printf("initial ROM recieve fail\n\n");
    }

    if ( !g_SiSCoreWrapper.reportToOS(true) )
    {
        //g_SiSCoreWrapper.closeDevice();
        //throw SisBaseException("Start report to OS fail", RET_FW_IO_ERROR);
        qDebug() << "SiSCore_ReportToOS(true) fail~!!";
    }

    //-----------------------------WRITE-------------------------------//

    *(DeviceOutput+0x23c)=PARA.paraSwap();//swap
    *(DeviceOutput+0x246)=PARA.paraInvx();//invx
    *(DeviceOutput+0x247)=PARA.paraInvy();//invy

    for(int i = 0x4000; i < rLEN; i++)
        *(allRom + i) =  *(DeviceOutput + i);

    //bSuc = g_SiSCoreWrapper.updateFw(wBlockAddr, DeviceOutput, wLEN);
    bSuc = g_SiSCoreWrapper.updateFw(addr, allRom, wLEN);

    if (bSuc) {
        printf("Write firmware memory successfully.\n");
        printf("\n");
    }else{
        printf("Write firmware memory failed.\n");
    }

    sis_sleep(DELAY*6);
    g_SiSCoreWrapper.closeDevice();

#else
    Device = IO_OpenDevice(NOT_SET);

    if(Device == INVALID_HANDLE_VALUE)
    {
        throw SisBaseException("Can't open SiS device", RetFail);
    }

    if(false == IO_ReportToOS(Device, false))
    {
        CloseHandle(Device);
        throw SisBaseException("Stop report to OS fail", RET_FW_IO_ERROR);
    }

    //-----------------------------READ-------------------------------//
    bOK = false;
    bOK = ReadFirmwareMem( Device, rADDR , rLEN, &nByteCntRead, DeviceOutput, FALSE );
    if ( bOK )
    {
        printf( "ROM = sucess" );
        printf("\n");
    }
    else
    {
        printf("initial ROM recieve fail\n\n");
    }

    //-----------------------------WRITE-------------------------------//
    *(DeviceOutput+0x23c)=PARA.paraSwap();//swap
    *(DeviceOutput+0x246)=PARA.paraInvx();//invx
    *(DeviceOutput+0x247)=PARA.paraInvy();//invy

    bSuc = Sub_BlockWriteMemBuffTo810(Device, wBlockAddr, DeviceOutput, wLEN);

    if (bSuc) {
        printf("Write firmware memory successfully.\n");
        printf("\n");
    }else{
        printf("Write firmware memory failed.\n");
    }

    if(false == IO_ReportToOS(Device, true))
    {
        CloseHandle(Device);
        throw SisBaseException("Start report to OS fail", RET_FW_IO_ERROR);
    }

    IO_Reset(Device);
    Sleep(DELAY*6);
    CloseHandle(Device);
#endif

    qDebug()<<"swap="<< *(DeviceOutput+0x23c);
    qDebug()<<"invx="<< *(DeviceOutput+0x246);
    qDebug()<<"invy="<< *(DeviceOutput+0x247);

    //-----------------------------INITIAL OUTSIDE-END---------------------------//

    delete bar;

    if(recieve_err==1)
        QMessageBox::warning(this,tr("Message Box"),tr("Update FW FAIL !!!"));
    else if(recieve_err==2)
        QMessageBox::warning(this,tr("Message Box"),tr("No Data !!!"));
    else
        QMessageBox::warning(this,tr("Message Box"),tr("Update FW OK !!!"));
}

bool dialog::ReadFirmwareMem( unsigned int uREADAddr, const unsigned int uMemBuffSize,
    unsigned short *FwMemByteIdx, unsigned char* paFwMemBuff, bool bPrintData )
{
    int memRetryBlock = 3;
    //UNREFERENCED_PARAMETER( bPrintData );
    bool bSuccess = false;
    unsigned int ReadIdx  = 0;
    unsigned int ReadAddr = uREADAddr;
    unsigned int errorNum = 0;
    unsigned char InReportBuffer[MAX_TRANSMISSION_SIZE*10];

    //DeviceType type = IO_GetDeviceType();
    //DWORD in_len;
    *FwMemByteIdx = 0;

    int isVirtualAddr = ( ( ( uREADAddr >> 28 ) & 0xf ) == 0x5 );

    //while( ReadIdx < uMemBuffSize && errorNum < ERROR_ENDURE )
    while( ReadIdx < uMemBuffSize && errorNum < memRetryBlock )
    {
        unsigned int nBytes = 0;

#ifdef SISCORE_USE
        bSuccess = g_SiSCoreWrapper.cmd86Read(ReadAddr, InReportBuffer, uMemBuffSize);
//        qDebug() << "819: ReadFirmwareMem()";
//        if (nBytes%4 !=0)
//        {
//            printf("The byte number of data isn't a multiple of 4.\n");
//        }

//        in_len = ( ReadIdx + nBytes > uMemBuffSize ) ? ( uMemBuffSize - ReadIdx ) : nBytes;

//        //TODO : replace by memcpy(paFwMemBuff+ReadIdx+i, InReportBuffer+i, in_len);
//        for( unsigned int i = 0; i < nBytes && i < in_len; i++ )
//            paFwMemBuff[ReadIdx+i]  = InReportBuffer[i];

//        if(ret)
//        {
//            bSuccess = TRUE;
//            errorNum = 0;
//            ReadIdx  += nBytes;

//            if ( isVirtualAddr )
//                ReadAddr += nBytes/4;
//            else
//                ReadAddr += nBytes;
//        }
//        else
//        {
//            bSuccess = FALSE;
//            errorNum++;
//            printf("Read %08X Error(%d)..., Retry!!!\n", ReadAddr, errorNum);
//        }
#else
        int result = IO_ReadData( Device, ReadAddr, InReportBuffer, &nBytes );
        if (nBytes%4 !=0)
        {
            printf("The byte number of data isn't a multiple of 4.\n");
        }

        in_len = ( ReadIdx + nBytes > uMemBuffSize ) ? ( uMemBuffSize - ReadIdx ) : nBytes;

        //TODO : replace by memcpy(paFwMemBuff+ReadIdx+i, InReportBuffer+i, in_len);
        for( unsigned int i = 0; i < nBytes && i < in_len; i++ )
            paFwMemBuff[ReadIdx+i]  = InReportBuffer[i];

        if(result == OK)
        {
            bSuccess = TRUE;
            errorNum = 0;
            ReadIdx  += nBytes;

            if ( isVirtualAddr )
                ReadAddr += nBytes/4;
            else
                ReadAddr += nBytes;
        }
        else
        {
            bSuccess = FALSE;
            errorNum++;
            printf("Read %08X Error(%d)..., Retry!!!\n", ReadAddr, errorNum);
        }
        *FwMemByteIdx = ( unsigned short )ReadIdx;
#endif
    }

    return bSuccess;
}

bool dialog::Sub_BlockWriteMemBuffTo810( unsigned int p_nAddress, unsigned char* pBuff, unsigned int nByteCnt, bool check_ack , bool isRecal)
{
    bool bSuccess = false;
    int nBlockCnt;
    int wrtIndex;
    int result;
    unsigned char bytePerLine;
    unsigned short nLineCnt;
    unsigned int flashWrtUnit;
    unsigned int wrtSize, remainSize;
    unsigned int currLen, writeAddr;
    int wrtErr = 0;
    int memRetryTotal = 3;
    int memRetryBlock = 3;

#ifdef SISCORE_USE
    flashWrtUnit = 12 * 1024;
    bytePerLine = 12;
#else
    IO_GetUpdateFwUnit(&flashWrtUnit, &bytePerLine);
#endif
    while(( wrtErr++ < memRetryTotal) && nByteCnt)
    {
        wrtIndex = 0;

        bSuccess = false;
        currLen = ( nByteCnt > flashWrtUnit) ? flashWrtUnit : nByteCnt;
        nBlockCnt = GaussianUpper( currLen, flashWrtUnit );
        int progErr;
        progErr = 0;

        while( nBlockCnt && (progErr < memRetryBlock) )
        {
            qDebug()<<"nBlockCnt="<<nBlockCnt<<endl;

            remainSize = nByteCnt - wrtIndex;
            wrtSize = (remainSize > flashWrtUnit) ? flashWrtUnit : remainSize;
            //printf("WrtSize=%d\n", wrtSize);
            nLineCnt = ( unsigned short )GaussianUpper( wrtSize, bytePerLine );
            writeAddr = p_nAddress + wrtIndex;
            printf( "\nWriteAddr=%08X nBlocks=%d\n", writeAddr, nLineCnt );

#ifndef SISCORE_USE
            result = IO_UpdateFwInfo( Device, writeAddr, nLineCnt , isRecal);
#endif
            printf("IO_UpdateFwInfo=%d\n", result);
            bSuccess = ( result == OK );

            if( !bSuccess )
            {
                printf("Write info, NG!\n");
                progErr++;
                continue;
            }

            for( unsigned short i = 0; i < nLineCnt && bSuccess; i++ )
            {
                unsigned int remainPending = remainSize - i*bytePerLine;
                unsigned char pendingLen = (remainPending>bytePerLine)?bytePerLine:remainPending;
#ifndef SISCORE_USE
                result = IO_UpdateFwData( Device, &pBuff[wrtIndex + i*bytePerLine], pendingLen, i );
#endif
                printf(".");

                // some PC need delay to avoid UpdateFwData command no response
                if (i%40==0) {
                    sis_sleep(10);//to avoid 84 cmd fail at sometimes
                }
                bSuccess &= ( result == OK );
            }

            sis_sleep(10);//to avoid 84 cmd fail at sometimes

            if( bSuccess )
            {
                printf( "\nWrite all %d blocks ...", nLineCnt );
                progErr = 0;
#ifndef SISCORE_USE
                result = IO_UpdateFwDataFinish_9200( Device );
#endif
                bSuccess &= ( result == OK );

                if ( check_ack )
                {
                    qDebug()<<"before bSuccess="<<bSuccess<<endl;
                    sis_sleep(1000);
#ifndef SISCORE_USE
                    result = IO_WaitReady( Device );
#endif
                    sis_sleep(10);
                    bSuccess &= ( result == OK );
                    qDebug()<<"after bSuccess="<<bSuccess<<endl;
                    if ( false == bSuccess )
                    {
                        progErr++;
                    }
                }
            }
            else
            {
                progErr++;
            }

            if( bSuccess )
            {
                nBlockCnt--;
                wrtIndex += flashWrtUnit;
            }
        } // end of while(nBlockCnt && (progErr < memRetryBlock))

        if(bSuccess)
        {
            p_nAddress += currLen;
            pBuff += currLen;
            nByteCnt -= currLen;
            wrtErr = 0;
            printf(" Successfully\n");
        }
    }
    return bSuccess;
}

int dialog::GaussianUpper(int size, int unit)
{
    return (size + unit -1)/unit;
}

void dialog::exitUpdateFW()
{
    recieve_err=0;

#ifdef INNER_ALG
    betaNum = ALG.getBetaNum();

//    QMessageBox msgBox;
//    msgBox.setText(QString("betaNum: %1").arg(betaNum));
//    msgBox.exec();

    if(betaNum < 1)
    {
        //ALG.data_err=1;
        printf("no data!!\n");
        //system("pause");
    }
#else
    double check[30];
    myfile.setFileName(PathManager::getFilePath("pattern/trainTargetX.txt"));
    if(myfile.open(QIODevice::ReadOnly))
    {
        i=0;
        betaNum=0;
        QTextStream stream(&myfile);
        while(!stream.atEnd())
        {
            stream>>check[i++];
            betaNum++;
        }
        myfile.close();
    }
#endif

    if(PARA.getPointMode() == 1)
    {
        if(betaNum < 10-1)
            recieve_err=2;
    }
    else if(PARA.getPointMode() == 2)
    {
        if(betaNum < 5-1)
            recieve_err=2;
    }

    if(!recieve_err)
    {
#ifdef INNER_ALG
        int mode = 1;
        if(PARA.getPointMode() == 1)
            mode = 3;
        if(PARA.getPointMode() == 2)
            mode = 1;
        ALG.run(mode);
#else
        char strFile[512];
        QString commandStr = "";
        QProcess *process = new QProcess();

        if(PARA.getPointMode() == 1)
        {
            sprintf(strFile, "%s\\CoordCali_NonOutside_1050516.exe\t%d", QCoreApplication::applicationDirPath().toStdString().c_str(), 3);
            qDebug()<<"strFile is "<<strFile;
            commandStr = QString("CoordCali_NonOutside_1050516.exe\t%1").arg(3);
            commandStr = QString("CoordCali_NonOutside_AVG1041203.exe\t%1").arg(3);
        }
        else if(PARA.getPointMode() == 2)
        {
            sprintf(strFile, "%s\\CoordCali_NonOutside_1050516.exe\t%d", QCoreApplication::applicationDirPath().toStdString().c_str(), 1);
            qDebug()<<"strFile is "<<strFile;
            commandStr = QString("CoordCali_NonOutside_1050516.exe\t%1").arg(1);
            commandStr = QString("CoordCali_NonOutside_AVG1041203.exe\t%1").arg(1);
        }
        qDebug()<<"command is "<<commandStr;
        //process->execute(commandStr, QStringList());
        //process->start(commandStr, QStringList());
        //process->startDetached(commandStr, QStringList());
        process->setProgram(commandStr);
        process->start();
#endif
    }

#ifdef ANDROID
    ui->Exit->setText("Updating FW..., please wait !!");
    QMessageBox msgBox;
    msgBox.setText(QString("Please touch any place to continue !!"));
    msgBox.exec();

#else
    this->hide();
    UpdateBar *bar=new UpdateBar();
    //bar->setMessage("Updating , don't worry~");
    bar->setMessage("Updating.................................");
    bar->start();
#endif

    sis_sleep(DELAY);
#ifdef WRITE_BIN
    Device = IO_OpenDevice(NOT_SET);

    if(Device == INVALID_HANDLE_VALUE)
    {
        throw SisBaseException("Can't open SiS device", RetFail);
    }

    if(false == IO_ReportToOS(Device, false))
    {
        CloseHandle(Device);
        throw SisBaseException("Stop report to OS fail", RET_FW_IO_ERROR);
    }

    bSus = false;
    bSus = ReadFirmwareMem( Device, rADDR , rLEN, &nByteCntRead, DeviceOutput, FALSE );
    if ( bSus )
    {
        printf( "ROM = sucess" );
        //PrintBuffer( DeviceOutput+rPOS, rLEN );
        printf("\n");
    }
    else
    {
        recieve_err=1;
        printf("ROM recieve fail\n\n");
    }

    bFileName = "ROM_BE_C000.bin";
    //fp.open(QIODevice::WriteOnly);
    fp.setFileName(bFileName);

    if (!fp.open(QIODevice::WriteOnly))
    {
        recieve_err=1;
        printf( "Bin file not found\n\n");
    }
    else
    {
        QDataStream stream(&fp);
        i=0;
        while(!(i>(rLEN-1)))
            stream<<*(DeviceOutput+(i++));
        fp.close();
    }
    if(false == IO_ReportToOS(Device, true))
    {
        CloseHandle(Device);
        throw SisBaseException("Start report to OS fail", RET_FW_IO_ERROR);
    }

    IO_Reset(Device);
    Sleep(DELAY*6);
    CloseHandle(Device);
#endif

    //-----------------------------WRITE-------------------------------//
#ifdef INNER_ALG
    result[0] = *ALG.result;
    result[1] = *(ALG.result+1);
    result[2] = *(ALG.result+2);
    result[3] = *(ALG.result+3);
    result[4] = *(ALG.result+4);
    result[5] = *(ALG.result+5);
    result[6] = *(ALG.result+6);
    result[7] = *(ALG.result+7);
#else
    myfile.setFileName(PathManager::getFilePath("output/result.txt"));
    if(myfile.open(QIODevice::ReadOnly))
    {
        i=0;
        QTextStream stream(&myfile);
        while(!stream.atEnd())
        {
            stream>>*(result+(i++));
        }
        myfile.close();
    }
#endif

    qDebug()<<"result :"<<result[0]<<","<<result[1]<<","<<result[2]<<","<<result[3]<<","<<result[4]<<","<<result[5]<<","<<result[6]<<","<<result[7];

    bOK = SIS_setCoordinateParam( result[0], result[3] , result[1], result[4], result[2], result[5], 2, result[6], result[7]);
    if ( bOK==0 )
    {
        printf( "update sucess!" );
        //PrintBuffer( DeviceOutput+rPOS, rLEN );
        printf("\n");
    }
    else
    {
        recieve_err=1;
        printf("update fail!\n\n");
    }

#ifdef WRITE_BIN
    Device = IO_OpenDevice(NOT_SET);

    if(Device == INVALID_HANDLE_VALUE)
    {
        throw SisBaseException("Can't open SiS device", RetFail);
    }

    if(false == IO_ReportToOS(Device, false))
    {
        CloseHandle(Device);
        throw SisBaseException("Stop report to OS fail", RET_FW_IO_ERROR);
    }

    bSus = false;
    bSus = ReadFirmwareMem( Device, rADDR , rLEN, &nByteCntRead, DeviceOutput, FALSE );
    if ( bSus )
    {
        printf( "ROM = sucess" );
        //PrintBuffer( DeviceOutput+rPOS, rLEN );
        printf("\n");
    }
    else
    {
        recieve_err=1;
        printf("ROM recieve fail\n\n");
    }

    bFileName = "ROM_AF_C000.bin";
    //fp.open(QIODevice::WriteOnly);
    fp.setFileName(bFileName);

    if (!fp.open(QIODevice::WriteOnly))
    {
        recieve_err=1;
        printf( "Bin file not found\n\n");
    }
    else
    {
        QDataStream stream(&fp);
        i=0;
        while(!(i>(rLEN-1)))
            stream<<*(DeviceOutput+(i++));
        fp.close();
    }
    if(false == IO_ReportToOS(Device, true))
    {
        CloseHandle(Device);
        throw SisBaseException("Start report to OS fail", RET_FW_IO_ERROR);
    }

    IO_Reset(Device);
    Sleep(DELAY*6);
    CloseHandle(Device);
#endif

//-----------------------------INITIAL OUTSIDE-------------------------------//

#ifdef SISCORE_USE

    //QMessageBox::information(this , "Debug Info", "exitUpdateFW()");
//    qDebug() << "exitUpdateFW()";

//    if( !g_SiSCoreWrapper.openDevice() )
//    {
//        //throw SisBaseException("Can't open SiS device", RetFail);
//    }

//    //-----------------------------READ-------------------------------//

////    if ( !g_SiSCoreWrapper.reportToOS(false))
////    {
////        //g_SiSCoreWrapper.closeDevice();
////        throw SisBaseException("Stop report to OS fail", RET_FW_IO_ERROR);
////        qDebug() << "rtToOS(false) fail~!!";
////    }

//    qDebug() << "tmp 1";

//    unsigned int addr = rADDR;
//    if(g_SiSCoreWrapper.getFwGeneration() == "819")
//        addr = rADDR819;

//    bOK = false;
//    bOK = g_SiSCoreWrapper.cmd86Read(addr, DeviceOutput, rLEN);
//    if ( bOK )
//    {
//        printf( "ROM = success" );
//        printf("\n");
//    }
//    else
//    {
//        printf("initial ROM recieve fail\n\n");
//    }

//    unsigned char allRom[ALLROMSIZE] = {0};
//    bOK = g_SiSCoreWrapper.cmd86Read(0xa0000000, allRom, ALLROMSIZE);
//    if ( bOK )
//    {
//        printf( "ROM = success" );
//        printf("\n");
//    }
//    else
//    {
//        printf("initial ROM recieve fail\n\n");
//    }

////    if ( !g_SiSCoreWrapper.reportToOS(true) )
////    {
////        //g_SiSCoreWrapper.closeDevice();
////        throw SisBaseException("Start report to OS fail", RET_FW_IO_ERROR);
////        qDebug() << "rtToOS(true) fail~!!";
////    }

//    //-----------------------------WRITE-------------------------------//

//    *(DeviceOutput+0x23c)=PARA.paraSwap();//swap
//    *(DeviceOutput+0x246)=PARA.paraInvx();//invx
//    *(DeviceOutput+0x247)=PARA.paraInvy();//invy

//    unsigned int bAddr = bADDR;
//    if(g_SiSCoreWrapper.getFwGeneration() == "819")
//        bAddr = bADDR819;

//    for(int i = bAddr; i < rLEN; i++)
//        *(allRom + i) =  *(DeviceOutput + i);

//    //bSuc = g_SiSCoreWrapper.updateFw(wBlockAddr, DeviceOutput, wLEN);
//    qDebug() << "g_SiSCoreWrapper.updateFw(addr, allRom, wLEN) before";
//    bSuc = g_SiSCoreWrapper.updateFw(addr, allRom, wLEN);
//    qDebug() << "g_SiSCoreWrapper.updateFw(addr, allRom, wLEN) after";

//    if (bSuc) {
//        printf("Write firmware memory successfully.\n");
//        printf("\n");
//    }else{
//        printf("Write firmware memory failed.\n");
//    }

//    //sis_sleep(DELAY*6);
//    g_SiSCoreWrapper.closeDevice();

#else


    Device = IO_OpenDevice(NOT_SET);

    if(Device == INVALID_HANDLE_VALUE)
    {
        throw SisBaseException("Can't open SiS device", RetFail);
    }

    if(false == IO_ReportToOS(Device, false))
    {
        CloseHandle(Device);
        throw SisBaseException("Stop report to OS fail", RET_FW_IO_ERROR);
    }

    //-----------------------------READ-------------------------------//
    bOK = false;
    bOK = ReadFirmwareMem( Device, rADDR , rLEN, &nByteCntRead, DeviceOutput, FALSE );
    if ( bOK )
    {
        printf( "ROM = sucess" );
        printf("\n");
    }
    else
    {
        printf("initial ROM recieve fail\n\n");
    }

    //-----------------------------WRITE-------------------------------//
    *(DeviceOutput+0x23c)=PARA.paraSwap();//swap
    *(DeviceOutput+0x246)=PARA.paraInvx();//invx
    *(DeviceOutput+0x247)=PARA.paraInvy();//invy

    bSuc = Sub_BlockWriteMemBuffTo810(Device, wBlockAddr, DeviceOutput, wLEN);

    if (bSuc) {
        printf("Write firmware memory successfully.\n");
        printf("\n");
    }else{
        printf("Write firmware memory failed.\n");
    }

    if(false == IO_ReportToOS(Device, true))
    {
        CloseHandle(Device);
        throw SisBaseException("Start report to OS fail", RET_FW_IO_ERROR);
    }

    IO_Reset(Device);
    Sleep(DELAY*6);
    CloseHandle(Device);
#endif

    qDebug()<<"swap="<< *(DeviceOutput+0x23c);
    qDebug()<<"invx="<< *(DeviceOutput+0x246);
    qDebug()<<"invy="<< *(DeviceOutput+0x247);

    //-----------------------------INITIAL OUTSIDE-END---------------------------//
#ifndef ANDROID
    delete bar;
#endif

//    if(recieve_err==1)
//        QMessageBox::warning(this,tr("Message Box"),tr("Update FW FAIL !!!"));
//    else if(recieve_err==2)
//        QMessageBox::warning(this,tr("Message Box"),tr("No Data !!!"));
//    else
//        QMessageBox::information(this,tr("Message Box"),tr("Update FW OK !!!"));

//    QMessageBox* msgBox = new QMessageBox();
//    msgBox->setBaseSize(400, 800);
//    //msgBox->setFixedSize();

//    QPushButton* okBtn = new QPushButton("okdfgdfgd");

//    okBtn->setFixedSize(200, 400);
//    msgBox->setDefaultButton(okBtn);
//    msgBox->exec();

    if(recieve_err==1)
        ui->Exit->setText("Update FW FAIL !!!");
    else if(recieve_err==2)
        ui->Exit->setText("No Data !!!");
    else
        ui->Exit->setText("Update FW OK !!!");

    ui->groupBox_2->setTitle("");

#ifndef ANDROID
    this->show();
#endif
}

SIS_STATUS dialog::SIS_setCoordinateParam( const double Ax, const double Bx, const double Ay, const double By, const double Dx, const double Dy, const int type, const double R1, const double R2 )
{
    const unsigned int nAllByte = 10 * 4;
    const unsigned int nCurrentParam = 8;

    unsigned char params[nAllByte];
    memset(params, 0, sizeof(char) * nAllByte);

    double currentParams[nCurrentParam];
    memset(currentParams, 0, sizeof(double) * nCurrentParam);

    currentParams[0] = Ax;
    currentParams[1] = Ay;
    currentParams[2] = Bx;
    currentParams[3] = By;
    currentParams[4] = Dx;
    currentParams[5] = Dy;
    currentParams[6] = R1;
    currentParams[7] = R2;

    if ( !separateDoubleToShortShort(currentParams, params, nCurrentParam, nAllByte) )
    {
        return SIS_STATUS_PARAMETER_ERROR;
    }

    QString paramsStr = "separateDoubleToShortShort params:\n";

    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 10; j++){
            char paramsByte[10] = "";
            sprintf(paramsByte, "%02x", params[i*10+j]);
            paramsStr += QString("%1 ").arg(paramsByte);
        }
        paramsStr += QString("\n");
    }
    QMessageBox msgBox;
    msgBox.setText(paramsStr);
    //msgBox.exec();

    qDebug() << "separateDoubleToShortShort params:";
    qDebug() << paramsStr;

#ifdef SISCORE_USE

//    if ( !g_SiSCoreWrapper.openDevice() )
//    {
//        //g_SiSCoreWrapper.closeDevice();
//        return SIS_STATUS_FAIL;
//    }

//    if ( !g_SiSCoreWrapper.reportToOS(false))
//    {
//        qDebug() << "SiSCore_ReportToOS(false) fail~!!";
//        //g_SiSCoreWrapper.closeDevice();
//        return SIS_STATUS_FAIL;
//    }

    //bool ret = call_88_for_coordinate(INVALID_HANDLE_VALUE, params, nAllByte, type);
    bool ret = call_88_for_coordinate(params, nAllByte, type);

    if ( !ret )
    {
        qDebug() << "call_88_for_swap_inverse fail~!!";
        //g_SiSCoreWrapper.closeDevice();
        return SIS_STATUS_FW_IO_ERROR;
    }

//    if ( !g_SiSCoreWrapper.reportToOS(true) )
//    {
//        qDebug() << "SiSCore_ReportToOS(true) fail~!!";
//        //g_SiSCoreWrapper.closeDevice();
//        return SIS_STATUS_FAIL;
//    }

//    g_SiSCoreWrapper.closeDevice();
#else
    HANDLE deviceHandle = INVALID_HANDLE_VALUE;
    deviceHandle = IO_OpenDevice(NOT_SET);

    if (deviceHandle == INVALID_HANDLE_VALUE)
    {
        deviceHandle = NULL;
        return SIS_STATUS_FAIL;
    }

    if (!IO_ReportToOS(deviceHandle, false))
    {
        CloseHandle(deviceHandle);
        return SIS_STATUS_FAIL;
    }

    // a parameter is two btye
    bool bSuc = call_88_for_coordinate(deviceHandle, params, nAllByte, type);

    if ( !bSuc )
    {
        IO_ReportToOS(deviceHandle, true);
        return SIS_STATUS_FW_IO_ERROR;
    }
    else
    {
        if (!IO_ReportToOS(deviceHandle, true))
        {
            CloseHandle(deviceHandle);
            return SIS_STATUS_FAIL;
        }
    }

    CloseHandle(deviceHandle);
#endif
    return SIS_STATUS_PASS;
}

bool dialog::call_88_for_coordinate(unsigned char * uWriteData, const int writeDataSize, const int type )
{
    unsigned int REPORT_ID_OUT = 0x9;
    const unsigned int CMD_88_LENGTH = 0x36;
    const unsigned int TYPE_LOW_BYTE = 52;
    const unsigned int TYPE_HEIGHT_BYTE = 53;

    unsigned char reportBuffer[MAX_TRANSMISSION_SIZE];

    memset( reportBuffer, 0, sizeof( reportBuffer ) );

#ifdef SISCORE_USE

    if(g_SiSCoreWrapper.getFwGeneration() == "819")
        REPORT_ID_OUT = 0x21;

    reportBuffer[0] = REPORT_ID_OUT; //reportid of vendor command
    reportBuffer[1] = CMD_88_LENGTH;
    reportBuffer[2] = 0x88;

    if(g_SiSCoreWrapper.getFwGeneration() == "819"){
        reportBuffer[1] = 0;
        reportBuffer[2] = CMD_88_LENGTH;
        reportBuffer[3] = 0x88;
    }

    unsigned char thirdByte = 0xfa;
    for ( int i = 0, bufferIdx = 4; i < writeDataSize; i++)
    {
        if ( ( i % 8 == 0 ) && ( bufferIdx != TYPE_LOW_BYTE ) && ( bufferIdx != TYPE_HEIGHT_BYTE ) )
        {
            if(g_SiSCoreWrapper.getFwGeneration() == "817"){
                reportBuffer[bufferIdx++] = 0x00;
                reportBuffer[bufferIdx++] = 0x00;
                reportBuffer[bufferIdx++] = thirdByte;
                reportBuffer[bufferIdx++] = 0x67;
            }
            else if(g_SiSCoreWrapper.getFwGeneration() == "819"){
                reportBuffer[bufferIdx++] = 0x67;
                reportBuffer[bufferIdx++] = thirdByte;
                reportBuffer[bufferIdx++] = 0x00;
                reportBuffer[bufferIdx++] = 0x00;
            }
            thirdByte++;
        }
        else if ( bufferIdx == TYPE_LOW_BYTE || bufferIdx == TYPE_HEIGHT_BYTE)
        {
            // a byte for type
            bufferIdx++;
        }

        reportBuffer[bufferIdx++] = uWriteData[i] & 0xff;
    }

    reportBuffer[TYPE_LOW_BYTE] = type & 0xff;
    reportBuffer[TYPE_HEIGHT_BYTE] = (unsigned char)(( (0x01<<8) & 0xff00 ) >> 8);


    qDebug()<<"input buffer :";
    for(int i = 0;i<MAX_TRANSMISSION_SIZE ; i++)
    {
        printf("%4x, ",reportBuffer[i]);
        if((i+1)%8 == 0)
            qDebug()<<endl;
    }
    qDebug()<<endl;


    bool bOK = false;
    bOK = g_SiSCoreWrapper.simpleRW88Write(reportBuffer, MAX_TRANSMISSION_SIZE);

    if ( !bOK )
    {
        return false;
    }

//    unsigned char byteCnt = 0;
//    unsigned char buffer[MAX_TRANSMISSION_SIZE];
//    bOK = IO_CmdRead(hDevice, 0x0, buffer, &byteCnt);

//    qDebug()<<"output buffer :";
//    for(int i = 0;i<MAX_TRANSMISSION_SIZE ; i++)
//    {
//        printf("%4x, ",buffer[i]);
//        if((i+1)%8 == 0)
//            qDebug()<<endl;
//    }
//    qDebug()<<endl;

//    if ( !bOK )
//    {
//        return false;
//    }
//    if ( buffer[4] != 0xEF && buffer[5] != 0xBE )
//    {
//        return false;
//    }
#else
    reportBuffer[0] = REPORT_ID_OUT; //reportid of vendor command
    reportBuffer[1] = CMD_88_LENGTH;
    reportBuffer[2] = 0x88;

    unsigned char thirdByte = 0xfa;
    for ( int i = 0, bufferIdx = 4; i < writeDataSize; i++)
    {
        if ( ( i % 8 == 0 ) && ( bufferIdx != TYPE_LOW_BYTE ) && ( bufferIdx != TYPE_HEIGHT_BYTE ) )
        {
            if(g_SiSCoreWrapper.getFwGeneration() == "817"){
                reportBuffer[bufferIdx++] = 0x00;
                reportBuffer[bufferIdx++] = 0x00;
                reportBuffer[bufferIdx++] = thirdByte;
                reportBuffer[bufferIdx++] = 0x67;
            }
            else if(g_SiSCoreWrapper.getFwGeneration() == "819"){
                reportBuffer[bufferIdx++] = 0x67;
                reportBuffer[bufferIdx++] = thirdByte;
                reportBuffer[bufferIdx++] = 0x00;
                reportBuffer[bufferIdx++] = 0x00;
            }
            thirdByte++;
        }
        else if ( bufferIdx == TYPE_LOW_BYTE || bufferIdx == TYPE_HEIGHT_BYTE)
        {
            // a byte for type
            bufferIdx++;
        }

        reportBuffer[bufferIdx++] = uWriteData[i] & 0xff;
    }

    reportBuffer[TYPE_LOW_BYTE] = type & 0xff;
    reportBuffer[TYPE_HEIGHT_BYTE] = (unsigned char)(( (0x01<<8) & 0xff00 ) >> 8);


    qDebug()<<"input buffer :";
    for(int i = 0;i<MAX_TRANSMISSION_SIZE ; i++)
    {
        printf("%4x, ",reportBuffer[i]);
        if((i+1)%8 == 0)
            qDebug()<<endl;
    }
    qDebug()<<endl;


    bool bOK = false;
    bOK = IO_CmdWrite( hDevice, reportBuffer, MAX_TRANSMISSION_SIZE );

    if ( !bOK )
    {
        return false;
    }

    unsigned char byteCnt = 0;
    unsigned char buffer[MAX_TRANSMISSION_SIZE];
    bOK = IO_CmdRead(hDevice, 0x0, buffer, &byteCnt);

    qDebug()<<"output buffer :";
    for(int i = 0;i<MAX_TRANSMISSION_SIZE ; i++)
    {
        printf("%4x, ",buffer[i]);
        if((i+1)%8 == 0)
            qDebug()<<endl;
    }
    qDebug()<<endl;

    if ( !bOK )
    {
        return false;
    }
    if ( buffer[4] != 0xEF && buffer[5] != 0xBE )
    {
        return false;
    }
#endif
    return true;
}

bool dialog::call_88_for_swap_inverse(unsigned char thirdByte, unsigned char swap, unsigned char inverseX, unsigned char inverseY, const int type )
{
    unsigned int REPORT_ID_OUT = 0x9;
    const unsigned int CMD_88_LENGTH = 0x36;
    const unsigned int TYPE_LOW_BYTE = 52;
    const unsigned int TYPE_HEIGHT_BYTE = 53;

    unsigned char reportBuffer[MAX_TRANSMISSION_SIZE];

    memset( reportBuffer, 0, sizeof( reportBuffer ) );

#ifdef SISCORE_USE

    if(g_SiSCoreWrapper.getFwGeneration() == "819"){
        REPORT_ID_OUT = 0x21;
    }

    reportBuffer[0] = REPORT_ID_OUT; //reportid of vendor command
    reportBuffer[1] = CMD_88_LENGTH;
    reportBuffer[2] = 0x88;

    if(g_SiSCoreWrapper.getFwGeneration() == "819"){
        reportBuffer[1] = 0;
        reportBuffer[2] = CMD_88_LENGTH;
        reportBuffer[3] = 0x88;
    }

    reportBuffer[4] = 0x00;
    reportBuffer[5] = 0x00;
    reportBuffer[6] = thirdByte;
    reportBuffer[7] = 0x67;
    reportBuffer[8] = swap;
    reportBuffer[9] = inverseX;
    reportBuffer[10] = inverseY;

    reportBuffer[TYPE_LOW_BYTE] = type & 0xff;
    reportBuffer[TYPE_HEIGHT_BYTE] = (unsigned char)(( (0x01<<8) & 0xff00 ) >> 8);

    qDebug()<<"input swap buffer :";
    for(int i = 0;i<MAX_TRANSMISSION_SIZE ; i++)
    {
        printf("%4x, ",reportBuffer[i]);
        if((i+1)%8 == 0)
            qDebug()<<endl;
    }
    qDebug()<<endl;

    bool bOK = false;
    bOK = g_SiSCoreWrapper.simpleRW88Write(reportBuffer, MAX_TRANSMISSION_SIZE);

    if ( !bOK )
    {
        return false;
    }

//    unsigned char byteCnt = 0;
//    unsigned char buffer[MAX_TRANSMISSION_SIZE];
//    bOK = IO_CmdRead(hDevice, 0x0, buffer, &byteCnt);

//    qDebug()<<"output swap buffer :";
//    for(int i = 0;i<MAX_TRANSMISSION_SIZE ; i++)
//    {
//        printf("%4x, ",buffer[i]);
//        if((i+1)%8 == 0)
//            qDebug()<<endl;
//    }
//    qDebug()<<endl;

//    if ( !bOK )
//    {
//        return false;
//    }
//    if ( buffer[4] != 0xEF && buffer[5] != 0xBE )
//    {
//        return false;
//    }
#else
    reportBuffer[0] = REPORT_ID_OUT; //reportid of vendor command
    reportBuffer[1] = CMD_88_LENGTH;
    reportBuffer[2] = 0x88;

    reportBuffer[4] = 0x00;
    reportBuffer[5] = 0x00;
    reportBuffer[6] = thirdByte;
    reportBuffer[7] = 0x67;
    reportBuffer[8] = swap;
    reportBuffer[9] = inverseX;
    reportBuffer[10] = inverseY;

    reportBuffer[TYPE_LOW_BYTE] = type & 0xff;
    reportBuffer[TYPE_HEIGHT_BYTE] = (unsigned char)(( (0x01<<8) & 0xff00 ) >> 8);

    qDebug()<<"input swap buffer :";
    for(int i = 0;i<MAX_TRANSMISSION_SIZE ; i++)
    {
        printf("%4x, ",reportBuffer[i]);
        if((i+1)%8 == 0)
            qDebug()<<endl;
    }
    qDebug()<<endl;

    bool bOK = false;
    bOK = IO_CmdWrite( hDevice, reportBuffer, MAX_TRANSMISSION_SIZE );

    if ( !bOK )
    {
        return false;
    }

    unsigned char byteCnt = 0;
    unsigned char buffer[MAX_TRANSMISSION_SIZE];
    bOK = IO_CmdRead(hDevice, 0x0, buffer, &byteCnt);

    qDebug()<<"output swap buffer :";
    for(int i = 0;i<MAX_TRANSMISSION_SIZE ; i++)
    {
        printf("%4x, ",buffer[i]);
        if((i+1)%8 == 0)
            qDebug()<<endl;
    }
    qDebug()<<endl;

    if ( !bOK )
    {
        return false;
    }
    if ( buffer[4] != 0xEF && buffer[5] != 0xBE )
    {
        return false;
    }
#endif
    return true;
}

bool dialog::separateDoubleToShortShort( double * _doubles, unsigned char * _bytes, const int nDoubles, const int nByte)
{
    /*
     *  Spearate a double into two short. The fractional part is always positive and multi by 65525.
     *  This double only uses integer to represent positive or negitive.
     *  So integer part has range between 127~-127, -128 represented -0 ( ex. -0.12 )
     */

    const double _65535 = 65535.0f;

    /* for perform float << 30 bits */
    //const double _2exp30 = 1000000000.0f;
//    const double _2exp30 = 1073741824.0f;
    /* for perform float << 24 bits */
//    const double _2exp24 = 16777216.0f;
    const double _2exp27 = 134217728.0f;

//    const double _230 = 1073741824.0f;
//    const double _208 = 256.0f;

//    const double _2exp8 = 100000000.0f;

    if ( nByte < (nDoubles * 4) ) { return false; }
    for ( int i = 0, j = 0; i < nDoubles; i++, j++ )
    {
        if ( ( _doubles[i] > 32767.0f ) || ( _doubles[i] < -32767.0f ) ) { return false; }
        double integer = 0.0f;
        double fractional = 0.0f;
        fractional = modf(_doubles[i], &integer);

        if ( 6 == i || 7 == i )
        {
            // R1, R2 only need fractional part, and these 2 need shift 30 bits

            //fractional = fractional * _2exp30;
            fractional = fractional * _2exp27;
            printf("the %d raw is %lf\n", i, fractional);
            //fractional = fractional * _2exp24;
            //fractional = fractional * _230;
            //printf("_230 is %lf\n", fractional);
            //fractional = fractional * _208;
            //printf("_208 is %lf\n", fractional);

            // for translate into integer
            //fractional = fractional * _2exp8;

            int sh_fra = (int)fractional;
            printf("the %d int is %d\n", i, sh_fra);

            //printf("the int is %d\n", sh_fra);

            //printf("the shift int is %d\n", sh_fra >> 8);

            _bytes[j*4+0] = (unsigned char)(( sh_fra >> 24 ) & 0xff);
            _bytes[j*4+1] = (unsigned char)(( sh_fra >> 16 ) & 0xff);
            _bytes[j*4+2] = (unsigned char)(( sh_fra >> 8 ) & 0xff);
            _bytes[j*4+3] = (unsigned char)(sh_fra & 0xff);
        }
        else
        {
            // the fractional must be positive
            if ( fractional < 0 )
            {
                fractional = fractional * -1.0f;
            }
            //printf("the f is %lf\n", fractional);

            fractional = fractional * _65535;

            //printf("the _65535 f is %lf\n", fractional);


            short sh_int = (short)integer;
            short sh_fra = (short)fractional;

            if ( _doubles[i] < 0 && integer == 0 )
            {
                sh_int = -32768;
            }

            _bytes[j*4+0] = ( sh_int >> 8 )& 0xff;
            _bytes[j*4+1] = sh_int & 0xff;
            _bytes[j*4+2] = ( sh_fra >> 8 )& 0xff;
            _bytes[j*4+3] = sh_fra & 0xff;
        }

//        if(g_SiSCoreWrapper.getFwGeneration() == "819")
//        {
//            unsigned char temp[4] = {0};
//            for(int ti = 0; ti < 4; ti++)
//                temp[ti] =  _bytes[j*4+ti];
//            for(int ti = 0; ti < 4; ti++)
//                _bytes[j*4+ti] =  temp[3-ti];

//        }
    }

    return true;
}

void dialog::PrintBuffer(unsigned char *id, unsigned int byteLen )
{
    unsigned int i;
    for(i = 0; i < byteLen; i++ )
    {
        if(i%16==0)
            printf("\n");
        printf( "%02X ", id[i] );
    }
    printf( "\n" );
}

void dialog::showLeave()
{
    ui->outside->setEnabled(false);
    ui->parseOutside->setEnabled(false);
}

void dialog::on_Exit_clicked()
{
    close();
}

//void dialog::processStart()
//{
//    m_progressDialog.setRange(0, 0);
//    m_progressDialog.setWindowTitle(QStringLiteral("Updating"));
//    m_progressDialog.setLabelText(QStringLiteral("Updating..."));
//    m_progressDialog.exec();
//}

//void dialog::processEnd()
//{
//    m_progressDialog.close();
//}
