#include "painttest.h"
#include "qdrawutil.h"
#include <QPalette>
#include "parameter.h"
#include <QPushButton>
#include <QTextStream>
#include "paintbackground.h"


PaintTest::PaintTest()
{
    m_background = QBrush(QColor(255,255,255));
}

PaintTest::~PaintTest()
{

}

void PaintTest::paint(QPainter *painter, QPaintEvent *event)
{
    double screenHeight = PARA.getScreenH();
    double screenWidth = PARA.getScreenW();
    double exitbtnR = 50;

    IpaintTable* topObject = NULL;
    painter->fillRect(event->rect(),m_background);

    if(paintBackground::instance().flag)
    {
        QBrush b(QColor(128,128,0,0xa0));
        QPalette p;
        qDrawWinButton(painter, screenWidth/4-exitbtnR, screenHeight/2-exitbtnR, exitbtnR*2, exitbtnR*2, p , false, &b);
        font = painter->font() ;
        font.setPointSize ( 15 );
        font.setWeight(QFont::DemiBold);
        painter->setFont(font);

        painter->drawText(QPoint(screenWidth/4-5*exitbtnR/8, screenHeight/2+exitbtnR/4),"EXIT");

        QBrush c(QColor(0,128,128,0xa0));
        qDrawWinButton(painter, 3*screenWidth/4-exitbtnR, screenHeight/2-exitbtnR, exitbtnR*2, exitbtnR*2, p , false, &c);
        painter->drawText(QPoint(3*screenWidth/4-7*exitbtnR/8, screenHeight/2+exitbtnR/4),"RETRY");
    }

    if(m_paintObject.empty()==0)
    {
        for(int i=0;i<4;++i)
        {
            m_paintObject[i]->paint(painter,event);
        }

        if(topObject != NULL)
            topObject->paint(painter,event);
    }
}

