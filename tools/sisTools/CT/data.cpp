#include <iostream>

#include "ctexitcode/ctexitcode.h"
#include "sislog.h"
#include "ctexception/ctexception.h"
#include "spexception/spexception.h"
#include "getfirmwaredatacore/getfirmwaredatacore.h"

using namespace std;
using namespace CT;
using namespace CT::CTBP;
using namespace CT::BFP;
using namespace CT::GFIDP;
using namespace SiS;
using namespace SiS::Procedure;
using namespace SE;
#define TAG "DATA" 

const int GoToNextFrame = 1;
int g_tx = 0;
int g_rx = 0;

int main(int argc, char *argv[])
{
    USING_EXIT_CODE;
    unsigned int frameIdx = 0;
    bool shouldRun = true;
    int next = GoToNextFrame;
    //int tp = 0,pen = 0;
    GetFirmwareDataParameter* getFirmwareDataParameter = new GetFirmwareDataParameter();
    PARSE_ARGS(getFirmwareDataParameter, argc, argv);
    DEBUG_SETTING_IF_HAS_ARG_DEBUG_LOG(getFirmwareDataParameter);
    SHOW_CTCORE_VERSION;
    SHOW_HELP_IF_HAS_ARG_HELP(getFirmwareDataParameter);
    PARSE_ARGUMENT(getFirmwareDataParameter);

    GetFirmwareDataCore getFirmwareDataCore(getFirmwareDataParameter);
    getFirmwareDataCore.setVoltageType(getFirmwareDataParameter->getVoltageType());
    //getFirmwareDataCore.setVoltageType(VT_DIFF);
    CTCORE_INIT(getFirmwareDataCore);
    getFirmwareDataCore.getTxRxSize(&g_tx, &g_rx);
    //getFirmwareDataCore.getNVB(&tp, &pen);
    std::cout << "[init]\ncapturerTool\nFrameCount,0\nWidth," << g_tx <<"\nHeight," << g_rx <<"\nType,0\n" << std::endl;
    CTCORE_DESTROY(getFirmwareDataCore, true);
    while(shouldRun) {
      std::cout << "[input 1 to next frame. others to stop.]" << std::endl;
      std::cin >> next;
      if ( GoToNextFrame != next )
        break;
      std::cout << "[frame]\nNo,"<<frameIdx<<",0ms,Read Error Times,0" << std::endl;
      getFirmwareDataCore.setVoltageType(getFirmwareDataParameter->getVoltageType());
      CTCORE_INIT(getFirmwareDataCore);
      //CTCORE_EXEC(getFirmwareDataCore);
      getFirmwareDataCore.getRawdata(getFirmwareDataParameter->getVoltageSource());
      CTCORE_DESTROY(getFirmwareDataCore, true);
      frameIdx += 1;
    }

    delete getFirmwareDataParameter;
    RETURN_EXIT_CODE;
}

