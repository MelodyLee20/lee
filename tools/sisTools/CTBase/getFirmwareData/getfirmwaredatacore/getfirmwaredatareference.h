#ifndef GETFIRMWAREIDREFERENCE_H
#define GETFIRMWAREIDREFERENCE_H

#include "ctbasecore/ctbasereference.h"

namespace CT
{

class GetFirmwareDataReference : public CTBaseReference
{
public:
    explicit GetFirmwareDataReference();
    virtual ~GetFirmwareDataReference();
};

} // CT

#endif // GETFIRMWAREIDREFERENCE_H
