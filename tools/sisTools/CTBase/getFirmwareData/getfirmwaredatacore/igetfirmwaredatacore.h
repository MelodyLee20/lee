#ifndef IGETFIRMWAREIDCORE_H
#define IGETFIRMWAREIDCORE_H

#include "ctbasecore/ctbasecore.h"
#include "getfirmwaredatacore/getfirmwaredataparameter.h"
#include "getfirmwaredatacore/getfirmwaredatareference.h"

namespace CT
{

class IGetFirmwareDataCore : public CTBaseCore
{
public:
    explicit IGetFirmwareDataCore(GetFirmwareDataParameter* getFirmwareDataParameter);
    virtual ~IGetFirmwareDataCore();
    virtual void getRawdata(VoltageSource type);
    virtual void getPenReport();
    virtual void getTxRxSize(int *tx, int *rx);
    virtual void getNVB(int *tp, int *pen,int *tpDiff, int *penDiff);
    virtual void showData(SerialData* serialData, bool showEngineerMode);
    virtual void showRawdata(SerialData* serialData, int tx, int rx, int nvb, VoltageSource source, VoltageType type);

protected:
    virtual CTBaseReference* generateReference();
    virtual GetFirmwareDataReference* getGetFirmwareDataReference(ReferenceSource rs, int chipIndex);

    virtual void prepareAllBin();

    virtual void prepareXramRef(int chipIndex) = 0;
    virtual void prepareBinRef(int chipIndex) = 0;

    virtual CTExitCode showFirmwareData() = 0;
    virtual CTExitCode checkFirmwareDataUsingInput() = 0;
    virtual CTExitCode checkFirmwareDataUsingFile() = 0;

    virtual void checkInputConut(int inputCount) = 0;

protected:
    GetFirmwareDataParameter* m_getFirmwareDataParameter;
};

} // CT

#endif // IGETFIRMWAREIDCORE_H
