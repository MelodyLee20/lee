#ifndef GETFIRMWAREIDCORE_H
#define GETFIRMWAREIDCORE_H

#include "getfirmwaredatacore/igetfirmwaredatacore.h"
#include "getfirmwaredatacore/flowversion/getfirmwaredatacore_general.h"
#include "getfirmwaredatacore/flowversion/getfirmwaredatacore_generalsingle.h"

namespace CT
{

class GetFirmwareDataCore : public IGetFirmwareDataCore
{
public:
    explicit GetFirmwareDataCore(GetFirmwareDataParameter* getFirmwareDataParameter);
    virtual ~GetFirmwareDataCore();

    void switchFlowVersion(DeviceType deviceType);

    virtual void setSiSProcedure(SiSProcedure* sisProcedure);

    virtual void init();
    virtual CTExitCode exec();

protected:
    virtual void prepareBinRef(int chipIndex) {}
    virtual void prepareXramRef(int chipIndex) {}

    virtual CTExitCode showFirmwareData() { return undoException("IGetFirmwareDataCore Exception : "); }
    virtual CTExitCode checkFirmwareDataUsingInput() { return undoException("IGetFirmwareDataCore Exception : "); }
    virtual CTExitCode checkFirmwareDataUsingFile() { return undoException("IGetFirmwareDataCore Exception : "); }

    virtual void checkInputConut(int inputCount) {}

private:
    GetFirmwareDataCore_general* getGetFirmwareDataCore_general();
    GetFirmwareDataCore_generalSingle* getGetFirmwareDataCore_generalSingle();

private:
    IGetFirmwareDataCore* m_iGetFirmwareDataCore;
    GetFirmwareDataCore_general* m_getFirmwareDataCore_general;
    GetFirmwareDataCore_generalSingle* m_getFirmwareDataCore_generalSingle;
};

} // CT

#endif // GETFIRMWAREIDCORE_H
