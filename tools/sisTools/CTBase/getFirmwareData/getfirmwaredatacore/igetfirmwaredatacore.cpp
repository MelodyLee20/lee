#include "igetfirmwaredatacore.h"
#include <stdlib.h>
#include "sislog.h"
#include "ctexception/ctexception.h"

#include <typeinfo>

using namespace CT;
using namespace SiS;
using namespace SiS::Procedure;

#define EXCEPTION_TITLE "IGetFirmwareDataCore Exception : "
#define TAG "IGetFirmwareDataCore"

IGetFirmwareDataCore::IGetFirmwareDataCore(GetFirmwareDataParameter* getFirmwareDataParameter) :
    CTBaseCore(getFirmwareDataParameter),
    m_getFirmwareDataParameter(getFirmwareDataParameter)
{

}

IGetFirmwareDataCore::~IGetFirmwareDataCore()
{

}

CTBaseReference*
IGetFirmwareDataCore::generateReference()
{
    return new GetFirmwareDataReference();
}

void
IGetFirmwareDataCore::showData(SerialData* serialData, bool showEngineerMode)
{
    if(showEngineerMode)
    {
        SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "carey $%s : %s", serialData->getKeyword().c_str(), serialData->getSerial().c_str());
    }
    else
    {
        int shift = 0;
        unsigned char* dataBuf = serialData->getData();
        int dataSize = serialData->getSize();

        while( shift < dataSize )
        {
            /* 1. address */
            SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, " carey 0x%08x : ", serialData->getAddress() + shift);

            /* 2. hex */
            unsigned char _16Bytes[16] = { 0x0 };
            int count = 0;

            for(int i = 0; i < 16; i++)
            {
                if( shift + i < dataSize )
                {
                    _16Bytes[i] = dataBuf[shift + i];
                    count++;
                }
            }

            std::string _16BytesHex;
            for(int i = 0; i < 16; i++)
            {
                char bData[10] = "";
                if( i < count )
                {
                    sprintf(bData, "%02X ", _16Bytes[i]);
                }
                else
                {
                    sprintf(bData, "   ");
                }
                _16BytesHex.append(bData);
            }
            //SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "%s", _16BytesHex.c_str());

            /* 3. dump */
            std::string _16BytesASCII;
            for(int i = 0; i < count; i++)
            {
                char bData[10] = "";
                if ( (_16Bytes[i] < 0x20) || (_16Bytes[i] > 0xff) )
                {
                    sprintf(bData, ".");
                }
                else if( _16Bytes[i] == 0xff )
                {
                    sprintf(bData, ".");
                }
                else
                {
                    sprintf(bData, "%c", _16Bytes[i]);
                }
                _16BytesASCII.append(bData);
            }
            //SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "%s\n", _16BytesASCII.c_str());

            SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "0x%08x : %s %s", serialData->getAddress() + shift, _16BytesHex.c_str(), _16BytesASCII.c_str() );

            /* next */
            shift += 16;
        }
    }

}

void
IGetFirmwareDataCore::getNVB(int *tp, int *pen,int *tpDiff, int *penDiff )
{   
    /* switch SiSProcedure by OS DeviceType */
    m_sisProcedure->switchSiSProcedure( m_ctBaseParameter->getOSDeviceType() );
    SerialData* data = m_sisProcedure->readNVB(RS_XRAM, CI_MASTER);
    showData(data, true);
    unsigned char* dataBuf = data->getData();
    int tpNvb = dataBuf[12] & 0xf;
    int penNvb = dataBuf[12] & 0xf0;
    int tpDNvb = dataBuf[13] & 0xf;
    int penDNvb = dataBuf[13] & 0xf0;
    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "Tp[%d]:diff[%d], Pen[%d]:diff[%d]\n", tpNvb,tpDNvb, penNvb, penDNvb);
    *tp = tpNvb;
    *tpDiff = tpDNvb;
    *pen = penNvb;
    *penDiff = penDNvb;
}

void
IGetFirmwareDataCore::getTxRxSize(int *tx, int *rx)
{   
    /* switch SiSProcedure by OS DeviceType */
    m_sisProcedure->switchSiSProcedure( m_ctBaseParameter->getOSDeviceType() );
    SerialData* data = m_sisProcedure->readWidthHeight(RS_XRAM, CI_MASTER);
    showData(data, true);
    unsigned char* dataBuf = data->getData();
    int Tx = (dataBuf[0] << 8) + dataBuf[1];
    int Rx = (dataBuf[2] << 8) + dataBuf[3];
    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "Tx[%d]:[%02x][%02x], Rx[%d]:[%02x][%02x]\n", Tx,dataBuf[0], dataBuf[1], Rx,dataBuf[2], dataBuf[3]);
    *tx = Tx;
    *rx = Rx;
}


void
IGetFirmwareDataCore::getPenReport()
{   
    /* switch SiSProcedure by OS DeviceType */
    m_sisProcedure->switchSiSProcedure( m_ctBaseParameter->getOSDeviceType() );
    SerialData* data = m_sisProcedure->readPenReport(RS_XRAM, CI_MASTER);
    showData(data, true);
    unsigned char* dataBuf = data->getData();
    int id = dataBuf[0];
    int status = dataBuf[1];
    unsigned char invert = (dataBuf[1] & 0x1)?1:0;
    unsigned char barrSwi = (dataBuf[1] & 0x2)?1:0;
    unsigned char secBarr = (dataBuf[1] & 0x4)?1:0;
    unsigned char eraser = (dataBuf[1] & 0x8)?1:0;
    unsigned char tipSwi = (dataBuf[1] & 0x10)?1:0;
    unsigned char inRange = (dataBuf[1] & 0x20)?1:0;
    int x = (dataBuf[3] << 8) + dataBuf[2];
    int y = (dataBuf[5] << 8) + dataBuf[4];
    int pressure = (dataBuf[7] << 8) + dataBuf[6];
    short tileX = (dataBuf[9] << 8) + dataBuf[8];
    short tileY = (dataBuf[11] << 8) + dataBuf[10];
    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "id[%d]:[%02x]\n", id,dataBuf[0]);
    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "status[%d]:[%02x]\n", status, dataBuf[1]);
    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "x[%d]:[%02x][%02x]\n", x,dataBuf[3], dataBuf[2]);
    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "y[%d]:[%02x][%02x]\n", y,dataBuf[5], dataBuf[4]);
    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "pressure[%d]:[%02x][%02x]\n", pressure,dataBuf[7], dataBuf[6]);
    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "status[%d]:[%d]:[%d]:[%d]:[%d]:[%d]\n", invert,barrSwi,secBarr,eraser,tipSwi,inRange);
    char bData[256] = "";
    snprintf(bData, sizeof(bData), "ReportID,%d\nInvert,%d\nBarrSwi,%d\nSecBarr,%d\nEraser,%d\nTipSwi,%d\nInRange,%d\nX,%d\nY,%d\nPressure,%d\nTiltX,%d\nTiltY,%d",id,invert,barrSwi,secBarr,eraser,tipSwi,inRange,x,y,pressure,tileX,tileY);
    std::cout << bData << std::endl;
    std::cout << std::endl;

}

void
IGetFirmwareDataCore::showRawdata(SerialData* serialData, int tx, int rx, int nvb, VoltageSource source, VoltageType type)
{

    int shift = 0;
    unsigned char* dataBuf = serialData->getData();
    int dataSize = serialData->getSize();
    int i = 0;
    int count = 0;
    char bData[10] = "";

    while( count < dataSize ) {
      if (source == VS_TP) {
        for (i=0;i<rx;i++){
          if (nvb > 0) {
            sprintf(bData, "%02X%02X ", dataBuf[count],dataBuf[count+1]);
            std::cout << (short)strtol(bData, NULL, 16) << ",";
            count+=2;
          }else{
            //std::cout << (short)dataBuf[count] << ",";
            if (type == VT_RAWDATA)
              printf("%d,", (unsigned int)dataBuf[count]);
            else
              printf("%d,", (char )dataBuf[count]);
            count++;
          }
        }
        std::cout << std::endl;
      } else {
        for (i=0;i<tx;i++){
          if (nvb > 0) {
            sprintf(bData, "%02X%02X ", dataBuf[count],dataBuf[count+1]);
            std::cout << (short)strtol(bData, NULL, 16) << ",";
            count+=2;
          }else{
            //std::cout << (short)dataBuf[count] << ",";
            if (type == VT_RAWDATA)
              printf("%d,", (unsigned int)dataBuf[count]);
            else
              printf("%d,", (char )dataBuf[count]);
            count++;
          }
        }
        std::cout << std::endl;
        for (i=0;i<rx;i++){
          if (nvb > 0) {
            sprintf(bData, "%02X%02X ", dataBuf[count],dataBuf[count+1]);
            std::cout << (short)strtol(bData, NULL, 16) << ",";
            count+=2;
          }else{
            //std::cout << (short)dataBuf[count] << ",";
            if (type == VT_RAWDATA)
              printf("%d,", (unsigned int)dataBuf[count]);
            else
              printf("%d,", (char )dataBuf[count]);
            count++;
          }
        }
      }
    }
    std::cout << std::endl;
}

void
IGetFirmwareDataCore::getRawdata(VoltageSource type)
{   
    /* switch SiSProcedure by OS DeviceType */
    m_sisProcedure->switchSiSProcedure( m_ctBaseParameter->getOSDeviceType() );

    /* init Xram master reference */
    insertReference(RS_XRAM, CI_MASTER, generateReference());
    CTBaseReference* xramMasterRef = getCTBaseReference(RS_XRAM, CI_MASTER);
    int tp = 0, pen = 0,tpDiff = 0, penDiff = 0;
    int tx = 0, rx = 0;
    getTxRxSize(&tx, &rx);
    getNVB(&tp, &pen,&tpDiff, &penDiff);
    fetchDeviceType(RS_XRAM, CI_MASTER); /* 1. DeviceType */
    SerialData* data = NULL;
    int nvb = 0;
    if (tp > 1)
        tpDiff = tp;
    if (type == VS_TP) {
      if (m_voltageType == VT_DIFF)
        nvb = tp;
      else if (m_voltageType == VT_RAWDATA || m_voltageType == VT_BASE)
        nvb = tpDiff;
      data = m_sisProcedure->readRawdata(RS_XRAM, CI_MASTER, m_voltageType, tx*rx, nvb);
    } else {
      if (m_voltageType == VT_DIFF) 
        nvb = penDiff;
      else if (m_voltageType == VT_RAWDATA || m_voltageType == VT_BASE)
        nvb = pen;
      data = m_sisProcedure->readPenRawdata(RS_XRAM, CI_MASTER, m_voltageType, tx,rx, nvb);
    }
    showRawdata(data, tx, rx, nvb, type, m_voltageType);
    if (type == VS_PEN)
      getPenReport();
    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, " size 0x%08x, tx[%d], rx[%d], tp[%d], nvb[%d] ", data->getSize(), tx, rx,tp,nvb);


}


GetFirmwareDataReference*
IGetFirmwareDataCore::getGetFirmwareDataReference(ReferenceSource rs, int chipIndex)
{
    CTBaseReference* ctBaseRef = getCTBaseReference(rs, chipIndex);

    try
    {
        GetFirmwareDataReference* gRef = dynamic_cast<GetFirmwareDataReference*>(ctBaseRef);
        return gRef;
    }
    catch(bad_cast)
    {
        std::string msg = EXCEPTION_TITLE;
        char errorMsg[1024] = "";
        sprintf(errorMsg, "bad_cast GetFirmwareDataReference* : rs=%d, chipIndex=%d\n", rs, chipIndex);
        msg.append(errorMsg);
        throw CTException( msg );
    }
}

void
IGetFirmwareDataCore::prepareAllBin()
{
    for(std::map<ChipIndexKey, BinWrap*>::iterator it = m_getFirmwareDataParameter->getBinWrapMap().begin();
        it != m_getFirmwareDataParameter->getBinWrapMap().end();
        ++it)
    {
        BinWrap* binWrap = it->second;
        binWrap->loadBin();
    }

    m_sisProcedure->setBinWrapMap( m_getFirmwareDataParameter->getBinWrapMap() );
}

