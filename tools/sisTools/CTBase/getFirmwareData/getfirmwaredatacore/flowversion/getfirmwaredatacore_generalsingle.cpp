#include "getfirmwaredatacore_generalsingle.h"

#include "sislog.h"
#include "ctexception/ctexception.h"
#include "spexception/spexception.h"

using namespace CT;
using namespace SiS;
using namespace SiS::Procedure;

#define EXCEPTION_TITLE "GetFirmwareDataCore_generalSingle Exception : "
#define TAG "GetFirmwareDataCore_generalSingle"

GetFirmwareDataCore_generalSingle::GetFirmwareDataCore_generalSingle(GetFirmwareDataParameter* getFirmwareDataParameter) :
    GetFirmwareDataCore_general(getFirmwareDataParameter)
{

}

GetFirmwareDataCore_generalSingle::~GetFirmwareDataCore_generalSingle()
{

}


void
GetFirmwareDataCore_generalSingle::checkInputConut(int inputCount)
{
    int chipCount = 1; // single

    if( chipCount != inputCount )
    {
        /* stop if input conut not match chip count */
        std::string msg = EXCEPTION_TITLE;
        char errorMsg[1024] = "";
        sprintf(errorMsg, "checkInputConut : not match single ! chipCount(%d), inputCount(%d)\n"
                ,chipCount, inputCount);
        msg.append(errorMsg);
        throw CTException( msg );
    }
}

CTExitCode
GetFirmwareDataCore_generalSingle::showFirmwareData()
{
    int chipIndex = m_getFirmwareDataParameter->getSpecificChip();

    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "%s Firmware Id : %s\n",
                    ISiSProcedure::getCIStr(chipIndex).c_str(),
                    getGetFirmwareDataReference(RS_XRAM, chipIndex)->getFirmwareID()->getSerial().c_str() );

    return CT_EXIT_PASS;
}

CTExitCode
GetFirmwareDataCore_generalSingle::checkFirmwareDataUsingInput()
{
    int chipIndex = m_getFirmwareDataParameter->getSpecificChip();

    /* start to compare */

    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "%s :", ISiSProcedure::getCIStr(chipIndex).c_str());

    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "XRAM Firmware Id (%s)",
                    getGetFirmwareDataReference(RS_XRAM, chipIndex)->getFirmwareID()->getSerial().c_str() );

    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "compare to input (%s)",
                    this->m_getFirmwareDataParameter->getFirmwareCheckIdMap(chipIndex).c_str() );

    if( getGetFirmwareDataReference(RS_XRAM, chipIndex)->getFirmwareID()->getSerial().compare(this->m_getFirmwareDataParameter->getFirmwareCheckIdMap(chipIndex)) == 0 ||
            getGetFirmwareDataReference(RS_XRAM, chipIndex)->getFirmwareID()->getString().compare(this->m_getFirmwareDataParameter->getFirmwareCheckIdMap(chipIndex)) == 0 )
    {
        SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "the same\n");
    }
    else
    {
        SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "different !!!\n");
        return CT_EXIT_FAIL;
    }

    return CT_EXIT_PASS;
}

CTExitCode
GetFirmwareDataCore_generalSingle::checkFirmwareDataUsingFile()
{
    prepareAllBin();

    int chipIndex = m_getFirmwareDataParameter->getSpecificChip();

    /* prepareBinRef for each chip */
    prepareBinRef(chipIndex);

    /* start to compare */

    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "%s :", ISiSProcedure::getCIStr(chipIndex).c_str());

    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "XRAM Firmware Id (%s)",
                    getGetFirmwareDataReference(RS_XRAM, chipIndex)->getFirmwareID()->getSerial().c_str() );

    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "compare to file (%s)",
                    getGetFirmwareDataReference(RS_BIN, chipIndex)->getFirmwareID()->getSerial().c_str() );

    if( getGetFirmwareDataReference(RS_XRAM, chipIndex)->getFirmwareID()->getSerial().compare(
                getGetFirmwareDataReference(RS_BIN, chipIndex)->getFirmwareID()->getSerial()) == 0 )
    {
        SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "the same\n");
    }
    else
    {
        SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "different !!!\n");
        return CT_EXIT_FAIL;
    }

    return CT_EXIT_PASS;
}
