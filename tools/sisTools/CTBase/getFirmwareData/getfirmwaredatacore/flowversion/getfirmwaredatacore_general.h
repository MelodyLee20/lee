#ifndef GETFIRMWAREIDCORE_GENERAL_H
#define GETFIRMWAREIDCORE_GENERAL_H

#include "getfirmwaredatacore/igetfirmwaredatacore.h"

namespace CT
{

class GetFirmwareDataCore_general : public IGetFirmwareDataCore
{
public:
    explicit GetFirmwareDataCore_general(GetFirmwareDataParameter* getFirmwareDataParameter);
    virtual ~GetFirmwareDataCore_general();

    virtual CTExitCode exec();

protected:
    virtual void prepareXramRef(int chipIndex);
    virtual void prepareBinRef(int chipIndex);

    virtual CTExitCode showFirmwareData();
    virtual CTExitCode checkFirmwareDataUsingInput();
    virtual CTExitCode checkFirmwareDataUsingFile();

    virtual void checkInputConut(int inputCount);
};

} // CT

#endif // GETFIRMWAREIDCORE_GENERAL_H
