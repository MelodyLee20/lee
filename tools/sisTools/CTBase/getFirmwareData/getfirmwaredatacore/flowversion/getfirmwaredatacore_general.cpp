#include "getfirmwaredatacore_general.h"

#include "sislog.h"
#include "ctexception/ctexception.h"
#include "spexception/spexception.h"

using namespace CT;
using namespace SiS;
using namespace SiS::Procedure;

#define EXCEPTION_TITLE "GetFirmwareDataCore_general Exception : "
#define TAG "GetFirmwareDataCore_general"

GetFirmwareDataCore_general::GetFirmwareDataCore_general(GetFirmwareDataParameter* getFirmwareDataParameter) :
    IGetFirmwareDataCore(getFirmwareDataParameter)
{

}

GetFirmwareDataCore_general::~GetFirmwareDataCore_general()
{

}

CTExitCode
GetFirmwareDataCore_general::exec()
{
    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "firmware data exec" );
    getRawdata(VS_TP); // stop if broken
    return CT_EXIT_PASS;
}

void
GetFirmwareDataCore_general::prepareXramRef(int chipIndex)
{
    /* switch SiSProcedure by OS DeviceType */
    m_sisProcedure->switchSiSProcedure( m_ctBaseParameter->getOSDeviceType() );

    /* init reference */
    insertReference(RS_XRAM, chipIndex, generateReference());

    fetchFirmwareID(RS_XRAM, chipIndex); /* 8. FirmwareID */
}

void
GetFirmwareDataCore_general::prepareBinRef(int chipIndex)
{
    /* init reference */
    insertReference(RS_BIN, chipIndex, generateReference());

    fetchFirmwareID(RS_BIN, chipIndex); /* 8. FirmwareID */
}

void
GetFirmwareDataCore_general::checkInputConut(int inputCount)
{
    GetFirmwareDataReference* xramMasterRef = getGetFirmwareDataReference(RS_XRAM, CI_MASTER);
    int chipCount = xramMasterRef->getSlaveNum() + 1;

    if( chipCount != inputCount )
    {
        /* stop if input conut not match chip count */
        std::string msg = EXCEPTION_TITLE;
        char errorMsg[1024] = "";
        sprintf(errorMsg, "checkInputConut : not match ! chipCount(%d), inputCount(%d)\n"
                ,chipCount, inputCount);
        msg.append(errorMsg);
        throw CTException( msg );
    }
}

CTExitCode
GetFirmwareDataCore_general::showFirmwareData()
{
    GetFirmwareDataReference* xramMasterRef = getGetFirmwareDataReference(RS_XRAM, CI_MASTER);

    /* version */
    unsigned char major = 0;
    unsigned char minor = 0;
    unsigned char* idData = xramMasterRef->getFirmwareID()->getData();
	if( xramMasterRef->getFirmwareID()->getSize() == 14 && !xramMasterRef->isBroken() )
	{
        major = idData[12]; // (0x400e, 1bytes): FW major version
        minor = idData[13]; // (0x400f, 1bytes): FW small version
	}
    SIS_LOG_I( SiSLog::getOwnerSiS(), TAG, "Active Firmware Version : %d.%d\n", major, minor );

    for(int chipIndex = CI_MASTER; chipIndex < xramMasterRef->getSlaveNum() + 1; chipIndex++)
    {
        SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "%s Firmware Id : %s\n",
                        ISiSProcedure::getCIStr(chipIndex).c_str(),
                        getGetFirmwareDataReference(RS_XRAM, chipIndex)->getFirmwareID()->getSerial().c_str() );
    }

    return CT_EXIT_PASS;
}

CTExitCode
GetFirmwareDataCore_general::checkFirmwareDataUsingInput()
{
    GetFirmwareDataReference* xramMasterRef = getGetFirmwareDataReference(RS_XRAM, CI_MASTER);

    for(int chipIndex = CI_MASTER; chipIndex < xramMasterRef->getSlaveNum() + 1; chipIndex++)
    {
        /* start to compare */

        SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "%s :", ISiSProcedure::getCIStr(chipIndex).c_str());

        SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "XRAM Firmware Id (%s)",
                        getGetFirmwareDataReference(RS_XRAM, chipIndex)->getFirmwareID()->getSerial().c_str() );

        SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "compare to input (%s)",
                        this->m_getFirmwareDataParameter->getFirmwareCheckIdMap(chipIndex).c_str() );

        if( getGetFirmwareDataReference(RS_XRAM, chipIndex)->getFirmwareID()->getSerial().compare(this->m_getFirmwareDataParameter->getFirmwareCheckIdMap(chipIndex)) == 0 ||
                getGetFirmwareDataReference(RS_XRAM, chipIndex)->getFirmwareID()->getString().compare(this->m_getFirmwareDataParameter->getFirmwareCheckIdMap(chipIndex)) == 0 )
        {
            SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "the same\n");
        }
        else
        {
            SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "different !!!\n");
            return CT_EXIT_FAIL;
        }
    }

    return CT_EXIT_PASS;
}

CTExitCode
GetFirmwareDataCore_general::checkFirmwareDataUsingFile()
{
    prepareAllBin();

    GetFirmwareDataReference* xramMasterRef = getGetFirmwareDataReference(RS_XRAM, CI_MASTER);

    for(int chipIndex = CI_MASTER; chipIndex < xramMasterRef->getSlaveNum() + 1; chipIndex++)
    {
        /* prepareBinRef for each chip */
        prepareBinRef(chipIndex);

        /* start to compare */

        SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "%s :", ISiSProcedure::getCIStr(chipIndex).c_str());

        SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "XRAM Firmware Id (%s)",
                        getGetFirmwareDataReference(RS_XRAM, chipIndex)->getFirmwareID()->getSerial().c_str() );

        SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "compare to file (%s)",
                        getGetFirmwareDataReference(RS_BIN, chipIndex)->getFirmwareID()->getSerial().c_str() );

        if( getGetFirmwareDataReference(RS_XRAM, chipIndex)->getFirmwareID()->getSerial().compare(
                    getGetFirmwareDataReference(RS_BIN, chipIndex)->getFirmwareID()->getSerial()) == 0 )
        {
            SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "the same\n");
        }
        else
        {
            SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "different !!!\n");
            return CT_EXIT_FAIL;
        }
    }

    return CT_EXIT_PASS;
}
