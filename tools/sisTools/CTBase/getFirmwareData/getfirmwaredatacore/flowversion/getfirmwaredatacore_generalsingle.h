#ifndef GETFIRMWAREIDCORE_GENERALSINGLE_H
#define GETFIRMWAREIDCORE_GENERALSINGLE_H

#include "getfirmwaredatacore/flowversion/getfirmwaredatacore_general.h"

namespace CT
{

class GetFirmwareDataCore_generalSingle : public GetFirmwareDataCore_general
{
public:
    GetFirmwareDataCore_generalSingle(GetFirmwareDataParameter* getFirmwareDataParameter);
    ~GetFirmwareDataCore_generalSingle();

protected:

    virtual CTExitCode showFirmwareData();
    virtual CTExitCode checkFirmwareDataUsingInput();
    virtual CTExitCode checkFirmwareDataUsingFile();

    virtual void checkInputConut(int inputCount);
};

} // CT

#endif // GETFIRMWAREIDCORE_GENERALSINGLE_H
