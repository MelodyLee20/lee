#include "getfirmwaredatacore.h"

#include "sislog.h"
#include "ctexception/ctexception.h"

using namespace CT;
using namespace SiS;
using namespace SiS::Procedure;

#define EXCEPTION_TITLE "GetFirmwareDataCore Exception : "
#define TAG "GetFirmwareDataCore"

GetFirmwareDataCore::GetFirmwareDataCore(GetFirmwareDataParameter* getFirmwareDataParameter) :
    IGetFirmwareDataCore(getFirmwareDataParameter),
    m_iGetFirmwareDataCore(0),
    m_getFirmwareDataCore_general(0),
    m_getFirmwareDataCore_generalSingle(0)
{
    SIS_LOG_I(SiSLog::getOwnerSiS(), TAG, "!!! \n!!! \n voltageType =%d!!! \n!!! \n",m_voltageType);
    this->m_sisProcedure = new SiSProcedure();
    m_iGetFirmwareDataCore = getGetFirmwareDataCore_general();

    /* force to set OSDeviceInterface */
    if( getFirmwareDataParameter->getOSDeviceInterfaceUserDefined() && getFirmwareDataParameter->getOSDeviceInterface() == DI_HID_USB )
    {
        m_sisProcedure->getSiSDeviceIO()->setUserConnectType(SiSDeviceAttribute::CON_819_USB_HID);
    }
    else if( getFirmwareDataParameter->getOSDeviceInterfaceUserDefined() && getFirmwareDataParameter->getOSDeviceInterface() == DI_HID_I2C )
    {
        m_sisProcedure->getSiSDeviceIO()->setUserConnectType(SiSDeviceAttribute::CON_819_HID_OVER_I2C);
    }
}

GetFirmwareDataCore::~GetFirmwareDataCore()
{
    if( m_getFirmwareDataCore_general != 0 )
    {
        delete m_getFirmwareDataCore_general;
        m_getFirmwareDataCore_general = 0;
    }

    if( m_getFirmwareDataCore_generalSingle != 0 )
    {
        delete m_getFirmwareDataCore_generalSingle;
        m_getFirmwareDataCore_generalSingle = 0;
    }

    if( m_sisProcedure != 0 )
    {
        delete m_sisProcedure;
        m_sisProcedure = 0;
    }
}

GetFirmwareDataCore_general*
GetFirmwareDataCore::getGetFirmwareDataCore_general()
{
    if( this->m_getFirmwareDataCore_general == 0 )
    {
        this->m_getFirmwareDataCore_general = new GetFirmwareDataCore_general(this->m_getFirmwareDataParameter);
        this->m_getFirmwareDataCore_general->setSiSProcedure(m_sisProcedure);
    }

    return this->m_getFirmwareDataCore_general;
}

GetFirmwareDataCore_generalSingle*
GetFirmwareDataCore::getGetFirmwareDataCore_generalSingle()
{
    if( this->m_getFirmwareDataCore_generalSingle == 0 )
    {
        this->m_getFirmwareDataCore_generalSingle = new GetFirmwareDataCore_generalSingle(this->m_getFirmwareDataParameter);
        this->m_getFirmwareDataCore_generalSingle->setSiSProcedure(m_sisProcedure);
    }

    return this->m_getFirmwareDataCore_generalSingle;
}

void
GetFirmwareDataCore::switchFlowVersion(DeviceType deviceType)
{
    switch (deviceType) {
    case DT_817:
    case DT_819:
        if( this->m_getFirmwareDataParameter->getSpecificChipUserDefined() )
        {
            this->m_iGetFirmwareDataCore = getGetFirmwareDataCore_generalSingle();
            this->m_iGetFirmwareDataCore->setVoltageType(m_voltageType);
            SIS_LOG_D(SiSLog::getOwnerSiS(), TAG, "switchFlowVersion : generalSingle");
        }
        else
        {
            this->m_iGetFirmwareDataCore = getGetFirmwareDataCore_general();
            this->m_iGetFirmwareDataCore->setVoltageType(m_voltageType);
            SIS_LOG_D(SiSLog::getOwnerSiS(), TAG, "switchFlowVersion : general");
        }
        break;
    case DT_INVALID:
    default:
        std::string msg = EXCEPTION_TITLE;
        char errorMsg[1024] = "";
        sprintf(errorMsg, "switchFlowVersion FAIL : invalid (unknow) deviceType (%d)", (int) deviceType);
        msg.append(errorMsg);
        throw CTException( msg );
    }
}

void
GetFirmwareDataCore::setSiSProcedure(SiSProcedure* sisProcedure)
{
    std::string msg = EXCEPTION_TITLE;
    char errorMsg[1024] = "";
    sprintf(errorMsg, "GetFirmwareDataCore::setSiSProcedure : only set by itself !");
    msg.append(errorMsg);
    throw CTException( msg );
}

void
GetFirmwareDataCore::init()
{
    CTBaseCore::init();

    if( this->m_getFirmwareDataParameter->getSpecificChipUserDefined() == true )
    {
        /* m_firmwareCheckIdMap */
        if( this->m_getFirmwareDataParameter->getFirmwareCheckIdMap().size() > 1 )
        {
            std::string msg = EXCEPTION_TITLE;
            char errorMsg[1024] = "";
            sprintf(errorMsg, "GetFirmwareDataCore::init : specific chip only allow one input !");
            msg.append(errorMsg);
            throw CTException( msg );
        }
        else if( this->m_getFirmwareDataParameter->getFirmwareCheckIdMap().size() == 1 )
        {
            /* re-assign input */
            std::string input = "";
            for(std::map<ChipIndexKey, std::string>::iterator it = this->m_getFirmwareDataParameter->getFirmwareCheckIdMap().begin();
                it != this->m_getFirmwareDataParameter->getFirmwareCheckIdMap().end();
                ++it)
            {
                input = it->second;
                break;
            }
            this->m_getFirmwareDataParameter->clearFirmwareCheckIdMap();
            this->m_getFirmwareDataParameter->setFirmwareCheckIdMap( this->m_getFirmwareDataParameter->getSpecificChip(), input );
        }

        /* m_binWrapMap */
        if( this->m_getFirmwareDataParameter->getBinWrapMap().size() > 1 )
        {
            std::string msg = EXCEPTION_TITLE;
            char errorMsg[1024] = "";
            sprintf(errorMsg, "GetFirmwareDataCore::init : specific chip only allow one bin file !");
            msg.append(errorMsg);
            throw CTException( msg );
        }
        else if( this->m_getFirmwareDataParameter->getBinWrapMap().size() == 1 )
        {
            /* re-assign bin */
            std::string binFileName = "";
            for(std::map<ChipIndexKey, BinWrap*>::iterator it = this->m_getFirmwareDataParameter->getBinWrapMap().begin();
                it != this->m_getFirmwareDataParameter->getBinWrapMap().end();
                ++it)
            {
                BinWrap* binWrap = it->second;
                binFileName = binWrap->getName();
                break;
            }
            this->m_getFirmwareDataParameter->clearBinWrapMap();
            this->m_getFirmwareDataParameter->setBinWrapMap( this->m_getFirmwareDataParameter->getSpecificChip(),
                                                      new BinWrap(binFileName, this->m_getFirmwareDataParameter->getSpecificChip()) );
        }
    }

    /* auto switch FlowVersion by OS DeviceType */
    switchFlowVersion( this->m_getFirmwareDataParameter->getOSDeviceType() );
}

CTExitCode
GetFirmwareDataCore::exec()
{
    return this->m_iGetFirmwareDataCore->exec();
}


