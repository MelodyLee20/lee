import sys
import os
import time
import re
import base64
import datetime
import io
import csv


def preprocess(data):
    # Preprocess

    tuning = {}
    for key, value in data['tuning'].items():
        params = []
        for row in value:
            out = parse_instance(row)
            params.append(out)

        tuning[key] = params

    return tuning




def parse_instance(data):
    # Parse the parameter instance

    num = len(data)

    address = str(data[0])
    LBS = str(data[1])
    bytes = str(data[2])
    register_name = str(data[3])
    default = str(data[4])
    description = ''
    define_correlation = ''

    if num == 6:
        description = str(data[5])

    if num == 7:
        description = str(data[5])
        define_correlation = str(data[6])

    value = default

    out = {
        'address_offset': address, 'LBS': LBS, 'bytes': bytes,
        'register_name': register_name, 'value': value,
        'default': default, 'description': description,
        'define_correlation': define_correlation
    }

    return out


def get_category_groups():

    # Phase 1
    phase1 = [
        "P1_ITO_Order", "P1_TxOrder", "P1_ITO", "P1_MutRxCali", "P1_SelfSense", "P1_I2C", "P1_Multichip",
        "P1_Interrupt", "P1_AFE", "P1_Hsync", "P1_HVDD", "P1_MutSense", "P1_Baseline", "P1_Dump",
        "P1_GPIO_Button", "P1_FlowControl", "P1_DMA_CMQ", "P1_LOW_POWER", "P1_USBDevDesc" ]
    phase1 = sorted(phase1)

    # Phase 2
    phase2 = [
        "P2_Coordinates", "P2_MutDetect", "P2_PowerMode", "P2_DacPostHandle", "P2_PALM_DETECT",
        "P2_DET_INIT_TOUCH", "P2_WATER_PROOF", "P2_CODING", "P2_FrameMeanShift" ]
    phase2 = sorted(phase2)

    # Phase 3
    phase3 = [
        "P3_TOP", "P3_Sticky", "P3_Sticky_First", "P3_Reject", "P3_Anti_Fly",
        "P3_Pressure_Meanshift", "P3_First_Pixel", "P3_Boundary_Draw", "P3_Anti_Jittered_Line",
        "P3_Anti_Step_Jittered_Line", "P3_HCK_Related" ]
    phase3 = sorted(phase3)

    # Others
    others = [
        "O_Button", "O_USB", "O_Die", "O_XL_Struct", "O_Debug", "O_EFT_NOISE_DETECT", "O_FREQUENCY_HOPPING",
        "O_Pen_Mode", "O_AdjustAxis", "O_TIMER", "O_INTP_ALG", "O_FLOAT_PALM", "O_Oversample", "O_ForceTouch",
        "O_EFT_DEHIGH", "O_D2D3_FUNCTION", "O_DefineData", "O_DefineTable"]
    others = sorted(others)

    # All categories
    all_groups = phase1 + phase2 + phase3 + others

    # Categories
    category_group = {
        'phase1_name': 'Phase 1',
        'phase2_name': 'Phase 2',
        'phase3_name': 'Phase 3',
        'others_name': 'Others',
        'phase1': phase1,
        'phase2': phase2,
        'phase3': phase3,
        'others': others,
        'all': all_groups
    }

    return category_group


def load_yaml(file_path):
    with open(file_path, 'r') as stream:
        data = yaml.load(stream, Loader=yaml.FullLoader)

    return data


def rows_filter(rows):

    out = []
    for row in rows:
        row = row_filter(row)
        if row:
            out.append(row)

    return out

def row_filter(data):

    is_valid = True

    first = data[0] if len(data) > 0 else ""

    pattern="^\s*$"
    if re.match(pattern, first):
        is_valid = False

    # Check if it is a commnet line
    pattern = "^/////"
    if re.match(pattern, first):
        is_valid = False

    # Check if it has an indent
    pattern = "^  *"
    if re.match(pattern, first):
        is_valid = False

    # Check if it has an tap
    pattern = "^\t"
    if re.match(pattern, first):
        is_valid = False

    out = data if is_valid else None

    return out

