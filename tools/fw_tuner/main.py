#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Firmware tuner.
"""

import sys
import os
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtGui
from ui_main import Ui_MainWindow
from utils import get_category_groups, preprocess
import ruamel.yaml

class FwTuner(QMainWindow, Ui_MainWindow):

    def __init__(self):
        # Explaining super is out of the scope of this article
        # So please google it if you're not familar with it
        # Simple reason why we use it here is that it allows us to
        # access variables, methods etc in the design.py file
        super().__init__()
        self.setupUi(self)  # This is defined in design.py file automatically
        # Online tuning
        self.online_tuning = True

        # Files
        self.file_loaded = None
        self.file_saved = None

        # It sets up layout and widgets that are defined
        self.btn_load.clicked.connect(self.load_yaml)  # When the button is pressed

        # Save file
        self.btn_save.clicked.connect(self.save_yaml)

        # Category group
        self.category_groups = get_category_groups()

        # Paramters
        self.params = None # Read-in parameters
        self.tuning = None # Tuning parameters

        # Current category
        self_current_category = None

        # Set the tree wiget
        self.set_tree_widget()

        # Test only
        #with open('OfflineTable_M.yaml', 'r') as stream:
        #    self.params = ruamel.yaml.load(stream, Loader=ruamel.yaml.RoundTripLoader)
        #self.tuning = preprocess(self.params)


    def load_yaml(self):
        # Load the yaml file

        # Open the parameter file
        file_path, file_type = QFileDialog.getOpenFileName(self, "Please open the parameter file.",
                               "./", "YAML Files (*.yaml)")
        self.file_loaded = file_path

        # Verufy if the file exists or not
        file_exists = False
        if len(file_path) != 0:
            file_exists = True
        dir_path = os.path.dirname(file_path)
        file_name = os.path.basename(file_path)

        # Read data
        if file_exists:
            # Show the filename
            text = "File Loaded: {}".format(file_name)
            self.lb_file_loaded.setText(text)

            with open(file_path, 'r') as stream:
                self.params = ruamel.yaml.load(stream, Loader=ruamel.yaml.RoundTripLoader)

            self.tuning = preprocess(self.params)

        else:
            return # Return to the main UI

        # Display parameters on the UI
        if self.params:

            # Configuration
            self.set_config()


    def save_yaml(self):
        # Save the yaml file

        output_path = 'output_params.yaml'

        with open(output_path, 'w') as yaml_file:
            ruamel.yaml.dump(self.params, yaml_file, default_flow_style=False, allow_unicode=True, Dumper=ruamel.yaml.RoundTripDumper)

        message = "Save the parameters in {}.".format(output_path)
        QMessageBox.information(self, "Info", message, QMessageBox.Close)


    def set_config(self):
        # Set the configuration parameters

        config = self.params['config']

        if config['tuning_type'] == 'online':
            self.online_tuning = True
        else:
            self.online_tuning = False

        # Set the label widgets
        text = "Tuning Type: {}".format(config['tuning_type'])
        self.lb_tuning_type.setText(text)

        text = "Start Address: {}".format(config['start_address'])
        self.lb_start_address.setText(text)

        text = "ID Address: {}".format(config['id_address'])
        self.lb_id_address.setText(text)

        text = "Main Code ID: {}".format(config['main_code_id'])
        self.lb_main_code_id.setText(text)

        text = "Built Time: {}".format(config['built_time'])
        self.lb_built_time.setText(text)

        text = "PIN Table ID: {}".format(config['pin_table_id'])
        self.lb_pin_table_id.setText(text)

        text = "USB Desc ID: {}".format(config['usb_desc_id'])
        self.lb_usb_desc_id.setText(text)


    def set_category(self, item, column):


        if self.params:
            config = self.params['config']
            tuning = self.tuning

        else: # File is not loaded
            message = "Please load the parameter file first!"
            QMessageBox.information(self, "Error", message, QMessageBox.Close)
            config = []
            tuning = {}

            return

        table = self.tableWidget
        table.disconnect() # Disconnect all slots

        # Selected category exists
        category = item.text(column)
        if category in self.category_groups['all']:

            self.current_category = category

            # Initialize the table
            self.tableWidget.setRowCount(0)

            if category in tuning.keys(): # When the category exits

                # Insert the items
                for i, data in enumerate(tuning[category]):
                    table.insertRow(i)

                    # Address offset
                    text = data["address_offset"]
                    self.insert_table_item(table, i, 0, text)

                    # Register name
                    text = data["register_name"]
                    self.insert_table_item(table, i, 1, text)

                    # Current value
                    text = data["value"]
                    self.insert_table_item(table, i, 2, text)

                    # Default value
                    text = data["default"]
                    self.insert_table_item(table, i, 3, text)

                    # Description
                    text = data["description"]
                    self.insert_table_item(table, i, 4, text)

                    # Defince correlation
                    text = data["define_correlation"]
                    self.insert_table_item(table, i, 5, text)

        # Cell changed
        table.currentCellChanged.connect(self.update_tuning_parameters)


    def update_tuning_parameters(self, row, col, prev_row, prev_col):
        # Update the register data

        category = self.current_category
        data = self.tuning[category]
        table = self.tableWidget

        if prev_col == 2: # Value field
            text = table.item(prev_row, prev_col).text()
            data[prev_row]["value"] = text


    def insert_table_item(self, table, row, col, text):

        item = QTableWidgetItem(text)
        item.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)

        # Only 'value' column can be edited.
        if col != 2:
            item.setFlags(Qt.ItemIsEnabled)
            item.setBackground(QtGui.QColor('lightGray'))

        table.setItem(row, col, item)


    def set_tree_widget(self):
        # Set the tree widget

        tree_widget = self.treeWidget
        cgroups = self.category_groups

        # Phase1
        phase1 = QTreeWidgetItem(tree_widget)
        phase1.setText(0, cgroups['phase1_name'])
        for candidate in cgroups['phase1']:
             item = QTreeWidgetItem(phase1)
             item.setText(0, candidate)


        # Phase2
        phase2 = QTreeWidgetItem(tree_widget)
        phase2.setText(0, cgroups['phase2_name'])
        for candidate in cgroups['phase2']:
             item = QTreeWidgetItem(phase2)
             item.setText(0, candidate)

        # Phase3
        phase3 = QTreeWidgetItem(tree_widget)
        phase3.setText(0, cgroups['phase3_name'])
        for candidate in cgroups['phase3']:
             item = QTreeWidgetItem(phase3)
             item.setText(0, candidate)

        # Others
        others = QTreeWidgetItem(tree_widget)
        others.setText(0, cgroups['others_name'])
        for candidate in cgroups['others']:
             item = QTreeWidgetItem(others)
             item.setText(0, candidate)

        # Set the category
        tree_widget.itemClicked.connect(self.set_category)


def main():
    app = QApplication(sys.argv)  # A new instance of QApplication
    tuner = FwTuner()
    tuner.show()
    app.exec_()  # and execute the app


if __name__ == '__main__':  # if we're running file directly and not importing it
    main()  # run the main function
