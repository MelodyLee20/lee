# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import plotly.plotly as py
import plotly.graph_objs as go
import dash_table
import pandas as pd

import base64
import datetime
import io

import numpy as np
from layout_utils import set_layout
from graph_utils import render_scatter, render_heatmap, render_surface

from fw_data import FwData
from utils import parse_uploaded, parse_raw_data, parse_register_data, parse_main_info

# Fw data
fw_data = FwData()


# Stylesheet
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

# Initialize the application
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# Set layout
set_layout(app)


# Callbacks

@app.callback(
    Output('debug', 'children'),
    [Input('upload-data', 'contents')]
)
def update_on_reload(pathname):

    phase1 = fw_data.reg_data['phase1']
    children = []
    for key in phase1.keys():
        id = "debug-{}".format(key)
        children.append(html.Div(id=id))

    return children

@app.callback(
    Output('phase1-contents', 'children'),
    [Input('upload-data', 'contents')]
)
def update_phase1(open):

    phase1 = fw_data.reg_data['phase1']

    children = []
    for key in phase1.keys():
        target_id = "group-{}".format(key)
        children.append(html.Div(key, id=target_id))

    return children

app.config.supress_callback_exceptions = True

def generate_output_id(value):
    return 'group-{}'.format(value)
    #return 'debug-{}'.format(value)

def generate_phase_callback(target_id):
    def output_callback(n):
        print(n)
        tmp_id = "debug-{}".format(target_id)
        return tmp_id

    return output_callback


phase1 = fw_data.reg_data['phase1']
for key in phase1.keys():
    target_id = "group-{}".format(key)
    output_id = generate_output_id(target_id)
    app.callback(
        Output(output_id, 'children'),
        [Input(target_id, 'n_clicks')]
    )(generate_phase_callback(target_id))

@app.callback(
    Output('test-div', 'children'),
    [Input('test-div', 'n_clicks')]
)
def test_callback(n_clicks):
    return "test,  n_clicks: {}".format(n_clicks)


@app.callback(
    Output('text-main-info', 'children'),
    [Input('upload-data', 'contents')],
    [State('upload-data', 'filename')]
)
def update_main_info(contents, filename):

    if contents is None:
        return ""

    raw = parse_uploaded(contents, filename)
    head_data, reg_data, post_data = parse_raw_data(raw)
    fw_data.head_data = head_data
    fw_data.reg_data = reg_data
    fw_data.post_data = post_data

    info = parse_main_info(head_data)

    if contents is None:
        children = []
    else:
        children = [
            html.Div('File loaded: {}'.format(filename)),
            html.Div('Main Code ID: {}'.format(info['main_code_id'])),
            html.Div('Built Time: {}'.format(info['built_time'])),
            html.Div('PIN Table ID: {}'.format(info['pin_table_id'])),
            html.Div('USB Desc ID: {}'.format(info['usb_desc_id'])),
            html.Div('Start Address: {}'.format(info['start_address'])),
            html.Div('ID Address: {}'.format(info['id_address'])),
        ]

    return children


@app.callback(
    Output('counter', 'children'),
    [Input('interval-component', 'n_intervals')])
def update_counter(n):
    return "counter: {}".format(n)

@app.callback(
    Output('filename', 'children'),
    [Input('btn-save-file', 'n_clicks')]
)
def save_file(n_clicks):
    return "Save as voltage.csv. n_click: {}".format(n_clicks)

if __name__ == '__main__':
    #app.run_server(debug=False, processes=1)
    app.run_server(debug=True, processes=1)
