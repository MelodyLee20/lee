# -*- coding: utf-8 -*-
import dash
import plotly.plotly as py
import plotly.graph_objs as go


def render_scatter(x=None, y=None, ymin=0, ymax=1, x_title="X"):
    # Render the heatmap figure

    xaxis_dict = dict(
        title = x_title
    )

    yaxis_dict = dict(
        title = "Voltage",
        range = [ymin, ymax]
    )

    figure={
        'data': [
            go.Scatter(y=y)
        ],
        'layout':
            go.Layout(
                title = 'Voltage distribution',
                autosize = True,
                margin = dict(l=65, r=50, b=65, t=90),
                xaxis = xaxis_dict,
                yaxis = yaxis_dict
            )

    }

    return figure

def render_heatmap(x=None, y=None, z=None, zmin=0, zmax=1):
    # Render the heatmap figure

    figure={
        'data': [
            go.Heatmap(z=z, zmin=zmin, zmax=zmax)
        ],
        'layout':
            go.Layout(
                title='Voltage distribution',
                autosize=True,
                margin=dict(l=65, r=50, b=65, t=90),
                xaxis=dict(
                    title="X"
                ),
                yaxis=dict(
                    title="Y"
                )
            )

    }

    return figure

def render_surface(x=None, y=None, z=None, zmin=0, zmax=-1, view_type=0):
    # Render the surface figure

    if zmin is None or zmax is None:
        zaxis_dict = dict(
            title="Voltage",
        )
    else:
        zaxis_dict = dict(
            title="Voltage",
            range= [zmin, zmax]
        )

    if view_type == 0: # Default view
        camera = None

    elif view_type == 1: # X-Z plane
        camera = dict(
            up=dict(x=0, y=0, z=1),
            center=dict(x=0, y=0, z=0),
            eye=dict(x=0.1, y=2.5, z=0.1)
        )
    elif view_type == 2: # Y-Z plane
        camera = dict(
            up=dict(x=0, y=0, z=1),
            center=dict(x=0, y=0, z=0),
            eye=dict(x=2.5, y=0.1, z=0.1)
        )

    figure={
        'data': [
            go.Surface(z=z)
        ],
        'layout':
            go.Layout(
                title='Voltage distribution',
                autosize=True,
                margin=dict(l=50, r=50, b=80, t=80),
                scene=dict(
                    camera=camera,

                    xaxis=dict(
                        title="X"
                    ),
                    yaxis=dict(
                        title="Y"
                    ),
                    zaxis=zaxis_dict
                )
            )
    }

    return figure

