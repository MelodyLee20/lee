
class CategoryGroup(object):

    def __init__(self):

        # Phase 1
        phase1 = [
            "ITOOrder", "TxOrder", "ITO", "MutRxCali", "SelfSense", "I2C", "Multichip",
            "Interrupt", "AFE", "Hsync", "HVDD", "MutSense", "Baseline", "Dump", "GPIO Button",
            "FlowControl", "DMA_CMQ", "LOW_POWER", "USBDevDesc" ]
        phase1 = sorted(phase1)


        # Phase 2
        phase2 = [
            "Coordinates", "MutDetect", "PowerMode", "DacPostHandle", "PALM_DETECT",
            "DET_INIT_TOUCH", "WATER_PROOF", "CODING", "FrameMeanShift" ]
        phase2 = sorted(phase2)

        # Phase 3
        phase3 = [
            "P3_TOP", "P3_Sticky", "P3_Sticky_First", "P3_Reject", "P3_Anti_Fly",
            "P3_Pressure_Meanshift", "P3_First_Pixel", "P3_Boundary_Draw", "P3_Anti_Jittered_Line",
            "P3_Anti_Step_Jittered_Line", "P3_HCK_Related" ]
        phase3 = sorted(phase3)

        # Others
        others = [
            "Button", "USB", "Die", "XL_Struct", "Debug", "EFT_NOISE_DETECT", "FREQUENCY_HOPPING",
            "Pen_Mode", "AdjustAxis", "TIMER", "INTP_ALG", "FLOAT PALM", "Oversample", "ForceTouch",
            "EFT_DEHIGH", "D2D3_FUNCTION", "DefineData", "DefineTable"]
        others = sorted(others)

        # Set values
        self.root = ["Phase 1", "Phase 2", "Phase 3", "Others"]
        self.phase1 = phase1
        self.phase2 = phase2
        self.phase3 = phase3
        self.others = others
        self.all = phase1 + phase2 + phase3 + others

