import sys
import os
import time
import re
import base64
import datetime
import io
import csv

import numpy as np
import pandas as pd


def parse_uploaded(contents, filename):
    content_type, content_string = contents.split(',')
    decoded = base64.b64decode(content_string)
    try:
        if 'csv' in filename:
            f = io.StringIO(decoded.decode('utf-8'))

    except Exception as e:
        print(e)


    # Prepare the raw data
    raw_data = []
    reader = csv.reader(f)
    for row in reader:
        raw_data.append(row)

    return raw_data

def parse_raw_data(data):

    head_data = data[:4]
    rows = rows_filter(data[4:])

    # Register data
    reg_data = {}
    name = None # Curent category name
    for row in rows:
        first = row[0]
        # Break the loop while entering the section of Pin Assignment
        if first == "Pin Assignment":
            break
        # Parse the parameter categories
        name_keyword = "////"
        name_pattern = "^{}[a-z, A-Z]".format(name_keyword)
        if re.match(name_pattern, first): # Check category name
            name = first.replace(name_keyword, "").strip()
            if name in reg_data.keys():
                data = reg_data[name]
            else:
                data = []
                reg_data[name] = data

        else: # Append the category data
            data.append(parse_register_data(row))

    # Rest data
    add_rest = False
    post_data = []
    for row in rows:
        first = row[0]
        # Break the loop while entering the section of Pin Assignment
        if first == "Pin Assignment":
            add_rest = True

        if add_rest:
            post_data.append(row)

    return head_data, reg_data, post_data


def parse_register_data(data):

    num_data = len(data)

    address_offset = data[0]
    LSB = data[1]
    Bytes = data[2]
    register_name = data[3]
    default = data[4]

    if num_data > 5:
        description = data[5]
    else:
        description = ""

    if num_data > 6:
        define_correlation = data[6]
    else:
        define_correlation = ""

    data = {"address_offset": address_offset, "LSB": LSB, "bytes": Bytes,
            "register_name": register_name, "value": default, "default": default,
            "description": description, "define_correlation": define_correlation}

    return data

def parse_main_info(data):

    inputs = data[2]

    info = {}
    info['start_address'] = inputs[1]
    info['id_address'] = inputs[4]
    info['main_code_id'] = inputs[7]
    info['built_time'] = inputs[9]
    info['pin_table_id'] = inputs[11]
    info['usb_desc_id'] = inputs[13]

    return info


def rows_filter(rows):

    out = []
    for row in rows:
        row = row_filter(row)
        if row:
            out.append(row)

    return out

def row_filter(data):

    is_valid = True

    first = data[0] if len(data) > 0 else ""

    pattern="^\s*$"
    if re.match(pattern, first):
        is_valid = False

    # Check if it is a commnet line
    pattern = "^/////"
    if re.match(pattern, first):
        is_valid = False

    # Check if it has an indent
    pattern = "^  *"
    if re.match(pattern, first):
        is_valid = False

    # Check if it has an tap
    pattern = "^\t"
    if re.match(pattern, first):
        is_valid = False

    out = data if is_valid else None

    return out

