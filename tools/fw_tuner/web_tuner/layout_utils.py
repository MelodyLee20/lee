# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.plotly as py


def set_layout(app):

    app.layout = html.Div(children=[

        # The memory store will always get the default on page refreshes
        dcc.Store(id='memory'),


        html.H1('FwTuner'),
        html.Div(id='counter', children='Counter'),


        dcc.Upload(html.Button('Load File'), id='upload-data'),

        # Main parameters
        html.Div('', id='text-main-info'),

        # Save the csv file
        html.Div([
            html.Button('Save file', id='btn-save-file'),
            html.Div(id='filename', children=''),
            html.Button('Connect Device', id='btn-connect-device'),
            html.Button('Close Device', id='btn-close-device'),
            html.Button('Write to RAM', id='btn-write-to-ram'),
            html.Button('Copy RON to BIN', id='btn-copy-ron-to-bin')
        ]),
        html.Hr(),

        # Parameters
        html.Div([
            html.Div([
                html.Label('Parameter Category'),
                html.Details([
                    html.Summary('Phase 1'),
                    html.Div('Contents', id='phase1-contents')
                ], id='phase1-details'),

                html.Details([
                    html.Summary('Phase 2'),
                    html.Div('Contents', id='phase2-contents')
                ], id='phase2-details'),

                html.Details([
                    html.Summary('Phase 3'),
                    html.Div('Contents', id='phase3-contents')
                ], id='phase3-details'),

                html.Details([
                    html.Summary('Others'),
                    html.Div('Contents', id='others-contents')
                ], id='others-details'),

            ], style={'float': 'left', 'width':'30%'}),

            # Parameter table
            html.Div("Table", style={'float': 'left', 'width':'70%'}),
        ]),

        html.Div("Footer", style={'clear': 'both'}),
        html.Div("Debug", id='debug'),


        # Parameter table
        html.Div(id='p-table'),

        # Time internals
        dcc.Interval(
            id='interval-component',
            interval=1*1000, # in milliseconds
            n_intervals=0
        ),

        html.Div("test", id='test-div'),

        # Hidden voltage data
        html.Div(id='hidden-voltage', style={'display': 'none'})
    ])


