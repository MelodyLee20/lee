import sys
import os
import re
import numpy as np

from category_group import CategoryGroup
from utils import rows_filter

class FwData(object):
    # Firmware Data

    def __init__(self):

        self.head_data = None
        self.reg_data = self.init_reg_data()
        self.post_data = None

    def init_reg_data(self):
        # Prepare the data structure

        data = {}
        data['phase1'] = {}
        data['phase2'] = {}
        data['phase3'] = {}
        data['others'] = {}

        # Phase 1
        phase1 = [
            "ITOOrder", "TxOrder", "ITO", "MutRxCali", "SelfSense", "I2C", "Multichip",
            "Interrupt", "AFE", "Hsync", "HVDD", "MutSense", "Baseline", "Dump", "GPIO Button",
            "FlowControl", "DMA_CMQ", "LOW_POWER", "USBDevDesc" ]

        # Phase 2
        phase2 = [
            "Coordinates", "MutDetect", "PowerMode", "DacPostHandle", "PALM_DETECT",
            "DET_INIT_TOUCH", "WATER_PROOF", "CODING", "FrameMeanShift" ]

        # Phase 3
        phase3 = [
            "P3_TOP", "P3_Sticky", "P3_Sticky_First", "P3_Reject", "P3_Anti_Fly",
            "P3_Pressure_Meanshift", "P3_First_Pixel", "P3_Boundary_Draw", "P3_Anti_Jittered_Line",
            "P3_Anti_Step_Jittered_Line", "P3_HCK_Related" ]

        # Others
        others = [
            "Button", "USB", "Die", "XL_Struct", "Debug", "EFT_NOISE_DETECT", "FREQUENCY_HOPPING",
            "Pen_Mode", "AdjustAxis", "TIMER", "INTP_ALG", "FLOAT PALM", "Oversample", "ForceTouch",
            "EFT_DEHIGH", "D2D3_FUNCTION", "DefineData", "DefineTable"]

        # Categories of phase 1, 2, 3 and others
        for c in sorted(phase1):
            data['phase1'][c] = []
        for c in sorted(phase2):
            data['phase2'][c] = []
        for c in sorted(phase3):
            data['phase3'][c] = []
        for c in sorted(others):
            data['others'][c] = []

        return data


