import React, { Component } from 'react';
import {init} from './Canvas';

function show_usage() {

    console.log('Hot keys:')
    console.log('* f: Switch the full screen mode.')
    console.log('* p: Toggle the point mode.')
    console.log('* r: Toggle the pressure mode.')
    console.log('* up: Increase the line width.')
    console.log('* down: Decrease the line width.')
    console.log('* Esc: Clear the plots.')
}	


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
			//isFullScreen: false,
		};
	};	

  componentDidMount() {
	  init();
		show_usage();
	}	

  render() {
    return (
      <div>
      </div>
    );
  }
}

export default App;
