Hot keys:

* f: Switch the full screen mode.
* p: Toggle the point mode.
* r: Toggle the pressure mode.
* up: Increase the line width.
* down: Decrease the line width.
* Esc: Clear the plots.
