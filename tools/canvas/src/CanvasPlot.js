import {Point} from "./Point"

function getBezierControl(p0, p1, p2, p3) {
  // Get the Bezier [cubic] control points.

  let x0 = p0.x
  let y0 = p0.y
  let x1 = p1.x
  let y1 = p1.y
  let x2 = p2.x
  let y2 = p2.y
  let x3 = p3.x
  let y3 = p3.y

  let x_c1 = x1 + 0.25*(x2-x0)
  let y_c1 = y1 + 0.25*(y2-y0)
  let x_c2 = x2 - 0.25*(x3-x1)
  let y_c2 = y2 - 0.25*(y3-y1)

  let p_c1 = new Point(x_c1, y_c1)
  let p_c2 = new Point(x_c2, y_c2)

  return [p_c1, p_c2]
}

function getQuadraticControl(p0, p1) {
  // Get the Bezier [cubic] control points.

  let x0 = p0.x
  let y0 = p0.y
  let x1 = p1.x
  let y1 = p1.y

  let x_c = 0.5*(x0+x1)
  let y_c = 0.5*(y0+y1)

  let p_c = new Point(x_c, y_c)

  return p_c
}

export {getBezierControl, getQuadraticControl}
