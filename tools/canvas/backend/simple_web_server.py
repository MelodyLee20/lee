import tornado.ioloop
import tornado.web
import os

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")

#自己擴充套件url指向的類，呼叫其對應方法post/get
class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")
    def post(self, *args, **kwargs):
        self.write("post request")

#配置檔案
static_folder = os.path.join(os.getcwd(), "static")
template_folder = os.path.join(os.getcwd(), "templates")

settings={
    "template_path": template_folder,
    "static_path": static_folder,
}

#尾部**settings為載入配置檔案
def make_app():
    return tornado.web.Application([
        (r"/", IndexHandler),
        (r"/(.*)", tornado.web.StaticFileHandler, {"path": static_folder}),
    ],**settings)

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
