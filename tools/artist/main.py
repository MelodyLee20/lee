import sys
import types

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtGui, QtWidgets, QtCore

from ui_main import Ui_MainWindow
from view import View


VERSION = '0.7.0' # Version

MODES = [ 'pen' ]

COLORS = [
    '#000000', '#82817f', '#820300', '#868417', '#007e03', '#037e7b', '#040079',
    '#81067a', '#7f7e45', '#05403c', '#0a7cf6', '#093c7e', '#7e07f9', '#7c4002',

    '#ffffff', '#c1c1c1', '#f70406', '#fffd00', '#08fb01', '#0bf8ee', '#0000fa',
    '#b92fc2', '#fffc91', '#00fd83', '#87f9f9', '#8481c4', '#dc137d', '#fb803c',
]


class Artist(QMainWindow, Ui_MainWindow):

    def __init__(self, *args, **kwargs):
        super(Artist, self).__init__(*args, **kwargs)
        self.setupUi(self)

        self.view = View()
        self.main_layout.addWidget(self.view)

        # Set the special button
        self.set_special_button()

        # Set the button of different mode
        mode_group = QButtonGroup(self)

        mode_group.setExclusive(True)
        for mode in MODES:
            btn = getattr(self, '%sButton' % mode)
            btn.pressed.connect(lambda mode=mode: self.view.set_mode(mode))
            mode_group.addButton(btn)

        # Slect line width
        self.select_line_width.valueChanged.connect(lambda s: self.view.set_config('size', s))

        # Clear the image
        self.btn_clear.clicked.connect(self.view.reset)
        # Save the image
        self.btn_save.clicked.connect(self.save_file)
        # Open the image
        self.btn_open.clicked.connect(self.open_file)
        # Exit the app
        self.btn_exit.clicked.connect(self.exit)


        # Initialize button colours.
        for n, hex in enumerate(COLORS, 1):
            btn = getattr(self, 'colorButton_%d' % n)
            btn.setStyleSheet('QPushButton { background-color: %s; }' % hex)
            btn.hex = hex  # For use in the event below

            def patch_mousePressEvent(self_, e):
                if e.button() == Qt.LeftButton:
                    self.view.set_primary_color(self_.hex)

                elif e.button() == Qt.RightButton:
                    self.view.set_secondary_color(self_.hex)

            btn.mousePressEvent = types.MethodType(patch_mousePressEvent, btn)

        # Setup to agree with Canvas.
        self.set_primary_color('#000000')
        self.set_secondary_color('#ffffff')

        # Fullscreen mode
        self.start_fullscreen()

    def set_special_button(self):

        # Special effects
        menu = QMenu(self)

        toggle_pressure = QAction('Toggle pressure', self)
        toggle_pressure.triggered.connect(self.toggle_pressure)

        hide_panel = QAction('Hide panel', self)
        hide_panel.triggered.connect(self.hide_panel)

        wacom_stylus = QAction('Wacom stylus', self)
        wacom_stylus.triggered.connect(self.use_wacom_stylus)

        sis_stylus = QAction('SiS stylus', self)
        sis_stylus.triggered.connect(self.use_sis_stylus)

        menu.addAction(sis_stylus)
        menu.addAction(wacom_stylus)
        menu.addSeparator()
        menu.addAction(toggle_pressure)
        menu.addAction(hide_panel)

        self.btn_special.setMenu(menu)


    def start_fullscreen(self):
        # Start the fullscreen mode

        # Set fullscreen without the frame
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.showFullScreen()

        # Fix the size of Scene
        screen = QGuiApplication.primaryScreen()
        geo = screen.geometry()
        self.view.scene.setSceneRect(0, 0, geo.width(), geo.height())

        # Reset
        #self.reset()

    def exit_fullscreen(self):
        # Exit the fullscreen mode

        # Normal size
        self.showNormal()

        # Reset
        self.reset()

    def hide_panel(self):
        self.panel.hide()
        self.start_fullscreen()

    def exit(self):
        sys.exit()

    def toggle_pressure(self):
        self.view.toggle_pressure()

    def toggle_interpolation(self):
        self.view.toggle_interpolation()

    def use_wacom_stylus(self):
        self.view.set_wacom_stylus()

    def use_sis_stylus(self):
        self.view.set_sis_stylus()

    def choose_color(self, callback):
        dlg = QColorDialog()
        if dlg.exec():
            callback( dlg.selectedColor().name() )

    def set_primary_color(self, hex):
        self.view.set_primary_color(hex)

    def set_secondary_color(self, hex):
        self.view.set_secondary_color(hex)


    def open_file(self):
        """
        Open image file for editing, scaling the smaller dimension and cropping the remainder.
        :return:
        """
        path, _ = QFileDialog.getOpenFileName(self, "Open file", "", "PNG image files (*.png); JPEG image files (*jpg); All files (*.*)")

        if path:
            pixmap = QPixmap()
            pixmap.load(path)

            iw = pixmap.width()
            ih = pixmap.height()

            # Get the size of the space we're filling.
            cw, ch = self.view.width(), self.view.height()

            if iw/cw < ih/ch:  # The height is relatively bigger than the width.
                pixmap = pixmap.scaledToWidth(cw)
                hoff = (pixmap.height() - ch) // 2
                pixmap = pixmap.copy(
                    QRect(QPoint(0, hoff), QPoint(cw, pixmap.height()-hoff))
                )

            elif iw/cw > ih/ch:  # The height is relatively bigger than the width.
                pixmap = pixmap.scaledToHeight(ch)
                woff = (pixmap.width() - cw) // 2
                pixmap = pixmap.copy(
                    QRect(QPoint(woff, 0), QPoint(pixmap.width()-woff, ch))
                )

            self.view.scene.addPixmap(pixmap)

    def save_file(self):
        """
        Save active view to image file.
        :return:
        """
        path, _ = QFileDialog.getSaveFileName(self, "Save file", "", "PNG Image file (*.png)")

        if path:
            rect = self.view.sceneRect().toRect()
            pixmap = self.view.grab(rect)
            pixmap.save(path, "PNG" )

    def reset(self):

        # Reset the view
        self.view.reset()

    def resizeEvent(self, event):
        self.reset()



if __name__ == '__main__':

    app = QApplication([])
    artist = Artist()
    artist.show()
    app.exec_()
