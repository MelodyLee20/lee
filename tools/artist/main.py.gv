import sys
import types

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtGui, QtWidgets, QtCore

from scene import Scene
from ui import Ui
#from ui_main import Ui_MainWindow
#from graphics_view import GraphicsView


MODES = [ 'pen' ]

COLORS = [
    '#000000', '#82817f', '#820300', '#868417', '#007e03', '#037e7b', '#040079',
    '#81067a', '#7f7e45', '#05403c', '#0a7cf6', '#093c7e', '#7e07f9', '#7c4002',

    '#ffffff', '#c1c1c1', '#f70406', '#fffd00', '#08fb01', '#0bf8ee', '#0000fa',
    '#b92fc2', '#fffc91', '#00fd83', '#87f9f9', '#8481c4', '#dc137d', '#fb803c',
]


class Artist(QGraphicsView, QMainWindow, Ui):

    def __init__(self, *args, **kwargs):
        super(Artist, self).__init__(*args, **kwargs)
        self.setupUi(self)

        self.scene = Scene()
        self.proxy = self.scene.addWidget(self.panel)
        #self.proxy.setAcceptedMouseButtons(Qt.LeftButton)
        #self.proxy.setFlag(QGraphicsItem.ItemIsSelectable)
        self.setScene(self.scene)


        # z-values
        self.fg_z_value = -1 
        #self.fg_z_value = 999
        self.bg_z_value = -1

        # Background item
        self.bg_item = None

        # Fix the  on the top
        self.proxy.setZValue(self.fg_z_value)

        # Mode
        self.mode = 'pen'

        # Move the panel to top-right coner
        self.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTop|QtCore.Qt.AlignTrailing)

        # Tablet tracking
        self.setTabletTracking(True)
        # Anti-aliasing
        self.setRenderHint(QPainter.Antialiasing)

        # No Scroll bar
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        # Set the special button
        self.set_special_button()


        self.background_color = QColor(Qt.white)

        #self.reset()

        # Set the button of different mode
        mode_group = QButtonGroup(self)
        mode_group.setExclusive(True)

        for mode in MODES:
            btn = getattr(self, '%sButton' % mode)
            btn.pressed.connect(lambda mode=mode: self.scene.set_mode(mode))
            mode_group.addButton(btn)

        # Slect line width
        self.select_line_width.valueChanged.connect(lambda s: self.scene.set_config('size', s))

        # Clear the image
        self.btn_clear.clicked.connect(self.reset)
        # Save the image
        self.btn_save.clicked.connect(self.save_file)
        # Open the image
        self.btn_open.clicked.connect(self.open_file)
        # Exit the app
        self.btn_exit.clicked.connect(self.exit)


        # Initialize button colours.
        for n, hex in enumerate(COLORS, 1):
            btn = getattr(self, 'colorButton_%d' % n)
            btn.setStyleSheet('QPushButton { background-color: %s; }' % hex)
            btn.hex = hex  # For use in the event below

            def patch_mousePressEvent(self_, e):
                if e.button() == Qt.LeftButton:
                    self.scene.set_primary_color(self_.hex)

                elif e.button() == Qt.RightButton:
                    self.scene.set_secondary_color(self_.hex)

            btn.mousePressEvent = types.MethodType(patch_mousePressEvent, btn)

        # Fullscreen mode
        self.start_fullscreen()

    def set_special_button(self):

        # Special effects
        menu = QMenu(self)
        toggle_smooth = QAction('Toggle smooth', self)
        toggle_smooth.triggered.connect(self.toggle_smooth_effect)

        toggle_pressure = QAction('Toggle pressure', self)
        toggle_pressure.triggered.connect(self.toggle_pressure)

        toggle_interpolation = QAction('Toggle interpolation', self)
        toggle_interpolation.triggered.connect(self.toggle_interpolation)

        hide_panel = QAction('Hide panel', self)
        hide_panel.triggered.connect(self.hide_panel)

        wacom_pen = QAction('Wacom pen', self)
        wacom_pen.triggered.connect(self.use_wacom_pen)

        color_pen = QAction('Color Pen', self)
        color_pen.triggered.connect(self.use_color_pen)

        menu.addAction(color_pen)
        menu.addAction(wacom_pen)
        menu.addSeparator()
        menu.addAction(toggle_smooth)
        menu.addAction(toggle_pressure)
        menu.addAction(toggle_interpolation)
        menu.addAction(hide_panel)

        self.btn_special.setMenu(menu)


    def mouseMoveEvent(self, e):

        print('view move')
        self.update()


    def start_fullscreen(self):
        # Start the fullscreen mode

        # Set fullscreen without the frame
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.showFullScreen()

        # Reset
        #self.reset()

        # Fix the size of Scene
        screen = QGuiApplication.primaryScreen()
        geo = screen.geometry()
        self.setSceneRect(0, 0, geo.width(), geo.height())

    def exit_fullscreen(self):
        # Exit the fullscreen mode

        # Normal size
        self.showNormal()

        # Reset
        self.reset()

    def hide_panel(self):
        self.panel.hide()
        self.start_fullscreen()

    def exit(self):
        sys.exit()

    def toggle_smooth_effect(self):
        self.toggle_anti_aliasing()

    def toggle_pressure(self):
        self.toggle_pressure()

    def toggle_interpolation(self):
        self.toggle_interpolation()

    def toggle_prediction(self):
        self.toggle_prediction()

    def use_wacom_pen(self):
        self.pen = Pen()

    def use_color_pen(self):
        self.pen = ColorPen()

    def choose_color(self, callback):
        dlg = QColorDialog()
        if dlg.exec():
            callback( dlg.selectedColor().name() )

    def open_file(self):
        """
        Open image file for editing, scaling the smaller dimension and cropping the remainder.
        :return:
        """
        path, _ = QFileDialog.getOpenFileName(self, "Open file", "", "PNG image files (*.png); JPEG image files (*jpg); All files (*.*)")

        if path:
            pixmap = QPixmap()
            pixmap.load(path)

            iw = pixmap.width()
            ih = pixmap.height()

            # Get the size of the space we're filling.
            cw, ch = self.width(), self.height()

            if iw/cw < ih/ch:  # The height is relatively bigger than the width.
                pixmap = pixmap.scaledToWidth(cw)
                hoff = (pixmap.height() - ch) // 2
                pixmap = pixmap.copy(
                    QRect(QPoint(0, hoff), QPoint(cw, pixmap.height()-hoff))
                )

            elif iw/cw > ih/ch:  # The height is relatively bigger than the width.
                pixmap = pixmap.scaledToHeight(ch)
                woff = (pixmap.width() - cw) // 2
                pixmap = pixmap.copy(
                    QRect(QPoint(woff, 0), QPoint(pixmap.width()-woff, ch))
                )

            self.bg_item = self.scene.addPixmap(pixmap)
            self.bg_item.setZValue(self.bg_z_value)


    def save_file(self):
        """
        Save active view to image file.
        :return:
        """
        path, _ = QFileDialog.getSaveFileName(self, "Save file", "", "PNG Image file (*.png)")

        if path:
            pixmap = self.pixmap()
            pixmap.save(path, "PNG" )


    def reset(self):

        if self.bg_item:
            self.scene.removeItem(self.bg_item)
            self.bg_item = None

        #self.scene.destroyItemGroup
        self.update()

        # Create the pixmap for display.
        #self.view.setPixmap(QPixmap(self.canvas.width(), self.canvas.height()))

        # Clear the canvas.
        #self.view.pixmap().fill(self.background_color)


    # Toggle anti-aliasing
    def toggle_anti_aliasing(self):
        self.use_anti_aliasing = not self.use_anti_aliasing

    def toggle_pressure(self):
        self.use_pressure = not self.use_pressure

    def toggle_interpolation(self):
        self.use_interpolation = not self.use_interpolation

    def toggle_prediction(self):
        self.use_prediction = not self.use_prediction


if __name__ == '__main__':

    app = QApplication([])
    artist = Artist()
    artist.show()
    app.exec_()
