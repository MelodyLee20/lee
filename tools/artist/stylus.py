import sys

import touch.fwio2 as fwio
from touch import hidparser
from touch.hidparser.Device import Collection
from touch.hidparser.UsagePages import GenericDesktop, Button
from touch.hidparser.UsagePage import UsagePage, UsageType, Usage
from touch.hidparser.enums import CollectionType, ReportType, ReportFlags, UnitSystem
import logging
import time
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

class Stylus:

    def __init__(self):

        self.type = ""
        self.id = None
        self.is_down = False
        self.x = None
        self.y = None
        self.pressure = 0.0

    def get_pressure(self):

        return self.pressure


class SisStylus(Stylus):

    def __init__(self):
        super(__class__, self).__init__()

        self.type = "SisStylus"

        self.touch_dev = None
        self.touch_desc = None

        self.is_devic_open = False
        self.task_period = 10 # msec

        self.pressure_raw_max = 1023

    def get_normalized_pressure(self, p):

        return 1.0*p/self.pressure_raw_max 


    def get_color(self):

        color = QColor(Qt.blue)

        # Red
        if self.id == 77:
            color = QColor(Qt.red)
        # Green
        if self.id == 83:
            color = QColor(Qt.green)

        # Blue
        if self.id == 85:
            color = QColor(Qt.blue)

        # Black
        if self.id == 89:
            color = QColor(Qt.black)

        return color

    def schedule_task(self):

        # setup timer to poll hid rawdata
        self.timer = QTimer()
        self.timer.timeout.connect(self.check_hidraw)
        self.timer.start(self.task_period)


    def open_device(self):

        # setup touch device
        self.touch_dev = fwio.core(logging.ERROR, ui="pyqt")
        if self.touch_dev.search() != True:
            self.touch_dev = None
            sys.exit()
        self.touch_dev.open()
        def callback(ws, status, id, msg, data=[]):
            print([status, msg, [hex(item).upper() for item in data[1:]]])
        self.touch_dev.start_server(cb=callback)
        self.touch_dev.enter_normal_mode()

        # setup hidparser
        fname = "27inch_report_descriptor"
        desc_bin = bytes([])
        with open(fname, "rb") as f:
            desc_bin = f.read()
        self.touch_desc = hidparser.parse(desc_bin)
        print(self.touch_desc)



    def check_hidraw(self):
        tmp = self.touch_dev.get_hidraw()
        if not tmp:
            return
        try:
            data = bytearray(tmp[0])
            report_id = data[0]
            self.touch_desc.deserialize(data)
            tp = self.touch_desc.reports[report_id].inputs.touch_screen
            for key in tp._attrs.keys():
                if key == "finger":
                     if tp.finger[0]:
                        finger = tp.finger[0]
                        for key in finger._attrs.keys():
                            value = finger.__getattr__(key)
                            if key == "contact_identifier":
                                self.id = value 
                                print("key:{}, value:{}".format(key, value))
                            elif key == "tip_pressure":
                                self.pressure = self.get_normalized_pressure(value)
                                print("key:{}, value:{}".format(key, value))
        except:
            print("parser error")
            #print("parser error, data{}".format(tmp))

