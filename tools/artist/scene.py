from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtGui, QtWidgets, QtCore

from stylus import Stylus, ColorStylus
from parabolic_model import ParabolicModel
from prediction_model import PredictionModel


class PathItem(QGraphicsPathItem):

    def __init__(self, points):
        super(PathItem, self).__init__()

        self.polygon = QPolygonF(points)

        self.painter_path = QPainterPath()
        self.painter_path.addPolygon(self.polygon)

        self.setPath(self.painter_path)


class Scene(QGraphicsScene):

    def __init__(self, *args, **kwargs):
        super(Scene, self).__init__(*args, **kwargs)
        self.mode = 'pen'

        self.point_tm1 = None
        self.point_tm2 = None

        # Prediction
        self.use_prediction = False
        self.point_pred = None
        self.point_pred_tm1 = None
        self.point_pred_tm2 = None

        # Setup primary color
        self.set_primary_color('#000000')
        self.set_secondary_color('#ffffff')


        self.pen = QPen()
        self.stylus = Stylus()

        self.path_items = []
        self.path_item = None
        self.points = None
        self.is_new_path = False

        self.parabolic_model = ParabolicModel()
        self.prediction_model = PredictionModel()


        self.config = { 'size': 20 }

        self.active_color = None
        self.preview_pen = None

        self.is_down = False

        # Anti-aliasing
        self.use_anti_aliasing = True

        # Pressure
        self.use_pressure = True

        # Interpolation
        self.use_interpolation = True

    def mousePressEvent(self, e):

        fn = getattr(self, "%s_mousePressEvent" % self.mode, None)

        if fn:
            return fn(e)

    def mouseMoveEvent(self, e):

        fn = getattr(self, "%s_mouseMoveEvent" % self.mode, None)
        if fn:
            return fn(e)

    def mouseReleaseEvent(self, e):

        fn = getattr(self, "%s_mouseReleaseEvent" % self.mode, None)

        proxy = self.items()[0]
        out = self.sendEvent(proxy, e)

        if fn:
            return fn(e)

    def tabletEvent(self, e):

        stylus = self.stylus
        stylus.x = e.globalX()
        stylus.y = e.globalY()
        stylus.pressure = e.pressure()

        if e.type() == QTabletEvent.TabletPress:
            self.stylus.is_down = True

        elif e.type() == QTabletEvent.TabletMove:
            self.stylus.is_down = True

        elif e.type() == QTabletEvent.TabletRelease:
            self.stylus.is_down = False

        # Finalization
        #e.accept()

        # Update view
        #self.update()


    # Pen events
    def pen_mousePressEvent(self, e):

        print('pen press')
        self.is_down = True

        # Initialize a new path item
        self.path_item = None
        self.points = []

        # History points
        self.point_tm1 = e.pos()
        self.point_tm2 = e.pos()

        if e.button() == Qt.LeftButton:
            self.active_color = self.primary_color
        else:
            self.active_color = self.secondary_color

        # Initialize the position of prediction
        self.point_pred = None

        proxy = self.items()[0]
        out = self.sendEvent(proxy, e)

    def pen_mouseMoveEvent(self, e):

        print('pen move')
        #print(e.pos())
        #print('is_down', self.is_down)

        if self.is_down:
        #if self.point_tm1:

            # Set the color
            if self.stylus.type == 'ColorPen':
                # To do ...
                print('Use color stylus')
                color = self.active_color
            else: # Wacom pen
                color = self.active_color

            # Line width
            line_width = self.get_line_width(self.config['size'])

            # Set the pen style
            #p.setPen(QPen(color, line_width, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))

            # Position
            point = e.pos()
            x = point.x()
            y = point.y()

            # Remove the temporary item
            if self.path_item:
                self.removeItem(self.path_item)
            # Append a new point
            self.points.append(point)
            self.path_item = PathItem(self.points)
            self.addItem(self.path_item)

            # Set the parabolic model
            x_tm1 = self.point_tm1.x()
            y_tm1 = self.point_tm1.y()
            x_tm2 = self.point_tm2.x()
            y_tm2 = self.point_tm2.y()
            print(self.items())

            '''
            if self.use_interpolation and x_tm1 - x_tm2 != 0 and x - x_tm1 != 0:
                self.parabolic_model.set_points(x_tm2, y_tm2, x_tm1, y_tm1, x, y)

                # Draw interplated points
                x1 = x_tm1 + 1.0/3.0*(x-x_tm1)
                y1 = self.parabolic_model.predict(x1)
                point_interp1 = QPoint(x1, y1)
                x2 = x_tm1 + 2.0/3.0*(x-x_tm1)
                y2 = self.parabolic_model.predict(x2)
                point_interp2 = QPoint(x2, y2)

                p.drawLine(self.point_tm1, point_interp1)
                p.drawLine(point_interp1, point_interp2)
                p.drawLine(point_interp2, pos)


            else:
                p.drawLine(self.point_tm1, pos)

            # Add the predicted line
            if self.use_prediction:
                pass

            '''

            # Save old points
            self.point_tm2 = self.point_tm1
            self.point_tm1 = point

            self.point_pred_tm2 = self.point_pred_tm1
            self.point_pred_tm1 = self.point_pred

            # Update the view
            self.update()

        proxy = self.items()[0]
        out = self.sendEvent(proxy, e)

    def pen_mouseReleaseEvent(self, e):

        self.is_down = False
        self.path_items.append(self.path_item)

        # Remove the predicted line
        if self.use_prediction and self.point_pred:

            # Set the pen style
            color = self.background_color
            line_width = self.get_line_width(self.config['size'])
            #painter.setPen(QPen(color, line_width, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin))
            #painter.drawLine(self.point_tm1, self.point_pred)

            # Update the canvas
            #self.canvas.update()

            # Initialize the position of prediction
            self.point_pred = None

        # Initialize the position of history points
        self.point_tm1 = None
        self.point_tm2 = None

        proxy = self.items()[0]
        out = self.sendEvent(proxy, e)

    def get_line_width(self, w0):
        # Return the line width.

        if self.use_pressure:
            if self.stylus.is_down:
                p = self.pen.pressure
                p0 = 0.5
                width = p/p0*w0
                if width <= 1:
                    width = 1
            else:
                width = w0
        else:
            width = w0

        return width

    def set_mode(self, mode):

        # Initialize the last position
        self.point_tm1 = None
        self.point_tm2 = None

        # Apply the mode
        self.mode = mode

    def set_primary_color(self, hex):
        self.primary_color = QColor(hex)

    def set_secondary_color(self, hex):
        self.secondary_color = QColor(hex)

    def set_config(self, key, value):
        self.config[key] = value
