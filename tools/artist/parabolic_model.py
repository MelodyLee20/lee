class ParabolicModel:
    '''
    Parabolic model.
    '''
    def __init__(self):

        # Coefficients
        self.a = None
        self.b = None
        self.c = None

    def set_points(self, x0, y0, x1, y1, x2, y2):
        '''
        Set points.
        (x0, y0), (x1, y1), (x2, y2)
        '''

        # Estimate the coefficients
        self.a = ((y1-y0)*(x2-x0) - (y2-y0)*(x1-x0)) / ((x1*x1-x0*x0)*(x2-x0) - (x2*x2-x0*x0)*(x1-x0))

        self.b = ((y1-y0)*(x2*x2-x0*x0) - (y2-y0)*(x1*x1-x0*x0)) / ((x1-x0)*(x2*x2-x0*x0) - (x2-x0)*(x1*x1-x0*x0))

        self.c = ((y2*x1*x1-y1*x2*x2)*(x1*x0*x0-x0*x1*x1) - (y1*x0*x0-y0*x1*x1)*(x2*x1*x1-x1*x2*x2)) / ((x1*x1-x2*x2)*(x1*x0*x0-x0*x1*x1) - (x0*x0-x1*x1)*(x2*x1*x1-x1*x2*x2))

    def predict(self, x):
        return self.a*x*x + self.b*x + self.c

