class PredictionModel:
    '''
    Prediction model.
    '''
    def __init__(self):

        # Coefficients
        self.ax = None
        self.bx = None
        self.cx = None
        self.ay = None
        self.by = None
        self.cy = None

    def set_points(self, x0, y0, x1, y1, x2, y2):
        '''
        Set points.
        (x0, y0) at t0 = 0
        (x1, y1) at t1 = 1*dt
        (x2, y2) at t2 = 2*dt
        '''

        # Estimate the coefficients
        self.ax, self.bx, self.cx = self.get_coeffs(x0, x1, x2)
        self.ay, self.by, self.cy = self.get_coeffs(y0, y1, y2)

    def predict(self, t=1.5):

        x = self.predict_value(self.ax, self.bx, self.cx, t)
        y = self.predict_value(self.ay, self.by, self.cy, t)

        return x, y

    def get_coeffs(self, s0, s1, s2):

        a = -0.5*(-s2 + 2.0*s1 - s0) # skip dt^-2
        b = 0.5*(-s2+4.0*s1-3.0*s0) # skip dt^-1
        c = 1.0*s0

        return a, b, c

    def predict_value(self, a, b, c, t):
        # Model: a*t^2 + b*t + c
        return a*t*t + b*t + c
