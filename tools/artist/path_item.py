from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtGui, QtWidgets, QtCore

class CubicPathItem(QGraphicsPathItem):

    def __init__(self, point_s, point_c1, point_c2, point_e):
        super(CubicPathItem, self).__init__()

        self.painter_path = QPainterPath()
        self.painter_path.moveTo(point_s)
        self.painter_path.cubicTo(point_c1, point_c2, point_e)

        self.setPath(self.painter_path)

class QuadPathItem(QGraphicsPathItem):

    def __init__(self, point_s, point_c, point_e):
        super(QuadPathItem, self).__init__()

        self.painter_path = QPainterPath()
        self.painter_path.moveTo(point_s)
        self.painter_path.quadTo(point_c, point_e)

        self.setPath(self.painter_path)

class LinePathItem(QGraphicsPathItem):

    def __init__(self, point_s, point_e):
        super(LinePathItem, self).__init__()

        self.painter_path = QPainterPath()
        self.painter_path.moveTo(point_s)
        self.painter_path.lineTo(point_e)

        self.setPath(self.painter_path)

class PathItem(QGraphicsPathItem):

    def __init__(self, point_s, point_e):
        super(PathItem, self).__init__()

        points = [point_s, point_e]
        self.polygon = QPolygonF(points)

        self.painter_path = QPainterPath()
        self.painter_path.addPolygon(self.polygon)

        self.setPath(self.painter_path)




