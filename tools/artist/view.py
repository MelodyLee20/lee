from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtGui, QtWidgets, QtCore

from ui_main import Ui_MainWindow

from stylus import Stylus, SisStylus

from path_item import CubicPathItem, QuadPathItem, LinePathItem, PathItem
from point_item import PointItem
from touch.tracers import KalmanTracer1d, KalmanTracer2d


import numpy as np

class View(QGraphicsView):

    def __init__(self):
        super(View, self).__init__()


        self.scene = QGraphicsScene()
        self.setScene(self.scene)

        # Tablet tracking
        self.setTabletTracking(True)
        # Anti-aliasing and smooth pixmap
        self.setRenderHint(QPainter.Antialiasing )
        self.setRenderHint(QPainter.SmoothPixmapTransform)


        # No Scroll bar
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        # Stylus
        self.wacom_stylus = Stylus()
        self.sis_stylus = SisStylus()
        self.stylus = self.wacom_stylus # Active stylus

        # Flag to use SiS stylus
        self.use_sis_stylus = False

        # Painter
        self.painter = QPainter()

        self.paths = []
        self.curves = []
        self.lines = []
        self.line = None
        self.points = None
        self.is_new_curve = False

        # Point index
        self.point_index = 0

        # Fake path
        self.fake_path = None

        self.mode = 'pen'

        self.config = { 'size': 20 }

        self.preview_pen = None

        self.point_tm1 = None
        self.point_tm2 = None
        self.point_tm3 = None

        # Pressure
        self.use_pressure = True
        self.pressure_tracer = None

        # Colors
        self.background_color = QColor(Qt.white)
        self.primary_color = QColor(Qt.black)
        self.secondary_color = self.background_color
        self.active_color = self.primary_color

        # Unit time interval
        self.dt = 1.0

        self.reset()


    def set_wacom_stylus(self):
        self.use_sis_stylus = False
        self.stylus = self.wacom_stylus

    def set_sis_stylus(self):
        self.use_sis_stylus = True 
        self.stylus = self.sis_stylus

        if not self.stylus.is_devic_open:
            self.stylus.open_device()
            self.stylus.schedule_task()

        # Set flag
        self.stylus.is_devic_open = True

    def callback(self, ws, status, msg, data=[]):
        print([status, msg, data])

    def mousePressEvent(self, e):
        fn = getattr(self, "%s_mousePressEvent" % self.mode, None)
        if fn:
            return fn(e)

    def mouseMoveEvent(self, e):
        fn = getattr(self, "%s_mouseMoveEvent" % self.mode, None)
        if fn:
            return fn(e)

    def mouseReleaseEvent(self, e):
        fn = getattr(self, "%s_mouseReleaseEvent" % self.mode, None)
        if fn:
            return fn(e)


    def tabletEvent(self, e):

        stylus = self.stylus
        stylus.x = e.globalX()
        stylus.y = e.globalY()

        # Save the pressure for Wacom stylus
        if stylus.type != 'SisStylus':
            stylus.pressure = e.pressure()

        if e.type() == QTabletEvent.TabletPress:
            self.stylus.is_down = True

        elif e.type() == QTabletEvent.TabletMove:
            self.stylus.is_down = True


        elif e.type() == QTabletEvent.TabletRelease:
            self.stylus.is_down = False

        # Finalization
        e.accept()

        # Update view
        #self.update()


    def reset(self):

        self.scene.clear()
        self.update()

    def set_primary_color(self, hex):
        self.primary_color = QColor(hex)

    def set_secondary_color(self, hex):
        self.secondary_color = QColor(hex)

    def set_config(self, key, value):
        self.config[key] = value

    def set_mode(self, mode):

        # Apply the mode
        self.mode = mode

    # Pen events
    def pen_mousePressEvent(self, e):

        self.point_index = 0

        point = e.pos()
        self.point_tm1 = point
        self.point_tm2 = point
        self.point_tm3 = point
        self.points = []

        # Set active color
        if e.button() == Qt.LeftButton:
            self.active_color = self.primary_color
        else:
            self.active_color = self.secondary_color

        # Reset the fake path
        self.fake_path = None

        # Create the tracer
        self.tracer = KalmanTracer2d(point.x(), point.y(), self.dt, P_var=1.0, Q_var=1.0, R_var=100.0)
        self.pressure_tracer = KalmanTracer1d(self.stylus.get_pressure(), self.dt, P_var=1.0, Q_var=1.0, R_var=10000.0)

        # Plot the first point
        radius = 0.5*self.get_line_width()
        self.add_point(point.x(), point.y(), radius)

    def pen_mouseMoveEvent(self, e):

        self.point_index += 1

        # Current point
        point = e.pos()

        # Tracer
        self.tracer.update(point.x(), point.y())
        x, y = self.tracer.get_pos()
        point = QPoint(x,y)

        # Pressure tracer
        self.pressure_tracer.update(self.stylus.get_pressure())

        # Remove the fake path
        self.remove_path(self.fake_path)
        self.fake_path = None


        if self.point_index >= 2:

            # Bezier curve
            point_c1, point_c2 = self.get_cubic_control(self.point_tm3, self.point_tm2, self.point_tm1, point)

            path = self.add_cubic_path(self.point_tm2, point_c1, point_c2, self.point_tm1)


            # Add the fake path
            vx, vy = self.tracer.get_vel()
            x_fake = x + vx*self.dt
            y_fake = y + vy*self.dt
            point_fake = QPoint(x_fake, y_fake)

            point_c1, point_c2 = self.get_cubic_control(self.point_tm2, self.point_tm1, point, point_fake)

            self.fake_path = self.add_cubic_path(self.point_tm1, point_c1, point_c2, point_fake)

            # Remove the fake path when the distance is small
            if self.get_distance(self.point_tm1, point) < 5.0:
                self.remove_path(self.fake_path)
                self.fake_path = None

        # Save the previous points
        self.point_tm3 = self.point_tm2
        self.point_tm2 = self.point_tm1
        self.point_tm1 = point

        # Update the view
        self.update()

    def pen_mouseReleaseEvent(self, e):

        # Remove the ending fake path
        self.remove_path(self.fake_path)
        self.fake_path = None

        # Reset the count
        self.point_index = 0

    def get_line_width(self):
        # Return the line width.

        w0 = self.config['size']

        if self.use_pressure:

            if self.use_sis_stylus or self.stylus.is_down:
                p= self.pressure_tracer.get_pos()
                p0 = 0.5
                width = p/p0*w0
                if width <= 1:
                    width = 1
            else:
                width = w0
        else:
            width = w0

        return width

    def toggle_pressure(self):
        self.use_pressure = not self.use_pressure

    def get_pen(self):

        # Set the color
        if self.stylus.type == 'SisStylus':
            color = self.stylus.get_color()
        else: # Wacom stylus
            color = self.active_color

        line_width = self.get_line_width()

        pen = QPen(color, line_width, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)

        return pen

    def get_brush(self):

        color = self.active_color
        brush = QBrush(color, Qt.SolidPattern)

        return brush

    def add_point(self, x, y, radius):
        # Add point item to scene.

        point = PointItem(x, y, radius)
        point.setPen(QPen(Qt.NoPen))
        point.setBrush(self.get_brush())

        self.scene.addItem(point)

        return point

    def add_cubic_path(self, point_s, point_c1, point_c2, point_e):
        # Add path item to scene.

        path = CubicPathItem(point_s, point_c1, point_c2, point_e)
        path.setPen(self.get_pen())

        self.scene.addItem(path)

        return path

    def add_quad_path(self, point_s, point_c, point_e):
        # Add path item to scene.

        path = QuadPathItem(point_s, point_c, point_e)
        path.setPen(self.get_pen())

        self.scene.addItem(path)

        return path

    def add_line_path(self, point_s, point_e):
        # Add path item to scene.

        path = LinePathItem(point_s, point_e)
        path.setPen(self.get_pen())

        self.scene.addItem(path)

        return path

    def add_path(self, point_s, point_e):
        # Add path item to scene.

        path = PathItem(point_s, point_e)
        path.setPen(self.get_pen())

        self.scene.addItem(path)

        return path

    def add_line(self, x1, y1, x2, y2):
        # Add line item to scene.

        line = QGraphicsLineItem()
        qline = QLineF(x1, y1, x2, y2)
        line.setLine(qline)
        line.setPen(self.get_pen())

        self.scene.addItem(line)

        return line

    def get_cubic_control(self, p0, p1, p2, p3):
        # Get the cubic control points.

        x0 = p0.x()
        y0 = p0.y()
        x1 = p1.x()
        y1 = p1.y()
        x2 = p2.x()
        y2 = p2.y()
        x3 = p3.x()
        y3 = p3.y()

        x_c1 = x1 + 0.25*(x2-x0)
        y_c1 = y1 + 0.25*(y2-y0)
        x_c2 = x2 - 0.25*(x3-x1)
        y_c2 = y2 - 0.25*(y3-y1)

        point_c1 = QPoint(x_c1, y_c1)
        point_c2 = QPoint(x_c2, y_c2)

        return point_c1, point_c2

    def remove_path(self, path):
        # Remove the path item

        if path:
            self.scene.removeItem(path)
            path = None


    def get_distance(self, p0, p1):

        x0 = p0.x()
        y0 = p0.y()
        x1 = p1.x()
        y1 = p1.y()

        d = np.sqrt((x1-x0)*(x1-x0)+(y1-y0)*(y1-y0))

        return d
