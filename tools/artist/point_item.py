from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtGui, QtWidgets, QtCore

class PointItem(QGraphicsEllipseItem):

    def __init__(self, x, y, radius):
        super(PointItem, self).__init__()

        width = 2.0*radius
        height = 2.0*radius

        x = x - 0.5*width
        y = y - 0.5*height

        self.setRect(x, y, width, height)


