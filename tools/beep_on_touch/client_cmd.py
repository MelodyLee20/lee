import socket
import argparse

HOST = '127.0.0.1'
PORT = 10000

if __name__ == '__main__':

    # Construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-m", "--mute", action='store_true', required=False, default=False, help="Mute.")
    ap.add_argument("-d", "--duration", required=False, default=1.0, help="Beep duration.")
    args = ap.parse_args()

    # Arguments
    print(args)
    mute = args.mute
    duration = args.duration

    def send_cmd(cmd):
        clientMessage = cmd
        client.sendall(clientMessage.encode('utf-8'))
        result = str(client.recv(1024), encoding='utf-8')

        return result

    # Connect to the server
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((HOST, PORT))

    # Mute
    if mute:
        result = send_cmd('mute')
    else:
        result = send_cmd('make_sound')
    print('result: ', result)



    # Close connection
    result = send_cmd('close')
    print('result: ', result)

    client.close()
