#from touch.input_device import PenPoint, get_pen_point, InputDevice
import argparse
import threading
#from queue import Queue
from multiprocessing import Queue
import sys
import json
import datetime
import logging
from input_device import InputDevice

import socket
from server_socket import ServerSocket
from lib_beep import beep


# Log
logging.basicConfig(filename='sisd.log',level=logging.DEBUG)


def beep_on_touch():

    for event in device.read_loop():
        logging.debug('beep')
        print("\a")


if __name__ == '__main__':

    # Construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-e", "--event_name", required=False, default='', help="Event name, i.g. 'event7'.")
    args = vars(ap.parse_args())

    # Arguments
    event_name = args['event_name']

    # Device path
    device_path = "/dev/input/{}".format(event_name)

    try:
        device = InputDevice(device_path)
    except:
        device = None
        print("Failed to open the device: {}".format(device_path))

    #if device:
    #    beep_on_touch()

    HOST = 'localhost'	# Symbolic name meaning all available interfaces
    PORT = 10000	# Arbitrary non-privileged port

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    print('Socket created')
    logging.debug('Socket created')

    try:
        s.bind((HOST, PORT))
    except:
        print('Failed to bind to the port {}.'.format(PORT))
        sys.exit()

    print('Socket bind complete')
    logging.debug('Socket bind complete')

    # Listen
    s.listen(3)
    logging.debug('Socket now listening')

    def beep_worker(q):

        data = None
        make_sound = True

        while 1:
            if q.qsize() > 0:
                data = q.get()
                logging.debug("command: ", data)
                logging.debug("beep_worker")

                if data == 'make_sound':
                    logging.debug('Deamon: ', data)
                    make_sound = True

                if data == 'mute':
                    logging.debug('Deamon: ', data)
                    make_sound = False

            if make_sound:
                beep_on_touch()

    # Queues
    q = Queue()

    # Create the beep tread
    beep_thread = threading.Thread(target=beep_worker, args=(q, ))
    beep_thread.daemon = True # It's important for finalization
    beep_thread.start()

    while True: # All connections
        # Wait to accept a connection - blocking call
        conn, addr = s.accept()
        print('Connected with ' + addr[0] + ':' + str(addr[1]))

        stop_server = False
        while True: # Single connection
            data = conn.recv(1024)
            data_str = data.decode('utf-8')
            print('data: ', data)
            print('data_str: ', data_str)

            reply_str = 'OK... {}'.format(data_str)
            reply = reply_str.encode('utf-8')

            if data_str == "stop_server":
                stop_server = True
                break

            if data_str == "touch":
                q.put("touch")

            if data_str == "make_sound":
                q.put("make_sound")

            if data_str == "mute":
                q.put("mute")

            if data_str == "close":
                conn.sendall(reply)
                break

            conn.sendall(reply)

        print('Close connection.')
        conn.close()

        if stop_server:
            break

    # Finalization
    print('Close the socket of server.')
    s.close()
    q.join()
