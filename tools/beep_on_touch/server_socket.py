import socket
import sys


class ServerSocket():

    def __init__(self):

        self.host = host
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def bind(self, host='localhost', port=10000):

        try:
            self.socket.bind((host, port))
        except:
            print('Failed to bind to the port {}.'.format(PORT))
            sys.exit()


        self.socket.listen(3)

    def start(self):

        #wait to accept a connection - blocking call
        conn, addr = self.socket.accept()
        print('Connected with ' + addr[0] + ':' + str(addr[1]))

        while 1:
            data = conn.recv(1024)
            print('data: ', data)
            data_str = data.decode('utf-8')
            print('data_str: ', data_str)
            reply_str = 'OK... {}'.format(data_str)

            reply = reply_str.encode('utf-8')

            end_bytes = b'exit\r\n'

            if data == end_bytes:
                break

            conn.sendall(reply)

        print('Close the connection.')
        conn.close()

        self.socket.close()
