import React from 'react';
import Plot from 'react-plotly.js';

// Graph size
var width = 800;
var height = 800;
// Plot configuration
var config = {staticPlot: true, displayModeBar: false}; 
var surfaceConfig = {staticPlot: false, displayModeBar: false}; 

class Lines extends React.Component {
  //constructor(props) {
  //  super(props);
	//}

  jsonFactory(y0, y1, ymin=0, ymax=-1) {

    if (ymin > ymax) {
      ymin = 0;
	    ymax = 10;
	  }	 

		var json = {
			data: [
	  		{
          y: y0,
          type: 'scatter',
          mode: 'lines+points',
	  			name: 'X-V'
		    },
		  	{
          y: y1,
          type: 'scatter',
          mode: 'lines+points',
	  			name: 'Y-V'
	  	  }
	  	],
		  layout: {
				yaxis: {
          title: 'Voltage',
          range: [ymin, ymax]
				},	
				width: width,
				height: height 
			}
		}

		return json
	}

	render() {
		const y0 = this.props.y0;
		const y1 = this.props.y1;
		const ymin = this.props.ymin
		const ymax = this.props.ymax
		const json = this.jsonFactory(y0, y1, ymin, ymax);

    return (
			  <Plot data={json.data} layout={json.layout} config={config} />
  	);	
	}
}	

class Heatmap extends React.Component {

  jsonFactory(z, zmin=0, zmax=-1) {

		var json = {
			data: [
	  		{
          z: z,
					type: 'heatmap',
				  zmin: zmin, 
					zmax: zmax,
					//showscale: true
		    }
	  	],
		  layout: {
				xaxis: { title: 'X' },
				yaxis: { title: 'Y', autorange: 'reversed'},
				width: width,
				height: height 
			}
		};

		return json;
	}

	render() {
		const z = this.props.z;
		const zmin = this.props.zmin
		const zmax = this.props.zmax

    const json = this.jsonFactory(z, zmin, zmax);

    return (
			  <Plot data={json.data} layout={json.layout} config={config} />
  	);	
	}
}	

class Surface extends React.Component {

  jsonFactory(z, zmin=0, zmax=-1, viewType=0) {

    var camera;

		if (zmin > zmax) {
      zmin = null;
      zmax = null;
		}	

    switch (viewType){
		  case 0:
		    camera = null;
		    break;
		  case 1:
		    camera = {
					up: {x:0, y:0, z:1},
          center: {x:0, y:0, z:0},
					eye: {x:0.1, y:2.5, z:0.1}
				};
	  	  break;
		  case 2:
		    camera = {
					up: {x:0, y:0, z:1},
          center: {x:0, y:0, z:0},
					eye: {x:2.5, y:0.1, z:0.1}
				};
	  	  break;
			default:
		    camera = null;
    }

		var json = {
			data: [
	  		{
          z: z,
					type: 'surface',
				  zmin: zmin, 
					zmax: zmax,
					showscale: true
		    }
	  	],
		  layout: {
				scene: {
          camera: camera,
  				xaxis: {title: 'X', autorange: 'reversed'},	
				  yaxis: {title: 'Y'},	
		  	  zaxis: {title: 'Voltage', range: [zmin, zmax]},	
				},
        margin: {
          l: 50,
          r: 50,
          b: 80,
          t: 80,
        },
				autosize: false,
				width: width,
				height: height 
			}
		};

		return json;
	}

	render() {
		const z = this.props.z;
		const zmin = this.props.zmin
		const zmax = this.props.zmax
		const viewType = this.props.viewType

    const json = this.jsonFactory(z, zmin, zmax, viewType);

    return (
			  <Plot data={json.data} layout={json.layout} config={surfaceConfig} />
  	);	
	}
}	

// Export moudules
export {Lines, Heatmap, Surface}
