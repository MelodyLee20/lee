import React from 'react';

class DeviceChoice extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
		const device = e.target.value
		const dataType = this.props.dataType

    this.props.onDeviceChange(device);

		// This can't work well 
	  if (dataType === 'cycle' || dataType === 'loop') {
      this.props.onDataTypeChange('diff');
		}	
  }

  render() {

    const device = this.props.device;

    return (
			<div>
			  <label>
          Device:&nbsp;
			  </label>
        <input type="radio" value='tp' checked={device === 'tp'} 
          onChange={this.handleChange} /> TP &nbsp;
        <input type="radio" value='pen' checked={device === 'pen'} 
          onChange={this.handleChange} /> Pen &nbsp;
			</div>
    );
  }
}

class DataTypeChoice extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);

		this.state = {
	    selectedOption: null,
	  }
  }

  handleChange(e) {
    this.props.onDataTypeChange(e.target.value);
  }

  render() {
		const device = this.props.device;
		const dataType = this.props.dataType;

		if (device === 'tp') { // for TP
      return (
				<div>
	  		  <label>
            Device:&nbsp;
	  		  </label>
          <input type="radio" value='mut_diff' checked={dataType === 'mut_diff'} 
            onChange={this.handleChange} /> Diff &nbsp;
          <input type="radio" value='mut_cycle' checked={dataType === 'mut_cycle'} 
            onChange={this.handleChange} /> Cycle &nbsp;
          <input type="radio" value='mut_loop' checked={dataType === 'mut_loop'} 
            onChange={this.handleChange} /> Loop &nbsp;
          <input type="radio" value='mut_base' checked={dataType === 'mut_base'} 
            onChange={this.handleChange} /> Base &nbsp;
          <input type="radio" value='mut_rawdata' checked={dataType === 'mut_rawdata'} 
            onChange={this.handleChange} /> Raw data &nbsp;
          <input type="radio" value='mut_rawdiff' checked={dataType === 'mut_rawdiff'} 
            onChange={this.handleChange} /> Raw diff &nbsp;
				</div>
      );
		} else { // for Pen
      return (
				<div>
	  		  <label>
            Device:&nbsp;
	  		  </label>
          <input type="radio" value='mut_diff' checked={dataType === 'mut_diff'} 
            onChange={this.handleChange} /> Diff &nbsp;
          <input type="radio" value='mut_base' checked={dataType === 'mut_base'} 
            onChange={this.handleChange} /> Base &nbsp;
          <input type="radio" value='mut_rawdata' checked={dataType === 'mut_rawdata'} 
            onChange={this.handleChange} /> Raw data &nbsp;
          <input type="radio" value='mut_rawdiff' checked={dataType === 'mut_rawdiff'} 
            onChange={this.handleChange} /> Raw diff &nbsp;
				</div>
      );
		}
  }
}


// Export modules
export {DeviceChoice, DataTypeChoice}
