import React from 'react';
import {Button} from 'react-bootstrap';
//import Button from 'react-bootstrap/lib/Button';

class DevOperations extends React.Component {
  constructor(props) {
    super(props);
		this.state = {
      message: null
		}	
		// Handlers 
		this.handleStatusArrive = this.handleStatusArrive.bind(this);
		this.handleStartClicked = this.handleStartClicked.bind(this);
		this.handleStopClicked = this.handleStopClicked.bind(this);
		this.handleExitClicked = this.handleExitClicked.bind(this);
		this.dc = this.props.dataCenter;
	}

  componentDidMount() {
		this.dc.setDevStatusArriveHandler(this.handleStatusArrive)
  }
  handleStatusArrive(msg) {
    //this.dataStatus = msg;
    console.log('status:'+ this.dataStatus);
    this.setState({message: msg});
	}	

  handleStartClicked(e) {
		this.setState({message: ' Starting'});
    const wsdata = '{"cmd":"devOperation","value":"start"}';
    this.dc.send(wsdata);
		console.log('Start')
  }		

  handleStopClicked(e) {
		this.setState({message: ' Stopping'});
    const wsdata = '{"cmd":"devOperation","value":"stop"}';
    this.dc.send(wsdata);
		console.log('Stop')
  }		

  handleExitClicked(e) {
		this.setState({message: ' Exiting'});
    const wsdata = '{"cmd":"devOperation","value":"exit"}';
    this.dc.send(wsdata);
		console.log('Exit')
  }		

	render() {
    return (
      <div>
			  <Button variant="outline-primary" onClick={this.handleStartClicked}> Start touch device
			  </Button>
			  <Button variant="outline-primary" onClick={this.handleStopClicked}> Stop touch device
			  </Button>
			  {this.state.message}
	    </div>
		);	
	}
}	

export {DevOperations}

