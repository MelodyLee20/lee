import React from 'react';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import d3 from 'd3';
import { getMinArray, getMaxArray } from './MathUtils';


function renderNumTable(z, vmin, vmax) {

  let globalMin = getMinArray(z); 
  let globalMax = getMaxArray(z); 

  if (vmin > vmax) {
		vmin = globalMin;
		vmax = globalMax;
	}	

	const rows = z.length;
	const cols = z[0].length;

  const data = prepareData(z, rows, cols);
  const columns = prepareColumns(cols, vmin, vmax);

  return <ReactTable data={data} columns={columns} sortable={false} />
}

function prepareData(data, rows, cols) {
 
  let out = [];
  let content;
  for (let i = 0; i < rows; i++) {
    content = {};
    for (let j = 0; j < cols; j++) {
      content["x"+j] = data[i][j].toFixed(0);
    }
    out.push(content);
  }	
  return out;
}


function prepareColumns(cols, min, max) {

	let colorPicker = palette(min, max);

  let columns = [];
  let content;
  for (let j = 0; j < cols; j++) {
    content = {
			Header: 'x'+j,
      accessor: 'x'+j, 
      Cell: props => ( 
        <div style={{backgroundColor: colorPicker(props.value)}} > 
          {props.value} 
        </div>
      ),
      minWidth: 100, // Minimum cell width 
    };
	  columns.push(content);
  }	
 
	return columns;
}

function palette(min, max) { // Support 10 steps of color scale
    var d = (max-min)/10;
    return d3.scale.threshold()
        .range(['#ffffe0','#ffe3af','#ffc58a','#ffa474','#fa8266','#ed645c','#db4551','#c52940','#aa0e27','#8b0000'])
        .domain([min+1*d,min+2*d,min+3*d,min+4*d,min+5*d,min+6*d,min+7*d,min+8*d,min+9*d,min+10*d]);
}



export {renderNumTable};
