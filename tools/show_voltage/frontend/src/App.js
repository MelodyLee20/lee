import React from 'react';
import { DeviceChoice, DataTypeChoice } from './Dropdowns';
import { DevOperations } from './DevOperations';
import { FileSaving } from './FileSaving';
import { OnlinePlot } from './Plots';
import { DataCenter } from './DataCenter';
import { finalizeBeforeClosingTab, disablePinZoom } from './CommonUtils';
import './App.css';

// Websocket address
//var wsUri = "ws://192.168.0.111:8050/ws";
var wsUri = "ws://127.0.0.1:8050/ws";


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			device: 'tp', 
			//dataType: 'mut_rawdata',
			dataType: 'mut_diff',
			data: null,
		};

		// Minimun and maximum values
		this.minSnap = null; // Snapshot
		this.maxSnap = null;
		this.minHist = null; // History
		this.maxHist = null;

    // Handlers
		this.handleConnectChange = this.handleConnectChange.bind(this);
		this.handleDeviceChange = this.handleDeviceChange.bind(this);
		this.handleDataTypeChange = this.handleDataTypeChange.bind(this);
		this.handleDataArrive = this.handleDataArrive.bind(this);

		// Data Center
    this.dataCenter = new DataCenter(wsUri);
		this.dataCenter.setDataArriveHandler(this.handleDataArrive)
		this.dataCenter.connect();

	}

  componentDidMount() {
		// Disable pin-zoom
		disablePinZoom();
		// Finalize before closing the browser tab.
    finalizeBeforeClosingTab(this.dataCenter);
	}	

  handleConnectChange() {
		const dc = this.dataCenter;
    if (dc.connected) {
      //dc.disconnect();
		}	else {
      dc.connect();
		}	
  }

  handleDeviceChange(value) {
		const dc = this.dataCenter;
    this.setState({device: value});
    const wsdata = '{"cmd":"changeDevMode","value":"'+ value+'"}';
		console.log(wsdata);
    dc.send(wsdata);
		dc.resetHist();
  }

  handleDataTypeChange(value) {
		const dc = this.dataCenter;
    this.setState({dataType: value});

    const wsdata = '{"cmd":"changeDataType","value":"'+ value+'"}';
    dc.send(wsdata);
		dc.resetHist();
  }

  handleDataArrive(timestamp) {
    this.setState({timestamp: timestamp});
	}	

  showConnectStatus(connected) {
		return connected? 'Connected': 'Disconnected';
	}	

  render() {
		const device = this.state.device;
		const dataType = this.state.dataType;
		const dataCenter = this.dataCenter;

    return (
      <div className="App">
			  <h2> ShowVoltage </h2>
			  <div>
          <DeviceChoice
			      device={device} 
			      onDeviceChange={this.handleDeviceChange}
			      onDataTypeChange={this.handleDataTypeChange} />
			  </div>
			  <div>
          <DataTypeChoice 
			      device={device}
			      dataType={dataType}
			      onDataTypeChange={this.handleDataTypeChange} />
			  </div>
			  <div>
          <DevOperations
			      dataCenter={dataCenter} />
			  </div>
			  <div>
          <FileSaving
			      dataCenter={dataCenter} />
			  </div>
			  <div>
          <OnlinePlot 
			      device={device} 
			      dataType={dataType}
			      dataCenter={dataCenter} />
			  </div>
      </div>
    );
  }
}

export default App;
