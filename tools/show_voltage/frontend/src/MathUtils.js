

function genRandom1D(nx=8){
    var result = [];
    for (var i = 0 ; i < nx; i++) {
      result[i] = Math.random()*10;
    }
    return result;
}

function genRandom2D(nx=8, ny=4){
    var result = [];
    for (var i = 0 ; i < nx; i++) {
        result[i] = [];
        for (var j = 0; j < ny; j++) {
            result[i][j] = Math.random()*10;
        }
    }
    return result;
}

function flatten(a) {
	// Flatten an array.

	const out = [].concat.apply([], a);
	return out;

}	



function minimum(a) {
	// Return the minimum value of an array.

	//const flat = a.flat();
	const flat = flatten(a);
	return Math.min(...flat)
}	

function maximum(a) {
	// Return the maximum value of an array.

	//const flat = a.flat();
	const flat = flatten(a);
	return Math.max(...flat)
}	


/*
 The functions below are deprecated.
*/

function getMinArray(a) {
	// Return the minimum value of an array.

	//const flat = a.flat();
	const flat = flatten(a);
	return Math.min(...flat)
}	

function getMaxArray(a) {
	// Return the maximum value of an array.

	//const flat = a.flat();
	const flat = flatten(a);
	return Math.max(...flat)
}	


// Export moudules
export {
	genRandom1D, genRandom2D, getMinArray, getMaxArray
}
