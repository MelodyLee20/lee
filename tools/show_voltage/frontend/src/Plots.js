import React from 'react';
//import Plot from 'react-plotly.js';
import {Lines, Heatmap, Surface} from './PlotUtils.js'
import {renderNumTable} from './NumTable.js'

function parseLimits(vmin, vmax) {

  var zmin = vmin
  var zmax = vmax
  if (isNaN(zmin) || zmin === '') {
    zmin = 0;
  }	
  if (isNaN(zmax) || zmax === '') {
    zmax = -1;
  }
  zmin = parseFloat(zmin);
  zmax = parseFloat(zmax);

  return [zmin, zmax]

}


class ValueLimits extends React.Component {
  constructor(props) {
    super(props);

		this.handleVminChange = this.handleVminChange.bind(this);
		this.handleVmaxChange = this.handleVmaxChange.bind(this);
	}

  handleVminChange(e) {
	  this.props.onVminChange(e.target.value);
  }		

  handleVmaxChange(e) {
	  this.props.onVmaxChange(e.target.value);
  }

	render() {
		const vmin = this.props.vmin;
		const vmax = this.props.vmax;

    return (
      <div>
        <label> vmin/vmax: </label>
 	      <input type="text" value={vmin} onChange={this.handleVminChange} />
		    <input type="text" value={vmax} onChange={this.handleVmaxChange} />
		  </div>
  	);	
	}
}	


class TPPlot extends React.Component {
  constructor(props) {
    super(props);
		this.state = {
			plotType: 'heatmap',
		};
		this.handleTypeChange = this.handleTypeChange.bind(this);

	}


  handleTypeChange(e) {
	  this.setState({plotType: e.target.value});
  }		

	render() {
		const data = this.props.data;
		const plotType = this.state.plotType;
		const zmin = this.props.vmin;
		const zmax = this.props.vmax;

		var plot;
		switch (plotType) {
			case 'heatmap':
        plot = <Heatmap z={data.z} zmin={zmin} zmax={zmax}/>;
				break;
			case 'surface':
        plot = <Surface z={data.z} zmin={zmin} zmax={zmax} viewType={0}/>;
				break;
			case 'surface(x-v)':
        plot = <Surface z={data.z} zmin={zmin} zmax={zmax} viewType={1}/>;
				break;
			case 'surface(y-v)':
        plot = <Surface z={data.z} zmin={zmin} zmax={zmax} viewType={2}/>;
				break;
			case 'table':
        plot = renderNumTable(data.z, zmin, zmax);
				break;
			default:
				console.log('Invalid type');
		}

    return (
      <div>
			  <div>
          <input type="radio" value='heatmap' checked={plotType === 'heatmap'} 
            onChange={this.handleTypeChange} /> Heatmap &nbsp;
          <input type="radio" value='surface' checked={plotType === 'surface'} 
            onChange={this.handleTypeChange} /> Surface &nbsp;
          <input type="radio" value='surface(x-v)' checked={plotType === 'surface(x-v)'} 
            onChange={this.handleTypeChange} /> Surface(X-V) &nbsp;
          <input type="radio" value='surface(y-v)' checked={plotType === 'surface(y-v)'} 
            onChange={this.handleTypeChange} /> Surface(Y-V) &nbsp;
          <input type="radio" value='table' checked={plotType === 'table'} 
            onChange={this.handleTypeChange} /> Table &nbsp;
			  </div>
			  <div>
			    {plot}
			  </div>
	    </div>
  	);	
	}
}	


class PenPlot extends React.Component {
  constructor(props) {
    super(props);
		this.state = {
			plotType: 'lines',
		};

    // Handlers
		this.handleTypeChange = this.handleTypeChange.bind(this);
	}

  componentDidMount() {
  }

  handleTypeChange(e) {
	  this.setState({plotType: e.target.value});
  }		

  prepareTableData(y0, y1) {
	
		const n0 = y0.length;
		const n1 = y1.length;
		const cols = Math.max(n0, n1); // Max. grid number

		const rows = 2;
		var data = [];
		for (let i=0; i<rows; i++) {
			const arr = Array.from(Array(cols), () => 0);
      data.push(arr);
    }
	
    // y0
		for (let j=0; j<n0; j++) {
			data[0][j] = y0[j];
		}

    // y1
		for (let j=0; j<n1; j++) {
			data[1][j] = y1[j];
		}

		return data;
	}

	render() {
		const plotType = this.state.plotType;
		const y0 = this.props.data.y0;
		const y1 = this.props.data.y1;
		const ymin = this.props.vmin;
		const ymax = this.props.vmax;
		const tableData = this.prepareTableData(y0, y1);

		var plot;
		switch (plotType) {
			case 'lines':
        plot = <Lines y0={y0} y1={y1} ymin={ymin} ymax={ymax}/>
				break;
			case 'table':
        plot = renderNumTable(tableData, ymin, ymax);
				break;
			default:
				console.log('Invalid type');
		}

    return (
      <div>
			  <div>
          <input type="radio" value='lines' checked={plotType === 'lines'} 
            onChange={this.handleTypeChange} /> Lines &nbsp;
          <input type="radio" value='table' checked={plotType === 'table'} 
            onChange={this.handleTypeChange} /> Table &nbsp;
			  </div>
			  <div >
			    {plot}
			  </div>
	    </div>
  	);	
	}
}	


class OnlinePlot extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
			timestamp: null,
			vmin: 0,
			vmax: -1,
		};

		// Handlers 
		this.handleDataArrive = this.handleDataArrive.bind(this);
		// Limits
		this.handleVminChange = this.handleVminChange.bind(this);
		this.handleVmaxChange = this.handleVmaxChange.bind(this);
		this.handleSetLimits = this.handleSetLimits.bind(this);
	}	

  componentDidMount() {
		this.props.dataCenter.setDataArriveHandler(this.handleDataArrive)
  }

  handleDataArrive(timestamp) {
    this.setState({timestamp: timestamp});
	}	

  handleVminChange(value) {
	  this.setState({vmin: value});
  }		

  handleVmaxChange(value) {
	  this.setState({vmax: value});
  }

  handleSetLimits(e) {
		const dc = this.props.dataCenter;
		const vmin = dc.min.toFixed(0); 
		const vmax = dc.max.toFixed(0); 
	  this.setState({vmin: vmin, vmax: vmax});
  }

  renderPlot() {
   
		const device = this.props.device;
		const dataType = this.props.dataType;
		const dc = this.props.dataCenter;
		var data = dc.fetchData(device, dataType);

		// Convert to the numerical values
    var [vmin, vmax] = parseLimits(this.state.vmin, this.state.vmax);

		var plot;
		if (device === 'tp') { // For TP
      plot = <TPPlot data={data} vmin={vmin} vmax={vmax} />
		} else { // For Pen
      plot = <PenPlot data={data} vmin={vmin} vmax={vmax}/>
		}
    
		return plot

	}	

	render() {
		const dc = this.props.dataCenter;
		// Limits
    var [vmin, vmax] = [this.state.vmin, this.state.vmax];

		// Data
		var min = dc.min; 
		var max = dc.max; 
		min = min ? dc.min.toFixed(0) : min;
		max = max ? dc.max.toFixed(0) : min;
		var minHist = dc.minHist; 
		var maxHist = dc.maxHist; 
		minHist = minHist ? dc.minHist.toFixed(0) : minHist;
		maxHist = maxHist ? dc.maxHist.toFixed(0) : minHist;

    return (
      <div>
			  <div> Snap. min/max: {min} / {max}</div>
			  <div> Hist. min/max:{minHist} / {maxHist} </div>
 	      <input type="button" onClick={this.handleSetLimits} value="Set limits"/>
			  <ValueLimits 
			    vmin={vmin} vmax={vmax} 
			    onVminChange={this.handleVminChange} onVmaxChange={this.handleVmaxChange} />
			  {this.renderPlot()}
	    </div>
  	);	
	}
}	

export {OnlinePlot}
