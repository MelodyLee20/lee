import { genRandom1D, genRandom2D } from './MathUtils';
import { getMinArray, getMaxArray } from './MathUtils';



class DataCenter { 
	// Data Center which takes care the data exchange via websocket.

	constructor(wsUri) {
    this.data = [];
		this.timestamp = 0;

		// Websocket
		this.wsUri = wsUri;
		this.websocket = null;
		this.connected = false;
		this.dataSent = null;
		this.dataReceived = null;
		this.dataStatus = null;

		// Handler for the situation when data arrives
		this.dataArriveHandler = null;

		// Minimun and maximum values
		this.min = null; // Snapshot
		this.max = null;
		this.minHist = null; // History
		this.maxHist = null;

	}

  connect() {
		this.websocket = new WebSocket(this.wsUri);

		// callbacks
	  this.websocket.onopen = (e) => { 
			console.log('Connected');
		  this.connected = true;
      // Test sending data
			const data = '{"cmd":"test","value":"Hello"}'
			console.log('send: ' + data);
		  this.websocket.send(data);
		};
	  this.websocket.onclose = (e) => { 
			console.log('Disconnected');
		  this.connected = false;
		};
	  this.websocket.onmessage = (e) => {
			this.dataReceived = JSON.parse(e.data);

      if (this.dataReceived.type === "data") {
        //console.log('Received id: '+ this.dataReceived.frameId);
        this.updateTimestamp();
        if (this.dataArriveHandler !== null) { 
          this.dataArriveHandler(this.timestamp);
        }
      } else if (this.dataReceived.type === "fileStatus") {
        console.log('Type: '+ this.dataReceived.type);
        if (this.fileStatusArriveHandler !== null) {
          this.fileStatusArriveHandler(this.dataReceived.data);
        }
      } else if (this.dataReceived.type === "devStatus") {
        console.log('Type: '+ this.dataReceived.type);
        if (this.devStatusArriveHandler !== null) { 
          this.devStatusArriveHandler(this.dataReceived.data);
        }
      }
		};
	  this.websocket.onerror = (e) => { 
			console.log('Connection failed');
		};
	}

	disconnect() {
		console.log('user Disconnected');
		this.websocket.close();
	  this.connected = false;
	}

  send(data) {
		this.dataSent = data
		this.websocket.send(data);
	}	

  updateTimestamp() {
    this.timestamp++;
	}	

  setDevStatusArriveHandler(handler) {
		this.devStatusArriveHandler = handler;
	}

  setFileStatusArriveHandler(handler) {
		this.fileStatusArriveHandler = handler;
	}

  setDataArriveHandler(handler) {
		this.dataArriveHandler = handler;
	}

  resetHist() {
	  this.minHist = null;
	  this.maxHist = null;
	}

  setHistData(arr2d) {

    var min = getMinArray(arr2d);
    var max = getMaxArray(arr2d);

    this.min = min;
    this.max = max;

		if (this.minHist == null || min < this.minHist) {
      this.minHist = min; 
    }

		if (this.maxHist == null || max > this.maxHist) {
      this.maxHist = max; 
    }
	
	}

	fetchData(device, dataType) { 
		// Fetch data via websocket

		var data;
		var arr2d; 

		if (device === 'tp') { // For TP
      if (this.dataReceived == null || this.dataReceived.dev !== 'tp') {
			  data = {z: genRandom2D(80,40)}	
      } else {
			  data = {z: this.dataReceived.data}
      }
			arr2d = data.z;
		} else { // For Pen
      if (this.dataReceived == null || this.dataReceived.dev !== 'pen' ) {
        data = {
          y0: genRandom1D(80),
          y1: genRandom1D(40)
        }
      } else {
        data = {
          y0: this.dataReceived.data[0],
          y1: this.dataReceived.data[1]
        }
      }
			arr2d = [...data.y0, ...data.y1]
		}	
    
    this.setHistData(arr2d);

    this.data = data;

		return this.data;
	}	

}

export {DataCenter}
