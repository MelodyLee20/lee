import React from 'react';

class ToggleButton extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
		this.props.onToggleChange();
  }

  render() {
    return (
      <button onClick={this.handleClick}>
        {this.props.isToggleOn ? 'ON' : 'OFF'}
      </button>
    );
  }
}

export {ToggleButton}
