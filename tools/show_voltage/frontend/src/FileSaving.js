import React from 'react';
import {Button} from 'react-bootstrap';
//import Button from 'react-bootstrap/lib/Button';

class FileSaving extends React.Component {
  constructor(props) {
    super(props);
		this.state = {
      message: null
		}	
		// Handlers 
		this.handleStatusArrive = this.handleStatusArrive.bind(this);
		this.handleStartClicked = this.handleStartClicked.bind(this);
		this.handleStopClicked = this.handleStopClicked.bind(this);
		this.dc = this.props.dataCenter;
	}

  componentDidMount() {
		this.dc.setFileStatusArriveHandler(this.handleStatusArrive)
  }
  handleStatusArrive(msg) {
    //this.dataStatus = msg;
    console.log('status:'+ this.dataStatus);
    this.setState({message: msg});
	}	

  handleStartClicked(e) {
		this.setState({message: ' Starting'});
    const wsdata = '{"cmd":"saveData","value":"start"}';
    this.dc.send(wsdata);
		console.log('Start')
  }		

  handleStopClicked(e) {
		this.setState({message: ' Stopping'});
    const wsdata = '{"cmd":"saveData","value":"stop"}';
    this.dc.send(wsdata);
		console.log('Stop')
  }		

	render() {
    return (
      <div>
			  <Button variant="outline-primary" onClick={this.handleStartClicked}> Save File Start
			  </Button>
			  <Button variant="outline-primary" onClick={this.handleStopClicked}> Save File Stop
			  </Button>
			  {this.state.message}
	    </div>
		);	
	}
}	

export {FileSaving}

