import inspect
import os
from datetime import datetime
import csv
import numpy as np

# base
class sisCsvWriter():
    def __init__(self, tp_type, csv_dir):
        self.csv_dir = csv_dir + "/data"
        if not os.path.exists(self.csv_dir):
            os.makedirs(self.csv_dir)

        self.csv_path = "%s/tmp.csv" % self.csv_dir
        self.csv_path_bak = "%s/tmp.csv.bak" % self.csv_dir
        self.file_bak = None
        self.writer_bak = None
        self.frame_count = 0
        self.active = False
        self.tp_type = tp_type

    def write_start(self, _sis_rawdata):
        self.file_bak = open(self.csv_path_bak,"w")
        self.writer_bak = csv.writer(self.file_bak)
        data = [ 
                 [_sis_rawdata.tool_name],
                 ['FrameCount', _sis_rawdata.frame_count],
                 ['Width', _sis_rawdata.width_tx],
                 ['Height', _sis_rawdata.height_rx],
                 ['Type', _sis_rawdata.type],
                 [],
        ]
        self.writer_bak.writerows(data)
        self.frame_count = 0
        self.active = True

    def write_frame(self, _sis_rawdata):
        data = []
        if self.tp_type == 'pen':
            data = [ 
                 ['No', _sis_rawdata.frame_number, '%dms' % _sis_rawdata.frame_arrived_time , 'Read Error Times' , _sis_rawdata.read_error_times],
                 _sis_rawdata.tx_list,
                 _sis_rawdata.rx_list,
                 ['ReportID', _sis_rawdata.report_id],
                 ['Invert', _sis_rawdata.invert],
                 ['BarrSwi', _sis_rawdata.barr_swi],
                 ['SecBarr', _sis_rawdata.sec_barr],
                 ['Eraser', _sis_rawdata.eraser],
                 ['TipSwi', _sis_rawdata.tip_swi],
                 ['InRange', _sis_rawdata.in_range],
                 ['X', _sis_rawdata.position_x],
                 ['Y', _sis_rawdata.position_y],
                 ['Pressure', _sis_rawdata.pressure],
                 ['TiltX', _sis_rawdata.tilt_x],
                 ['TiltY', _sis_rawdata.tilt_y],
                 [],
            ]
        elif self.tp_type == 'tp':
            data = [ 
                 ['No', _sis_rawdata.frame_number, '%dms' % _sis_rawdata.frame_arrived_time , 'Read Error Times' , _sis_rawdata.read_error_times],
            ]

            # save width_tx with rx
            tx = _sis_rawdata.get_values().tolist()
            for i in range(0, _sis_rawdata.width_tx):
                rx_list_tmp = tx[i]
                data.append(rx_list_tmp)
            data.append([])
     
        self.frame_count = _sis_rawdata.frame_count
        self.writer_bak.writerows(data)


    def write_finished(self, _sis_rawdata):
        self.active = False
        self.write_back_frame_count(_sis_rawdata)
        print("save file!")

    def write_back_frame_count(self, _sis_rawdata):
        # prepare to write csv
        self.time = datetime.now().strftime("%Y_%m_%d-%H-%M-%S")
        self.csv_path = "%s/%s_%s_%s.csv" % (self.csv_dir, _sis_rawdata.tool_name, _sis_rawdata.type , self.time) # assign csv path
        print("write .csv : %s" % self.csv_path)
        # close csv_bak
        self.file_bak.close()
        # modify frame_count row
        frame_count_index = 1
        new_value = 'FrameCount,%d\r\n' % _sis_rawdata.frame_count
        with open(self.csv_path_bak, "r") as f_bak, open(self.csv_path, 'w') as f:
            # gives you a list of the lines
            contents = f_bak.readlines()
            # delete the old line and insert the new one
            contents.pop(frame_count_index)
            contents.insert(frame_count_index, new_value)
            # join all lines and write it back
            contents = "".join(contents)
            f.write(contents)
            pass # end_with
        # remove csv_bak
        self.remove_csv_bak()

    def remove_csv_bak(self):
        try:
            os.remove(self.csv_path_bak)
            print("remove .csv bak : %s" % self.csv_path_bak)
        except:
            print(".csv bak doesn't exist : %s" % self.csv_path_bak)
        
    def __del__(self):
        print("sis_csv_writer __del__, begin")
        self.file_bak.close()


