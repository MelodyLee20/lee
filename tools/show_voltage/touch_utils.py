# -*- coding: utf-8 -*-
import inspect
import optparse
import os
import sys
from subprocess import Popen, PIPE
from remote_in_system import SiSOSTouchDevice
from sis_rawdata import sis_rawdata_mut
from sis_rawdata import sis_rawdata_pen
from sis_csv_writer import sisCsvWriter
#import colorama as color
import time
import numpy as np
import json

class touchUtils():
    def __init__(self, options, WsHandler):
        self.frame_idx = 0
        self.touch_dev = None
        self.touch_connected = False
        self._sis_rawdata = None
        self._sis_csv_writer = None
        self.options = options
        self.active = False
        self.ws = WsHandler

    def initialize_sis_rawdata(self):
        _sis_rawdata = None
        if(self.options.source_rawdata == 'tp' and
             (self.options.type_rawdata == 'mut_cycle' or
                self.options.type_rawdata == 'mut_loop' or
                self.options.type_rawdata == 'mut_base' or
                self.options.type_rawdata == 'mut_rawdata' or
                self.options.type_rawdata == 'mut_diff' or
                self.options.type_rawdata == 'mut_rawdiff')):
            _sis_rawdata = sis_rawdata_mut()
        elif(self.options.source_rawdata == 'pen' and
                 (self.options.type_rawdata == 'mut_base' or
                    self.options.type_rawdata == 'mut_rawdata' or
                    self.options.type_rawdata == 'mut_diff' or
                    self.options.type_rawdata == 'mut_rawdiff')):
            _sis_rawdata = sis_rawdata_pen()
        #else:
        #    print '[Warning] Please use -s (--source_rawdata) and -t (--type_rawdata) to indicate a rawdata type.'

        return _sis_rawdata

    def initialize_sis_csv_writer(self):
        TOOL_ROOT_DIR = os.path.dirname(os.path.realpath(inspect.getfile(inspect.currentframe())))
        print("PATH = [{}]".format(TOOL_ROOT_DIR))
        _sis_csv_writer = sisCsvWriter(self.options.source_rawdata, TOOL_ROOT_DIR) # (not write a .csv)

        return _sis_csv_writer


    def initialize_sis_touch(self):
        # Connect to the DUT
        ret = False
        while (ret == False):
            print(self.options)
            self.touch_dev = SiSOSTouchDevice(self.options.addr, self.options.capturer, self.options.source_rawdata, self.options.type_rawdata, self.options.report_in_frame, self.options.delay_85)
            ret = self.touch_dev._GetInitFrame(self._sis_rawdata)
            time.sleep(0.5)
        self.touch_connected = True

    def touch_init(self):
        # Connect to the DUT
        print("main begin : %s" % __file__)
        # Parse and validate the command line arguments

        # new sis_rawdata
        self._sis_rawdata = self.initialize_sis_rawdata()
        print("inited rawdata")

        # Workaround for raw diff
        request_rawdiff = False
        if (self.options.type_rawdata == 'mut_rawdiff'):
            request_rawdiff = True
            self.options.type_rawdata = 'mut_rawdata'
        else:
            request_rawdiff = False
        self._sis_rawdata.init_rawdiff(request_rawdiff)


        self.initialize_sis_touch()
        print("inited touch_dev")

        # new csv_pen_writer
        self._sis_csv_writer = self.initialize_sis_csv_writer()
        print("inited writer")

        self._sis_rawdata.print_all()
        print("inited finish")

        # get frame
        self.frame_idx = 0
        self.active = True
        self.touch_dev._GetNextFrame(self._sis_rawdata) # get next frame

    def start(self):
        self.touch_init()

    def stop(self):
        if self.active == True:
            self.active = False
            self.touch_connected = False
            self.touch_dev.quit()
            if self._sis_csv_writer.active == True:
                self._sis_csv_writer.write_finished(self._sis_rawdata)
            time.sleep(0.5)

    def restart(self, data_type=None, source_type=None):
        self.stop()
        if data_type is not None:
            self.options.type_rawdata = data_type
        if source_type is not None:
            self.options.source_rawdata = source_type
        print(self.options)
        self.active = False
        self.touch_connected = False
        self.start()

    def get_data(self):
        rawdata = self._sis_rawdata
        if(self.active == True and self.touch_dev.capturer_process != None and (self.frame_idx < int(self.options.get_frame_num) or int(self.options.get_frame_num) <= 0)):
            print("frame_idx = {},{}, writer.active={}".format(self.frame_idx, rawdata.frame_count, self._sis_csv_writer.active))
            if self.frame_idx != rawdata.frame_count:
                self.frame_idx = rawdata.frame_count;
                ret = self.touch_dev._GetNextFrame(rawdata) # get next frame
                if self._sis_csv_writer.frame_count != rawdata.frame_count and self._sis_csv_writer.active == True:
                    self._sis_csv_writer.write_frame(rawdata) # write to csv
                #_sis_rawdata.print_all()

                # Workaround for raw diff
                ret = rawdata.get_values()

                #ret = rawdata.values.copy()
                wsData = {"type":"data","dev":self.options.source_rawdata,"frameId":rawdata.frame_count,"w":rawdata.width_tx,"h":rawdata.height_rx,"max":int(rawdata.max),"min":int(rawdata.min),"data":ret.tolist()}
                #print(wsData)
                self.ws.send_updates(json.dumps(wsData))

    def get_info(self):
        rawdata = self._sis_rawdata
        info = "history[max={}, min={}], current[max={}, min={}]".format(rawdata.history_max, rawdata.history_min, rawdata.max, rawdata.min)
        return info, rawdata.frame_count, [rawdata.width_tx,rawdata.height_rx,rawdata.max,rawdata.min]

    def save_data(self, act):
        if act == "start":
            print("save file btn!")
            if self._sis_csv_writer.active == False:
                self._sis_csv_writer.write_start(self._sis_rawdata)
                self._sis_rawdata.frame_count = 0;
                self._sis_rawdata.frame_number = 0;
                return "start record data."
            else:
                return "already start record data."
        elif act == "stop":
            print("save file stop btn!")
            if self._sis_csv_writer.active == True:
                self._sis_csv_writer.write_finished(self._sis_rawdata)
                print("stop saveing!")
                return "save as {}. ".format(self._sis_csv_writer.csv_path)
            return "write not active."
