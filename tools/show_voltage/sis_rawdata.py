'''
[init]
capturerTool
FrameCount,0
Width,42
Height,92
Type,pen

[input 1 to next frame. others to stop.]
1
[frame]
No,1,0ms,Read Error Times,0
0,0,11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,22,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
ReportID,139
Invert,0
BarrSwi,0
SecBarr,0
Eraser,1
TipSwi,1
InRange,1
X,60792
Y,33667
Pressure,34289
TiltX,11173
TiltY,40596

[input 1 to next frame. others to stop.]
0
'''

import time
import numpy as np

# base
class sis_rawdata():
  def __init__(self):
    # init_data
    self.tool_name = 'sis_rawdata_test'
    self.frame_count = -1
    self.width_tx = 0
    self.height_rx = 0
    self.type = ''
    # frame_data
    self.frame_number = -1
    self.frame_arrived_time = None # ms
    self.read_error_times = 0
    self.tx_list = []
    self.rx_list = []
    self.tx_list_int = []
    self.rx_list_int = []
    self.values = None
    self.report_id = 0
    self.invert = 0
    self.barr_swi = 0
    self.sec_barr = 0
    self.eraser = 0
    self.tip_swi = 0
    self.in_range = 0
    self.position_x = 0
    self.position_y = 0
    self.pressure = 0
    self.tilt_x = 0
    self.tilt_y = 0
    # calculate data
    self.begin_time = 0
    self.current_time = 0
    self.pre_arrived_time = None
    self.interval_time = None
    self.avg = 0
    self.min = 0
    self.max = 0
    self.history_min = None
    self.history_max = None
    self.last_fnonzero = 0

    # Workaround for rawdiff
    self.request_rawdiff = False
    self.snap_values = None

  def update_type(self, source_rawdata, type_rawdata):
    if(source_rawdata == 'tp'):
      if(type_rawdata == 'mut_cycle'):
        self.type = 'CYCLE'
      elif(type_rawdata == 'mut_loop'):
        self.type = 'LOOP'
      elif(type_rawdata == 'mut_base'):
        self.type = 'BASE'
      elif(type_rawdata == 'mut_rawdata'):
        self.type = 'RAWDATA'
      elif(type_rawdata == 'mut_diff'):
        self.type = 'DIFF'
    elif(source_rawdata == 'pen'):
      if(type_rawdata == 'mut_base'):
        self.type = 'PEN_BASE'
      elif(type_rawdata == 'mut_rawdata'):
        self.type = 'PEN_RAWDATA'
      elif(type_rawdata == 'mut_diff'):
        self.type = 'PEN_DIFF'

  def parse_init(self, line_list):
    print('sis_rawdata()-parse_init : not impl')
    # to impl

  def parse_frame(self, line_list):
    print('sis_rawdata()-parse_frame : not impl')
    # to impl

  def print_all(self):
    print('sis_rawdata()-print_all : not impl')
    # to impl

  def init_rawdiff(self, flag=False):
      self.request_rawdiff = flag

  def get_values(self):

      if self.request_rawdiff == True:
          print("get rawdiff, {}, {} ".format(type(self.snap_values[0]), self.values))
          if isinstance(self.snap_values[0], list):
              tx = []
              rx = []
              for i in range(len(self.snap_values[0])):
                  val = self.snap_values[0][i]-self.values[0][i] 
                  tx.append(val) 
              for i in range(len(self.snap_values[1])): 
                  val = self.snap_values[1][i]-self.values[1][i] 
                  rx.append(val) 
              self.values = np.array((tx, rx)) 
              return self.values
          else:
              return self.snap_values - self.values
      else:
          return self.values


# pen
class sis_rawdata_pen(sis_rawdata):
  def __init__(self):
    sis_rawdata.__init__(self)

  def parse_init(self, line_list):
    #print( "parse_init : " + str(line_list) )
    self.tool_name = line_list.pop(0).strip('\n').split(',').pop(0)
    self.frame_count = int( line_list.pop(0).strip('\n').split(',').pop(1) ) # pop(0)
    self.frame_count = 0
    self.width_tx = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.height_rx = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.type = line_list.pop(0).strip('\n').split(',').pop(1)
    # calculate
    self.begin_time = time.time()

  def parse_frame(self, line_list):
    self.frame_count += 1
    #print( "parse_frame : " + str(line_list) )

    tmp_split = line_list.pop(0).strip('\n').split(',')
    self.read_error_times = int( tmp_split.pop(4) )
    #self.frame_arrived_time = tmp_split.pop(2)
    #self.frame_number = int( tmp_split.pop(1) )
    self.frame_number = self.frame_count # begin from 1

    # self.tx_list/rx_list
    self.tx_list = line_list.pop(0).strip('\n').split(',')
    self.rx_list = line_list.pop(0).strip('\n').split(',')
    self.tx_list_int = []
    self.rx_list_int = []

    # assign self.tx_list_int
    tx_list_tmp = self.tx_list
    if( tx_list_tmp[-1] == '' ):
      tx_list_tmp.pop() # pop ''
      pass
    tx_list_tmp = [int(n) for n in tx_list_tmp]
    self.tx_list_int.append(tx_list_tmp)

    #print("{},{}".format(type(self.rx_list),self.rx_list))
    # assign self.rx_list_int
    rx_list_tmp = self.rx_list
    if( rx_list_tmp[-1] == '' ):
      rx_list_tmp.pop() # pop ''
      pass
    rx_list_tmp = [int(n) for n in rx_list_tmp]
    self.rx_list_int.append(rx_list_tmp)
    self.values = np.asarray((self.tx_list_int[0], self.rx_list_int[0]))
    if self.request_rawdiff == True:
      if self.snap_values is None:
        self.snap_values = self.values.copy()
        self.type = 'RAWDIFF'
    self.value_tmp = np.column_stack([self.tx_list_int, self.rx_list_int])

    print(self.values.shape)
    # sensing something
    self.report_id = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.invert = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.barr_swi = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.sec_barr = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.eraser = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.tip_swi = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.in_range = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.position_x = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.position_y = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.pressure = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.tilt_x = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.tilt_y = int( line_list.pop(0).strip('\n').split(',').pop(1) )

    # calculate frame_arrived_time/interval_time
    self.pre_arrived_time = self.frame_arrived_time
    self.current_time = time.time()
    self.frame_arrived_time = int((self.current_time - self.begin_time) * 1000 + 0.5) # ms
    if( self.pre_arrived_time is not None ):
        self.interval_time = self.frame_arrived_time - self.pre_arrived_time

    # calculate min/max/avg/history
    self.avg = np.mean(self.value_tmp)
    self.min = np.min(self.value_tmp)
    self.max = np.max(self.value_tmp)
    if(self.history_min is None or self.min < self.history_min):
      self.history_min = self.min
    if(self.history_max is None or self.max > self.history_max):
      self.history_max = self.max

  def print_all(self):
    print("====================")
    print("'self.tool_name=%s'" % self.tool_name)
    print("'self.frame_count=%d'" % self.frame_count)
    print("'self.width_tx=%d'" % self.width_tx)
    print("'self.height_rx=%d'" % self.height_rx)
    print("'self.type=%s'" % self.type)
    print("--")
    print("'self.frame_number=%d'" % self.frame_number)
    print("'self.frame_arrived_time=%s'" % self.frame_arrived_time)
    print("'self.read_error_times=%d'" % self.read_error_times)
    print("'self.tx_list(%d)=%s'" % (len(self.tx_list) -1, self.tx_list))
    print("'self.rx_list(%d)=%s'" % (len(self.rx_list) -1, self.rx_list))
    print("'self.report_id=%d'" % self.report_id)
    print("'self.invert=%d'" % self.invert)
    print("'self.barr_swi=%d'" % self.barr_swi)
    print("'self.sec_barr=%d'" % self.sec_barr)
    print("'self.eraser=%d'" % self.eraser)
    print("'self.tip_swi=%d'" % self.tip_swi)
    print("'self.in_range=%d'" % self.in_range)
    print("'self.position_x=%d'" % self.position_x)
    print("'self.position_y=%d'" % self.position_y)
    print("'self.pressure=%d'" % self.pressure)
    print("'self.tilt_x=%d'" % self.tilt_x)
    print("'self.tilt_y=%d'" % self.tilt_y)
    print("==")

# mut
class sis_rawdata_mut(sis_rawdata):
  def __init__(self):
    sis_rawdata.__init__(self)

  def parse_init(self, line_list):
    #print( "parse_init : " + str(line_list) )
    self.tool_name = line_list.pop(0).strip('\n').split(',').pop(0)
    self.frame_count = int( line_list.pop(0).strip('\n').split(',').pop(1) ) # pop(0)
    self.frame_count = 0
    self.width_tx = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.height_rx = int( line_list.pop(0).strip('\n').split(',').pop(1) )
    self.type = line_list.pop(0).strip('\n').split(',').pop(1)

    # calculate
    self.begin_time = time.time()

  def parse_frame(self, line_list):
    self.frame_count += 1
    #print( "parse_frame : " + str(line_list) )

    tmp_split = line_list.pop(0).strip('\n').split(',')
    self.read_error_times = int( tmp_split.pop(4) )
    #self.frame_arrived_time = tmp_split.pop(2)
    #self.frame_number = int( tmp_split.pop(1) )
    self.frame_number = self.frame_count # begin from 1
    # parse width_tx with rx
    self.tx_list = []
    self.tx_list_int = []
    for i in range(0, self.width_tx):
      # assign self.tx_list
      rx_list_tmp = line_list.pop(0).strip('\n').split(',')
      self.tx_list.append(rx_list_tmp)
      # assign self.tx_list_int
      rx_list_tmp2 = list(rx_list_tmp)
      if( rx_list_tmp2[-1] == '' ):
        rx_list_tmp2.pop() # pop ''
        pass
      rx_list_tmp2 = [int(n) for n in rx_list_tmp2]
      self.tx_list_int.append(rx_list_tmp2)
      pass # end_for
    self.values = np.array(self.tx_list_int)
    if self.request_rawdiff == True:
      if self.snap_values is None:
        self.snap_values = self.values.copy()
        self.type = 'RAWDIFF'
    #if self.type == 'RAWDATA' or self.type == 'PEN_RAWDATA':
    first_nonzero = (self.values[0] != 0).argmax()
    offset = abs(self.last_fnonzero - first_nonzero)
    print("last{}, off{}".format(self.last_fnonzero, offset)) 
    if offset > 10 or (offset > 0 and first_nonzero < self.last_fnonzero):
        self.last_fnonzero = first_nonzero
    if self.last_fnonzero > 0: 
        self.values = np.roll(self.values, 0 - self.last_fnonzero)
    print(self.values.shape)

    # calculate frame_arrived_time/interval_time
    self.pre_arrived_time = self.frame_arrived_time
    self.current_time = time.time()
    self.frame_arrived_time = int((self.current_time - self.begin_time) * 1000 + 0.5) # ms
    if( self.pre_arrived_time is not None ):
        self.interval_time = self.frame_arrived_time - self.pre_arrived_time

    # calculate min/max/avg/history
    self.avg = np.mean(self.values)
    self.min = np.min(self.values)
    self.max = np.max(self.values)
    if(self.history_min is None or self.min < self.history_min):
      self.history_min = self.min
    if(self.history_max is None or self.max > self.history_max):
      self.history_max = self.max
    print(self.values)
    print(self.values.shape)

  def print_all(self):
    print("====================")
    print("'self.tool_name=%s'" % self.tool_name)
    print("'self.frame_count=%d'" % self.frame_count)
    print("'self.width_tx=%d'" % self.width_tx)
    print("'self.height_rx=%d'" % self.height_rx)
    print("'self.type=%s'" % self.type)
    print("--")
    print("'self.frame_number=%d'" % self.frame_number)
    print("'self.frame_arrived_time=%s'" % self.frame_arrived_time)
    print("'self.read_error_times=%d'" % self.read_error_times)
    #print("'self.tx_list(%d)=%s'" % (len(self.tx_list), self.tx_list))
    print("'self.tx_list(%d)=...'" % len(self.tx_list))
    print("==")

