import inspect
import os
import re
import select
import stat
from subprocess import PIPE, Popen

#import mt
#from mt.input import linux_input
#from remote import sis_os_touch_device

class SiSOSTouchDevice():
  def __init__(self, addr, capturer, source_rawdata, type_rawdata, report_in_frame, delay_85):
    self.addr = addr
    self.script_path = '/usr/local/rawdata/rawData.sh'
    self.capturer = "" 
    #self.capturer = capturer
    self.source_rawdata = source_rawdata
    self.type_rawdata = type_rawdata
    self.report_in_frame = report_in_frame
    self.delay_85 = delay_85
    self.capturer_process = None
    self.begin_capturer_cmd = '%s' % self.capturer
    self.keyword_next = "[input 1 to next frame. others to stop.]\n"
    self.error_count = 0
    # more args
    if(self.source_rawdata == 'tp' and 
       (self.type_rawdata == 'mut_cycle' or 
        self.type_rawdata == 'mut_loop' or
        self.type_rawdata == 'mut_base' or
        self.type_rawdata == 'mut_rawdata' or
        self.type_rawdata == 'mut_diff')):
      self.begin_capturer_cmd = '-t=%s -r=%s' % (self.source_rawdata, self.type_rawdata)
    elif(self.source_rawdata == 'pen' and 
         (self.type_rawdata == 'mut_base' or
          self.type_rawdata == 'mut_rawdata' or
          self.type_rawdata == 'mut_diff')):
      self.begin_capturer_cmd = '-t=%s -r=%s' % (self.source_rawdata, self.type_rawdata)
    self._InitializeCapturerSubprocess()
    print("cmd:{}".format(self.begin_capturer_cmd))

  def _InitializeCapturerSubprocess(self):
    # Initiate the streaming connection
    self.capturer_process =  self._RunRemoteCmd(self.begin_capturer_cmd)

    # Check to make sure it didn't terminate immediately
    ret_code = self.capturer_process.poll()
    if ret_code is not None:
      print('ERROR: streaming terminated unexpectedly (%d)' % ret_code)
      return False

    # Block until there's *something* to read, indicating everything is ready
    readable, _, _, = select.select([self.capturer_process.stdout], [], [])
    return self.capturer_process.stdout in readable

  def _GetInitFrame(self, _sis_rawdata, timeout=None):
    is_begin = False
    line_list = []
    line = self._GetNextLine(timeout)
    while(line is not None):
      if( line == "[init]\n"):
        is_begin = True
      elif( line == self.keyword_next ):
        break
      elif( is_begin == True ):
        #print "_GetInitFrame append : " + str(line)
        line_list.append(line)
        print("append data")
      line = self._GetNextLine(timeout)
      pass # end_while
    if is_begin == False:
        return False
    # go next and parse init
    try:
        self._IsNextFrame(1)
        _sis_rawdata.parse_init(line_list)
        _sis_rawdata.update_type(self.source_rawdata, self.type_rawdata)
    except:
        print("dumpdata error, retry again")
        return False
    return True

  def _GetNextFrame(self, _sis_rawdata, timeout=None):
    is_begin = False
    line_list = []
    print("get next!!")
    line = self._GetNextLine(timeout)
    print(line)
    while(line is not None):
      if( line == "[frame]\n"):
        is_begin = True
      elif( line == self.keyword_next ):
        break
      elif( is_begin == True ):
        #print "_GetNextFrame append : " + str(line)
        line_list.append(line)
      line = self._GetNextLine(timeout)

      pass # end_while

    # go next and parse current frame
    self._IsNextFrame(1)
    if len(line_list) > 4:
        try:
            print("len:{}, {} ".format(len(line_list), line_list[1]))
            _sis_rawdata.parse_frame(line_list)
            return True
        except:
            print("parse error msg [{}]".format(line_list))
    else:
        print("touch return error msg [{}]".format(line_list))
    return False

  def _StopNextFrame(self, timeout=None):
    line = self._GetNextLine(timeout)
    while(line is not None):
      #print("_StopNextFrame : " + str(line))
      if( line == self.keyword_next ):
        break
      line = self._GetNextLine(timeout)
      pass # end_while

    # stop next
    self._IsNextFrame(0)

  def _IsNextFrame(self, isNext=1):
    # self.keyword_next = "[input 1 to next frame. others to stop.]\n"
    #self.capturer_process.stdin.write("%d\n\r" % isNext)
    #self.capturer_process.communicate(input = "%d\n" % isNext)[0]
    self.capturer_process.stdin.write("%d\n" % isNext)

    print("ask next frame")

  def _GetNextLine(self, timeout=None):
    if timeout:
      inputs = [self.capturer_process.stdout]
      readable, _, _, = select.select(inputs, [], [], timeout)
      if inputs[0] not in readable:
        return None

    line = self.capturer_process.stdout.readline()

    # If the capturer_process had been terminated, just return None.
    if self.capturer_process is None:
      return None

    if line == '' and self.capturer_process.poll() != None:
      self.capturer_process = None
      return None

    #print("_GetNextLine : " + str(line))
    return line

  def quit(self):
      self.__del__()

  def __del__(self):
    print("SiSOSTouchDevice __del__, begin")
    self._StopNextFrame()
    self.capturer_process.wait()

    return_code = self.capturer_process.poll()
    if return_code is None:
      print("return_code is None. to terminate ...")
      self.capturer_process.terminate()
      return_code = self.capturer_process.wait()
      if return_code is None:
        print('Error in killing the event_stream_process!')

    self.capturer_process = None
    print("SiSOSTouchDevice __del__, return_code=" + str(return_code))
    return return_code

  def _RunRemoteCmd(self, cmd):
    """ Run a command on the shell of a remote ChromeOS DUT """

    args = ['sh', self.script_path, cmd]

    print("_RunRemoteCmd, args=" + str(args))
    return Popen(args, shell=False, bufsize=1, stdin=PIPE, stdout=PIPE, stderr=PIPE, universal_newlines=True)

