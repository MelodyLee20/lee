import numpy as np
import json
import random
from touch_utils import touchUtils as tu


class FakeRawData:

    def __init__(self, options):

        self.height_rx = 92
        self.width_tx = 42
        self.values = []
        self.min = 0
        self.max = 0
        self.frame_count = 0
        self.renew(options)
        self.history_min = 0
        self.history_max = 0

    def renew(self, options):
        if options.source_rawdata == 'tp':
            #self.values = 10.0*np.ones((self.height_rx, self.width_tx))
            self.values = np.random.random_integers(10, size=(self.height_rx, self.width_tx))
        elif options.source_rawdata == 'pen':
            tx = [int(10*random.random()) for i in range(self.width_tx)]
            rx = [int(10*random.random()) for i in range(self.height_rx)]
            self.values = np.array([[[tx]],[[rx]]])
        self.min = 0
        self.max = 9
        self.frame_count += 1

class FakeWriter:

    def __init__(self):
        self.active = False
        self.csv_path = "filepath"

    def write_start(self, data):
        self.active = True
        pass

    def write_finished(self, data):
        self.active = False
        pass


class FakeTouch(tu):
    def __init__(self, options, WsHandler):
        #super().__init__()
        self.ws = WsHandler
        self.options = options
        self.touch_connected = True
        self.counter = 0
        self.active = False
        self._sis_rawdata = FakeRawData(self.options)
        self._sis_csv_writer = FakeWriter()

    def restart(self, data_type=None, source_type=None):
        if data_type is not None:
            self.options.type_rawdata = data_type
        if source_type is not None:
            self.options.source_rawdata = source_type
        self.start()

    def stop(self):
        self.active = False

    def start(self):
        self._sis_rawdata = FakeRawData(self.options)
        self._sis_csv_writer = FakeWriter()
        self.active = True

    def get_data(self):
        rawdata = self._sis_rawdata
        if self.active == True:
            self.counter += 1
            if self.counter % 5 == 1:
                rawdata.renew(self.options)
                ret = self._sis_rawdata.values.copy()
                wsData = {"type":"data","dev":self.options.source_rawdata,"frameId":rawdata.frame_count,"w":rawdata.width_tx,"h":rawdata.height_rx,"max":int(rawdata.max),"min":int(rawdata.min),"data":ret.tolist()}
                self.ws.send_updates(json.dumps(wsData))
