# -*- coding: utf-8 -*-
import sys
import os
import time
import numpy as np
import optparse
from touch_utils import touchUtils as tu
from fake_touch import FakeTouch
import tornado.web
import tornado.websocket
import tornado.httpserver
import tornado.ioloop
import json

# Global variables
touch_util = None
debug_mode = True
NET_PORT = 8050


def parse_arguments():
  parser = optparse.OptionParser()

  parser.add_option('-a', '--addr', dest='addr', default=None,
                    help=('The address of the DUT'))

  parser.add_option('-c', '--capturer', dest='capturer', default=None,
                    help=('The capturer tool at the DUT for capturing rawdata'))

  parser.add_option('-s', '--source_rawdata', dest='source_rawdata', default='tp',
                    help=('The source of rawdata for capturing. [tp|pen]'))

  parser.add_option('-t', '--type_rawdata', dest='type_rawdata', default='mut_diff',
                    help=('The type of rawdata for capturing. \
                           tp:[mut_cycle|mut_loop|mut_base|mut_rawdata|mut_diff] \
                           pen:[mut_base|mut_rawdata|mut_diff]'))

  parser.add_option('-g', '--get_frame_num', dest='get_frame_num', default=0,
                    help=('How many frame to get'))

  parser.add_option('-d', '--delay_interval', dest='delay_interval', default=1,
                    help=('Dealy interval of frames (ms)'))

  parser.add_option('--delay85', '--delay_85', dest='delay_85', default=5,
                    help=('Dealy after CMD_85 (ms), default is 5 (ms)'))

  parser.add_option('-r', '--report_in_frame', dest='report_in_frame', default=0,
                    help=('Report time in a frame (ms), default (d85) + r(0)'))

  parser.add_option('--csvlog', dest='is_save_csv_log',
                    default=False, action='store_true',
                    help=('Enable to save .csv log'))

  parser.add_option('--debug', dest='debug',
                    default=False, action='store_true',
                    help=('Debug mode.'))

  (options, args) = parser.parse_args()

  # check args
  if(options.source_rawdata == 'tp' and
     (options.type_rawdata == 'mut_cycle' or
      options.type_rawdata == 'mut_loop' or
      options.type_rawdata == 'mut_base' or
      options.type_rawdata == 'mut_rawdata' or
      options.type_rawdata == 'mut_diff' or
      options.type_rawdata == 'mut_rawdiff')):
    pass
  elif(options.source_rawdata == 'pen' and
       (options.type_rawdata == 'mut_base' or
        options.type_rawdata == 'mut_rawdata' or
        options.type_rawdata == 'mut_diff' or
        options.type_rawdata == 'mut_rawdiff')):
    pass
  else:
    print('[Warning] Please use -s (--source_rawdata) and -t (--type_rawdata) to indicate a rawdata source and type.')
    sys.exit(1)

  return options, args

class WsHandler(tornado.websocket.WebSocketHandler):
    waiters = set()
    counter = 0
    begin = 0
    touch_util = None

    def check_origin(self, origin):
        return True

    def open(self):
        WsHandler.waiters.add(self)
        print("client IP:" + self.request.remote_ip)

    def on_message(self, message):
        print(message)
        data = json.loads(message)
        print("cmd:{}, value:{}".format(data['cmd'], data['value']))

        if data['cmd'] == "changeDevMode":
            print("change device to {}".format(data['value']))
            touch_util.restart(source_type=data['value'])
        elif data['cmd'] == "changeDataType":
            print("change data type to {}".format(data['value']))
            touch_util.restart(data_type=data['value'])
        elif data['cmd'] == "saveData":
            print("change save status to {}".format(data['value']))
            ret = touch_util.save_data(act=data['value'])
            wsData = {"type":"fileStatus","data":ret}
            print(wsData)
            self.send_updates(json.dumps(wsData))
        elif data['cmd'] == "devOperation":
            if data['value'] == "start":
                print("start touch device")
                touch_util.start()
                wsData = {"type":"devStatus","data":"start touch device"}
                print(wsData)
                self.send_updates(json.dumps(wsData))
            elif data['value'] == "stop":
                print("stop touch device")
                touch_util.stop()
                wsData = {"type":"devStatus","data":"stop touch device"}
                print(wsData)
                self.send_updates(json.dumps(wsData))
            elif data['value'] == "exit":
                print("Exit from app")
                touch_util.stop()
                tornado.ioloop.IOLoop.instance().stop()
                wsData = {"type":"devStatus","data":"exited"}
                print(wsData)
                self.send_updates(json.dumps(wsData))

        elif data['cmd'] == "requestFinalization":
            # Finalize the run.
            print('Perform finalization.')
            touch_util.stop()
            tornado.ioloop.IOLoop.instance().stop()

    def on_close(self):
        WsHandler.waiters.remove(self)
        print("close unity ws")

    @classmethod
    def send_updates(cls, msg ):
        for waiter in cls.waiters:
            if cls.counter == 0:
                cls.begin = time.time()
            cls.counter += 1
            #print(msg)

            waiter.write_message( msg )

    def set_touch_dev(self, tu):
        touch_util = tu

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        return self.render('index.html')

class PageNotFoundHandler(tornado.web.RequestHandler):
    def get(self):
        return self.write_error(404)

class Application(tornado.web.Application):
    def __init__(self):

        handlers = [
            (r"/ws", WsHandler),
            (r"/", IndexHandler),
            (r".*", PageNotFoundHandler),
        ]

        if getattr(sys, 'frozen', False): # For deployment
            static_folder = os.path.join(sys._MEIPASS, 'static')
            template_folder = os.path.join(sys._MEIPASS, 'templates')
        else: # For development
            static_folder = os.path.join(os.path.dirname(__file__), "static")
            template_folder = os.path.join(os.path.dirname(__file__), "templates")

        settings = dict(
            static_path=static_folder,
            template_path=template_folder,
        )
        tornado.web.Application.__init__(self, handlers, **settings)



if __name__ == '__main__':
    application = Application()
    server = tornado.httpserver.HTTPServer(application, xheaders=True)
    server.listen(NET_PORT)
    ioLoop = tornado.ioloop.IOLoop.instance()
    options, args = parse_arguments()
    if options.debug:
        touch_util = FakeTouch(options, WsHandler)
    else:
        touch_util = tu(options, WsHandler)

    tornado.ioloop.PeriodicCallback( touch_util.get_data, 50).start()
    print ("Web address: http://localhost:8050 ")
    print ("websocket server start")
    ioLoop.start()
    print ("websocket server stop")

