import sys
import types

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtGui, QtWidgets, QtCore
import pyqtgraph as pg

import numpy as np

from ui_main import Ui_MainWindow

from touch.algorithm.gtd import GTD
from surface_widget import SurfaceWidget

import logging
import touch.fwio2 as fwio

VERSION = '0.9.0'

class Diag(QMainWindow, Ui_MainWindow):

    def __init__(self, *args, **kwargs):
        super(Diag, self).__init__(*args, **kwargs)
        self.setupUi(self)

        self.debug_view = False #False

        self.source_type = 'stop'
        self.view_type = 'table'
        self.device_type = 'touch'

        # RAW data
        self.rawdata = None
        self.multi_chip_rawdata = [] 

        # Base rawdata snap buffer for var_base
        self.base_rawdata_snap = []
        self.multi_chip_base_rawdata_snap = []

        # VIEW data
        self.view_data = None

        # Combobox widget
        self.source_type_combobox.currentIndexChanged.connect(lambda: self.set_source_type(self.source_type_combobox))
        self.view_type_combobox.currentIndexChanged.connect(lambda: self.set_view_type(self.view_type_combobox))

        # Table widget
        self.table_widget = self.create_table_widget()

        # Surface
        self.surface_widget = SurfaceWidget()

        # Plot widget
        self.plot_widget = self.table_widget

        # Timer
        #self.timer = QTimer(self)
        #self.timer.start(500)
        #self.timer.timeout.connect(self.refresh_view)

        x = np.arange(1000)
        y = np.random.normal(size=(3, 1000))

        plot_widget = pg.PlotWidget()

        # Add plot widget to the view layout
        self.add_widget(self.plot_widget)

        # GTD (Ghost Detection)
        self.gtd = GTD()

        # Plot min/max
        self.plot_min = 0.0
        self.plot_max = -1.0

        self.edit_plot_min.textChanged.connect(self.set_plot_min)
        self.edit_plot_max.textChanged.connect(self.set_plot_max)

        # Open sis touch device
        self.touch_dev = None
        if self.debug_view == False:
            self.open_device()

    def open_device(self):

        def callback(ws, status, id, msg, data=[]):
            print([id, status, msg, data])

        # setup touch device
        self.touch_dev = fwio.core(logging.DEBUG, ui="pyqt")
        if self.touch_dev.search() != True:
            self.touch_dev = None
            sys.exit()
        self.touch_dev.start_server(cb=callback)
        self.touch_dev.open()
        self.touch_dev.enter_normal_mode()
        def get_slave_num_cb(ws, status, msg, data=[]):
            for i in range(self.touch_dev.master_chip_info["slave_num"] + 1):
                print(i)
                self.touch_dev.get_tx_rx_size(chip_address=i)
            self.touch_dev.get_nvb()
            self.fw_data_init()
        self.touch_dev.get_slave_num(cb=get_slave_num_cb)

    def set_plot_min(self):

        value = self.edit_plot_min.text()

        try:
            value = float(value)
        except:
            value = 0.0

        self.plot_min = value

    def set_plot_max(self):

        value = self.edit_plot_max.text()

        try:
            value = float(value)
        except:
            value = -1.0

        self.plot_max = value

    def add_widget(self, widget):

        widget.setParent(self.central_widget)
        self.view_layout.addWidget(widget)

    def remove_widget(self, widget):

        self.view_layout.removeWidget(widget)
        widget.setParent(None)

    def set_source_type(self, combobox):

        self.source_type = combobox.currentText() 
        print(self.source_type)
        self.request_raw_data()

    def set_view_type(self, combobox):
        self.view_type = combobox.currentText()

        self.remove_widget(self.plot_widget)

        if self.view_type == 'table':
            self.plot_widget = self.table_widget

        if self.view_type == 'surface':
            self.plot_widget = self.surface_widget

        self.add_widget(self.plot_widget)

    def refresh_view(self):
        data = self.view_data
        # Update the global min/max.

        global_min = np.min(data)
        global_max = np.max(data)

        text = 'global min : {:.2f}'.format(global_min)
        self.lb_global_min.setText(text)

        text = 'global max : {:.2f}'.format(global_max)
        self.lb_global_max.setText(text)

        # Update the view
        if self.view_type == 'table':
            self.update_table_widget(data)

        if self.view_type == 'surface':
            self.update_surface(data)

    def request_raw_data(self):
        if self.source_type != "stop":
            if self.touch_dev.enter_cmd_mode() == True:
                source_type = self.source_type
                if self.source_type == 'vardiff':
                    if len(self.base_rawdata_snap) == 0:
                        source_type = 'base'
                    else:
                        source_type = 'rawdata'
                if self.chip_selector_combobox.currentText() == "All": #show all chips
                    for i in range(self.touch_dev.master_chip_info["slave_num"] + 1):
                        self.touch_dev.get_rawdata(device_type=self.device_type, data_type=source_type, cb=self.get_fw_data, chip_address=i)
                else:
                    self.touch_dev.get_rawdata(device_type=self.device_type, data_type=source_type, cb=self.get_fw_data, chip_address=int(self.chip_selector_combobox.currentText())) 
                self.touch_dev.leave_cmd_mode()
            else:
                print("cannot enter diagnosis mode")
        else:
            self.fw_data_init()

    def get_fw_data(self, ws, status, id, msg, buf=[]):
        if self.source_type == "stop":
            return
        if len(buf) > 0:
            rx = self.touch_dev.master_chip_info["rx"]
            tx = self.touch_dev.master_chip_info["tx"]
            chip_address = buf['chip_address']
            if chip_address > 0:
                rx = self.touch_dev.slave_chip_info[chip_address]["rx"]
                tx = self.touch_dev.slave_chip_info[chip_address]["tx"]
            data = buf['data']
            print("carey:chip_address:{}, data:{}".format(chip_address, data))
            slave_num = self.touch_dev.master_chip_info["slave_num"]
            rawdata = np.array(data).reshape(tx, rx)
            if self.chip_selector_combobox.currentText() == "All" and slave_num > 0:
                if self.source_type == 'vardiff':
                    if self.multi_chip_base_rawdata_snap[chip_address] is None: 
                        self.multi_chip_base_rawdata_snap[chip_address] = np.copy(rawdata)
                        if chip_address == slave_num: # merge the master and slave chips rawdata
                            QTimer.singleShot(0, self.request_raw_data)
                    else:
                        self.multi_chip_rawdata[chip_address] = np.copy(rawdata)
                        if chip_address == slave_num: # merge the master and slave chips rawdata
                            self.rawdata = np.concatenate(tuple(self.multi_chip_rawdata), axis=1)
                            self.base_rawdata_snap = np.concatenate(tuple(self.multi_chip_base_rawdata_snap), axis=1)
                            # Estimate the varaince.
                            self.gtd.set_data(self.rawdata)
                            var_rawdata = self.gtd.get_variance()
                            self.gtd.set_data(self.base_rawdata_snap)
                            var_base = self.gtd.get_variance()
                            self.view_data = var_rawdata - var_base
                            print("var:{}".format(self.rawdata))
                            self.fw_data_init()
                            QTimer.singleShot(0, self.refresh_view)
                            QTimer.singleShot(0, self.request_raw_data)
                else:
                    self.multi_chip_rawdata[chip_address] = np.copy(rawdata)
                    if chip_address == slave_num: # merge the master and slave chips rawdata
                        self.rawdata = np.concatenate(tuple(self.multi_chip_rawdata), axis=1)
                        self.view_data = self.rawdata.copy()
                        print("\033[42mvar:{}\033[0m".format(self.rawdata))
                        QTimer.singleShot(0, self.refresh_view)
                        QTimer.singleShot(0, self.request_raw_data)
            else:
                # Get the FW data
                self.rawdata = rawdata 
                if self.source_type == 'vardiff':
                    if len(self.base_rawdata_snap) == 0:
                        self.base_rawdata_snap = self.rawdata.copy()
                    else:
                        # Estimate the varaince.
                        self.gtd.set_data(self.rawdata)
                        var_rawdata = self.gtd.get_variance()
                        self.gtd.set_data(self.base_rawdata_snap)
                        var_base = self.gtd.get_variance()
                        self.view_data = var_rawdata - var_base
                        print("var:{}".format(self.rawdata))
                        self.fw_data_init()
                        QTimer.singleShot(0, self.refresh_view)
                else:
                    self.view_data = self.rawdata.copy()
                    print("\033[42mvar:{}\033[0m".format(self.rawdata))
                    QTimer.singleShot(0, self.refresh_view)
                QTimer.singleShot(0, self.request_raw_data)

    def fw_data_init(self):
        chip_num = self.touch_dev.master_chip_info["slave_num"] + 1
        self.base_rawdata_snap = []
        self.multi_chip_base_rawdata_snap = [None] * chip_num
        #for i in self.multi_chip_base_rawdata_snap:
        self.multi_chip_rawdata = [0] * chip_num

    def update_surface(self, data):

        self.plot_widget.refresh(data)

    def update_table_widget(self, data):
        print("\033[42mdata shape:{}\033[0m".format(data.shape))
        rows, cols = data.shape
        for row in range(rows):
            for col in range(cols):

                text = '{0:.2f}'.format(data[row, col])
                self.insert_table_item(self.table_widget, row, col, text)


    def insert_table_item(self, table, row, col, text):

        item = QTableWidgetItem(text)
        item.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)

        # Set color
        if self.plot_min < self.plot_max:
            value = float(text)
            if value < self.plot_min or value > self.plot_max:
                item.setBackground(QtGui.QColor('Red'))

        table.setItem(row, col, item)


    def create_table_widget(self):

        table = QtWidgets.QTableWidget()
        table.disconnect() # Disconnect all slots

        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(table.sizePolicy().hasHeightForWidth())
        table.setSizePolicy(sizePolicy)

        table.setColumnCount(300)
        table.setRowCount(300)
        for i in range(300):
            table.setColumnWidth(i, 50)
        for i in range(300):
            table.setRowHeight(i, 20)
        return table

    def get_fake_data(self):

        return np.random.randint(10, size=(self.num_rows, self.num_cols))


    def closeEvent(self, event):
        event.accept()
        self.touch_dev.close()
        print("QT quit")

if __name__ == '__main__':

    app = QApplication([])
    diag = Diag()
    diag.show()
    app.exec_()
