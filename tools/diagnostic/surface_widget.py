import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from matplotlib import cm

import numpy as np

class SurfaceWidget(FigureCanvas):

    def __init__(self):

        self.fig = plt.figure(figsize=(12,12))
        self.ax = self.fig.add_subplot(111, projection='3d')
        self.surface = None

        FigureCanvas.__init__(self, self.fig)

    def set_parent(self, parent):
        self.setParent(parent)

    def refresh(self, data):

        num_rows, num_cols = data.shape

        # Update the plot.
        x = np.arange(num_cols)
        y = np.arange(num_rows)
        X,Y = np.meshgrid(x,y)
        Z = data

        # Make plot
        self.ax.clear()
        self.surface = self.ax.plot_surface(X, Y, Z)
        self.ax.set_xlabel('x')
        self.ax.set_ylabel('y')
        self.ax.set_zlabel('z')
        self.ax.figure.canvas.draw()
