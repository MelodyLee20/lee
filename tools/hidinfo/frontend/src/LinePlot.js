import React from 'react';
import Plot from 'react-plotly.js';

// Graph size
var width = 800;
var height = 800;
// Plot configuration
var config = {staticPlot: false, displayModeBar: true}; 

class Line extends React.Component {

  jsonFactory(x, y, ymin=0, ymax=-1) {


    if (ymin > ymax) {
      ymin = 0;
	    ymax = 10;
	  }	 

		var json = {
			data: [
	  		{
					x: x,
          y: y,
          type: 'scatter',
          mode: 'lines+points',
	  			name: 'Freq-Voltage'
		    },
	  	],
		  layout: {
				xaxis: {
          title: 'Frequency (Hz)',
				},	
				yaxis: {
          title: 'Voltage',
          range: [ymin, ymax]
				},	
				width: width,
				height: height 
			}
		}

		return json
	}

	render() {
		const x = this.props.x;
		const y = this.props.y;
		const ymin = this.props.ymin
		const ymax = this.props.ymax
		const json = this.jsonFactory(x, y, ymin, ymax);

    return (
			  <Plot data={json.data} layout={json.layout} config={config} />
  	);	
	}
}	

// Export moudules
export {Line, Lines, Heatmap, Surface}
