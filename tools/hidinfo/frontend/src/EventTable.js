import React from 'react';
import ReactTable from "react-table";
import 'react-table/react-table.css';
//import {Button} from 'react-bootstrap';

function drop_points(points, numMax) {
// Drop points.

  let num = points.length;
  let ia, iz;
  let out;

  if (num > numMax) {
     iz = num-1;
     ia = iz - numMax + 1;
     out = points.slice(ia, iz);
  } else {
    out= points;
  }  

  return out;
}

function renderTable(events, numEventsMax) {

  const columns = prepareColumns(events);
  let data = events.reverse();

  return <ReactTable data={data} columns={columns} sortable={false} pageSize={100} pageSizeOptions={[100, 200]} />
}


function makeColumn(header) {

  let col;

  col = {
    Header: () => (
      <span>
        {header}
      </span>
    ),
    accessor: header, 
    //accessor: header.toUpperCase(), 
    Cell: props => ( 
      <div> 
        {props.value}
      </div>
    ),
    minWidth: 100, // Minimum cell width 
  };

  return col;

}  


function prepareColumns(events) {

  let columns = [];
  let col;
  if (events.length > 0 && events !== 'null' && events !== 'undefined'){
    let keys = Object.keys(events[0])
    for (let i=0;i<keys.length;i++) {
      col = makeColumn(keys[i]) 
      columns.push(col);
    }
  }
  return columns;
}

class EventTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      events: [],
    };  

    this.numPointsMax = 1000;
    this.points = []
    this.intervalId = null;
    

    // Handlers 
    this.handlePointsArrived = this.handlePointsArrived.bind(this);
    this.updateRefreshRate = this.updateRefreshRate.bind(this);
    this.dc = this.props.dc;
  }

  componentDidMount() {
    this.dc.setPointsArrivedHandler(this.handlePointsArrived)
    this.dc.setRefreshRateHandler(this.updateRefreshRate);

    // Refresh the events  after a short interval of 1 sec
    this.intervalId = setInterval( () => this.refreshEvents(), 1000)
  }  

  handlePointsArrived(newPoints) {

    this.points = this.points.concat(newPoints);

    // Drop the previous points when the number exceeds the maximun value
    this.points = drop_points(this.points, this.numPointsMax);
  }

  updateRefreshRate(rate) {
    console.log('EventTable')
    console.log(rate)

    let duration = Math.round(1.0/rate*1000.0);
    console.log('duration')
    console.log(duration)
  
    // Clear the old interval
    clearInterval(this.intervalId);
    // Create a new interval
    this.intervalId = setInterval( () => this.refreshEvents(), duration)
  }

  refreshEvents() {
    // Clone points as events
    let events = Object.assign([], this.points);
    // Refresh the touch events
    this.setState({events: events});
  }  

  render() {
    const events = this.state.events;
    return (
      <div>
        {renderTable(events)}
      </div>
    );  
  }
}  

export {EventTable}

