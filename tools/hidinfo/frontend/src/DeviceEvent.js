import React from 'react';
import {Button} from 'react-bootstrap';

class DeviceEvent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deviceType: "pen",
      refreshRate: 1,
      fitXY: 0
    };  

    // Handlers 
    this.requestTouchPointsStart = this.requestTouchPointsStart.bind(this);
    this.requestTouchPointsStop = this.requestTouchPointsStop.bind(this);
    this.setDeviceType = this.setDeviceType.bind(this);
    this.setFitXY = this.setFitXY.bind(this);
    this.setRefreshRate = this.setRefreshRate.bind(this);
    this.dc = this.props.dc;
  }

  requestTouchPointsStart() {
    let wsdata = {"cmd":"requestTouchPoints", "action": "start", "devType":this.state.deviceType, "fitXY":this.state.fitXY, 
                    "width":window.screen.width*window.devicePixelRatio, "height":window.screen.height*window.devicePixelRatio };
    wsdata = JSON.stringify(wsdata);
    console.log(wsdata)
    this.dc.send(wsdata);
  }
  requestTouchPointsStop() {
    let wsdata = {"cmd":"requestTouchPoints", "action": "stop"};
    wsdata = JSON.stringify(wsdata);
    this.dc.send(wsdata);
  }

  setFitXY(e) {
	  this.setState({fitXY: e.target.value});
  }

  setDeviceType(e) {
	  this.setState({deviceType: e.target.value});
  }

  setRefreshRateHandler(handler) {
    this.refreshRateChanged = handler;
  }

  setRefreshRate(e) {
      let rate = e.target.value
      console.log('DeviceEvent')
      console.log(rate)
	  //this.setState({refreshRate: rate});
      //this.refreshRateChanged(e);
      this.dc.updateRefreshRate(rate)
      
  }

  render() {
    return (
      <div>
        <label> Refresh Rate (fps) </label>
        <select onChange={this.setRefreshRate}>
          <option key="1" value="1">1</option>
          <option key="10" value="10">10</option>
          <option key="20" value="20">20</option>
        </select>
        <label>, Device Type </label>
        <select onChange={this.setDeviceType}>
          <option key='pen' value='pen'>pen</option>
          <option key='touch' value='touch'>touch</option>
        </select>
        <label>, fit X/Y to  </label>
        <select onChange={this.setFitXY}>
          <option key='0' value='0'>firmware scale</option>
          <option key='1' value='1'>{window.screen.width*window.devicePixelRatio+ " x " + window.screen.height*window.devicePixelRatio}</option>
        </select>
        <Button variant="outline-primary" onClick={this.requestTouchPointsStart}> Start </Button>
        <Button variant="outline-primary" onClick={this.requestTouchPointsStop}> Stop </Button>
      </div>
    );  
  }
}  

export {DeviceEvent}

