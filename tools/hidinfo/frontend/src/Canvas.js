
var canvas = document.getElementById('canvas');
var ctx = canvas.getContext("2d");
var colors = ['Red', 'Orange', 'Yellow', 'Green', 'Blue', 'Purple', 'Gold', 'Salmon', 'Pink', 'Grey'];
var scale = 1;
var lastPt = {};
var lineWidth = 5;
var newLineWidth = lineWidth;
var pointMode = false; // Point mode
var pressureMode = false; // Pressure mode
var reportRateSum = 0;
var startTime = Date.now(); // in milliseconds

//const channel = new BroadcastChannel('TOUCH_BROADCAST');

function init() {

  canvas = initCanvas();
  initHotkeys();

	return false;
}

function pointerDownHandler(event) {
  event.preventDefault();

	console.log('pressure: ' + event.pressure);
	if (pointMode) { // Plot a point
		newLineWidth = lineWidth;
		if (pressureMode) {
		  newLineWidth = broadenLineWidth(lineWidth, event.pressure);
		}	
		const point = {x:event.pageX, y:event.pageY, }
		const radius = 0.5*newLineWidth;
    const id = event.pointerId;
    const numColors = colors.length;
    ctx.strokeStyle = colors[id%numColors];
    plotPoint(ctx, point.x, point.y, radius);
	}	

  canvas.addEventListener("pointermove", draw, false); 
  canvas.addEventListener("pointermove", updateReportRate, false); 

	reportRateSum++;

	return false;
}

function pointerUpHandler(event) {
  var id = event.pointerId;
  canvas.removeEventListener("pointermove", draw, false); 
  canvas.removeEventListener("pointermove", updateReportRate, false); 


  // Terminate this touch
  delete lastPt[id];

	return false;
}


function initCanvas() {

  var newscale = 1;
	//var newscale = window.devicePixelRatio ? window.devicePixelRatio : 1;
  var newwidth = window.screen.width * newscale;
  var newheight = window.screen.height * newscale;

  if (canvas.width !== newwidth || canvas.height !== newheight || scale !== newscale) {
      // resizing a canvas clears it, so do it only when it's dimensions have changed.
      scale = newscale;
      canvas.width = newwidth;
      canvas.height = newheight;
      canvas.style.width = window.screen.width + "px";
      canvas.style.height = window.screen.height + "px";
  }

  if(window.PointerEvent) {
    canvas.addEventListener("pointerdown", pointerDownHandler, false);
    canvas.addEventListener("pointerup", pointerUpHandler, false);
  }
  else {
		console.error('This browser cannot support PointEvent.')
  }

	return canvas;

}
 
function initHotkeys() {

  document.addEventListener("keyup", function(e) {
      switch(e.which) {
      // Press the 'ESC' key
      case 27:
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        break;

      // Press the 'p' key to toggle the point mode
      case 80:
        pointMode = !pointMode;
        break;

      // Press the 'r' key to toggle the pressure mode
      case 82:
        pressureMode = !pressureMode;
        break;

      // Press the 'up' key to increase the line width
      case 38:
        if (lineWidth < 30) { lineWidth += 1; }
        console.info('lineWidth: ' + lineWidth);
        break;

      // Press the 'down' key to decrease the line width
      case 40:
        if (lineWidth > 1) { lineWidth -= 1; }	
        console.info('lineWidth: ' + lineWidth);
        break;


      // Press the 'f' or 'Enter' key
      case 70:
      case 13:
        if (document.documentElement.webkitRequestFullscreen) {
            if (document.webkitFullscreenElement)
                document.webkitCancelFullScreen();
            else
                document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
        break;
		
			default:
					// Do nothing
      }


  });

	return false;
}	

function draw(event) {

  const id = event.pointerId; 
  const numColors = colors.length;

	// Current point
	const point = {x: event.pageX, y: event.pageY};
  const lastPoint = lastPt[id];
 
	// Pressure
  newLineWidth = lineWidth;
	if (pressureMode) {
	  newLineWidth = broadenLineWidth(lineWidth, event.pressure);
	}	

	// Plot a line
	if (pointMode) { // Point
		const radius = 0.5*newLineWidth;
    //ctx.strokeStyle = 'black' // Set Color
    ctx.strokeStyle = colors[id%numColors];
    plotPoint(ctx, point.x, point.y, radius);
	} else { // Line	
    ctx.lineWidth = newLineWidth; // Line width
    ctx.strokeStyle = colors[id%numColors];
    if (lastPoint) {
      plotLine(ctx, lastPoint.x, lastPoint.y, point.x, point.y);
    }
	}	

  // Store last point
  lastPt[id] = point;

}
 
function updateReportRate(event) {
	// Update the report rate

	// It's important to use coalesced events to get more accuarate report rate.
	const events = event.getCoalescedEvents();
	for (let e of events) { // jshint ignore:line
	  reportRateSum++;
	  console.log('pressure: ' + event.pressure);
	}

  let now = Date.now(); // in milliseconds
	if (now-startTime > 1000) {
     startTime = now;
		 plotReportRate();
	   reportRateSum = 0;
	}

}	

function getNumPointers() {
  return Object.keys(lastPt).length;
}

function plotReportRate() {
	const fontHeight=20;
	const [x, y] = [5, 5+fontHeight];

  const numPointers = getNumPointers();
  const reportRate = (reportRateSum/numPointers).toFixed(0) // Average value

	console.log(reportRate); 
  ctx.font = "30px Arial";
	ctx.clearRect(0, 0, 250, 40);
  ctx.fillText("Report Rate: " + reportRate, x, y);
}	
	
function plotLine(ctx, x0, y0, x1, y1) {
  ctx.beginPath();
  ctx.moveTo(x0, y0);
  ctx.lineTo(x1, y1);
  ctx.stroke();
}	

function plotPoint(ctx, x, y, radius) {
	ctx.lineWidth = 2*radius; // This produces a solid point
	plotCircle(ctx, x, y, radius);
}	

function plotCircle(ctx, x, y, radius) {
  ctx.beginPath();
  ctx.arc(x, y, radius, 0, 2*Math.PI);
  ctx.stroke();
}	

function broadenLineWidth(lineWidth, pressure) {
  // Broaden the line width

	let newLineWidth = lineWidth;
	const pressureMin = 0.1;
	if (pressure > pressureMin) {
    newLineWidth = pressure/pressureMin * lineWidth;
	} else {
	  newLineWidth = lineWidth;
	}	

	return newLineWidth;
}	

export {init}

