import React from 'react';
import { DeviceEvent } from './DeviceEvent';
import { EventTable } from './EventTable';
import { DataCenter } from './DataCenter';
import { finalizeBeforeClosingTab, disablePinZoom } from './CommonUtils';
import './App.css';

// Websocket address
const wsUri = "ws://127.0.0.1:8050/ws";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };

    // Handlers

    // Data Center
    this.dc = new DataCenter(wsUri);
    this.dc.connect();
  }

  componentDidMount() {
    // Disable pin-zoom
    disablePinZoom();
    // Finalize before closing the browser tab.
    finalizeBeforeClosingTab(this.dc);
  }  

  showConnectStatus(connected) {
    return connected? 'Connected': 'Disconnected';
  }  

  render() {
    return (
      <div className="App">
        <DeviceEvent dc={this.dc} /> 
        <EventTable dc={this.dc} /> 
      </div>
    );
  }
}

export default App;
