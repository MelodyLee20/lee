import React from 'react';
import {Button} from 'react-bootstrap';

class CalibrationType extends React.Component {
  constructor(props) {
    super(props);

    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.handleTypeConfirm = this.handleTypeConfirm.bind(this);
  }

  handleTypeChange(evt) {
		const type = evt.target.value
    this.props.onTypeChange(type);
  }

  handleTypeConfirm(evt) {
    this.props.onTypeConfirm();
  }

  render() {
    const type = this.props.type;
    return (
		  <div>
		    <label>
          Calibration Type:&nbsp;
        </label>
        <input type="radio" value='2x2' checked={type === '2x2'} 
          onChange={this.handleTypeChange} /> 2x2 &nbsp;
        <input type="radio" value='3x3' checked={type === '3x3'} 
          onChange={this.handleTypeChange} /> 3x3 &nbsp;
		    <Button variant="outline-primary" onClick={this.handleTypeConfirm}> Confirm </Button>
	    </div>
    );
  }
}


// Export modules
export {CalibrationType}
