/*
 Common utilities.
 */


function disablePinZoom() {

  document.addEventListener("touchmove", (event) => {
		// Disable touch operations
		event.preventDefault(); 
  }, {passive: false});

  document.addEventListener("mousewheel", (event) => {
		// Disable mousewheel operations
    event.preventDefault(); // Both pin-zoom and scroll
  }, {passive: false});
}	

function finalizeBeforeClosingTab(dc) {
  let requestFinalization = () => {
    let data = JSON.stringify({cmd: "requestFinalization", value: 1});
    dc.send(data);
  };
  window.addEventListener("beforeunload", requestFinalization);
}	

// Export moudules
export {disablePinZoom, finalizeBeforeClosingTab}
