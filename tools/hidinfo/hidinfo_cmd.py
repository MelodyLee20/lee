# -*- coding: utf-8 -*-
import sys
import time 
import os
import csv
import optparse
sys.path.append("../../../touch")
from touch import hidparser
from touch.hidparser.Device import Collection
from touch.hidparser.UsagePages import GenericDesktop, Button
from touch.hidparser.UsagePage import UsagePage, UsageType, Usage
from touch.hidparser.enums import CollectionType, ReportType, ReportFlags, UnitSystem

def parse_arguments():
  parser = optparse.OptionParser()

  parser.add_option('-t', '--dev_type', dest='device_type', default="pen",
                    help=('touch or pen'))

  (options, args) = parser.parse_args()

  return options, args

def hidraw_info(device):
  with open("/sys/class/hidraw/" + device + "/device/modalias") as f:
    info = f.read().strip() 
    pid = info[info.find("p")+1:]
    vid = info[info.find("v")+1:info.find("p")]
    desc = "/sys/class/hidraw/" + device + "/device/report_descriptor"
    return int(vid,16), int(pid,16), desc

def find_sis_touch(name=""):
    SIS_VID = 0x457
    for dirname, dirnames, filenames in os.walk('/dev'):
        for filename in filenames:
            if "hidraw" in filename:
                vid, pid, desc_path = hidraw_info(filename)
                print("name:{},vid:{},pid:{},descriptor:{}".format(filename, hex(vid), hex(pid), desc_path))
                dev = {'name': "/dev/"+filename, 'vid': vid, 'pid': pid, 'desc_path': desc_path}
                if len(name) > 0:
                    if name == filename:
                        return dev 
                elif vid == SIS_VID:
                    return dev 
    return None 



if __name__ == '__main__':
    data_counter = 0
    parser_counter = 0
    writer = None
    touch_dev = find_sis_touch()
    with open(touch_dev["desc_path"], "rb") as f:
        desc_bin = f.read()
    touch_desc = hidparser.parse(desc_bin)
    fd = os.open(touch_dev["name"], os.O_RDONLY)
    writer_fd = open("output.csv", "w")
    options, args = parse_arguments()
    while True:
        data = os.read(fd, 64)
        if len(data) > 0:
            data_counter += 1
            device_type = options.device_type 
            got_data = 0
            x_max = 0
            y_max = 0
            report = {}
            finger = {}
            buf = [data, int(time.time()*1000)] 
            #print(buf[0].hex())
            timestamp = buf[1]
            report = {"timestamp":timestamp}
            report_id = buf[0][0]
            touch_desc.deserialize(buf[0])
            tp = touch_desc.reports[report_id].inputs
            for key in tp._attrs.keys():
                if key == "touch_screen" and device_type == "touch":
                    ts = tp.touch_screen
                    if ts.finger[0]:
                        for i in ts.finger[0].items:
                            if i.usages:
                                if i.usages[0]._name_ == "X":
                                    x_max = i.physical_range.maximum
                                    #print("xmin:{}, max:{}".format(i.physical_range.minimum,i.physical_range.maximum))
                                elif i.usages[0]._name_ == "Y":
                                    y_max = i.physical_range.maximum
                                    #print("ymin:{}, max:{}".format(i.physical_range.minimum,i.physical_range.maximum))
                        
                    for i in range(len(ts.finger)):
                        finger = ts.finger[i]
                        for key in finger._attrs.keys():
                            value = finger.__getattr__(key)
                            got_data = 1
                            report["%s_%d" % (key,i)] = value 
                elif key == "pen" and device_type == "pen":
                    pen = tp.pen.stylus
                    for item in pen.items:
                        try:
                            if item.usages:
                                if item.usages[0]._name_ == "X":
                                    x_max = item.physical_range.maximum
                                elif item.usages[0]._name_ == "Y":
                                    y_max = item.physical_range.maximum
                                #print(item.usages[0]._name_)
                                #print("mitem.:{}, max:{}".format(item.physical_range.minimum,item.physical_range.maximum))
                        except:
                            pass
                    for key in pen._attrs.keys():
                        value = pen.__getattr__(key)
                        #print("key:{}, value:{}".format(key, value))
                        got_data = 1
                        if isinstance(value, Collection):
                            for sub_key in value._attrs.keys():
                                sub_value = value.__getattr__(sub_key)
                                report[sub_key] = sub_value 
                        elif isinstance(value, list):
                            for j in range(len(value)):
                                sub_value = value[j].fget()
                                sub_key = "{}_{}".format(key,j)
                                report[sub_key] = sub_value 
                        else:
                            if key == "transducer_serial_number" or key[:6] =="vendor":
                                report[key] = hex(value)
                            else:
                                report[key] = value 
                    
                else:
                    print("!!!!! parse error, {}".format(tp.__getattr__(key)))
            if got_data == 1:
                if parser_counter == 0:
                    fieldnames = report.keys()
                    writer = csv.DictWriter(writer_fd, fieldnames=fieldnames)
                    writer.writeheader()
                    
                parser_counter += 1
                print("data_counter:{},parser_counter:{}".format(data_counter, parser_counter))
                writer.writerow(report)
                
    os.close(fd) 
