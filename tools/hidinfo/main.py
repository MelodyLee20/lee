# -*- coding: utf-8 -*-
import sys
import os
import time
import optparse
import tornado.web
import tornado.websocket
import tornado.httpserver
import tornado.process
import tornado.ioloop
import json
import collections
import threading
sys.path.append("../../../touch")
from touch import hidparser
from touch.hidparser.Device import Collection
from touch.hidparser.UsagePages import GenericDesktop, Button
from touch.hidparser.UsagePage import UsagePage, UsageType, Usage
from touch.hidparser.enums import CollectionType, ReportType, ReportFlags, UnitSystem
from touch.writers import csv_dict_writer
# Version
__version__ = '0.8.0'

# Global variables
touch_dev = None
debug_mode = True
NET_PORT = 8050
fname = "chromebook_report_descriptor"
fname = "/sys/devices/pci0000:00/0000:00:0c.0/usb1/1-2/1-2:1.0/0003:0457:153B.0006/report_descriptor"
fname = '/sys/devices/pci0000:00/0000:00:15.0/i2c_designware.0/i2c-6/i2c-PNP0C50:00/0018:0457:1905.0001/report_descriptor'
fdata = "/dev/hidraw1"
fdata = "/dev/hidraw0"

def hidraw_info(device):
  with open("/sys/class/hidraw/" + device + "/device/modalias") as f:
    info = f.read().strip() 
    pid = info[info.find("p")+1:]
    vid = info[info.find("v")+1:info.find("p")]
    desc = "/sys/class/hidraw/" + device + "/device/report_descriptor"
    return int(vid,16), int(pid,16), desc

def find_sis_touch(name=""):
    SIS_VID = 0x457
    for dirname, dirnames, filenames in os.walk('/dev'):
        for filename in filenames:
            if "hidraw" in filename:
                vid, pid, desc_path = hidraw_info(filename)
                print("name:{},vid:{},pid:{},descriptor:{}".format(filename, hex(vid), hex(pid), desc_path))
                dev = {'name': "/dev/"+filename, 'vid': vid, 'pid': pid, 'desc_path': desc_path}
                if len(name) > 0:
                    if name == filename:
                        return dev 
                elif vid == SIS_VID:
                    return dev 
    return None 

def parse_arguments():
  parser = optparse.OptionParser()

  parser.add_option('-p', '--desc_path', dest='desc_path', default=None,
                    help=('The path of report decriptor'))

  parser.add_option('-d', '--dev_path', dest='dev_path', default=None,
                    help=('The path of hidraw device'))

  parser.add_option('-n', '--dev_name', dest='dev_name', default=None,
                    help=('The name of hidraw device'))

  parser.add_option('-a', '--addr', dest='addr', default=None,
                    help=('The address of the DUT'))

  parser.add_option('--debug', dest='debug',
                    default=False, action='store_true',
                    help=('Debug mode.'))

  (options, args) = parser.parse_args()

  return options, args

class WsHandler(tornado.websocket.WebSocketHandler):
    waiters = set()
    counter = 0
    begin = 0
    touch_desc = None
    start = 0
    data_polling = None
    data_queue = collections.deque(maxlen=10)
    num_points_max = 10000000
    points = collections.deque(maxlen=num_points_max)
    fit_to_screen = 0
    screen_width = 0
    screen_height = 0

    def check_origin(self, origin):
        return True

    def open(self):
        WsHandler.waiters.add(self)
        print("client IP:" + self.request.remote_ip)

    def on_message(self, message):
        global touch_dev
        print(message)
        data = json.loads(message)
        print("received data: {}".format(data))

        if data['cmd'] == "requestTouchPoints":
            action = data['action']
            if action == "start":
                #"devType":pen/touch, "fitXY":0/1(fit X/Y to screen reslution),"width":screen width, "height":screen height
                device_type = data["devType"]
                WsHandler.fit_to_screen = int(data["fitXY"])
                WsHandler.screen_width = int(data["width"])
                WsHandler.screen_height = int(data["height"])
                print(data)
                desc_bin = bytes([])
                with open(touch_dev["desc_path"], "rb") as f:
                    desc_bin = f.read()
                WsHandler.touch_desc = hidparser.parse(desc_bin)
                print(WsHandler.touch_desc)
                def read_data():
                    fd = os.open(touch_dev["name"], os.O_RDONLY)
                    while True:
                        data = os.read(fd, 64)
                        WsHandler.data_queue.append([data, int(time.time()*1000)])
                        if WsHandler.start == 0:
                            break
                if WsHandler.start == 0:
                    WsHandler.start = 1
                    read_thread = threading.Thread(target=read_data)
                    read_thread.daemon = False
                    read_thread.start()
                if WsHandler.data_polling is None:
                    def data_handler():
                        if len(WsHandler.data_queue) > 0:
                            got_data = 0
                            x_max = 0
                            y_max = 0
                            report = {}
                            finger = {}
                            buf = WsHandler.data_queue.popleft()
                            print(buf[0].hex())
                            timestamp = buf[1]
                            report = {"timestamp":timestamp}
                            report_id = buf[0][0]
                            WsHandler.touch_desc.deserialize(buf[0])
                            tp = WsHandler.touch_desc.reports[report_id].inputs
                            for key in tp._attrs.keys():
                                print(key)
                                if key == "touch_screen" and device_type == "touch":
                                    ts = tp.touch_screen
                                    if ts.finger[0]:
                                        for i in ts.finger[0].items:
                                            if i.usages:
                                                if i.usages[0]._name_ == "X":
                                                    x_max = i.physical_range.maximum
                                                    print("xmin:{}, max:{}".format(i.physical_range.minimum,i.physical_range.maximum))
                                                elif i.usages[0]._name_ == "Y":
                                                    y_max = i.physical_range.maximum
                                                    print("ymin:{}, max:{}".format(i.physical_range.minimum,i.physical_range.maximum))
                                        
                                    for i in range(len(ts.finger)):
                                        finger = ts.finger[i]
                                        for key in finger._attrs.keys():
                                            value = finger.__getattr__(key)
                                            if WsHandler.fit_to_screen == 1:
                                                print("key:{}, value:{}".format(key, value))
                                                if key == "x":
                                                   value = int(value * WsHandler.screen_width / x_max ) 
                                                   print("key:{}, value:{}".format(key, value))
                                                if key == "y":
                                                   value = int(value * WsHandler.screen_height / y_max ) 
                                                   print("key:{}, value:{}".format(key, value))
                                            got_data = 1
                                            report["%s_%d" % (key,i)] = value 
                                elif key == "pen" and device_type == "pen":
                                    pen = tp.pen.stylus
                                    for item in pen.items:
                                        try:
                                            if item.usages:
                                                if item.usages[0]._name_ == "X":
                                                    x_max = item.physical_range.maximum
                                                elif item.usages[0]._name_ == "Y":
                                                    y_max = item.physical_range.maximum
                                                print(item.usages[0]._name_)
                                                print("mitem.:{}, max:{}".format(item.physical_range.minimum,item.physical_range.maximum))
                                        except:
                                            pass
                                    for key in pen._attrs.keys():
                                        value = pen.__getattr__(key)
                                        print("key:{}, value:{}".format(key, value))
                                        if WsHandler.fit_to_screen == 1:
                                            if key == "x":
                                               value = int(value * WsHandler.screen_width / x_max ) 
                                            if key == "y":
                                               value = int(value * WsHandler.screen_height / y_max ) 
                                        got_data = 1
                                        if isinstance(value, Collection):
                                            for sub_key in value._attrs.keys():
                                                sub_value = value.__getattr__(sub_key)
                                                report[sub_key] = sub_value 
                                        elif isinstance(value, list):
                                            for j in range(len(value)):
                                                sub_value = value[j].fget()
                                                sub_key = "{}_{}".format(key,j)
                                                report[sub_key] = sub_value 
                                        else:
                                            if key == "transducer_serial_number" or key[:6] =="vendor":
                                                report[key] = hex(value)
                                            else:
                                                report[key] = value 
                                    
                                else:
                                    print("!!!!! parse error, {}".format(tp.__getattr__(key)))
                            if got_data == 1:
                                print(report)
                                WsHandler.points.append(report)
                                ws_data = {"type": "touchPoints", "data": report}
                                WsHandler.send_updates(json.dumps(ws_data))
                        

                    WsHandler.data_polling = tornado.ioloop.PeriodicCallback(data_handler, 10)
                    WsHandler.data_polling.start()

            elif action == "stop":
                WsHandler.start = 0

            # Set the ioloop instance
            ioloop_instance = tornado.ioloop.IOLoop.instance()

        if data['cmd'] == "requestFinalization":
            # Finalize the run.
            print('Perform finalization.')
            
            WsHandler.start = 0

            # Save file
            if len(WsHandler.points) > 0:
                csv_dict_writer("output.csv", WsHandler.points)
                print("wirte out file: output.csv") 

            # Stop the IOLoop
            tornado.ioloop.IOLoop.instance().stop()
            

    def on_close(self):
        WsHandler.waiters.remove(self)
        print("close ws")

    @classmethod
    def send_updates(cls, msg ):
        for waiter in cls.waiters:
            if cls.counter == 0:
                cls.begin = time.time()
            cls.counter += 1
            #print(msg)

            waiter.write_message( msg )


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        return self.render('index.html')

class PageNotFoundHandler(tornado.web.RequestHandler):
    def get(self):
        return self.write_error(404)

class Application(tornado.web.Application):
    def __init__(self):

        handlers = [
            (r"/ws", WsHandler),
            (r"/", IndexHandler),
            (r".*", PageNotFoundHandler),
        ]

        if getattr(sys, 'frozen', False): # For deployment
            static_folder = os.path.join(sys._MEIPASS, 'static')
            template_folder = os.path.join(sys._MEIPASS, 'templates')
        else: # For development
            static_folder = os.path.join(os.path.dirname(__file__), "static")
            template_folder = os.path.join(os.path.dirname(__file__), "templates")

        settings = dict(
            static_path=static_folder,
            template_path=template_folder,
        )
        tornado.web.Application.__init__(self, handlers, **settings)


def main():
    global touch_dev
    application = Application()
    server = tornado.httpserver.HTTPServer(application, xheaders=True)
    server.listen(NET_PORT)
    ioLoop = tornado.ioloop.IOLoop.instance()

    options, args = parse_arguments()
    if options.desc_path and options.dev_path:
        touch_dev = {'name': options.dev_path, 'vid': None, 'pid': None, 'desc_path': options.desc_path}
        print("path:{},dev:{}, touch:{}".format(options.desc_path, options.dev_path, touch_dev))
    elif options.dev_name:
        touch_dev = find_sis_touch(options.dev_name)
        print("dev name:{}, touch:{}".format(options.dev_name, touch_dev))
    else:
        touch_dev = find_sis_touch()

    if touch_dev is not None:   
        print ("Web address: http://localhost:{}".format(NET_PORT))
        print ("websocket server start")
        ioLoop.start()
        print ("websocket server stop")
    else:
        print ("device not found")

if __name__ == '__main__':

    main()
