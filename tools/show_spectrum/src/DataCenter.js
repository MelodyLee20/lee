
class DataCenter {
  // Data Center which takes care the data exchange via websocket.

  constructor(wsUri) {
    this.data = [];
    this.cmdId = 0;

    // Pen
    this.widthId = null
    this.colorhId = null
    this.typeId = null
    this.infoId = null
    this.disableWidthAPI = 0
    this.disableColorAPI = 0
    this.disableTypeAPI = 0

    // Websocket
    this.wsUri = wsUri;
    this.websocket = null;
    this.connected = false;
    this.numReconnctMax = 3;
    this.numReconnct = 0;

    // Handler for the situation when data arrives
    this.updateAttributeHandler = null
    this.updateResultStatusHandler = null

  }

  connect() {
    this.websocket = new WebSocket(this.wsUri);

    // callbacks
    this.websocket.onopen = (e) => {
      console.log('Connected');
      this.connected = true;

      this.openDevice()
    };

    this.websocket.onclose = (e) => {
      console.log('onclose: ' + e.data)

      console.log('Disconnected');
      this.connected = false;

      setTimeout( () => this.reconnect(), 3000);

    };

    this.websocket.onmessage = (e) => {
      console.log('onmessage: ' + e.data)

      const data = JSON.parse(e.data);
      if (data.hasOwnProperty('result')) {
        const status = data.result.status
        console.log(status)
        this.updateResultStatusHandler(status)
        if (status == "successful") {
          this.updateAttributeFromData(data)
        }
      } else if (data.hasOwnProperty('params')) {
          this.dataArriveFromData(data)

      }
    };

    this.websocket.onerror = (e) => {
       console.log('onerror: ' + e.data);
    };
  }

  disconnect() {
    console.log('user Disconnected');
    this.websocket.close();
    this.connected = false;
  }

  reconnect() {

    if (this.numReconnct <= this.numReconnctMax) {
      this.numReconnct ++
      console.log('Reconnecting ...')
      this.connect()
    } else {
      console.log('Stop reconnecting!')
    }
  }

  send(data) {
    this.cmdId ++
    data.id = this.cmdId

    const msg = JSON.stringify(data)
    console.log('send: ' + msg)

    this.websocket.send(msg);
  }

  openDevice() {
    var data = {"id": null, "method": "command","params": {"command":"open", "property":"touch_device"}}
    this.send(data)
  }

  closeDevice() {
    var data = {"id": null, "method": "command","params": {"command":"close", "property":"touch_device"}}
    this.send(data)
  }

  openUartDevice(val) {
    var data = {"id": null, "method": "command","params": {"command":"open", "property":"uart_device", "value": val}}
    this.send(data)
    data = {"id": null, "method": "feed_data","params": {"property":"noise_spectrum", "action": "start"}}
    this.send(data)
  }

  closeUartDevice() {
    var data = {"id": null, "method": "command","params": {"command":"close", "property":"uart_device"}}
    this.send(data)
    data = {"id": null, "method": "feed_data","params": {"property":"noise_spectrum", "action": "stop"}}
    this.send(data)
  }

  setCDC() {
    var data = {"id": null, "method": "set","params": {"property":"cdc_mode"}}
    this.send(data)
  }

  setUpdateAttributeHandler(handler) {
    this.updateAttributeHandler = handler
  }

  setUpdateResultStatusHandler(handler) {
    this.updateResultStatusHandler = handler
  }

  setDataArriveHandler(handler) {
    this.dataArriveHandler = handler
  }

  dataArriveFromData(data) {
    this.dataArriveHandler(data.params.data[2])
  }

  updateAttributeFromData(data) {
  }

}

export {DataCenter}
