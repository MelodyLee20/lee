import csv
import os
import numpy as np
import matplotlib.pyplot as plt

##################input data 
DBG_MSG         = int(0)
DBG_DRAW_POINT  = int(0)
SWAP_ENA        = int(0)
DIGI_FILE       = "digi_out"
ROBOT_FILE      = "rbt_result"
ERR_THRES       = float(5.0) #unit : mm

##################define 
ROBOT_H_W       = np.array([173.8, 309.4], dtype = float)
FW_X_Y          = np.array([2159.0, 3839.0], dtype = float)
DISP_X_Y        = np.array([768.0, 1366.0], dtype = float)
TX              = int(0)
RX              = int(1)
DIGI_X          = int(1)
DIGI_Y          = int(2)
DIGI_TIP        = int(7)

EDGE_NUM        = int(0)
CENTER_NUM      = int(0)
TIP_NUM_PER_PTS = int(0)
BOUND_W         = float(4.2)
##################global variable 
g_robot_pt      = np.array(np.zeros(2), dtype = float) 
g_pre_tip       = int(0)
g_summary_data  = []

##################Function
def ConvertPosi_Disply2Robot(fw_x, fw_y):
    #decide fw tx/rx
    if (SWAP_ENA == 1):
        fw_rx = fw_x
        fw_tx = fw_y
    else:
        fw_tx = fw_x
        fw_rx = fw_y
    #fw 2 display
    disp_tx = fw_tx * DISP_X_Y[TX] / FW_X_Y[TX]
    disp_rx = fw_rx * DISP_X_Y[RX] / FW_X_Y[RX]
    #display 2 mm
    tx_mm = disp_tx * ROBOT_H_W[TX] / DISP_X_Y[TX]
    rx_mm = disp_rx * ROBOT_H_W[RX] / DISP_X_Y[RX]
    
    if (tx_mm < BOUND_W or tx_mm > ROBOT_H_W[TX] - BOUND_W or
        rx_mm < BOUND_W or rx_mm > ROBOT_H_W[RX] - BOUND_W):
        boundary = 1
    else:
        boundary = 0
        
    if (SWAP_ENA == 1):
        return (round(tx_mm, 10), round(rx_mm, 10), boundary)
    else:
        return (round(rx_mm, 10), round(tx_mm, 10), boundary)

def GetAccuracyErr(robot_x, robot_y, rpt_x, rpt_y):
    dx = robot_x - rpt_x
    dy = robot_y - rpt_y
    return round((np.sqrt(np.power(dx,2) + np.power(dy,2))),4)

def InitParam():
    global g_robot_pt
    global g_pre_tip
    global g_summary_data    
    
    del g_summary_data[:]
    g_robot_pt      = np.array(np.zeros(2), dtype = float) 
    g_pre_tip       = int(0)
    if (DBG_DRAW_POINT == 1):
        plt.figure(figsize=(16, 9))

def UpdatePointInfo(rbt_spt_num):
    global EDGE_NUM
    global CENTER_NUM
    global TIP_NUM_PER_PTS
    
    if (rbt_spt_num == 22):   #five point
        EDGE_NUM        = int(0)
        CENTER_NUM      = int(5)
        TIP_NUM_PER_PTS = int(3)        
    elif (rbt_spt_num == 23): #loop 22*43
        EDGE_NUM        = int(0)
        CENTER_NUM      = int(946)
        TIP_NUM_PER_PTS = int(1)  
    elif (rbt_spt_num == 24): #full test
        EDGE_NUM        = int(8)
        CENTER_NUM      = int(32)
        TIP_NUM_PER_PTS = int(32)  
    elif (rbt_spt_num == 999): #allion debug test
        EDGE_NUM        = int(1)
        CENTER_NUM      = int(48)
        TIP_NUM_PER_PTS = int(4)  
    else:
        print ("unknow robot script info")
        os._exit() 
    #print (EDGE_NUM, CENTER_NUM, TIP_NUM_PER_PTS)

def ParserRbotInfo(rb_reader):
    for row in rb_reader:
        if (row[0] == 'X'):
            ROBOT_H_W[TX] = float(row[1])
            FW_X_Y[TX]    = float(row[2])
            DISP_X_Y[TX]  = float(row[3])
        elif (row[0] == 'Y'):
            ROBOT_H_W[RX] = float(row[1])
            FW_X_Y[RX]    = float(row[2])
            DISP_X_Y[RX]  = float(row[3])
        elif (row[0] == 'Plane_X'):
            UpdatePointInfo(int(row[6]))
            continue
        elif (row[0] == 'Plane_Y'):
            global SWAP_ENA
            SWAP_ENA = int(row[6])
            continue
        elif (row[0] == 'Point_No'):
            break

def main(robot_file_name):    
    InitParam()
    #path setting
    os.chdir(str(os.getcwd()))
    
    #open file
    digi_f = open(DIGI_FILE, 'r')
    robot_f = open(robot_file_name, 'r')
    
    #get file point    
    digi_reader = csv.reader(digi_f)
    robot_reader = csv.reader(robot_f)
    
    #parser robot coordinate info.
    ParserRbotInfo(robot_reader)
        
    if (DBG_MSG == 1):
        print ('{0: <10}{1: <10}{2: <15}{3: <15}{4: <10}{5: <10}{6: <10}'.format
               ("Rbot_X", "Rbot_Y", "Disp_X(mm)", "Disp_Y(mm)", "Dist_Err", "dx", "dy"))
    
    global g_pre_tip
    global g_robot_pt
    errVal = 0
    dx = float(0)
    dy = float(0)
    digi_x_mm = float(0)
    digi_y_mm = float(0)
    valid_bir_pt = int(0)
    #parser digi info.
    for row in digi_reader:
        
        #if (float(row[DIGI_X])==float(3622) or float(row[DIGI_Y])==float(250)):
        #    continue
        
        #Birth Point
        if ((g_pre_tip == 0 and int(row[DIGI_TIP]) == 1)):
            
            #resolution 2 mm
            #digi_x_mm, digi_y_mm, boundary = ConvertPosi_Disply2Robot(round(float(row[DIGI_X])*X_RATIO,2), round(float(row[DIGI_Y])*Y_RATIO,2)) 
            
            #FW 2 mm
            digi_x_mm, digi_y_mm, boundary = ConvertPosi_Disply2Robot(float(row[DIGI_X]), float(row[DIGI_Y])) 
            if (boundary):
                valid_bir_pt = valid_bir_pt + 1;
                g_pre_tip = int(row[DIGI_TIP])
                continue
            
            while(1):
                robot_row = next(robot_reader)
                g_robot_pt[0] = float(robot_row[4])
                g_robot_pt[1] = float(robot_row[5])
                #resolution 2 mm
                #digi_x_mm, digi_y_mm = ConvertPosi_Disply2Robot(float(row[DIGI_X]), float(row[DIGI_Y])) 
                #dx, dy (mm)
                dx = round((g_robot_pt[0] - digi_x_mm), 3)
                dy = round((g_robot_pt[1] - digi_y_mm), 3)
                errVal = GetAccuracyErr(g_robot_pt[0], g_robot_pt[1], digi_x_mm, digi_y_mm)
                
                #digi_out loss pt handl                    
                if (errVal < ERR_THRES): #magic number
                    break
                #print (errVal, g_robot_pt, float(row[DIGI_X]), float(row[DIGI_Y]), digi_x_mm, digi_y_mm)
                #return
            valid_bir_pt = valid_bir_pt + 1;
            if ( valid_bir_pt > (EDGE_NUM * TIP_NUM_PER_PTS * 4)): #skip edge points.
            
                if (DBG_MSG == 1):
                    print ('{0: <10}{1: <10}{2: <15}{3: <15}{4: <10}{5: <10}{6: <10}'.format
                           (g_robot_pt[0], g_robot_pt[1], 
                            digi_x_mm, digi_y_mm, errVal, 
                            dx, dy))

                if (DBG_DRAW_POINT == 1):
                    plt.plot(g_robot_pt[1], g_robot_pt[0], 'g+', digi_y_mm, digi_x_mm, 'rx')

                g_summary_data.append(errVal)      

        g_pre_tip = int(row[DIGI_TIP])
        
    robot_f.close()
    digi_f.close()             

    try:
        retVal = round(sum(g_summary_data)/len(g_summary_data), 4), max(g_summary_data), round((np.std(np.asarray(g_summary_data))), 4)
    except:
        retVal = (-1, -1, -1)
        
    return (retVal)

print (main(ROBOT_FILE))