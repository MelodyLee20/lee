# Tool used to show the voltage.

# Install the dependent packages via the requirements.txt
pip install -r requirements.txt

# Install the dependent packages for the frontend
yarn install

# Start the UI server
yarn start

# Build and deploy the UI files
yarn build 
yarn deploy

# Package the app as an executable
pyinstaller main.spec
or
pyinstaller -w -F --add-data "templates:templates" --add-data "static:static" main.py -p ../../../touch 
pyinstaller -F --add-data templates;templates --add-data static;static main.py -p ../../../touch (for windows)

# Generate requirements file
pip freeze > requirements.txt

# Install from the given requirements file
pip install -r requirements.txt



