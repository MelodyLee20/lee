# -*- coding: utf-8 -*-
import sys
import os
import time
import json
import logging
sys.path.append("../../../touch")
import touch.fwio2 as fwio
from touch import hidparser
from touch.hidparser.Device import Collection
from touch.hidparser.UsagePages import GenericDesktop, Button
from touch.hidparser.UsagePage import UsagePage, UsageType, Usage
from touch.hidparser.enums import CollectionType, ReportType, ReportFlags, UnitSystem
import platform

# Version
__version__ = '0.1.0'

if __name__ == '__main__':
    touch_dev = fwio.core(logging.DEBUG)
    desc_bin = bytes([])
    desc_parser = None
    feed_data_prperty = "hidraw"
    ver = "0.1"

    def callback(ws, status, id, msg, data=[]):
        print([id, status, msg, data])
        if status == 0:
            status_str = "successful"
        else:
            status_str = "failed"

        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'", "data": "'+str(data)+'"}}'
        ws.send_updates(ws_data)

    def handler(data):
        print("chk data:{}".format(data))
        id = data['id']
        method = data['method']
        params = data['params']
        ws_data = ""
        if method == "command":
            if params["command"] == "requestFinalization":
                # Finalize the run.
                logging.debug('Perform finalization.')
                dev_close()
                return
        elif method == "get":
            status_str = "successful"
            # data = [1,2,1], data[0] is pen id, data[1] is value, data[2] is read only.
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'", "data":"[1, 2, 1]"}}'
        else:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        print("reponse: {}".format(ws_data))
        touch_dev.server.ws_handler.send_updates(ws_data)

    def dev_close():
        print("close")
        touch_dev.stop_server()
    #touch_dev.start_server(handler, callback)
    touch_dev.start_server(handler, callback, ip='127.0.0.1')
    #touch_dev.start_server(handler, callback, ip='192.168.43.10')
    #touch_dev.start_server(handler, callback, ip='192.168.43.12')
