# -*- coding: utf-8 -*-
import sys
import os
import time
import json
import logging
sys.path.append("../../../touch")
import touch.fwio2 as fwio
from touch import hidparser
from touch.hidparser.Device import Collection
from touch.hidparser.UsagePages import GenericDesktop, Button
from touch.hidparser.UsagePage import UsagePage, UsageType, Usage
from touch.hidparser.enums import CollectionType, ReportType, ReportFlags, UnitSystem
import platform
import optparse
import collections

# Version
__version__ = '0.1.0'

def parse_arguments():
  parser = optparse.OptionParser()

  parser.add_option('-i', '--ip', dest='ip', default="127.0.0.1",
                    help=('the IP address of server'))
  parser.add_option('-p', '--port', dest='port', default=8050,
                    help=('the port of server'))
  (options, args) = parser.parse_args()

  return options, args


if __name__ == '__main__':
    options, args = parse_arguments()
    ip = options.ip 
    port = int(options.port)
    touch_dev = fwio.core(logging.DEBUG)
    desc_bin = bytes([])
    desc_parser = None
    feed_data_prperty = "hidraw"
    ver = "0.1"
    hid_fd = None

    def callback(ws, status, id, msg, data=[]):
        print([id, status, msg, data])
        if status == 0:
            status_str = "successful"
        else:
            status_str = "failed"

        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'", "data": "'+str(data)+'"}}'
        ws.send_updates(ws_data)

    def dev_open(id, path=None):
        global desc_parser, touch_dev, hid_fd
        ret = touch_dev.search()
        if ret == True:
            print("Open Device")
            ret = touch_dev.open(enable_read_thread=False)
            if ret == True:
                with open(touch_dev.desc.rd_path, "rb") as f:
                    desc_bin = f.read()
                    print("report read finish")
                    desc_parser = hidparser.parse(desc_bin)
                    print(desc_parser)
                touch_dev.get_pen_capability("start", id)
        else:
            print("open failed")
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        touch_dev.server.ws_handler.send_updates(ws_data)

    def handler(data):
        print("chk data:{}".format(data))
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                # Finalize the run.
                logging.debug('Perform finalization.')
                dev_close()
            elif params["command"] == "hello":
                print('new client connected!!')
            elif params["command"] == "open":
                dev_open(id)
            elif params["command"] == "close":
                dev_close()

        elif method == "get":
            if params["property"] == "usi_color":
                touch_dev.usi_pen_get_cmd("color", id=id)
            elif params["property"] == "usi_width":
                touch_dev.usi_pen_get_cmd("width", id=id)
            elif params["property"] == "usi_type":
                touch_dev.usi_pen_get_cmd("type", id=id)
            if params["property"] == "usi_pen_info":
                print( params["property"] )
                status_str = "failed"
                result = []
                hid_fd = os.open(touch_dev.desc.path, os.O_RDONLY)
                if hid_fd is not None and desc_parser is not None:
                    print( hid_fd )
                    buf = os.read(hid_fd, 64)
                    if len(buf) > 0:
                        hidraw_buf = collections.deque(maxlen=1)
                        hidraw_buf.append(buf)
                        ret = hidparser.parse_data(hidraw_buf, desc_parser)
                        result = []
                        if len(ret) > 0:
                            data = ret[0]
                            print(data)
                            if "preferred_color" in data and "preferred_line_width" in data and "ink" in data:
                                status_str = "successful"
                                result = [data["preferred_line_width"], data["preferred_color"], data["ink"]] 
                os.close(hid_fd)
                ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'", "data": "'+str(result)+'"}}'
                touch_dev.server.ws_handler.send_updates(ws_data)
                            
        elif method == "set":
            if params["property"] == "usi_color":
                touch_dev.usi_pen_set_cmd("color", params["value"], id=id)
            elif params["property"] == "usi_width":
                touch_dev.usi_pen_set_cmd("width", params["value"], id=id)
            elif params["property"] == "usi_type":
                touch_dev.usi_pen_set_cmd("type", params["value"], id=id)

        elif method == "feed_data":
            if params["action"] == "start":
                feed_data_prperty = params["property"]
                print("start feed data")
                def data_cb():
                    buf = touch_dev.get_hidraw()
                    if buf is not None:
                        print(feed_data_prperty)
                        if feed_data_prperty == "hidraw_parsed":
                            data = hidparser.parse_data(buf, desc_parser) 
                        elif feed_data_prperty == "hidraw":
                            data = list(buf)
                        ws_data = '{"params": {"data": "'+str(data)+'"}}'
                        touch_dev.server.ws_handler.send_updates(ws_data)
                touch_dev.server.periodic_callback(data_cb, 10)
            elif params["action"] == "stop":
                touch_dev.server.periodic_callback(enable=0)
                print("stop feed data")
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
            touch_dev.server.ws_handler.send_updates(ws_data)

    def dev_close():
        print("close")
        touch_dev.get_pen_capability("stop")
        touch_dev.stop_server()
        #touch_dev.close()
    touch_dev.start_server(handler, callback, ip, port)
