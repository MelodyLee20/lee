from PyQt5.QtCore import *

import os
import sys
import numpy as np
import cv2 as cv

from touch.pcali import Calibrator
from touch.utils.opencv import Webcam, flip_image
import touch.fwio2 as fwio
import logging


VERSION = '0.5.0'

class Calibration(QThread):

    def __init__(self):
        super().__init__()

        self.calibrator = Calibrator()
        self.score = None
        self.popt = None

        self.use_image_file = True
        #self.use_image_file = False

        self.flip_snapshot = True
        #self.flip_snapshot = False

        #self.access_fw = True
        self.access_fw = False

        # Webcam device
        self.webcam_device = 0 # The second device.

        # Image size
        self.image_width = 3264
        self.image_height = 2448

        # Open sis touch device
        self.touch_dev = None
        if self.access_fw:
            self.open_device()

        # Snapshot
        #time.sleep(3)
        self.webcam = Webcam()
        self.snapshot_name = 'snapshot.jpg'

    def open_device(self):
        def callback(ws, status, id, msg, data=[]):
            print([status, msg, data])

        self.touch_dev = fwio.core(logging.ERROR, ui="pyqt")
        if self.touch_dev.search() != True:
            self.touch_dev = None
            sys.exit()
        self.touch_dev.start_server(cb=callback)
        self.touch_dev.open()

    def run(self):

        # Take picture
        image = self.take_picture()

        # Save the snapshot
        self.save_snapshot(image)

        # Projective calibration
        c = self.calibrator
        input_points, target_points = c.get_cali_data(image)
        self.score, self.popt = c.fit()
        rmse = c.get_rmse()

        print('score: {:.3f}'.format(self.score))
        print('popt: {}'.format(self.popt))
        print('RMSE: {:.3f}'.format(rmse))

        # Update the firmware
        out = self.update_firmware(self.popt)

        print('Calibration is done, bye~')

        self.finished.emit()

    def open_webcam(self):
        # Open the webcam.

        # Release the resource which had been used.
        if self.webcam.is_open():
            self.webcam.release()

        self.webcam.open(self.webcam_device, width=self.image_width, height=self.image_height)

    def close_webcam(self):
        # Close the webcam.

        self.webcam.release()

    def take_picture(self):
        # Take picture.

        # Check if the snapshot exists or not.
        cwd = os.getcwd()
        filepath = os.path.join(cwd, self.snapshot_name)
        if os.path.isfile(filepath):
            snapshot_existed = True
        else:
            snapshot_existed = False


        if self.use_image_file and snapshot_existed:
            # Read image
            image = cv.imread(filepath)

        else:
            # Open the webcam
            self.open_webcam()

            # Read image
            for i in range(3):
                image = self.webcam.read()

            # Flip the snapshot
            if self.flip_snapshot:
                image = flip_image(image, flag=0) # Vertically
                image = flip_image(image, flag=1) # Horizontally

            # Close the webcam
            self.close_webcam()

        return image

    def save_snapshot(self, image):
        # Save the snapshot.

        cwd = os.getcwd()
        filepath = os.path.join(cwd, self.snapshot_name)
        cv.imwrite(filepath, image)

    def convert_fitting_params(self, params):
        for i in range(6):
            if params[i] < 0 and params[i] > -1:
                params[i] -= 32768
                print(params[i])

        print('params:', params)
        numerator = np.modf(np.array([params[0],params[1],params[3],params[4],params[2],params[5]]))
        denominator = np.modf(np.array([params[6],params[7]]))
        numerator_fra = (np.absolute(numerator[0])* 65536).astype(int)
        numerator_int = numerator[1].astype(int)
        denominator_fra = (denominator[0] * (1<<27)).astype(int)
        n_fra = []
        n_int = []
        d_fra = []
        data = []
        for i in range(len(numerator_int)):
            n_fra.append([(numerator_fra[i]>>8) & 0xff, numerator_fra[i] & 0xff])
            n_int.append([(numerator_int[i]>>8) & 0xff, numerator_int[i] & 0xff])
        for i in range(len(denominator)):
            d_fra.append([(denominator_fra[i]>>24) & 0xff, (denominator_fra[i]>>16) & 0xff, (denominator_fra[i]>>8) & 0xff, denominator_fra[i] & 0xff])
        for i in range(0,len(numerator_int),2):
            data.append([n_int[i][0], n_int[i][1], n_fra[i][0], n_fra[i][1]])
            data.append([n_int[i+1][0], n_int[i+1][1], n_fra[i+1][0], n_fra[i+1][1]])
        data.append([d_fra[0][0], d_fra[0][1], d_fra[0][2], d_fra[0][3]])
        data.append([d_fra[1][0], d_fra[1][1], d_fra[1][2], d_fra[1][3]])
        return data

    def update_firmware(self, popt):
        # Update the firmware.
        def fw_callback(ws, status, id, msg, data=[]):
            print([status, msg, data])
            self.touch_dev.stop_server()
            self.touch_dev.close()

        data = self.convert_fitting_params(popt)
        print('data:', data)
        if self.access_fw:
            self.touch_dev.calibrate_coordinates(data, cb=fw_callback)
        err_code = 0
        return err_code


