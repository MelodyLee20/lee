from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtGui, QtWidgets, QtCore

import numpy as np

class View(QGraphicsView):

    def __init__(self):
        super(View, self).__init__()

        # Add the scene
        self.scene = QGraphicsScene()
        self.setScene(self.scene)

        self.add_background()
        #self.add_screen_markers()


    def add_screen_markers(self):

        screen = QGuiApplication.primaryScreen()
        geo = screen.geometry()
        xa = geo.x()
        ya = geo.y()
        xz = geo.width()
        yz = geo.height()

        # Marker shape
        w = 100
        h = 100

        # First marker
        x = xa
        y = ya
        self.add_marker(x, y, w, h)

        # Second marker
        x = xa
        y = yz-h
        self.add_marker(x, y, w, h)

        # Third marker
        x = xz-w
        y = yz-h
        self.add_marker(x, y, w, h)

        # Forth marker
        x = xz-w
        y = ya
        self.add_marker(x, y, w, h)


    def add_marker(self, x, y, w, h):
        # Add point item to scene.

        marker = QGraphicsRectItem(x, y, w, h)
        marker.setPen(self.get_qpen())
        marker.setBrush(self.get_qbrush())

        self.scene.addItem(marker)

        return marker

    def add_background(self):
        # Add background.

        screen = QGuiApplication.primaryScreen()
        geo = screen.geometry()
        x = geo.x()
        y = geo.y()
        w = geo.width()
        h = geo.height()

        marker = QGraphicsRectItem(x, y, w, h)
        marker.setPen(Qt.blue)
        marker.setBrush(Qt.blue)

        self.scene.addItem(marker)

        return marker

    def get_qpen(self):
        return QPen(Qt.NoPen)

    def get_qbrush(self):
        return QBrush(Qt.darkGreen, Qt.SolidPattern)

