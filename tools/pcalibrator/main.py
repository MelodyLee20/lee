from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import QtGui, QtWidgets, QtCore

import os
import sys

from ui_main import Ui_MainWindow
from view import View
from calibration import Calibration


VERSION = '0.5.0'


class Main(QMainWindow, Ui_MainWindow):

    def __init__(self, *args, **kwargs):
        super(Main, self).__init__(*args, **kwargs)
        self.setupUi(self)

        # Hide elements
        self.control_widget.hide()

        # Add the view
        self.add_view()

        # Full screen
        self.start_fullscreen()

        # Calibration
        self.calibrate()

    def calibrate(self):
        # Calibrate.

        #thread = Thread()
        thread = Calibration()
        thread.finished.connect(app.exit)
        thread.start()
        sys.exit(app.exec_())


    def add_view(self):
        # Add the view without scrollbar.

        self.view = View()
        self.view.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.view.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.main_layout.addWidget(self.view)

        return self.view

    def start_fullscreen(self):
        # Start the fullscreen mode

        # Set fullscreen without the frame
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.showFullScreen()

        # Fix the size of Scene
        screen = QGuiApplication.primaryScreen()
        geo = screen.geometry()
        self.view.scene.setSceneRect(0, 0, geo.width(), geo.height())

    def exit_fullscreen(self):
        # Exit the fullscreen mode

        # Normal size
        self.showNormal()

        # Reset
        self.reset()


if __name__ == '__main__':

    app = QApplication([])
    main = Main()
    main.show()
    app.exec_()
