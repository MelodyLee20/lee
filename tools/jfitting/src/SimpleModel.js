
import fmin from 'fmin'

function r2ScoreBasic(ys, fs) {

  let ySum = 0.0
  let num = ys.length
  let i, tmp
  ys.forEach(item => {
    ySum += item
  })

  let yAvg = ySum / num

  let ssTot = 0.0
  ys.forEach(item => {
    tmp = item - yAvg
    ssTot += tmp*tmp
  })

  let ssRes = 0.0
  for (i = 0; i < num; i++) {
    tmp = ys[i] - fs[i]
    ssRes += tmp*tmp
  }

  // r2 score
  let score = 1.0 - ssRes/ssTot

  return score

}

function getElements(ys, j) {

  let num = ys.length
  let out = []
  let i
  for (i = 0; i < num; i++ ) {
      out.push(ys[i][j])
  }

  return out
}


function r2Score(ys, fs) {

  let num = ys.length
  let dim = ys[0].length

  let scores = []
  let score
  let i, j
  let y, f
  let yArr, fArr

  for (j = 0; j < dim; j++ ) {
    yArr = getElements(ys, j)
    fArr = getElements(fs, j)
    score = r2ScoreBasic(yArr, fArr)
    scores.push(score)
  }

  let sum = 0.0
  scores.forEach(item => {
    sum += item
  })
  let r2Score = sum / dim

  return r2Score
}

class SimpleModel {
  // Simple model for fitting.

  constructor() {
    this.score = 0.0
    this.result = null
  }

  forward(point, params) {
    /*
    Obtain the predicted points.

    x' = (Ax*x+Bx*y+Cx) / (Dx*x+Ex*y+1)
    y' = (Ay*x+By*y+Cy) / (Dy*x+Ey*y+1)

    Parameters
    ----------
    point: array-like
        touch point.
    */

    let x, y
    [x, y] = point

    let Ax, Bx, Cx, Ay, By, Cy, R1, R2
    [Ax, Bx, Cx, Ay, By, Cy, R1, R2] = params

    //let x_out = (Ax*x+Bx*y+Cx)
    //let y_out = (Ay*x+By*y+Cy)
    let x_out = (Ax*x+Bx*y+Cx)/(1.0+R1*x+R2*y)
    let y_out = (Ay*x+By*y+Cy)/(1.0+R1*x+R2*y)

    let point_out = [x_out, y_out]

    return point_out

  }

  find_distance(point0, point1) {

    let x0, y0
    [x0, y0] = point0
    let x1, y1
    [x1, y1] = point1

    //let d = Math.sqrt((x1-x0)**2)
    let d = Math.sqrt((x1-x0)**2 + (y1-y0)**2)

    return d

  }

  costFunc(params, inputPoints, targetPoints) {
    /*
    Cost function.

    Parameters
    ----------
    params: array-like
        The independent parameters.

    input_points: 2d array
        Touch point array in shape of (num_points, 2).

    target_points: 2d array
        Target point array in shape of (num_points, 2).
    */

    let numPoints = inputPoints.length

    let cost = 0.0
    let inputPoint, targetPoint
    let predPoint
    let d = 0.0
    let i
    for (i = 0; i < numPoints; i++) {
      inputPoint = inputPoints[i]
      targetPoint = targetPoints[i]

      predPoint = this.forward(inputPoint, params)
      d = this.find_distance(predPoint, targetPoint)
      cost += d
    }

    return cost
  }

  toPredPoints(points, params) {
    // Mapping into target points

    let outputs = []
    let out
    for (let i=0; i< points.length; i++) {
      out = this.forward(points[i], params)
      outputs.push(out)
    }

    return outputs
  }


  fit(inputPoints, targetPoints) {
    /*
    Fit the model.

    Parameters
    ----------
    inputPoints: 2d array
      Touch point array in shape of (numPoints, 2).

    target_points: 2d array
      Target point array in shape of (numPoints, 2).

    method: str
      Method used for fitting, available options: 'BFGS', 'Powell', 'Nelder-Mead'.
    */


    // Cost function
    let func = function (params) {
      let out = 0.0

      out = this.costFunc(params, inputPoints, targetPoints)

      //console.log("Ax: " + params[0] + " Bx:" + params[1])
      //console.log("out: " + out)

      return out
    }
    func = func.bind(this)

    // Guessed solution
    let params0 = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0]

    // Fitting
    var solution = fmin.nelderMead(func, params0);
    let popt = solution.x
    let costOpt = solution.fx

    // Make predictions
    let predPoints = this.toPredPoints(inputPoints, popt)

    // Test score (R2 score). In perfect situation, the score is 1.
    let score = r2Score(targetPoints, predPoints)

    return [score, popt]
  }

}

export {SimpleModel}
