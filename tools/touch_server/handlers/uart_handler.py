# -*- coding: utf-8 -*-
import logging
import serial
import tornado.ioloop

PORT="COM17"
BAUDRATE=115200
MAGICWORD = '#surf'
DATA_EOF = '\r\n\r\n'
DATALEN = 50

class uart_handler():
    def __init__(self, WsHandler, debug=logging.ERROR):
        self.uart_dev = None
        self.uartBuf = ''
        self.ws = WsHandler
        self.counter = 0
        self.active = 0
        self.scheduler = None
        
    def open(self, port=PORT, baudrate=BAUDRATE, id=0):
        ret = True
        try:
            self.uart_dev = serial.Serial(
                            port=port,
                            baudrate=baudrate,
                            parity=serial.PARITY_NONE,
                            stopbits=serial.STOPBITS_ONE,
                            bytesize=serial.EIGHTBITS)
            self.uart_dev.flushInput()
            self.uart_dev.flushOutput()
        except:
            logging.error("can't open {}".format(port))
            ret = False
            self.active = 0
            self.uart_dev = None
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)


    def start(self):
        if self.uart_dev is not None:
            self.active = 1
            self.scheduler = tornado.ioloop.PeriodicCallback(self.get_data, 500)
            self.scheduler.start()

    def stop(self):
        self.active = 0
        if self.scheduler is not None:
            self.scheduler.stop()
            self.scheduler = None
        
    def close(self):
        try:
            self.uart_dev.close()
        except:
            logging.debug("exit")

    def get_data(self):
        if self.active == 1:
            bytesToRead = self.uart_dev.inWaiting()
            tmp = self.uart_dev.read(bytesToRead).decode("ascii") 
            logging.debug(tmp)
            self.uartBuf = self.uartBuf + tmp
            #strip uartBuf until the beginning of uartBuf is MAGICWORD
            if (self.uartBuf[:5]!= MAGICWORD):
                self.uartBuf = self.uartBuf[self.uartBuf.find(MAGICWORD):]
    
            freq = []
            noise = []
            while self.uartBuf.find(DATA_EOF) >= 0:
                if self.uartBuf[:5]!=MAGICWORD:
                    logging.debug ("\n uart error" + self.uartBuf[:5])
                    self.uartBuf = self.uartBuf[self.uartBuf.find(MAGICWORD):]
                    if self.uartBuf.find(DATA_EOF) < 0:
                        break
                rows = self.uartBuf[:self.uartBuf.find(DATA_EOF)].split('\r\n')
                freq = []
                noise = []
                point = []
                for row in rows:
                    if row[:5]!= MAGICWORD:
                        data = row.split(' ')
                        freq.append(int(data[0]))
                        noise.append(int(data[1]))
                        point.append({'x':int(int(data[0])/10),'y':int(data[1])})
                #logging.debug("freq {}, noise{}".format(freq,noise))
                #find the next MAGICWORD
                self.uartBuf = self.uartBuf[1:]
                self.uartBuf = self.uartBuf[self.uartBuf.find(MAGICWORD):]
                self.counter += 1
                wsData = {"params":{"data":[max(noise),min(noise),point]}}
                #logging.debug(wsData)
                self.ws.send_updates(wsData)

    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close uart device.')
                self.close()
            elif params["command"] == "open":
                if params["property"] == "uart_device":
                    self.open(port=params["value"],id=id)
            elif params["command"] == "close":
                if params["property"] == "uart_device":
                    self.close()
        elif method == "get":
            pass
        elif method == "set":
            pass
        elif method == "feed_data":
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
            if params["action"] == "start":
                if params["property"] == "noise_spectrum":
                    self.start()
                    logging.debug("start uart spectrum data")
                    self.ws.send_updates(ws_data)
            elif params["action"] == "stop":
                if params["property"] == "noise_spectrum":
                    self.stop()
                    logging.debug("stop uart spectrum data")
                    self.ws.send_updates(ws_data)

