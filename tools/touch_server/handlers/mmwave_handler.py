# -*- coding: utf-8 -*-
import logging
import serial
import tornado.ioloop
import sys
import struct
import time
from typing import Tuple, TextIO, BinaryIO
import numpy as np
import binascii
import math
import json
import curses
sys.path.append("../../touch")
from mmwave.tracking import EKF
from .rect_detection import RectDetection
import pandas as pd

configFileName = 'mmw_PC_14m_OfficeSpace_240.cfg'
configFileName = 'handlers/data/mmw_PC_14m_Hallway_410.cfg'
#configFileName = '14m_Hallway.cfg'
CFG_PORT="COM7"
DATA_PORT="COM8"
BAUDRATE=115200
DATA_BAUDRATE=921600

FN = "20200108-124049.dat" # basketball
#FN = "20200113-122149.dat"

#FN = "20200110-094507.dat"
#FN = "20200107-155432.dat"


def arctan2_circular(y, x):
    # Circular arctan2 which outputs angle from zero to 2*pi.
    theta = np.arctan2(y, x)
    if y < 0:
        theta = 2.0*np.pi+ theta

    return theta


# region: classess
class ULong: #uint64
    length: int = 8
    def __init__(self):
        pass

    def fromBytes(self, data: bytes, offset: int = 0):
        return struct.unpack_from('<Q', data, offset)[0]

class UInt: #uint32
    length: int = 4
    def __init__(self):
        pass

    def fromBytes(self, data: bytes, offset: int = 0):
        return struct.unpack_from('<I', data, offset)[0]

class UShort: #uint16
    length: int = 2
    def __init__(self):
        pass

    def fromBytes(self, data: bytes, offset: int = 0):
        return struct.unpack_from('<H', data, offset)[0]

class UByte: #uint8
    length: int = 1
    def __init__(self):
        pass

    def fromBytes(self, data: bytes, offset: int = 0):
        return struct.unpack_from('<B', data, offset)[0]

class Float:
    length: int = 4
    def __init__(self):
        pass

    def fromBytes(self, data: bytes, offset: int = 0):
        return struct.unpack_from('<f', data, offset)[0]

#endregion

#region dataTypes

# Initiate the data types, used to convert from CTypes to Python Types
# To convert simply iterate over the values and increase the offset by the length property
frameHeaderStructType: dict = { #52 bytes long
    #'sync':             ULong(), # See syncPatternUINT64 below
    'version':          UInt(),
    'platform':         UInt(),
    'timestamp':        UInt(), # 600MHz clocks
    'packetLength':     UInt(), # In bytes, including header
    'frameNumber':      UInt(), # Starting from 1
    'subframeNumber':   UInt(),
    'chirpMargin':      UInt(), # Chirp Processing margin, in ms
    'frameMargin':      UInt(), # Frame Processing margin, in ms
    'uartSentTime':     UInt(), # Time spent to send data, in ms
    'trackProcessTime': UInt(), # Tracking Processing time, in ms
    'numTLVs':          UShort(), # Number of TLVs in thins frame
    'checksum':         UShort() # Header checksum
}

frameHeaderLengthInBytes: int = 52
tlvHeaderLengthInBytes: int = 8
pointLengthInBytes: int = 16
targetLengthInBytes: int = 68

tlvHeaderStruct: dict = { # 8 bytes
    'type':             UInt(), # TLV object Type
    'length':           UInt() # TLV object Length, in bytes, including TLV header
}


# Point Cloud TLV object consists of an array of points.
# Each point has a structure defined below
# Type 6
pointStruct: dict = { # 20 bytes
    'range':            Float(), # Range, in m
    'angle':            Float(), # Angel, in rad
    'doppler':          Float(), # Doplper, in m/s
    'snr':              Float()  # SNR, ratio
}
# Target List TLV object consists of an array of targets.
# Each target has a structure define below
# Type 7
targetStruct: dict = { #40 bytes
    'tid':              UInt(),# Track ID
    'posX':             Float(), # Target position in X dimension, m
    'posY':             Float(), # Target position in Y dimension, m
    'velX':             Float(), # Target velocity in X dimension, m/s
    'velY':             Float(), # Target velocity in Y dimension, m/s
    'accX':             Float(), # Target acceleration in X dimension, m/s2
    'accY':             Float(), # Target acceleration in Y dimension, m/s
    'EC1':              Float(),
    'EC2':              Float(),
    'EC3':              Float(),
    'EC4':              Float(),
    'EC5':              Float(),
    'EC6':              Float(),
    'EC7':              Float(),
    'EC8':              Float(),
    'EC9':              Float(),
    'G':                Float(),
}

cTypesInfo: dict = {
    'uint64': "<Q",
    'uint32': "<I",
    'uint16': "<H",
    'uint8': "<B"
}
#endregion

# Rectangular detection
range_max = 14.0
angle_max = np.pi/3.0
length = 15.0 # Length of venue
width = 9.0 # Width od venue
rect_detection = RectDetection(range_max, angle_max, length, width)


class mmwave_handler():
    def __init__(self, WsHandler, debug=logging.ERROR):
        self.cfg_dev = None
        self.data_dev = None
        self.file_dev = None
        self.device = "uart"
        self.data_buf = bytearray()
        self.ws = WsHandler
        self.counter = 0
        self.active = 0
        self.scheduler = None
        self.numTargets = 0
        self.numPoints = 0
        self.bin_file: BinaryIO = None
        self.tracker = EKF()

    def open(self, device="uart", data_fn="20200107-155432.dat", cfg_port=CFG_PORT, data_port=DATA_PORT, baudrate=BAUDRATE, data_baudrate=DATA_BAUDRATE, id=0):
        ret = True
        self.device = device
        if self.device == "uart":
            if self.data_dev is None:
                filename = time.strftime("%Y%m%d-%H%M%S") + '.dat'
                self.bin_file = open("./data/" + filename, 'ab')
                self.cfg_dev = serial.Serial(port=cfg_port, baudrate=baudrate)
                self.cfg_dev.flushInput()
                self.cfg_dev.flushOutput()
                self.data_dev = serial.Serial(port=data_port, baudrate=data_baudrate)
                self.data_dev.flushInput()
                self.data_dev.flushOutput()
                # Read the configuration file and send it to the board
                config = [line.rstrip('\r\n') for line in open(configFileName)]
                for i in config:
                    self.cfg_dev.write((i+'\n').encode())
                    print(i)
                    time.sleep(0.01)
        elif self.device == "file":
                self.file_dev = open("./data/" + data_fn, 'rb')
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def start(self):
        if self.data_dev is not None or self.file_dev is not None:
            timer = 50
            if self.device == "file":
                timer = 50
            logging.debug("start mmwave scheduler")
            self.active = 1
            self.scheduler = tornado.ioloop.PeriodicCallback(self.get_data, timer)
            self.scheduler.start()

    def stop(self):
        self.active = 0
        if self.scheduler is not None:
            self.scheduler.stop()
            self.scheduler = None

    def close(self):
        try:
            if self.device == "uart":
                self.binfFile.close()
                self.cfg_dev.close()
                self.data_dev.close()
            elif self.device == "file":
                self.file_dev.close()
        except:
            logging.debug("exit")
        self.cfg_dev = None
        self.data_dev = None
        self.file_dev = None


    def rect(self, r, theta):
        x = r * math.sin(theta) #math.cos(math.radians(theta))
        y = r * math.cos(theta) #math.sin(math.radians(theta))
        return x,y

    def parse_header(self, dev):
        syncPattern = b'\x02\x01\x04\x03\x06\x05\x08\x07'
        # Wait for the serial port to receive the sync pattern
        dataBuffer = bytearray(syncPattern)
        while True:
            currentData = dev.read(1)
            # Remove last e
            dataBuffer.pop(0)
            dataBuffer += currentData
            if self.bin_file:
                self.bin_file.write(currentData)
            # Break the loop if the data is the same as the syncPattern
            if bytes(dataBuffer) == syncPattern:
                break

        # Initialize the data dictionary
        dataDict = {
            'tlvHeaders': list()
        }
        # Decode the header, iterate over every type as defined in frameHeaderStructType and decode it to the corresponding data type
        # Same process is repeated for every other type of data (tlvHeader, pointCloud and target)
        for dataPoint in frameHeaderStructType.items():
            currentData = dev.read(dataPoint[1].length)
            dataDict[dataPoint[0]] = dataPoint[1].fromBytes(currentData, 0)
            #print("data:{}, dataDict:{}".format(currentData, dataDict))
            if self.bin_file:
                self.bin_file.write(currentData)

        # length of only the data in bytes, so just the packet without the header, used for reading the whole packet in one go
        dataLength = dataDict['packetLength'] - frameHeaderLengthInBytes
        return dataDict, dataLength

    def get_data(self):
        if self.active == 1:
            dev = self.data_dev
            if self.device == "file":
                dev = self.file_dev

            dataLength = 0
            while dataLength == 0:
                dataDict, dataLength = self.parse_header(dev)
            # Read the remaining data
            data = dev.read(dataLength)
            targetsData = list()
            pointsData = list()
            #print("data:{}".format(data))
            # offset to keep track of which bytes to read next
            offset: int = 0
            for nTlv in range(0, dataDict['numTLVs']):
                tlvData = {}
                # Decode TLV Header
                for dataPoint in tlvHeaderStruct.items():
                    tlvData[dataPoint[0]] = dataPoint[1].fromBytes(data, offset)
                    offset += dataPoint[1].length

                # Length of the TLV Data in bytes, this is the total length as specified in the tlvHeader minus the tlvHeader
                valueLength = tlvData['length'] - tlvHeaderLengthInBytes
                if valueLength + offset > dataLength:
                    print("parse error!!! type:{}, len:{}, offset:{}, datalen:{}".format(tlvData['type'], tlvData['length'], offset, dataLength))
                    return

                print("type:{}, len:{}, offset:{}, datalen:{}".format(tlvData['type'], tlvData['length'], offset, dataLength))
                # Decode TLV Data
                if tlvData['type'] == 6: # type 6: Point Cloud Data
                    # Number of points, so the data length divided by the length in bytes of one point cloud data point
                    self.numPoints = math.floor(valueLength/pointLengthInBytes)
                    points = list()
                    for i in range(0, self.numPoints):
                        point = {}
                        for pointData in pointStruct.items():
                            point[pointData[0]] = pointData[1].fromBytes(data, offset)
                            offset += pointData[1].length
                        point["posX"], point["posY"] = self.rect(point["range"], point["angle"])
                        print(point)
                        points.append(point)
                        pointsData.append(point)

                elif tlvData['type'] == 7: # type 7: Target List
                    self.numTargets = math.floor(valueLength/targetLengthInBytes)
                    # Print number of targets to console
                    print('numTargets: ' + str(self.numTargets))
                    targets = list()
                    # Iterate over all targets
                    for i in range(0, self.numTargets):
                        target = {}
                        # Decode Target data
                        for targetData in targetStruct.items():
                            target[targetData[0]] = targetData[1].fromBytes(data, offset)
                            offset += targetData[1].length
                            #print("targetData:{}, len:{}".format(targetData, targetData[1].length))
                        #print("x:{}, y:{}".format(target["posX"], target["posY"]))

                        targets.append(target)
                    tlvData['targets'] = targets
                    targetsData.append(targets)

                # type 8 contains some additional data regarding targets, seems to be something to do with stance etc, but shouldn't be necessary
                elif tlvData['type'] == 8: # type 8: additional target data
                    offset += valueLength
                else:
                    return
                dataDict['tlvHeaders'].append(tlvData)
            if False:
                if len(pointsData) > 0:
                    df = pd.DataFrame(pointsData)
                    ranges = df['range']
                    azimuths = df['angle']
                    dopplers = df['doppler']
                    snrs = df['snr']
            else:
                #if self.numTargets > 0:
                if len(pointsData) > 0:
                    # We got a target
                    # The data is in a fixed position based on the target id, this makes it easier to track individual targets by a mere human
                    point_cloud = []
                    points = []
                    people = []
                    cluster_data = []
                    for t in pointsData:
                        point = {'x':round(t['posX'],3),'y':round(t['posY'],3)}
                        point_cloud.append(point)
                        #t.pop('range', None)
                        #t.pop('angle', None)
                        #t.pop('snr', None)
                        cluster_data.append(t)
                    for targets in targetsData:
                        try:
                            for t in targets:
                                target = {'x':round(t['posX'],2),'y':round(t['posY'],2), 'markerSize':20, 'color':'Crimson'}
                                people.append(target)
                        except:
                            print("targets draw error")

                    if False:
                        center_points = self.cluster(cluster_data)
                        for p in center_points:
                            point = {'x':round(p[1],2),'y':round(p[2],2), 'markerSize':10, 'color':'Blue'}
                            points.append(point)
                    else:
                        if len(pointsData) > 0:
                            df = pd.DataFrame(pointsData)
                            ranges = df['range']
                            azimuths = df['angle']
                            dopplers = df['doppler']
                            snrs = df['snr']
                            if ranges is not None:
                                # --- put into EKF
                                self.tracker.update_point_cloud(ranges, azimuths, dopplers, snrs)
                                targetDescr, tNum = self.tracker.step()
                                for t, tid in zip(targetDescr, range(int(tNum[0]))):
                                    x_vel = t.S[2]
                                    y_vel = t.S[3]
                                    angle = arctan2_circular(y_vel, x_vel)
                                    angle = float(angle)
                                    #angle = math.atan2(y_vel, x_vel)

                                    point = {'x':round(t.S[0].item(),2),'y':round(t.S[1].item(),2), 'angle':angle, 'markerSize':20, 'color':'Yellow'}
                                    points.append(point)

                                    # Caliculated the target position respective to the origin of venue.
                                    x_radar, y_radar = point['x'], point['y']
                                    x_venue, y_venue = rect_detection.get_position(x_radar, y_radar)

                                    print("x_radar, y_radar = {}, {}".format(x_radar, y_radar))
                                    print("x_venue, y_venue = {}, {}".format(x_venue, y_venue))
                                    print("angle = {}".format(angle))


                    wsData = {"params":{"data":[point_cloud, people, points]}}
                    #logging.debug(wsData)
                    self.ws.send_updates(wsData)

                    self.counter += 1



            # Write the raw binary data to file if --rawdata PATH is specified
            if self.bin_file:
                self.bin_file.write(data)
            self.data_dev.flushInput()
            self.data_dev.flushOutput()

    def cluster(self, points):
        data = np.array([list(i.values()) for i in points])
        from sklearn.cluster import DBSCAN
        db_default = DBSCAN(eps=0.6, min_samples=4).fit(data)
        labels = db_default.labels_
        centers = []
        for i in range(max(labels) + 1):
            clust = data[labels == i]
            if len(clust) > 0:
                center = np.mean(clust, axis=0)
                centers.append(center.tolist())
        print(centers)
        return centers

    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close mmwave device.')
                self.close()
            elif params["command"] == "open":
                if params["property"] == "mmwave_device":
                    self.open(id=id)
                if params["property"] == "mmwave_file_device":
                    self.open(device="file", data_fn=FN, id=id)
            elif params["command"] == "close":
                if params["property"] == "mmwave_device":
                    self.close()
        elif method == "get":
            pass
        elif method == "set":
            pass
        elif method == "feed_data":
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
            if params["action"] == "start":
                if params["property"] == "mmwave":
                    self.start()
                    logging.debug("start mmwave data")
                    self.ws.send_updates(ws_data)
            elif params["action"] == "stop":
                if params["property"] == "mmwave":
                    self.stop()
                    logging.debug("stop mmwave data")
                    self.ws.send_updates(ws_data)
