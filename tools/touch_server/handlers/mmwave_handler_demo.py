# -*- coding: utf-8 -*-
import logging
import serial
import tornado.ioloop
import sys
import struct
import time

configFileName = 'profile.cfg'
CFG_PORT="COM7"
DATA_PORT="COM8"
BAUDRATE=115200
DATA_BAUDRATE=921600
MAGICWORD = '#surf'
DATA_EOF = '\r\n\r\n'
DATALEN = 50

class mmwave_handler():
    def __init__(self, WsHandler, debug=logging.ERROR):
        self.cfg_dev = None
        self.data_dev = None
        self.data_buf = bytearray()
        self.ws = WsHandler
        self.counter = 0
        self.active = 0
        self.scheduler = None
        
    def open(self, cfg_port=CFG_PORT, data_port=DATA_PORT, baudrate=BAUDRATE, data_baudrate=DATA_BAUDRATE, id=0):
        ret = True
        self.cfg_dev = serial.Serial(port=cfg_port, baudrate=baudrate)
        self.cfg_dev.flushInput()
        self.cfg_dev.flushOutput()
        self.data_dev = serial.Serial(port=data_port, baudrate=data_baudrate)
        self.data_dev.flushInput()
        self.data_dev.flushOutput()
        # Read the configuration file and send it to the board
        config = [line.rstrip('\r\n') for line in open(configFileName)]
        for i in config:
            self.cfg_dev.write((i+'\n').encode())
            print(i)
            time.sleep(0.01)
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)


    def start(self):
        if self.data_dev is not None:
            self.active = 1
            self.scheduler = tornado.ioloop.PeriodicCallback(self.get_data, 50)
            self.scheduler.start()

    def stop(self):
        self.active = 0
        if self.scheduler is not None:
            self.scheduler.stop()
            self.scheduler = None
        
    def close(self):
        try:
            self.cfg_dev.close()
            self.data_dev.close()
        except:
            logging.debug("exit")

    def get_data(self):
        magicOK = 0
        if self.active == 1:
            bytesToRead = self.data_dev.inWaiting()
            tmp = self.data_dev.read(bytesToRead)
            logging.debug(tmp)
            self.data_buf = self.data_buf + tmp

            # Check that the buffer has some self.data_buf
            if len(self.data_buf) > 36:

                magic = b'\x02\x01\x04\x03\x06\x05\x08\x07'
                offset = self.data_buf.find(magic)
                self.data_buf = self.data_buf[offset:]
                magicOK = 1

            # If magicOK is equal to 1 then process the message
            if magicOK:
                headerLength = 36
                magic, version, length, platform, frameNum, cpuCycles, numObj, numTLVs = struct.unpack('Q7I', self.data_buf[:headerLength])
                if length > len(self.data_buf):
                    return
                print("magic:\t%x "%(magic))
                print("Packet ID:\t%d "%(frameNum))
                print("Version:\t%x "%(version))
                print("TLV:\t\t%d "%(numTLVs))
                print("Detect Obj:\t%d "%(numObj))
                print("Platform:\t%X "%(platform))
                if version > 0x01000005:
                    subFrameNum = struct.unpack('I', self.data_buf[36:40])[0]
                    headerLength = 40
                    print("Subframe:\t%d "%(subFrameNum))
                pendingBytes = length - headerLength
                self.data_buf = self.data_buf[headerLength:]
                for i in range(numTLVs):
                    tlvType, tlvLength = self.tlvHeaderDecode(self.data_buf[:8])
                    self.data_buf = self.data_buf[8:]
                    if (tlvType == 1):
                        self.parseDetectedObjects(self.data_buf, tlvLength)
                    elif (tlvType == 2):
                        self.parseRangeProfile(self.data_buf, tlvLength)
                    elif (tlvType == 6):
                        self.parseStats(self.data_buf, tlvLength)
                    else:
                        print("Unidentified tlv type %d"%(tlvType))
                    self.data_buf = self.data_buf[tlvLength:]
                    pendingBytes -= (8+tlvLength)
                self.data_buf = self.data_buf[pendingBytes:]



    def tlvHeaderDecode(self, data):
        tlvType, tlvLength = struct.unpack('2I', data)
        return tlvType, tlvLength

    def parseDetectedObjects(self, data, tlvLength):
        numDetectedObj, xyzQFormat = struct.unpack('2H', data[:4])
        print("\tDetect Obj:\t%d "%(numDetectedObj))
        points = []
        for i in range(numDetectedObj):
            print("\tObjId:\t%d "%(i))
            rangeIdx, dopplerIdx, peakVal, x, y, z = struct.unpack('HhH3h', data[4+12*i:4+12*i+12])
            point = {'x':(x*1.0/(1 << xyzQFormat)),'y':(y*1.0/(1 << xyzQFormat))}
            points.append(point)
            print("\t\tDopplerIdx:\t%d "%(dopplerIdx))
            print("\t\tRangeIdx:\t%d "%(rangeIdx))
            print("\t\tPeakVal:\t%d "%(peakVal))
            print("\t\tX:\t\t%07.3f "%(x*1.0/(1 << xyzQFormat)))
            print("\t\tY:\t\t%07.3f "%(y*1.0/(1 << xyzQFormat)))
            print("\t\tZ:\t\t%07.3f "%(z*1.0/(1 << xyzQFormat)))
        self.counter += 1
        wsData = {"params":{"data":points}}
        #logging.debug(wsData)
        self.ws.send_updates(wsData)


    def parseRangeProfile(self, data, tlvLength):
        for i in range(256):
            rangeProfile = struct.unpack('H', data[2*i:2*i+2])
            #print("\tRangeProf[%d]:\t%07.3f "%(i, rangeProfile[0] * 1.0 * 6 / 8  / (1 << 8)))
        print("\tTLVType:\t%d "%(2))

    def parseStats(self, data, tlvLength):
        interProcess, transmitOut, frameMargin, chirpMargin, activeCPULoad, interCPULoad = struct.unpack('6I', data[:24])
        return
        print("\tOutputMsgStats:\t%d "%(6))
        print("\t\tChirpMargin:\t%d "%(chirpMargin))
        print("\t\tFrameMargin:\t%d "%(frameMargin))
        print("\t\tInterCPULoad:\t%d "%(interCPULoad))
        print("\t\tActiveCPULoad:\t%d "%(activeCPULoad))
        print("\t\tTransmitOut:\t%d "%(transmitOut))
        print("\t\tInterprocess:\t%d "%(interProcess))


    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close mmwave device.')
                self.close()
            elif params["command"] == "open":
                if params["property"] == "mmwave_device":
                    self.open(id=id)
            elif params["command"] == "close":
                if params["property"] == "mmwave_device":
                    self.close()
        elif method == "get":
            pass
        elif method == "set":
            pass
        elif method == "feed_data":
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
            if params["action"] == "start":
                if params["property"] == "mmwave":
                    self.start()
                    logging.debug("start mmwave data")
                    self.ws.send_updates(ws_data)
            elif params["action"] == "stop":
                if params["property"] == "mmwave":
                    self.stop()
                    logging.debug("stop mmwave data")
                    self.ws.send_updates(ws_data)

