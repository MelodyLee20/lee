# -*- coding: utf-8 -*-
import logging
import sys 
sys.path.append("../../../touch")
import touch.fwio as fwio
from touch import hidparser
from touch.hidparser.Device import Collection
from touch.hidparser.UsagePages import GenericDesktop, Button
from touch.hidparser.UsagePage import UsagePage, UsageType, Usage
from touch.hidparser.enums import CollectionType, ReportType, ReportFlags, UnitSystem
import platform
import tornado.ioloop


class touch_handler():
    def __init__(self, WsHandler, io_loop, debug=logging.ERROR):
        self.ws = WsHandler
        self.counter = 0
        self.active = 0
        self.touch_dev = fwio.core(debug)
        self.io_loop = io_loop
        self.desc_parser = None
        self.scheduler = None
        self.feed_data_property = "" 
 
    def report_descriptor_callback(self, status, id, msg, data=[]):
        if status == 0:
            status_str = "successful"
            desc_bin = bytes(data)
            logging.debug("get_report_descriptor {}".format(desc_bin))
            logging.debug("report read finish")
            self.desc_parser = hidparser.parse(desc_bin)
            logging.debug(self.desc_parser)
        else:
            status_str = "failed"
            logging.debug("failed to read report descriptor")

        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'", "data": "'+str(data)+'"}}'

    def callback(self, status, id, msg, data=[]):
        if status == 0:
            status_str = "successful"
        else:
            status_str = "failed"

        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'", "data": "'+str(data)+'"}}'
        print("websocket response ws_data:{}".format(ws_data))
        self.ws.send_updates(ws_data)

    def cdc_callback(self, status, id, msg, data=[]):
        print([id, status, msg, data])
        if status == 0:
            status_str = "successful"
        else:
            status_str = "failed"

        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'", "data": "'+str(data)+'"}}'
        self.ws.send_updates(ws_data)
        self.close()


    def open(self, id=0, path=None):
        ret = self.touch_dev.search()
        if ret == True:
            logging.debug("Open Device")
            ret = self.touch_dev.open(enable_read_thread=False, io_loop=self.io_loop, cb=self.callback)
            if ret == True:
                if platform.system() == "Windows":
                    # Windows
                    self.touch_dev.execute(id, "get", property="report_descriptor",callback=self.report_descriptor_callback)
                elif platform.system() == "Linux":
                    with open(self.touch_dev.desc.rd_path, "rb") as f:
                        desc_bin = f.read()
                        logging.debug("report read finish")
                        self.desc_parser = hidparser.parse(desc_bin)
                        logging.debug(self.desc_parser)
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def close(self):
        self.touch_dev.close()
    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close touch device.')
                self.touch_dev.close()
            elif params["command"] == "hello":
                logging.debug('new client connected!!')
            elif params["command"] == "open":
                if params["property"] == "touch_device":
                    self.open(id)
            elif params["command"] == "close":
                if params["property"] == "touch_device":
                    self.close()
            elif params["command"] == "reset":
                if params["property"] == "touch_device":
                    self.touch_dev.execute(id, method, property=params["property"])

        elif method == "get":
            if params["property"] == "inv_x" or params["property"] == "inv_y" or params["property"] == "swap_xy" :
                self.touch_dev.execute(id, method, property=params["property"])

        elif method == "set":
            if params["property"] == "inv_x" or params["property"] == "inv_y" or params["property"] == "swap_xy" :
                self.touch_dev.execute(id, method, property=params["property"], value=params["value"])
            elif params["property"] == "normal_mode":
                self.touch_dev.execute(id, method, property=params["property"])
            elif params["property"] == "command_mode":
                if params["action"] == "start":
                    self.touch_dev.execute(id, method, property=params["property"], action=params["action"])
                if params["action"] == "stop":
                    self.touch_dev.execute(id, method, property=params["property"], action=params["action"])
                    self.touch_dev.execute(id, method, property=params["property"])
            elif params["property"] == "cdc_mode":
                    self.touch_dev.execute(id, method, property=params["property"], callback=self.cdc_callback)

        elif method == "feed_data":
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
            if params["action"] == "start":
                self.feed_data_property = params["property"]
                def data_cb():
                    ws_data = ""
                    buf = self.touch_dev.get_hidraw()
                    if buf is not None:
                        logging.debug(feed_data_prperty)
                        data = []
                        if self.feed_data_prperty == "hidraw_parsed":
                            if self.desc_parser is not None:
                                data = hidparser.parse_data(buf, self.desc_parser) 
                        elif self.feed_data_prperty == "hidraw":
                            data = list(buf)
                        ws_data = '{"params": {"data": "'+str(data)+'"}}'
                    if len(ws_data) > 0:
                        self.ws.send_updates(ws_data)
                if self.feed_data_property == "hidraw" or self.feed_data_property == "hidraw_parsed":
                    logging.debug("start feed data")
                    self.scheduler = tornado.ioloop.PeriodicCallback(data_cb, 10)
                    self.scheduler.start()
                    self.ws.send_updates(ws_data)
            elif params["action"] == "stop":
                if self.feed_data_property == "hidraw" or self.feed_data_property == "hidraw_parsed":
                    logging.debug("stop feed data")
                    if self.scheduler is not None:
                        self.scheduler.stop()
                        self.scheduler = None
                    self.ws.send_updates(ws_data)
                    self.feed_data_property = "" 

