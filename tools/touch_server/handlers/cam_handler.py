# -*- coding: utf-8 -*-
import sys
import os
import logging
import serial
import tornado.ioloop
import numpy as np

from touch.utils.opencv import Webcam
#from .libs.target_tracker import TargetTracker
from touch.trackers import TargetTracker

class cam_handler():
    def __init__(self, WsHandler, debug=logging.ERROR):
        self.ws = WsHandler
        self.frame_width = 600
        self.frame_height = 300
        self.webcam = None
        self.tracker = None
        self.device = 1 # Device ID
        #self.device = 0 # Device ID
        self.active = 0
        self.scheduler = None


    def cam_scheduler(self):
        if self.active == 1:
            frame = self.webcam.read() # Read the frame

            self.tracker.update(frame) # Update the tracker
            position = self.tracker.get_position_norm()
            print("position is {}".format(position))

            # Obtain the normalized x
            if position is not None:
                x = position[0]
            else:
                x = -1.0

            # Data transfer
            wsData = {"params":{"data":[x]}}

            logging.debug(wsData)
            self.ws.send_updates(wsData)

    def open(self, id=0):
        if self.active == 0:
            # Wencam
            self.webcam = Webcam()
            self.webcam.open(self.device, width=self.frame_width, height=self.frame_height)
            # Tracker
            self.tracker = TargetTracker(frame_width=self.frame_width, frame_height=self.frame_height)

            self.active = 1
            self.scheduler = tornado.ioloop.PeriodicCallback(self.cam_scheduler, 150)
            self.scheduler.start()
        ret = True
        ws_data = ""
        if ret == True:
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        else:
            status_str = "failed"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def close(self, id):
        self.active = 0
        if self.scheduler is not None:
            self.scheduler.stop()
            self.scheduler = None
        self.webcam.release()
        status_str = "successful"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close cam device.')
                self.close()
            elif params["command"] == "open":
                if params["property"] == "cam_device":
                    self.open(id=id)
            elif params["command"] == "close":
                if params["property"] == "cam_device":
                    self.close(id=id)
        elif method == "get":
            pass
        elif method == "set":
            pass
