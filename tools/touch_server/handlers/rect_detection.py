import numpy as np


class RectDetection():
    # Detection of a rectanuglr venue.

    def __init__(self, range_max=None, angle_max=None, length=None, width=None):
        '''
        range_max: float
            The maximum range of detection.
        angle_max: float
            The maximum angle of detection which is from zero to a specific angle in radain.
        length: float
            The length of venue.
        width: float
            The width of venue.
        '''

        self.range_max = 1.0*range_max
        self.angle_max = 1.0*angle_max

        self.length = 1.0*length
        self.width = 1.0*width

        # The distance betwee venue and detector.
        # d0 = L / (2*tan(angle_max))
        self.d0 = 0.5*self.length / np.tan(self.angle_max)

        # The origin relative to the detector.
        # x0 = -L/2 and y0 = d0
        self.x0 = -0.5*length
        self.y0 = self.d0

    def get_position(self, x_in, y_in):
        # Get the position relative to the venue.

        x = x_in - self.x0
        y = y_in - self.y0

        return (x, y)



