# -*- coding: utf-8 -*-
import logging
import sys
import platform
from tornado.ioloop import IOLoop
import socket
from tornado.platform.auto import set_close_exec
import os
import csv
import time
import errno
import math

def bind_sockets(port, address=None, family=socket.AF_UNSPEC, backlog=25):
    sockets = []
    if address == "":
        address = None
    flags = socket.AI_PASSIVE
    if hasattr(socket, "AI_ADDRCONFIG"):
        flags |= socket.AI_ADDRCONFIG
    for res in set(socket.getaddrinfo(address, port, family, socket.SOCK_DGRAM,
                                  0, flags)):
        af, socktype, proto, canonname, sockaddr = res
        sock = socket.socket(af, socktype, proto)
        set_close_exec(sock.fileno())
        if os.name != 'nt':
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        if af == socket.AF_INET6:
            if hasattr(socket, "IPPROTO_IPV6"):
                sock.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_V6ONLY, 1)
        sock.setblocking(0)
        sock.bind(sockaddr)
        sockets.append(sock)
    return sockets

if hasattr(socket, 'AF_UNIX'):
    def bind_unix_socket(file, mode=0o600, backlog=128):
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
        set_close_exec(sock.fileno())
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.setblocking(0)
        try:
            st = os.stat(file)
        except (OSError, err):
            if err.errno != errno.ENOENT:
                raise
        else:
            if stat.S_ISSOCK(st.st_mode):
                os.remove(file)
            else:
                raise ValueError("File %s exists and is not a socket", file)
        sock.bind(file)
        os.chmod(file, mode)
        sock.listen(backlog)
        return sock

def add_accept_handler(sock, callback, io_loop=None):
    if io_loop is None:
        io_loop = IOLoop.instance()

    def accept_handler(fd, events):
        while True:
            try:
                data, address = sock.recvfrom(5000)
            # except (socket.error, e):
            except Exception as e:
                if e.args[0] in (errno.EWOULDBLOCK, errno.EAGAIN):
                    return
                raise
            callback(data, address)
    io_loop.add_handler(sock.fileno(), accept_handler, IOLoop.READ)


class UDPServer(object):
    def __init__(self, io_loop, ws, time_interval=0.1, distance_threshole=20):
        self.io_loop = io_loop
        self._sockets = {}  # fd -> socket object
        self._pending_sockets = []
        self._started = False
        self.point_data = []
        self.ws = ws
        self.time_interval = time_interval
        self.last_receive_time = time.time()
        self.distance_threshole = distance_threshole
        self.hwnd = None

    def add_sockets(self, sockets):
        if self.io_loop is None:
            self.io_loop = IOLoop.instance()

        for sock in sockets:
            self._sockets[sock.fileno()] = sock
            add_accept_handler(sock, self._on_recive,
                               io_loop=self.io_loop)

    def bind(self, port, address=None, family=socket.AF_UNSPEC, backlog=25):
        sockets = bind_sockets(port, address=address, family=family,
                               backlog=backlog)
        if self._started:
            self.add_sockets(sockets)
        else:
            self._pending_sockets.extend(sockets)

    def start(self, num_processes=1):
        assert not self._started
        self._started = True
        if num_processes != 1:
            process.fork_processes(num_processes)
        sockets = self._pending_sockets
        self._pending_sockets = []
        self.add_sockets(sockets)

    def stop(self):
        for fd, sock in self._sockets.items():
            self.io_loop.remove_handler(fd)
            sock.close()

    def _on_recive(self, data, address):
        data_str = str(data, encoding="utf-8")
        point_data = []
        if len(data_str) > 4 and (time.time() - self.last_receive_time) > self.time_interval:
            points = list(csv.reader([data_str], delimiter='|'))[0]
            for point in points:
                tmp = list(csv.reader([point]))[0]
                if len(tmp) == 4:
                    point_data.append({'type':int(tmp[1]),'x':int(tmp[2]),'y':int(tmp[3])})
            point_data = self.point_filter(point_data)
            wsData = {"params":{"data":[[],[], point_data]}}
            logging.debug(wsData)
            self.ws.send_updates(wsData)
            self.last_receive_time = time.time()
    def point_filter(self, point_data):
        tmp = []
        length = len(point_data)
        for i in range(length):
            if point_data[i]['type'] == 0:
                continue
            for j in range(length-i-1):
                k = j + i + 1
                if point_data[k]['type'] == 0:
                    continue
                if self.calculate_distance(point_data[i], point_data[k]) < self.distance_threshole:
                    point_data[k]['type'] = 0
        for i in range(length):
            if point_data[i]['type'] > 0:
                tmp.append(point_data[i])
                print(point_data[i])
        return tmp

    def calculate_distance(self, p1, p2):
         x1 = p1['x']
         x2 = p2['x']
         y1 = p1['y']
         y2 = p2['y']
         dist = math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
         return dist


class radar_handler():
    def __init__(self, WsHandler, io_loop, debug=logging.ERROR):
        self.debug = debug
        self.ws = WsHandler
        self.counter = 0
        self.active = 0
        self.io_loop = io_loop
        self.scheduler = None
        self.feed_data_property = ""
        self.udp_server = None

    def create_windows(self):
        import win32con
        import win32api
        import win32gui
        hinst = win32api.GetModuleHandle(None)
        wndclass = win32gui.WNDCLASS()
        wndclass.hInstance = hinst
        wndclass.lpszClassName = "PyWindow"
        messageMap = {
        }
        wndclass.lpfnWndProc = messageMap
        #self.ScanDevices()
        try:
            myWindowClass = win32gui.RegisterClass(wndclass)
            hwnd = win32gui.CreateWindowEx(win32con.WS_EX_LEFT, wndclass.lpszClassName, "kludpwin", 0, 0, 0,
                                           win32con.CW_USEDEFAULT, win32con.CW_USEDEFAULT, win32con.NULL, win32con.NULL, hinst, None)
        except Exception as e:
            print("Exception: %s" % str(e))
        return hwnd

    def callback(self, status, id, msg, data=[]):
        if status == 0:
            status_str = "successful"
        else:
            status_str = "failed"

        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'", "data": "'+str(data)+'"}}'
        print("websocket response ws_data:{}".format(ws_data))
        self.ws.send_updates(ws_data)

    def open(self, id=0, path=None):
        if self.udp_server == None:
            self.udp_server = UDPServer(self.io_loop, self.ws)
            self.udp_server.bind(9999)
            logging.debug("start udp server")
            self.hwnd = self.create_windows()
        ws_data = ""
        status_str = "successful"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
        self.ws.send_updates(ws_data)

    def close(self):
        if self.udp_server is not None:
            self.udp_server.stop()

    def handler(self, data):
        id = data['id']
        method = data['method']
        params = data['params']
        if method == "command":
            if params["command"] == "requestFinalization":
                logging.debug('close radar server.')
                self.close()
            elif params["command"] == "hello":
                logging.debug('new client connected!!')
            elif params["command"] == "open":
                if params["property"] == "radar_device":
                    self.open(id)
            elif params["command"] == "close":
                if params["property"] == "radar_device":
                    self.close()

        elif method == "get":
            pass

        elif method == "set":
            pass

        elif method == "feed_data":
            status_str = "successful"
            ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'"}}'
            if params["action"] == "start":
                if params["property"] == "radar":
                    self.udp_server.start()
                    logging.debug("start radar data")
                    self.ws.send_updates(ws_data)
            elif params["action"] == "stop":
                if params["property"] == "radar":
                    self.udp_server.stop()
                    logging.debug("stop radar data")
                    self.ws.send_updates(ws_data)
