# -*- coding: utf-8 -*-
import sys
import os
import time
import json
import logging
sys.path.append("../../../touch")
import touch.web as web
import platform
import optparse
import tornado.ioloop
from handlers.touch_handler import touch_handler
from handlers.uart_handler import uart_handler
from handlers.cam_handler import cam_handler
#from handlers.mmwave_handler import mmwave_handler
#from handlers.radar_handler import radar_handler
#from handlers.mock_handler import mock_handler

# Version
__version__ = '0.1.0'

def parse_arguments():
    parser = optparse.OptionParser()
    parser.add_option('-i', '--ip', dest='ip', default="127.0.0.1",
                    help=('the IP address of server'))
    parser.add_option('-p', '--port', dest='port', default=8050,
                    help=('the port of server'))
    (options, args) = parser.parse_args()
    return options, args

class touch_server():
    def __init__(self, ip="127.0.0.1", port=8050, debug=logging.ERROR):
        self.debug = debug 
        logging.basicConfig(level=debug, format='%(asctime)s -(%(filename)s)%(levelname)s : %(message)s')
        self.io_loop = tornado.ioloop.IOLoop()
        self.server = web.web_server(proprietary_cmd_handler=self.handler, debug=self.debug) 
        
        # setup handler
        self.handler_register = []
        self.uart_handler =  uart_handler(self.server.ws_handler, self.debug)
        self.handler_register.append(self.uart_handler) 
        self.touch_handler = touch_handler(self.server.ws_handler, self.io_loop, self.debug)
        self.handler_register.append(self.touch_handler) 
        #self.mmwave_handler = mmwave_handler(self.server.ws_handler)
        #self.handler_register.append(self.mmwave_handler) 
        #self.radar_handler = radar_handler(self.server.ws_handler, self.io_loop, self.debug)
        #self.handler_register.append(self.radar_handler) 
        self.cam_handler = cam_handler(self.server.ws_handler)
        self.handler_register.append(self.cam_handler) 
        #self.mock_handler = mock_handler(self.server.ws_handler)
        #self.handler_register.append(self.mock_handler) 

        self.server.start(ip=ip, port=port, io_loop=self.io_loop)

    def callback(self, status, id, msg, data=[]):
        print([id, status, msg, data])
        if status == 0:
            status_str = "successful"
        else:
            status_str = "failed"
        ws_data = '{"id": '+str(id)+', "result": {"status": "'+status_str+'", "data": "'+str(data)+'"}}'
        self.server.ws_handler.send_updates(ws_data)

    def handler(self, data):
        print("chk data:{}".format(data))
        if 'method' in data and 'params' in data and 'id' in data:
            method = data['method']
            params = data['params']
            for handler in self.handler_register:
                handler.handler(data)
            if method == "command":
                if params["command"] == "close":
                    if params["property"] == "server":
                        self.close()
                        return
        else:
            status_str = "failed"
            ws_data = '{"id": 0, "result": {"status": "'+status_str+'", "data": ""}}'
            self.server.ws_handler.send_updates(ws_data)
            print("command error, failed!!")

    def close(self):
        self.server.stop()
        os._exit(1)

if __name__ == '__main__':
    options, args = parse_arguments()
    ip = options.ip 
    port = int(options.port)
    debug = logging.DEBUG 
    server = touch_server(ip, port, debug)

