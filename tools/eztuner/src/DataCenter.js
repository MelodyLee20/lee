
class DataCenter {
  // Data Center which takes care the data exchange via websocket.

  constructor(wsUri) {
    this.data = [];
    this.cmdId = 0;

    // Pen
    this.widthId = null
    this.colorhId = null
    this.typeId = null

    // Websocket
    this.wsUri = wsUri;
    this.websocket = null;
    this.connected = false;
    this.numReconnctMax = 3;
    this.numReconnct = 0;

    // Handler for the situation when data arrives
    this.updateAttributeHandler = null
    this.updateResultStatusHandler = null

  }

  connect() {
    this.websocket = new WebSocket(this.wsUri);

    // callbacks
    this.websocket.onopen = (e) => {
      console.log('Connected');
      this.connected = true;

      this.openDevice()
    };

    this.websocket.onclose = (e) => {
      console.log('onclose: ' + e.data)

      console.log('Disconnected');
      this.connected = false;

      setTimeout( () => this.reconnect(), 3000);

    };

    this.websocket.onmessage = (e) => {
      console.log('onmessage: ' + e.data)

      const data = JSON.parse(e.data);
      const status = data.result.status

      this.updateResultStatusHandler(status)
      this.updateAttributeFromData(data)

    };

    this.websocket.onerror = (e) => {
       console.log('onerror: ' + e.data);
    };
  }

  disconnect() {
    console.log('user Disconnected');
    this.websocket.close();
    this.connected = false;
  }

  reconnect() {

    if (this.numReconnct <= this.numReconnctMax) {
      this.numReconnct ++
      console.log('Reconnecting ...')
      this.connect()
    } else {
      console.log('Stop reconnecting!')
    }
  }

  send(data) {
    this.cmdId ++
    data.id = this.cmdId

    const msg = JSON.stringify(data)
    console.log('send: ' + msg)

    this.websocket.send(msg);
  }

  openDevice() {
    var data = {"id": null, "method": "command","params": {"command":"open"}}
    this.send(data)
  }

  closeDevice() {
    var data = {"id": null, "method": "command","params": {"command":"close"}}
    this.send(data)
  }


  setPenWidth(val) {
    var data = {"id": null, "method": "set","params": {"property":"usi_width", "value": val}}
    this.send(data)
  }

  setPenColor(val) {
    var data = {"id": null, "method": "set","params": {"property":"usi_color", "value": val}}
    this.send(data)
  }

  setPenType(val) {
    var data = {"id": null, "method": "set","params": {"property":"usi_type", "value": val}}
    this.send(data)
  }

  requestPenWidth() {
    var data = {"id": null, "method": "get","params": {"property":"usi_width", "value": 0}}
    this.send(data)
    this.widthId = this.cmdId
  }

  requestPenColor() {
    var data = {"id": null, "method": "get","params": {"property":"usi_color", "value": 0}}
    this.send(data)
    this.colorId = this.cmdId
  }

  requestPenType() {
    var data = {"id": null, "method": "get","params": {"property":"usi_type", "value": 0}}
    this.send(data)
    this.typeId = this.cmdId
  }

  setUpdateAttributeHandler(handler) {
    this.updateAttributeHandler = handler
  }

  setUpdateResultStatusHandler(handler) {
    this.updateResultStatusHandler = handler
  }

  updateAttributeFromData(data) {

    var target = null
    var value = null

    switch (data.id) {

      case this.widthId:
        console.log(data.result.data)
        value = this.parse_data(data.result.data)
        this.updateAttributeHandler("width", value)
        this.widthId = null
        break

      case this.colorId:
        value = this.parse_data(data.result.data)
        this.updateAttributeHandler("color", value)
        this.colorId = null
        break

      case this.typeId:
        value = this.parse_data(data.result.data)
        this.updateAttributeHandler("type", value)
        this.typeId = null
        break
    }
  }

  parse_data(d) {
    var list = JSON.parse(d)
    const value = parseInt(list[1])

    return value
  }

}

export {DataCenter}
