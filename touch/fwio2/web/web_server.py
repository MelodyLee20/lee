# -*- coding: utf-8 -*-
import os
import sys
import time
import tornado.web
import tornado.websocket
import tornado.httpserver
import tornado.ioloop
import json
import platform
import webbrowser
import logging
import threading


NET_PORT = 8050

class ws_handler(tornado.websocket.WebSocketHandler):
    waiters = set()
    counter = 0
    begin = 0
    touch_util = None
    hid_util = None
    proprietary_cmd_handler = None
    common_cmd_handler = None
    command_cb = None

    def set_handler(self, common_cmd_handler=None, proprietary_cmd_handler=None):
        ws_handler.proprietary_cmd_handler = proprietary_cmd_handler
        ws_handler.common_cmd_handler = common_cmd_handler
        logging.debug("set handler proprietary")
        logging.debug(ws_handler.proprietary_cmd_handler)

    def check_origin(self, origin):
        return True

    def open(self):
        ws_handler.waiters.add(self)
        logging.debug("client IP:" + self.request.remote_ip)

    def on_message(self, message):
        logging.debug(message)
        data = json.loads(message)
        logging.debug("cmd:{}".format(data))
        if ws_handler.proprietary_cmd_handler is not None:
            logging.debug("check proprietary")
            ws_handler.proprietary_cmd_handler(data)
        if ws_handler.common_cmd_handler is not None:
            logging.debug("check common")
            ws_handler.common_cmd_handler(data)
        try:
            if data['cmd'] == "requestFinalization":
                # Finalize the run.
                logging.debug('Perform finalization.')
                tornado.ioloop.IOLoop().current().call_later(1, lambda: self.stop()) 
        except:
            method = data['method']
            params = data['params']
            if method == "command":
                if params["command"] == "requestFinalization":
                    # Finalize the run.
                    logging.debug('Perform finalization.')
                    tornado.ioloop.IOLoop().current().call_later(1, lambda: self.stop()) 

    def stop(self):
        tornado.ioloop.IOLoop.instance().stop()

    def on_close(self):
        ws_handler.waiters.remove(self)
        logging.debug("close ws")

    @classmethod
    def send_updates(cls, msg ):
        for waiter in cls.waiters:
            if cls.counter == 0:
                cls.begin = time.time()
            cls.counter += 1
            #print(msg)

            waiter.write_message( msg )

class TestHandler(tornado.web.RequestHandler):
    def get(self):
        return self.render('test.html')

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        return self.render('index.html')

class PageNotFoundHandler(tornado.web.RequestHandler):
    def get(self):
        return self.write_error(404)

class Application(tornado.web.Application):
    def __init__(self):

        if getattr(sys, 'frozen', False): # For deployment
            static_folder = os.path.join(sys._MEIPASS, 'static')
            template_folder = os.path.join(sys._MEIPASS, 'templates')
        else: # For development
            static_folder = os.path.join(os.getcwd(), "static")
            template_folder = os.path.join(os.getcwd(), "templates")

        handlers = [
            (r"/ws", ws_handler),
            (r"/", IndexHandler),
            (r"/test", TestHandler),
            (r"/(.*)", tornado.web.StaticFileHandler, {"path": static_folder}),
            (r".*", PageNotFoundHandler),
        ]

        settings = dict(
            static_path=static_folder,
            template_path=template_folder,
        )
        tornado.web.Application.__init__(self, handlers, **settings)

class web_server():
    def __init__(self, common_cmd_handler=None, proprietary_cmd_handler=None, debug=logging.ERROR):
        self.debug = debug
        self.proprietary_cmd_handler = proprietary_cmd_handler
        self.common_cmd_handler = common_cmd_handler
        self.ws_handler = ws_handler
        self.period_cb = None
        logging.basicConfig(level=debug, format='%(asctime)s -(%(filename)s)%(levelname)s : %(message)s')

#    def open_browser(self):
    def open_browser(self, ip, port):
#        ip="127.0.0.1"
#        port=NET_PORT
        chrome_path = ""
        url = 'http://%s:%d' % (ip, port)
        if platform.system() == "Windows":
            # Windows
            chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'
        elif platform.system() == "Linux":
            # Linux
            chrome_path = '/usr/bin/google-chrome %s'
        elif platform.system() == "Darwin":
            # MacOS
            chrome_path = 'open -a /Applications/Google\ Chrome.app %s'
        logging.debug(chrome_path)
        if len(chrome_path) >5:
            webbrowser.get(chrome_path).open(url)


    def start(self, ip="127.0.0.1", port=NET_PORT, io_loop=None, auto_open_browser=True):
        application = Application()
        logging.debug(self.proprietary_cmd_handler)
        ws_handler.set_handler(self, common_cmd_handler=self.common_cmd_handler, proprietary_cmd_handler=self.proprietary_cmd_handler)
        server = tornado.httpserver.HTTPServer(application, xheaders=True)
        server.listen(port, ip)
        if io_loop is not None:
            ioLoop = io_loop
        else:
            ioLoop = tornado.ioloop.IOLoop.instance()
        if auto_open_browser == True:
            self.web_thread = threading.Thread(target=self.open_browser,args=(ip,port))
            self.web_thread.daemon = True
            self.web_thread.start()

        print("websocket server start in http:{}:{}".format(ip, port))
        ioLoop.start()
        print("websocket server stop")

    def periodic_callback(self, cb=None, cb_time=10, enable=1):
        if enable == 1:
            if self.period_cb is not None and self.period_cb.is_running() == True:
                self.period_cb.stop()
            self.period_cb = tornado.ioloop.PeriodicCallback(cb, cb_time)
            self.period_cb.start()
        else:
            if self.period_cb is not None and self.period_cb.is_running() == True:
                self.period_cb.stop()
                self.period_cb = None
