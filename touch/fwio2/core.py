# -*- coding: utf-8 -*-
import logging
from .address import *
from .job import *
import sys
import platform
import collections
import threading, time
from .web.web_server import web_server
import tornado.ioloop

def common_cmd_handler(data):
    pass

class core:
    def __init__(self, debug=logging.ERROR, ui="web"):
        self.dev = None
        self.desc = None
        self.sis_vid = 0x457
        self.ui = ui
        self.master_chip_info = {"tx":0,"rx":0,"nvb":0,"diff_nvb":0,"slave_num":0}
        self.slave_chip_info = [] 
        self.snap_rawdata = [] 
        self.rawdata_buf = [] 
        self.debug = debug
        self.server = None
        self.job_thread = None
        self.usi_state = 0 
        logging.basicConfig(level=debug, format='%(asctime)s -(%(filename)s)%(levelname)s : %(message)s')

    def open(self, enable_read_thread=True):
        if self.dev != None:
            self.dev.open(enable_read_thread=enable_read_thread)
            self.job_thread.set_dev(self.dev)
            print("open device")
            return True
        return False

    def search(self, dev_path=None):
        dev = None
        ret = False
        logging.debug(platform.system())
        if platform.system() == "Windows":
            from .device_io import windows
            #from .device_io import pywinusb_io
            #dev = pywinusb_io.io(self.debug)
            dev = windows.io(self.debug)
        elif platform.system() == "Darwin":
            from .device_io import pyhidapi_io
            dev = pyhidapi_io.io(self.debug)
        else:
            from .device_io import linux 
            dev = linux.io(self.debug)
        if dev_path is not None:
            ret = dev.search(dev_path=dev_path)
        else:
            ret = dev.search(vid=self.sis_vid)
        if ret == True:
            self.dev = dev
            self.desc = dev.get_desc()
        return ret 

    def close(self):
        if self.dev != None:
            self.dev.close()
            self.dev = None

    def get_hidraw(self):
        if len(self.dev.hidraw_buf) > 0:
            buf = self.dev.hidraw_buf.copy()
            self.dev.hidraw_buf.clear()
            return buf

    def stop_server(self):
        #import tornado.gen
        #await IOLoop.current().run_in_executor(None, time.sleep, 100)
        #await gen.sleep(1)
        tornado.ioloop.IOLoop().current().call_later(1, lambda: self.job_thread.stop()) 
        tornado.ioloop.IOLoop().current().call_later(1, lambda: self.close()) 

    def start_server(self, handler=None, cb=None, ip="127.0.0.1", port=8050):
        self.job_thread = job_thread("device_io", 0.01)  
        self.job_thread.start()
        self.job_thread.set_callback(cb)
        if self.ui == "web":
            io_loop = tornado.ioloop.IOLoop()
            self.server = web_server(common_cmd_handler=common_cmd_handler, proprietary_cmd_handler=handler) 
            self.job_thread.set_websocket(self.server.ws_handler, io_loop)
            #tornado.ioloop.PeriodicCallback(self.io_polling, 50).start()
            self.server.start(ip=ip, port=port, io_loop=io_loop)
        elif self.ui == "pyqt" or self.ui == "none":
            # start read thread
            #self.read_thread = threading.Thread(target=self.io_polling)
            #self.read_thread.daemon = True
            #self.read_thread.start()
            pass


    def reset_device(self):
        self.job_thread.add_job(cmd_id=0x82, addr=[])

    # read data
    def cmd86(self, reg, read_size, parser=None, callback=None, shift=0, chip_address=0, id=0, report_id=0x2d):
        if shift > 0:
            reg = int(reg + shift)
        convert = lambda n : [int(i) for i in n.to_bytes(4, byteorder='big', signed=False)]
        addr = convert(reg)
        if self.desc.chip == 817 or self.desc.bus == "HID_OVER_I2C":
            data = [read_size&0xff, read_size>>8]
            addr.reverse()
        elif self.desc.chip == 819:
            data = [read_size>>8, read_size&0xff, 3]
        logging.debug("addr={}, data={}, chip_address={}".format(addr, data, chip_address))
        self.job_thread.add_job(cmd_id=0x86, addr=addr, data=data, chip_address=chip_address, read_size=read_size, parser=parser, id=id, callback=callback, report_id=report_id)

    # change mode
    def cmd85(self, name, retry=True, device="", id=0):
        reg = CMD85_ADDRESS[name]
        addr = [reg>>8, reg&0xff] # it doesn't match with SiS9257_I2C_USB_Cmd_Table_CY.xls 
        #addr = [reg&0xff, reg>>8]
        logging.debug("reg={}".format(addr))
        #print("reg={}".format(addr))
        self.job_thread.add_job(cmd_id=0x85, addr=addr, id=id)

    def enter_cmd_mode(self, id=0):
        self.cmd85('PWR_CMD_ACTIVE', id=id)
        self.cmd85('ENABLE_DIAGNOSIS_MODE', id=id)
        return True 

    def leave_cmd_mode(self, id=0):
        self.cmd85('PWR_CMD_FWCTRL', id=id)
        self.cmd85('DISABLE_DIAGNOSIS_MODE', id=id)
        return True

    def enter_normal_mode(self, id=0):
        return self.cmd85('NORMAL_MODE', id=id)

    def enter_cdc_mode(self, id=0):
        return self.cmd85('ENABLE_HIDCDC_INTERFACE', id=id)

    def calibrate_coordinates(self, data=[], reset=False, id=0, cb=None):
        buf = []
        reg = 0x67
        tail = [0x2,0x1]
        var_list = [0xFA, 0xFB, 0xFC, 0xFD]
        reset_var = [[0,1,0,0],[0,0,0,0],[0,0,0,0],[0,1,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
        
        if reset == True or len(data) == 0:
            data = reset_var
        if self.desc.chip == 819:
            for i in range(4):
                offset = i * 2
                tmp = [reg, var_list[i], 0, 0]
                tmp.extend(data[offset])
                tmp.extend(data[offset+1])
                buf.extend(tmp)
            buf.extend(tail)
        elif self.desc.chip == 817:
            for i in range(4):
                offset = i * 2
                #data[offset].reverse()
                #data[offset+1].reverse()
                #i have no idea why 817 doesn't need to do reverse.
                #SiS9257_I2C_USB_Cmd_Table_CY.xls shows 817's LSB and MSB different with 819 
                data[offset]
                data[offset+1]
                tmp = [0, 0, var_list[i], reg]
                tmp.extend(data[offset])
                tmp.extend(data[offset+1])
                buf.extend(tmp)
            buf.extend(tail)
        self.job_thread.add_job(cmd_id=0x88, addr=buf, data=[], id=id, callback=cb)

    def online_train_mode(self, train_type=1, mode=1, abswt=0, total_negative_wt=0, tx_freq=0, id=0):
        buf = []
        if self.desc.chip == 819:
            addr = [0x69, 0, train_type, mode]
            data = [abswt,total_negative_wt,tx_freq>>8,tx_freq&0xff,0,0,0]
            buf.extend(addr)
            buf.extend(data)
        elif self.desc.chip == 817:
            addr = [mode, train_type, 0, 0x69]
            data = [abswt,total_negative_wt,tx_freq>>8,tx_freq&0xff,0,0,0]
            buf.extend(addr)
            buf.extend(data)
        self.dev.hidraw_buf.clear()
        self.job_thread.add_job(cmd_id=0x88, addr=buf, data=[], id=id)

    def get_slave_num(self, source='xram', cb=None, id=0):
        print('carey')
        def get_slave_num_parser(buf, job):
            status = 0
            slave_num = buf[0]
            self.master_chip_info["slave_num"] = slave_num
            if slave_num > 0:
                tmp = {"tx":0, "rx":0}
                for i in range(slave_num+1):
                    self.slave_chip_info.append(tmp.copy())
            logging.debug("get_slave_num_parser,{}".format(self.master_chip_info))
            return self.master_chip_info, status
        addr = FA_ADDRESS['MULTI_SLAVE_NUMBER_ROM']
        if source == 'xram':
            addr = FA_ADDRESS['XRAM_BASE_ADDRESS'] + addr
        self.cmd86(addr, FA_SIZE['MULTI_SLAVE_NUMBER_ROM'], parser=get_slave_num_parser, callback=cb, id=id) 

    def get_tx_rx_size(self, cb=None, chip_address=0, id=0):
        def get_tx_rx_size_parser(buf, job):
            status = 0
            chip_address = job["chip_address"]
            tx = 0
            rx = 0
            if self.desc.chip == 819:
                tx = (buf[0]<<8) + buf[1]
                rx = (buf[2]<<8) + buf[3]
            elif self.desc.chip == 817:
                tx = buf[1]
                rx = buf[2]
            if chip_address == 0:
                self.master_chip_info["tx"] = tx
                self.master_chip_info["rx"] = rx
            else:
                self.slave_chip_info[chip_address]["tx"] = tx 
                self.slave_chip_info[chip_address]["rx"] = rx 
            logging.debug("\033[32m get_tx_rx_size_parser,chip_address:{},master:{},slave:{}\033[0m".format(chip_address,self.master_chip_info, self.slave_chip_info))
            return self.master_chip_info, status
        self.cmd86(INFO_ADDRESS['WIDTH_HEIGHT_ADDRESS'], INFO_SIZE['WIDTH_HEIGHT_ADDRESS'], parser=get_tx_rx_size_parser, callback=cb, chip_address=chip_address, id=id) 

    def get_nvb(self, id=0):
        def get_nvb_parser(buf, job):
            status = 0
            self.master_chip_info["nvb"] = buf[12]
            self.master_chip_info["diff_nvb"] = buf[13]
            logging.debug("get_nvb_parser,{}".format(self.master_chip_info))
            logging.debug("get_nvb_parser, tp:{}:{}, pen:{}:{}".format(self.master_chip_info["nvb"]&0xf, self.master_chip_info["diff_nvb"]&0xf,self.master_chip_info["nvb"]&0xf0, self.master_chip_info["diff_nvb"]&0xf0))
            return self.master_chip_info, status

        self.cmd86(FA_ADDRESS['XRAM_BASE_ADDRESS'] + FA_ADDRESS['NON_DIFF_NVB_ROM'], FA_SIZE['NON_DIFF_NVB_ROM'], parser=get_nvb_parser, id=id) 

    # if nvb == 1, data is 1byte, elif nvb == 2, data is 2bytes.
    def convert_data_to_2bytes(self, data):
        high_byte = data[0:][::2]
        low_byte = data[1:][::2]
        tmp = [0] * len(high_byte)
        for i in range(len(high_byte)):
           tmp[i] = (high_byte[i] << 8) + low_byte[i] 
        return tmp

    def get_rawdata(self, device_type='touch', data_type='diff', cb=None, chip_address=0, id=0):
        if self.master_chip_info["tx"] == 0:
            self.get_tx_rx_size()
            return False
        def rawdata_parser(data, job):
            status = 0
            nvb = 1
            chip_address = job["chip_address"]
            if chip_address == 0:
                tx = self.master_chip_info["tx"] 
                rx = self.master_chip_info["rx"] 
            else:
                tx = self.slave_chip_info[chip_address]["tx"] 
                rx = self.slave_chip_info[chip_address]["rx"] 
            if self.rawdata_type == 'diff':
                if self.master_chip_info["diff_nvb"]&0xf > 0:
                    nvb = 2
            elif self.master_chip_info["nvb"]&0xf > 0:
                nvb = 2
            width = rx * nvb 
            size = tx * width 
            align_4bytes = (4 - (width % 4)) % 4 #rawdata rx must align 4 bytes
            size += align_4bytes * tx
            self.rawdata_buf.extend(data)
            logging.debug("rawdata buf len:{}, size:{}, chip_address:{}".format(len(self.rawdata_buf), size, chip_address))
            if len(self.rawdata_buf) >= size:
                if nvb == 2:
                    self.rawdata_buf = self.convert_data_to_2bytes(self.rawdata_buf)
                data = self.rawdata_buf
                if self.rawdata_type == 'rawdiff':
                    if len(self.snap_rawdata) == 0:
                        self.snap_rawdata = data
                        data = [0] * len(data)
                    else:
                        buf = []
                        for i in range(len(self.snap_rawdata)):
                            val = self.snap_rawdata[i]-data[i]
                            buf.append(val)
                        data = buf
                elif self.rawdata_type == 'diff':
                    mask = 0x7f
                    imp = 0x100
                    if nvb > 1:
                        mask = 0x7fff
                        imp = 0x10000
                    for i in range(len(data)):
                        if data[i] > mask:
                            data[i] = data[i] - imp
                self.rawdata_buf = data

                print("\033[41m chip_address:{}, tx:{}, rx:{}, align_4bytes:{}, nvb:{}, len:{}\ndata:{}\033[0m".format(chip_address, tx, rx, align_4bytes, nvb, len(self.rawdata_buf), self.rawdata_buf))

                if align_4bytes > 0:
                    align_4bytes = int(align_4bytes / nvb)
                    if False:
                        # print rows
                        for i in range(tx):
                            width = rx + align_4bytes
                            shift = i * width
                            print('[{}]'.format(', '.join(hex(x) for x in self.rawdata_buf[shift:shift+width])))
                    # remove alignment data
                    for i in range(1, tx + 1):
                        offset = i * rx
                        del self.rawdata_buf[offset:offset + align_4bytes]
                ret = {'chip_address':chip_address, 'data':self.rawdata_buf.copy()}
                self.rawdata_buf.clear()
                return ret, status
            return [], status

        self.rawdata_type = data_type
        if data_type != 'rawdiff': 
            self.snap_rawdata.clear()

        if device_type == 'touch':
            nvb = 1 #if nvb is 1 that means data is 8bit, if nvb is 2 then data is 16bit
            if self.master_chip_info["nvb"]&0xf > 0 and data_type != 'diff':
                nvb = 2
            if data_type == 'base': 
                addr = RAWDATA_ADDRESS['BASE_VOLTAGE']
            elif data_type == 'rawdata': 
                addr = RAWDATA_ADDRESS['RAW_VOLTAGE']
            elif data_type == 'diff': 
                if self.master_chip_info["diff_nvb"]&0xf > 0:
                    nvb = 2
                addr = RAWDATA_ADDRESS['DIFF_VOLTAGE']
            elif data_type == 'cycle': 
                addr = RAWDATA_ADDRESS['CYCLE_VOLTAGE']
            elif data_type == 'loop': 
                addr = RAWDATA_ADDRESS['LOOP_VOLTAGE']
            elif data_type == 'rawdiff': 
                addr = RAWDATA_ADDRESS['RAW_VOLTAGE']
            tx = 0
            rx = 0
            if chip_address == 0:
                tx = self.master_chip_info["tx"] 
                rx = self.master_chip_info["rx"] 
            else:
                tx = self.slave_chip_info[chip_address]["tx"] 
                rx = self.slave_chip_info[chip_address]["rx"] 
            width = rx * nvb 
            size = tx * width 
            align_4bytes = (4 - (width % 4)) % 4 #rawdata rx must align 4 bytes
            size += align_4bytes * tx
        print("\033[46m size = {},tx = {}, rx = {}, align_4bytes = {}, nvb = {}, chip_address = {}\033[0m ".format(size, tx, rx, align_4bytes, nvb, chip_address))
        if False:
            self.cmd86(addr, size, parser=rawdata_parser, callback=cb, id=id)
        else:
            shift = 0
            step = 800 
            while shift < size:
                if shift + step > size:
                    step = size - shift
                self.cmd86(addr, step, parser=rawdata_parser, callback=cb, shift=shift/4, chip_address=chip_address, id=id)
                shift += step
        return True

    def get_report_descriptor(self, id=0, cb=None):
        def get_report_descriptor(buf, job):
            status = 0
            return buf, status
        def get_report_descriptor_size(buf, job):
            cb = job["callback"]
            status = 1
            size = buf[0]<<24 + buf[1]<<16 + buf[2]<<8 + buf[3]
            size = (buf[2] << 8) + buf[3]
            print("#####get size = {}, buf:{}, type:{}, 2:{}".format(size, buf, type(buf[2]), buf[2]<<8, buf[3]))
            self.cmd86(INFO_ADDRESS['REPORT_DESCRIPTOR_ADDRESS'], size, parser=get_report_descriptor, callback=cb, id=id) 
            return buf, status

        self.cmd86(INFO_ADDRESS['REPORT_DESCRIPTOR_SIZE_ADDRESS'], INFO_SIZE['REPORT_DESCRIPTOR_SIZE_ADDRESS'], parser=get_report_descriptor_size, callback=cb, id=id, report_id=0x21) 


    # USI command
    def cmd_usi(self, report_id, read_id=0, value=[], read_size=0, parser=None, callback=None, id=0):
        logging.debug("value={}".format(value))
        self.job_thread.add_job(cmd_id=0x1, addr=value, report_id=report_id, read_id=read_id, device="usi", read_size=read_size, parser=parser, callback=callback, id=id)

    def usi_data_checker(self, buf, job):
        status = 0
        report_id = 0x6d
        read_id = job["read_id"]
        cb = job["callback"]
        id = job["id"]
        
        if self.usi_state == 0 and (len(buf)==0 or buf[0] == 0 or buf[1] == 0):
            self.usi_state = 1
            if read_id == 0:
                read_id = job["report_id"]
            self.cmd_usi(report_id, read_id, read_size=3, parser=self.usi_data_checker, callback=cb, id=id)
            status = 1 
        else:
            if len(buf) > 0 and (buf[0] == 0 or buf[1] == 0):
               status = -1 
        print("state:{}, report_id:{}, read_id:{}, buf:{}, status:{}".format(self.usi_state, report_id, read_id, buf, status))
        return buf, status

    def usi_pen_set_cmd(self, config, value, cb=None, id=0, verify=False):
        self.usi_state = 0
        report_id = 0x65
        if config == "color":
            report_id = 0x65
        elif config == "width":
            report_id = 0x66
        elif config == "type":
            report_id = 0x67
        parser = None
        if verify == True:
            parser = self.usi_data_checker
        self.cmd_usi(report_id, value=[value, 0], parser=parser, callback=cb, id=id)

    def usi_pen_get_cmd(self, config, cb=None, id=0):
        self.usi_state = 0
        report_id = 0x6d
        read_id = 0x65
        if config == "color":
            read_id = 0x65
        elif config == "width":
            read_id = 0x66
        elif config == "type":
            read_id = 0x67
            
        self.cmd_usi(report_id, read_id, read_size=3, parser=self.usi_data_checker, callback=cb, id=id)

    def get_pen_capability(self, action, id=0):
        if action == "start":
            self.cmd85('ENABLE_GET_PEN_CAPABILITY', device="usi", id=id)
        else: 
            self.cmd85('DISABLE_GET_PEN_CAPABILITY', device="usi", id=id)
        return True 

