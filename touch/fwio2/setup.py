#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os, sys

try:
    from setuptools import setup, find_packages
except ImportError:
    sys.stdout.write("Setuptools or Distribute packages are required.")
    sys.exit(1)

VERSION = '0.0.1'
README  = os.path.join(os.path.dirname(__file__), 'README.rst')
CHANGES = os.path.join(os.path.dirname(__file__), 'CHANGES.rst')

long_description = open(README).read() + open(CHANGES).read() + 'nn'

setup(name = 'fwio',
      version = VERSION,
      description=("A package that SIS touch device communications"),
      long_description=long_description,
      classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Win32 (MS Windows)",
        "Operating System :: Microsoft :: Windows",
        "Programming Language :: Python :: 3.3",
        "License :: OSI Approved :: SIS",
        "Topic :: System :: Hardware",
        "Topic :: System :: Hardware :: Hardware Drivers",
        "Topic :: Scientific/Engineering :: Human Machine Interfaces",
        "Topic :: Software Development :: Embedded Systems",
        "Topic :: Software Development :: Libraries",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Utilities",
        "Intended Audience :: Developers",
        ],
      keywords='SIS usages',
      author='carey chou',
      author_email='carey_chou@sis.com',
      url='https://sis.com/',
      license='SIS',
      packages=find_packages(),
      package_data = {
          # for all packages
          ''         : ['*.txt', '*.rst', '*.py', 'device_io/*.py'],
          },
      include_package_data = True,
      namespace_packages=['fwio'],
      )

