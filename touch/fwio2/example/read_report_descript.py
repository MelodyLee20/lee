# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 16:12:27 2019

@author: carey
"""

import os
import sys
import time
import logging
sys.path.append("../../../")
import touch.fwio2 as fwio
from touch import hidparser
from touch.hidparser.Device import Collection
from touch.hidparser.UsagePages import GenericDesktop, Button
from touch.hidparser.UsagePage import UsagePage, UsageType, Usage
from touch.hidparser.enums import CollectionType, ReportType, ReportFlags, UnitSystem

def callback(ws, status, id, msg, data=[]):
    print("get callback, id:{}".format(id))
    if type(data) == 'list':
        print("\033[41m get data {} \033[0m".format([status, msg, [hex(item).upper() for item in data[1:]]]))
    else:
        print("\033[41m get data {} \033[0m".format([status, msg, data]))
    if ws:
        ws.send_updates(msg)

if __name__ == '__main__':
    touch_dev = fwio.core(logging.DEBUG, ui="none")
    if touch_dev.search() == True:
        touch_dev.start_server(cb=callback)
        touch_dev.open()
        def desc_cb(ws, status, id, msg, data=[]):
            print("get callback, id:{}".format(id))
            if status == 0:
                print("\033[41m get data {} \033[0m".format([status, msg, [hex(item).upper() for item in data[1:]]]))
                desc_bin = bytes(data)
                touch_desc = hidparser.parse(desc_bin)
                print(touch_desc)
            else:    
                print("\033[41m get data {} \033[0m".format([status, msg, data]))
        touch_dev.get_report_descriptor(cb=desc_cb)
    time.sleep(2)
    touch_dev.stop_server()
    touch_dev.close()

