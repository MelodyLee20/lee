import win32con
import win32api
import win32gui
import time
from ctypes import  POINTER, windll, Structure, cast, CFUNCTYPE, c_int, c_uint, c_void_p, c_bool, sizeof, Union, c_ushort, c_ulong, c_long, c_char, byref, create_string_buffer, create_unicode_buffer, c_ubyte
from comtypes import GUID
from ctypes.wintypes import BOOL, HANDLE, DWORD, USHORT, HWND, WPARAM, ULONG, LONG, UINT, BYTE
import threading

WM_INPUT = 0x00FF

class RAWINPUTDEVICE(Structure):
    _fields_ = [
        ("usUsagePage", c_ushort),
        ("usUsage", c_ushort),
        ("dwFlags", DWORD),
        ("hwndTarget", HWND),
    ]


class RAWINPUTHEADER(Structure):
    _fields_ = [
        ("dwType", DWORD),
        ("dwSize", DWORD),
        ("hDevice", HANDLE),
        ("wParam", WPARAM),
    ]

class _Buttons(Structure):
    _fields_ = [
        ('usButtonFlags', USHORT),
        ('usButtonData', USHORT),
    ]


class _U(Union):
    _anonymous_ = ('_buttons',)
    _fields_ = [
        ('ulButtons', ULONG),
        ('_buttons', _Buttons),
    ]


class RAWMOUSE(Structure):
    _anonymous_ = ('u',)
    _fields_ = [
        ('usFlags', USHORT),
        ('u', _U),
        ('ulRawButtons', ULONG),
        ('lLastX', LONG),
        ('lLastY', LONG),
        ('ulExtraInformation', ULONG),
    ]



class RAWKEYBOARD(Structure):
    _fields_ = [
        ("MakeCode", c_ushort),
        ("Flags", c_ushort),
        ("Reserved", c_ushort),
        ("VKey", c_ushort),
        ("Message", UINT),
        ("ExtraInformation", ULONG),
    ]


class RAWHID(Structure):
    _fields_ = [
        ("dwSizeHid", DWORD),
        ("dwCount", DWORD),
        ("bRawData", c_ubyte*64),
    ]


class RAWINPUT(Structure):
    class _U1(Union):
        _fields_ = [
            ("mouse", RAWMOUSE),
            ("keyboard", RAWKEYBOARD),
            ("hid", RAWHID),
        ]

    _fields_ = [
        ("header", RAWINPUTHEADER),
        ("_u1", _U1),
    ]
    _anonymous_ = ("_u1", )



GetRawInputData = windll.user32.GetRawInputData

class RAWINPUTDEVICELIST(Structure):
    _fields_ = [
        ("hDevice", HANDLE),
        ("dwType", DWORD),
    ]
class tagRAWINPUTDEVICELIST(Structure):
    pass
PRAWINPUTDEVICELIST = POINTER(tagRAWINPUTDEVICELIST)
tagRAWINPUTDEVICELIST._fields_ = [
    ('hDevice', HANDLE),
    ('dwType', DWORD),
]
RIDI_DEVICENAME = 536870919  # Variable c_int '536870919'
RIDI_DEVICEINFO = 536870923  # Variable c_int '536870923'
class tagRID_DEVICE_INFO(Structure):
    pass
class tagRID_DEVICE_INFO_MOUSE(Structure):
    pass
tagRID_DEVICE_INFO_MOUSE._fields_ = [
    ('dwId', DWORD),
    ('dwNumberOfButtons', DWORD),
    ('dwSampleRate', DWORD),
    ('fHasHorizontalWheel', BOOL),
]
RID_DEVICE_INFO_MOUSE = tagRID_DEVICE_INFO_MOUSE
class tagRID_DEVICE_INFO_KEYBOARD(Structure):
    pass
tagRID_DEVICE_INFO_KEYBOARD._fields_ = [
    ('dwType', DWORD),
    ('dwSubType', DWORD),
    ('dwKeyboardMode', DWORD),
    ('dwNumberOfFunctionKeys', DWORD),
    ('dwNumberOfIndicators', DWORD),
    ('dwNumberOfKeysTotal', DWORD),
]
RID_DEVICE_INFO_KEYBOARD = tagRID_DEVICE_INFO_KEYBOARD
class tagRID_DEVICE_INFO_HID(Structure):
    pass
tagRID_DEVICE_INFO_HID._fields_ = [
    ('dwVendorId', DWORD),
    ('dwProductId', DWORD),
    ('dwVersionNumber', DWORD),
    ('usUsagePage', USHORT),
    ('usUsage', USHORT),
]
RID_DEVICE_INFO_HID = tagRID_DEVICE_INFO_HID
class N18tagRID_DEVICE_INFO5DOLLAR_111E(Union):
    pass
N18tagRID_DEVICE_INFO5DOLLAR_111E._fields_ = [
    ('mouse', RID_DEVICE_INFO_MOUSE),
    ('keyboard', RID_DEVICE_INFO_KEYBOARD),
    ('hid', RID_DEVICE_INFO_HID),
]
tagRID_DEVICE_INFO._anonymous_ = ['_0']
tagRID_DEVICE_INFO._fields_ = [
    ('cbSize', DWORD),
    ('dwType', DWORD),
    ('_0', N18tagRID_DEVICE_INFO5DOLLAR_111E),
]
RID_DEVICE_INFO = tagRID_DEVICE_INFO
RIM_TYPEHID = 2  # Variable c_int '2'
RIM_TYPEKEYBOARD = 1  # Variable c_int '1'
RIM_TYPEMOUSE = 0  # Variable c_int '0'
RIM_TYPES = {
    RIM_TYPEHID: "HID",
    RIM_TYPEKEYBOARD: "Keyboard",
    RIM_TYPEMOUSE: "Mouse",
}
UINT = c_uint

class win32_hidraw():
    def __init__(self, cb=None):
        self.cb = cb
        self.running = False
        self.thread_stop = True 

    def wndproc(self, hwnd, msg, wparam, lparam):
        try:
            hwparam = hex(wparam)
        except:
            hwparam = wparam
        try:
            hlparam = hex(lparam)
        except:
            hlparam = lparam

        if msg == WM_INPUT:
            self.process_input(wparam, lparam)


    def ScanDevices(self):
        print("scan dev..")
        GetRawInputDeviceList = windll.user32.GetRawInputDeviceList
        GetRawInputDeviceInfo = windll.user32.GetRawInputDeviceInfoA
        nDevices = UINT(0)
        if -1 == GetRawInputDeviceList(
            None,
            byref(nDevices),
            sizeof(RAWINPUTDEVICELIST)
        ):
            raise WinError()
        rawInputDeviceList = (RAWINPUTDEVICELIST * nDevices.value)()
        if -1 == GetRawInputDeviceList(
            cast(rawInputDeviceList, PRAWINPUTDEVICELIST),
            byref(nDevices),
            sizeof(RAWINPUTDEVICELIST)
        ):
            raise WinError()
        print(nDevices)

        cbSize = UINT()
        for i in range(nDevices.value):
            try:
                GetRawInputDeviceInfo(
                    rawInputDeviceList[i].hDevice,
                    RIDI_DEVICENAME,
                    None,
                    byref(cbSize)
                )
                deviceName = create_unicode_buffer(cbSize.value)
                GetRawInputDeviceInfo(
                    rawInputDeviceList[i].hDevice,
                    RIDI_DEVICENAME,
                    byref(deviceName),
                    byref(cbSize)
                )
                ridDeviceInfo = RID_DEVICE_INFO()
                cbSize.value = ridDeviceInfo.cbSize = sizeof(RID_DEVICE_INFO)
                GetRawInputDeviceInfo(
                    rawInputDeviceList[i].hDevice,
                    RIDI_DEVICEINFO,
                    byref(ridDeviceInfo),
                    byref(cbSize)
                )
            except:
                continue
            if ridDeviceInfo.dwType != RIM_TYPEHID:
                continue
            print( "hDevice:", rawInputDeviceList[i].hDevice)
            print( "Type:", RIM_TYPES[rawInputDeviceList[i].dwType])
            print( "DeviceName:", deviceName.value)
            if ridDeviceInfo.dwType == RIM_TYPEHID:
                hid = ridDeviceInfo.hid
                print( "dwVendorId: %04X" % hid.dwVendorId)
                print( "dwProductId: %04X" % hid.dwProductId)
                print( "dwVersionNumber: %04X" % hid.dwVersionNumber)
                print( "usUsagePage:", hid.usUsagePage)
                print( "usUsage:", hid.usUsage)
            if ridDeviceInfo.dwType == RIM_TYPEKEYBOARD:
                kbd = ridDeviceInfo.keyboard
                print( "dwType:", kbd.dwType)
                print( "dwSubType:", kbd.dwSubType)
                print( "dwKeyboardMode:", kbd.dwKeyboardMode)
                print( "dwNumberOfFunctionKeys:", kbd.dwNumberOfFunctionKeys)
                print( "dwNumberOfIndicators:", kbd.dwNumberOfIndicators)
                print( "dwNumberOfKeysTotal:", kbd.dwNumberOfKeysTotal)
            if ridDeviceInfo.dwType == RIM_TYPEMOUSE:
                mouse = ridDeviceInfo.mouse
                print( "dwId:", mouse.dwId)
                print( "dwNumberOfButtons:", mouse.dwNumberOfButtons)
                print( "dwSampleRate:", mouse.dwSampleRate)
                print( "fHasHorizontalWheel:", mouse.fHasHorizontalWheel)
            print()

    def process_input(self, wParam, hRawInput):
        pcbSize = UINT()
        RID_INPUT = 0x10000003
        GetRawInputData(
            hRawInput, RID_INPUT, None, byref(pcbSize), sizeof(RAWINPUTHEADER)
        )
        buf = create_string_buffer(pcbSize.value)
        GetRawInputData(
            hRawInput, RID_INPUT, buf, byref(pcbSize), sizeof(RAWINPUTHEADER)
        )
        pRawInput = cast(buf, POINTER(RAWINPUT))
        ri =  pRawInput.contents
        #print('\n\ninput from background?:', wParam)
        #ri = RAWINPUT()
        #dwSize = c_uint(sizeof(ri))
        #GetRawInputData(hRawInput, RID_INPUT, byref(ri), byref(dwSize), sizeof(RAWINPUTHEADER))
        if ri.header.dwType == 0: #mouse
            #print('message:', ri.mouse.ulRawButtons)
            #print('message:', hex(ri.mouse.usFlags))
            #print('message:', ri.mouse.lLastX)
            #print('message:', ri.mouse.lLastY)
            pass
        elif ri.header.dwType == 1: #keyboard
            #print('message:', ri.keyboard)
            #print('vKey:', ri.keyboard.VKey)
            #print('scan code:', ri.keyboard.MakeCode)
            #print('state:', ri.keyboard.Flags)
            pass
        elif ri.header.dwType == 2: #hid
            #print('message:', ri.hid.dwSizeHid)
            #print('message:', ri.hid.dwCount)
            #print('message:', ri.hid.bRawData)
            rawdata = [ri.hid.bRawData[i] for i in range(ri.hid.dwSizeHid)]
            #rawdata = []
            #rawdata.extend(ri.hid.bRawData[i]) for i in range(ri.hid.dwSizeHid)]
            if self.cb is not None:
                self.cb(rawdata)
        pass


    def run(self):
        if self.running:
            return
        self.running = True
        hinst = win32api.GetModuleHandle(None)
        wndclass = win32gui.WNDCLASS()
        wndclass.hInstance = hinst
        wndclass.lpszClassName = "PyWindow"
        CMPFUNC = CFUNCTYPE(c_bool, c_int, c_uint, c_uint, c_void_p)
        wndproc_pointer = CMPFUNC(self.wndproc)
        messageMap = {
            WM_INPUT: self.wndproc
        }
        wndclass.lpfnWndProc = messageMap
        self.ScanDevices()
        try:
            myWindowClass = win32gui.RegisterClass(wndclass)
            #hwnd = win32gui.CreateWindowEx(win32con.WS_EX_LEFT, myWindowClass, "PyWindow", 0, 0, 0, win32con.CW_USEDEFAULT, win32con.CW_USEDEFAULT, 0, 0,
            #                               hinst, None)
            hwnd = win32gui.CreateWindowEx(win32con.WS_EX_LEFT, wndclass.lpszClassName, "PyWindow", 0, 0, 0,
                                           win32con.CW_USEDEFAULT, win32con.CW_USEDEFAULT, win32con.NULL, win32con.NULL,
                                           hinst, None)
        except Exception as e:
            print("Exception: %s" % str(e))

        if hwnd is None:
            print("hwnd is none!")
        else:
            print("hwnd: %s" % hwnd)
        '''
        rid = RAWINPUTDEVICE()
        rid.usUsagePage = 1
        rid.usUsage = 6
        rid.dwFlags = 0x00000100  # RIDEV_INPUTSINK
        rid.hwndTarget = HWND(hwnd)

        ridptr = POINTER(RAWINPUTDEVICE)(rid)
        ridsz = sizeof(rid)

        RegisterRawInputDevices = windll.user32.RegisterRawInputDevices
        print('register return:', RegisterRawInputDevices(ridptr, 1, ridsz))

        GetRegisteredRawInputDevices = windll.user32.GetRegisteredRawInputDevices
        num_ptr = POINTER(UINT)(c_uint(5))
        devs_ptr = POINTER(RAWINPUTDEVICE)(rid)
        print('registered devices:', GetRegisteredRawInputDevices(devs_ptr, num_ptr, ridsz))
        print(num_ptr.contents)
        '''
        # Register for raw input
        Rid = (1 * RAWINPUTDEVICE)()
        Rid[0].usUsagePage = 0x0D # 0d is digitizers, 
        Rid[0].usUsage = 0x00 #in page 0x0d, 04 is touch screen, 0x20 is Stylus, 02 is pen, 0x33 is touch
        #Rid[0].usUsagePage = 0x01 # 01 is generic desktop page 
        #Rid[0].usUsage = 2 #in page 0x01, 02 is mouse, 06 is keyboard
        RIDEV_INPUTSINK = 0x00000100  # Get events even when not focused
        RIDEV_NOLEGACY = 0x00000030
        RIDEV_PAGEONLY = 0x00000020
        Rid[0].dwFlags = RIDEV_INPUTSINK | RIDEV_PAGEONLY
        Rid[0].hwndTarget = hwnd

        RegisterRawInputDevices = windll.user32.RegisterRawInputDevices
        print('registering raw input device')
        RegisterRawInputDevices(Rid, 1, sizeof(RAWINPUTDEVICE))

        print('\nEntering loop')
        while self.thread_stop == False:
            win32gui.PumpWaitingMessages()
            time.sleep(0.01)
            #print('.', end='', flush=True)


    def thread_run(self):
        self.thread_stop = False 
        self.read_thread = threading.Thread(target=self.run)
        self.read_thread.daemon = True
        self.read_thread.start()
        print("win32 thread start")
    
    def thread_stop(self):
        self.thread_stop = True 
        self.read_thread.join()
        print("win32 thread stop")

if __name__ == "__main__":
    def cb(data):
        print("receive data from hidraw {}".format(data))
    a = win32_hidraw(cb)

    a.thread_run()
    while True:
        time.sleep(1)
