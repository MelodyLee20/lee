# -*- coding: utf-8 -*-
import usb
import os
import sys
import logging
import usb.util as util
import usb.control as ctrl
import usb.core
import threading, time


class io:
    def __init__(self, debug=logging.ERROR):
        logging.basicConfig(level=debug, format='%(asctime)s - %(levelname)s : %(message)s')
        logging.info("Init pyusb device.")
        self.vendor_id = 0x0457
        self.reattach = False
        self.dev = None
        self.chip = 819
        self.ep_in = None  # type: usb.core.Endpoint
        self.ep_out = None  # type: usb.core.Endpoint
        self.read_thread = None
        self.result_buf = []
        self.hidraw_buf = []
        self.buf_size = 1024

    def set_callback(self, cb):
        pass

    def search(self):
        self.dev = usb.core.find(idVendor=self.vendor_id)
        print(self.dev)
        if self.dev is None:
            logging.error("pyusb_io: SIS TP(X)");
            return False,self.chip
        logging.info("pyusb_io: SIS TP (O)");
        self.active()
        try:
            ctrl.set_feature(self.dev,  0x21)
        except:
            print("it's 817")
            self.chip = 817
        #self.deactive() 
        return True,self.chip

    def open(self):
        #self.active()
        self.cfg = self.dev.get_active_configuration()
        intf = self.cfg[(0, 0)]
        self.ep_in = usb.util.find_descriptor(intf, custom_match=lambda e: usb.util.endpoint_direction(e.bEndpointAddress) == usb.util.ENDPOINT_IN)
        self.ep_out = usb.util.find_descriptor(intf, custom_match=lambda e: usb.util.endpoint_direction(e.bEndpointAddress) == usb.util.ENDPOINT_OUT)
        # start read thread
        self.read_thread = threading.Thread(target=self.read_handler)
        self.read_thread.daemon = True
        self.read_thread.start()

        #self.deactive() 

    # handler for reading incoming raw HID packets via PyUSB (thread started in local open() method)
    def read_handler(self):
        while self.ep_in != None and self.dev:
            try:
                #self.active() 
                buf = self.dev.read(self.ep_in.bEndpointAddress, self.ep_in.wMaxPacketSize)
                if buf[0] == 0xa:
                    self.result_buf.append(buf.tolist())
                    logging.debug("pyusb get result")
                else:
                    if len(self.hidraw_buf) > self.buf_size:
                        self.hidraw_buf.clear()
                    self.hidraw_buf.append(buf.tolist())
                #self.deactive() 
                if len(buf) > 0 and buf[0] > 0:
                    logging.debug("pyusb get ep_in data {}".format(bytes(buf).hex()))
            except usb.core.USBError as e:
                if e.errno == 110 or "timed out" in str(e) or "not detach" in str(e):
                    pass
                    #logging.debug("PyUSB read thread error: %s" % e)
                elif e.errno == 5 or e.errno == 19 or "such device" in str(e):
                    # "Input/Output Error" or "No such device", this is serious
                    self.close()
                    return
                else:
                    logging.debug("PyUSB read thread error: %s" % e)

        logging.debug("pyusb read thread exited")

    def read(self, report_id=0x21):
        ret = False
        bmRequestType = util.build_request_type(util.CTRL_IN, util.CTRL_TYPE_CLASS, util.CTRL_RECIPIENT_INTERFACE)
        #self.active() 
        buf = self.dev.ctrl_transfer(bmRequestType, 0x1, (3 << 8) | report_id, 0, 64)
        #self.deactive() 
        if len(buf) > 0:
            ret = True
        return ret,buf.tolist()

    def write(self, chip, addr, data=[], report_id=0x21):
        if chip == 819:
            size = 64 * (report_id & 0xf)
            length = 3 + len(addr) + len(data)
            buf = [report_id, length>>8, length & 0xff]
        elif chip == 817:
            size = 64
            length = 1 + len(addr) + len(data)
            buf = [0x9, length & 0xff]
        bmRequestType = util.build_request_type(util.CTRL_OUT, util.CTRL_TYPE_CLASS, util.CTRL_RECIPIENT_INTERFACE)
        buf.extend(addr)
        if len(data) > 0:
            buf.extend(data)
        buf.extend([0] * (size - len(buf)))
        logging.debug(buf)
        #self.active() 
        if self.dev:
            if chip == 819:
                ret = self.dev.ctrl_transfer(bmRequestType, 0x9, (3 << 8) | report_id, 0, buf)
                if ret >= 0:
                    logging.debug("819 cmd[{}] success".format(buf));
                    return True
            elif chip == 817:
                ret =self.ep_out.write(buf)
                if ret >= 0:
                    logging.debug("817 cmd[{}] success".format(buf));
                    return True
            
        #self.deactive() 
        logging.debug("cmd[{}] fail".format(addr));
        return False

    def close(self):
        self.deactive() 

    def deactive(self):
        usb.util.dispose_resources(self.dev)  # prevent attach_kernel_driver from "Resource busy"
        if self.reattach:
            try:
                self.dev.attach_kernel_driver(0)  # may raise USBError if there's e.g. no kernel driver loaded at all
            except:
                logging.error("disconnect fail: no kernel driver loaded.")

    def active(self):
        try:
            if self.dev.is_kernel_driver_active(0) is True:
                self.reattach = True
                self.dev.detach_kernel_driver(0)
        except usb.core.USBError as e:
            sys.exit("Kernel driver won't give up control over device: %s" % str(e))

        self.dev.reset()
        self.dev.set_configuration()


