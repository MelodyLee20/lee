# -*- coding: utf-8 -*-
import os
import sys
import logging
import pywinusb.hid as hid

from .win32_hidraw import *
class io:
    def __init__(self, debug=logging.ERROR):
        print(debug)
        logging.basicConfig(level=debug, format='%(asctime)s - %(levelname)s : %(message)s')
        logging.info("Init pywinusb device.")
        self.vendor_id = 0x0457
        self.alive = False
        self.dev = None
        self.chip = 819
        self.in_report = None
        self.out_report = None
        self.feature_report = None
        self.result_buf = []
        self.hidraw_buf = []
        self.buf_size = 1024
        self.hidraw_reciver = None

    def read_cb(self, buf):
        if buf[0] == 0xa: # 817 report_id is 0xa
            logging.debug("read_cb buf:{}".format(buf))
            self.result_buf.append(buf)
        elif buf[0] == 0x61: # hidraw report_id is 0x61
            if len(self.hidraw_buf) > self.buf_size:
                self.hidraw_buf.clear()
            self.hidraw_buf.append(buf)

    def search(self):
        _filter = hid.HidDeviceFilter(vendor_id=self.vendor_id)
        hid_device = _filter.get_devices()
        logging.debug(hid_device)
        if len(hid_device) > 0:
            for dev in hid_device:
                dev.open()
                for report in dev.find_feature_reports():
                    if report.report_id == 0x21:
                        self.dev = dev
                        self.in_report = self.dev.find_input_reports()
                        self.out_report = self.dev.find_output_reports()
                        self.feature_report = self.dev.find_feature_reports()
                        self.alive = True
                        self.chip = 819
                        self.dev.set_raw_data_handler(self.read_cb)
                        logging.debug(self.feature_report)
                        logging.info("pywinusb_io: SIS 819 TP(O)");
                        return True, self.chip
                for report in dev.find_output_reports():
                    if report.report_id == 0x09:
                        self.dev = dev
                        self.out_report = self.dev.find_output_reports()
                        self.in_report = self.dev.find_input_reports()
                        self.feature_report = self.dev.find_feature_reports()
                        self.alive = True
                        self.chip = 817
                        self.dev.set_raw_data_handler(self.read_cb)
                        logging.info("pywinusb_io: SIS 817 TP(O)");
                        return True, self.chip
                dev.close()
        logging.info("pywinusb_io: SIS TP(X)");
        return False, self.chip

    def open(self):
        if self.hidraw_reciver is not None:
            return
        else:
            self.hidraw_reciver = win32_hidraw(self.read_cb) 
            self.hidraw_reciver.thread_run() 
            print("win32 thread start")

    def read(self, report_id=0x2d):
        ret = False
        buf = []
        if self.feature_report:
            for report in self.feature_report:
                if report.report_id == report_id:
                    buf = report.get()

        if len(buf) > 0:
            logging.debug("feature_report read: {}".format(buf))
            ret = True
        return ret,buf

    def write(self, chip, addr, data=[], report_id=0x2d):
        if chip == 819:
            size = 64 * (report_id & 0xf)
            length = 3 + len(addr) + len(data)
            buf = [report_id, length>>8, length & 0xff]
        elif chip == 817:
            size = 64
            length = 1 + len(addr) + len(data)
            buf = [0x9, length & 0xff]

        buf.extend(addr)
        if len(data) > 0:
            buf.extend(data)
        buf.extend([0] * (size - len(buf)))
        logging.debug("write: {}".format(buf))
 
        if self.dev:
            if chip == 819:
                if self.feature_report:
                    for report in self.feature_report:
                        if report.report_id == report_id:
                            logging.debug(buf)
                            report.set_raw_data(buf)
                            bytes_num = report.send()
                            if bytes_num > 0:
                                logging.debug("cmd[{}] success".format(addr));
                                return True

            elif chip == 817:
                if self.out_report:
                    for report in self.out_report:
                        if report.report_id == 0x9:
                            bytes_num = report.send(buf)
                            print("ret{}, buf{}".format(bytes_num, buf))
                            if bytes_num > 0:
                                logging.debug("cmd[{}] success".format(addr));
                                return True
        logging.info("cmd[{}] fail".format(addr));
        return False

    def close(self):
        self.alive = False
        if self.dev:
            self.dev.close()


