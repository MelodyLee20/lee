import fcntl
import ctypes
import os
import struct
import logging
import threading
import collections
import time

IOC_NRBITS = 8
IOC_TYPEBITS = 8
IOC_SIZEBITS = 14
IOC_DIRBITS = 2

IOC_NRSHIFT = 0
IOC_TYPESHIFT = IOC_NRSHIFT + IOC_NRBITS
IOC_SIZESHIFT = IOC_TYPESHIFT + IOC_TYPEBITS
IOC_DIRSHIFT = IOC_SIZESHIFT + IOC_SIZEBITS

IOC_NONE = 0
IOC_WRITE = 1
IOC_READ = 2


def IOC(dir, type, nr, size):
    """Linux _IOC macro as function."""
    return (((dir) << IOC_DIRSHIFT) |
            ((type) << IOC_TYPESHIFT) |
            ((nr) << IOC_NRSHIFT) |
            ((size) << IOC_SIZESHIFT))


def IOWR(type, nr, size):
    """Linux _IOWR macro as function."""
    return IOC(IOC_WRITE | IOC_READ, type, nr, size)


HIDIOCSFEATURE = lambda len: IOWR(ord('H'), 0x06, len)
HIDIOCGFEATURE = lambda len: IOWR(ord('H'), 0x07, len)

REPORT_DESCRIPTOR_KEY_MASK = 0xfc
LONG_ITEM_ENCODING = 0xfe
OUTPUT_ITEM = 0x90
INPUT_ITEM = 0x80
FEATURE_ITEM = 0xB0
COLLECTION_ITEM = 0xa0
REPORT_COUNT = 0x94
REPORT_SIZE = 0x74
REPORT_ID = 0x84
USAGE_PAGE = 0x04
USAGE = 0x08

class HidError(Exception):
    """in the hid usb transport protocol."""
    pass


def GetValueLength(rd, pos):
    """Get value length for a key in rd.

    For a key at position pos in the Report Descriptor rd, return the length
    of the associated value.    This supports both short and long format
    values.

    Args:
        rd: Report Descriptor
        pos: The position of the key in rd.

    Returns:
        (key_size, data_len) where key_size is the number of bytes occupied by
        the key and data_len is the length of the value associated by the key.
    """
    rd = bytearray(rd)
    key = rd[pos]
    if key == LONG_ITEM_ENCODING:
        # If the key is tagged as a long item (0xfe), then the format is
        # [key (1 byte)] [data len (1 byte)] [item tag (1 byte)] [data (n # bytes)].
        # Thus, the entire key record is 3 bytes long.
        if pos + 1 < len(rd):
            return (3, rd[pos + 1])
        else:
            raise HidError('Malformed report descriptor')

    else:
        # If the key is tagged as a short item, then the item tag and data len are
        # packed into one byte.    The format is thus:
        # [tag (high 4 bits)] [type (2 bits)] [size code (2 bits)] [data (n bytes)].
        # The size code specifies 1,2, or 4 bytes (0x03 means 4 bytes).
        code = key & 0x03
        if code <= 0x02:
            return (1, code)
        elif code == 0x03:
            return (1, 4)

    raise HidError('Cannot happen')


def readLsbBytes(rd, offset, value_size):
    """reads value_size bytes from rd at offset, least signifcant byte first."""

    encoding = None
    if value_size == 1:
        encoding = '<B'
    elif value_size == 2:
        encoding = '<H'
    elif value_size == 4:
        encoding = '<L'
    else:
        raise HidError('Invalid value size specified')

    ret, = struct.unpack(encoding, rd[offset:offset + value_size])
    return ret


class NoReportCountFound(Exception):
    pass


def ParseReportDescriptor(rd, desc):
    """Parse the binary report descriptor.

    Parse the binary report descriptor into a DeviceDescriptor object.

    Args:
        rd: The binary report descriptor
        desc: The DeviceDescriptor object to update with the results
                from parsing the descriptor.

    Returns:
        None
    """
    rd = bytearray(rd)

    pos = 0
    report_count = None
    report_size = None
    usage_page = None
    usage = None
    report_id = None

    while pos < len(rd):
        key = rd[pos]

        # First step, determine the value encoding (either long or short).
        key_size, value_length = GetValueLength(rd, pos)

        if key & REPORT_DESCRIPTOR_KEY_MASK == INPUT_ITEM:
            if report_count and report_size:
                byte_length = (report_count * report_size) // 8
                desc.internal_max_in_report_len = max(
                        desc.internal_max_in_report_len, byte_length)
                #report_count = None
                #report_size = None
        elif key & REPORT_DESCRIPTOR_KEY_MASK == OUTPUT_ITEM:
            if report_count and report_size:
                byte_length = (report_count * report_size) // 8
                desc.internal_max_out_report_len = max(
                        desc.internal_max_out_report_len, byte_length)
                #report_count = None
                #report_size = None
        elif key & REPORT_DESCRIPTOR_KEY_MASK == FEATURE_ITEM:
            if report_count and report_size:
                byte_length = (report_count * report_size) // 8
                desc.internal_max_feature_report_len = max(
                        desc.internal_max_feature_report_len, byte_length)
                #report_count = None
                #report_size = None
                print(report_id)
                if report_id == 0x21 or report_id == 0x25 or report_id == 0x29 or report_id == 0x2d:
                    desc.chip = 819
                elif report_id == 0x2:
                    desc.chip = 819
                    desc.bus = "HID_OVER_I2C"
        elif key & REPORT_DESCRIPTOR_KEY_MASK == COLLECTION_ITEM:
            if usage_page:
                desc.usage_page = usage_page
            if usage:
                desc.usage = usage
        elif key & REPORT_DESCRIPTOR_KEY_MASK == REPORT_COUNT:
            if len(rd) >= pos + 1 + value_length:
                report_count = readLsbBytes(rd, pos + 1, value_length)
        elif key & REPORT_DESCRIPTOR_KEY_MASK == REPORT_SIZE:
            if len(rd) >= pos + 1 + value_length:
                report_size = readLsbBytes(rd, pos + 1, value_length)
        elif key & REPORT_DESCRIPTOR_KEY_MASK == USAGE_PAGE:
            if len(rd) >= pos + 1 + value_length:
                usage_page = readLsbBytes(rd, pos + 1, value_length)
        elif key & REPORT_DESCRIPTOR_KEY_MASK == USAGE:
            if len(rd) >= pos + 1 + value_length:
                usage = readLsbBytes(rd, pos + 1, value_length)
        elif key & REPORT_DESCRIPTOR_KEY_MASK == REPORT_ID:
            if len(rd) >= pos + 1 + value_length:
                report_id = readLsbBytes(rd, pos + 1, value_length)

        pos += value_length + key_size
    return desc


def ParseUevent(uevent, desc):
    lines = uevent.split(b'\n')
    for line in lines:
        line = line.strip()
        if not line:
            continue
        k, v = line.split(b'=')
        if k == b'HID_NAME':
            desc.product_string = v.decode('utf8')
        elif k == b'HID_ID':
            _, vid, pid = v.split(b':')
            desc.vendor_id = int(vid, 16)
            desc.product_id = int(pid, 16)

class DeviceDescriptor(object):
  """Descriptor for basic attributes of the device."""

  usage_page = None
  usage = None
  vendor_id = None
  product_id = None
  product_string = None
  path = None
  rd_path = "" 
  chip = 817 
  bus = "USB"

  internal_max_in_report_len = 0
  internal_max_out_report_len = 0
  internal_max_feature_report_len = 0

  def ToPublicDict(self):
    out = {}
    for k, v in list(self.__dict__.items()):
      out[k] = v
    return out



class io:
    """Implementation of HID device for linux.

    Implementation of HID device interface for linux that uses block
    devices to interact with the device and sysfs to enumerate/discover
    device metadata.
    """
    def __init__(self, debug=logging.ERROR):
        logging.basicConfig(level=debug, format='%(asctime)s - %(levelname)s : %(message)s')
        logging.info("Init linux device.")
        self.dev = None 
        self.desc = None
        self.read_thread = None
        self.result_buf = collections.deque(maxlen=64)
        self.hidraw_buf = collections.deque(maxlen=2048)

    def Enumerate(self, vid=0, dev_path=None):
        for hidraw in os.listdir('/sys/class/hidraw'):
            if dev_path is not None and hidraw != dev_path[5:]:
                continue
            rd_path = (
                    os.path.join(
                            '/sys/class/hidraw', hidraw,
                            'device/report_descriptor'))
            uevent_path = os.path.join('/sys/class/hidraw', hidraw, 'device/uevent')
            rd_file = open(rd_path, 'rb')
            uevent_file = open(uevent_path, 'rb')
            desc = DeviceDescriptor()
            desc.path = os.path.join('/dev/', hidraw)
            desc.rd_path = rd_path 
            ParseReportDescriptor(rd_file.read(), desc)
            ParseUevent(uevent_file.read(), desc)
            print("dev:{}, desc:{}".format(rd_path, desc.ToPublicDict()))
            rd_file.close()
            uevent_file.close()
            if vid == 0 or desc.vendor_id == vid:
                self.dev = os.open(desc.path, os.O_RDWR) 
                self.desc = desc

    def search(self, vid=0, dev_path=None):
        self.Enumerate(vid=vid, dev_path=dev_path)
        if self.dev is not None:
            return True
        else:
            return False

    def open(self, enable_read_thread=False):
        logging.debug("linux hid open")

        if enable_read_thread == True:
            # start read thread
            self.read_thread = threading.Thread(target=self.read_handler)
            self.read_thread.start()

    # handler for reading incoming raw HID packets via PyUSB (thread started in local open() method)
    def read_handler(self):
        logging.debug("linux hid read thread start")
        while self.dev:
            #self.active() 
            buf = self.input_read()
            if len(buf) > 0 and buf[0] > 0:
                buf.insert(1, len(buf)+1) 
                if buf[0] == 0xa:
                    self.result_buf.append(buf)
                    logging.debug("linux hid get result")
                else:
                    self.hidraw_buf.append(buf)
                logging.debug("linux hid get ep_in data {}".format(bytes(buf).hex()))
            #logging.debug("[error]linux hid read thread")

        logging.debug("linux hid read thread exited")


    def get_desc(self):
        return self.desc

    def get_in_report_data_length(self):
        return self.desc.internal_max_in_report_len

    def get_out_report_data_length(self):
        return self.desc.internal_max_out_report_len

    def _ioctl(self, *args, **kwargs):
        result = 0
        try:
            result = fcntl.ioctl(self.dev, *args, **kwargs)
        except (IOError, OSError) as e:
            print("{}, {}, {}, {}".format(e,e.errno, e.filename,dir(e)))
        if result < 0:
            print(result)
            raise IOError(result)

        return result

    def feature_write(self, report):
        buf = (ctypes.c_char * len(report)).from_buffer(bytearray(report))
        self._ioctl(HIDIOCSFEATURE(len(report)), buf, True)
        logging.debug("feature_write: {}".format(buf))

    def feature_read(self, report_id=0x21):
        logging.debug("feature_read: {}".format(report_id))
        size = 64 * (report_id & 0xf)
        raw_data_type = ctypes.c_ubyte * size
        raw_data = raw_data_type()
        raw_data[0] = report_id
        self._ioctl(HIDIOCGFEATURE(size), raw_data, True)

        return list(bytearray(raw_data))


    def output_write(self, packet):
        """See base class."""
        out = bytearray(packet)    # Prepend the zero-byte (report ID)
        os.write(self.dev, out)
        logging.debug("output report: {}".format(list(out)))

    def input_read(self):
        """See base class."""
        raw_in = os.read(self.dev, self.get_in_report_data_length())
        decoded_in = list(bytearray(raw_in))
        logging.debug("input report: {}".format(decoded_in))
        return decoded_in

    def read(self, report_id=0x21):
        ret = False
        buf = []
        if report_id > 0x60 or (self.desc.chip == 819 and self.desc.bus == "USB"):
            buf = self.feature_read(report_id)
        else:
            buf = self.input_read()
            buf.insert(1, len(buf)+1) 
        if len(buf) > 0:
            logging.debug('device read:[{}]'.format(', '.join(hex(x) for x in buf)))
            ret = True
        print(buf)
        return ret,buf

    def write(self, addr, data=[], report_id=0x21):
        buf = []
        size = 0
        if report_id < 0x60:
            if self.desc.bus == "USB":
                if self.desc.chip == 819:
                    size = 64 * (report_id & 0xf)
                    length = 3 + len(addr) + len(data)
                    buf = [report_id, length>>8, length & 0xff]
                elif self.desc.chip == 817:
                    size = 64
                    length = 1 + len(addr) + len(data)
                    buf = [0x9, length & 0xff]
            elif self.desc.bus == "HID_OVER_I2C":
                if self.desc.chip == 819:
                    length = 2 + len(addr) + len(data)
                    size = length
                    buf = [0x9, 0]
        else:
            buf = [report_id]
            size = len(addr) + 1
        buf.extend(addr)
        if len(data) > 0:
            buf.extend(data)
        buf.extend([0] * (size - len(buf)))
 
        if self.dev:
            logging.debug('device write:[{}]'.format(', '.join(hex(x) for x in buf)))
            if report_id > 0x60 or (self.desc.chip == 819 and self.desc.bus == "USB"):
                self.feature_write(buf)
            else:
                self.output_write(buf)
            return True
        logging.info("cmd[{}] fail".format(addr));
        return False

    def close(self):
        os.close(self.dev)
        self.dev = None
