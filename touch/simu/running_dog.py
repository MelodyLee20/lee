import numpy as np

class RunningDog:

    def __init__(self, x0=0.0, y0=0.0, dt=0.1):

        self.x = x0
        self.y = y0
        self.vx = None
        self.vy = None

        self.dt = dt

    def run(self, vx=1.0, vy=1.0):

        self.vx = vx
        self.vy = vy

        self.x = self.x + self.vx*self.dt
        self.y = self.y + self.vy*self.dt

        # Add noise
        x_noise = 0.1*np.random.normal()
        y_noise = 0.1*np.random.normal()
        self.x = self.x * (1.0 + x_noise)
        self.y = self.y * (1.0 + y_noise)

        return (self.x, self.y)

    def get_pos(self):
        return (self.x, self.y)

    def get_vel(self):
        return (self.vx, self.vy)

