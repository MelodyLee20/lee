import numpy as np
from lmfit import Parameters, Model
#from scipy.optimize import curve_fit
from sklearn.metrics import r2_score


class SimpleModelLmfit:
    def __init__(self, ratio_x=None, ratio_y=None):

        """
        Simple fitting.

        ratio_x: float
            Resolution ratio of target to TP in x direction.
        ratio_y: float
            Resolution ratio of target to TP in y direction.
        """

        self.ratio_x = ratio_x
        self.ratio_y = ratio_y

        self.params_x = None
        self.params_y = None
        self.score = None
        self.pred_points = None

    @staticmethod
    def model_func(x, y, A, B, C, D, E):
        """
        Simulation model.

        out = (Ax*x+Bx*y+Cx) / (Dx*x+Ex*y+1)

        Parameters
        ----------
        point: array-like
            TP point.
        """

        out = (A*x + B*y + C) / (1.0 + D*x + E*y)

        return out

    def to_args(self, params):
        # Convert the param into arguments

        A = params['A']
        B = params['B']
        C = params['C']
        D = params['D']
        E = params['E']

        args = (A, B, C, D, E)

        return args

    def to_array(self, params):
        # Convert the params into an array.
        return np.array(self.to_args(params))

    def forward(self, point, params_x, params_y):
        """
        Obtain the target coordinate.

        x' = (Ax*x+Bx*y+Cx) / (Dx*x+Ex*y+1)
        y' = (Ay*x+By*y+Cy) / (Dy*x+Ey*y+1)

        Parameters
        ----------
        point: array-like
            TP point.
        """

        (x, y) = point

        Ax, Bx, Cx, Dx, Ex = self.to_args(params_x)
        Ay, By, Cy, Dy, Ey = self.to_args(params_y)

        x = self.model_func(x, y, Ax, Bx, Cx, Dx, Ex)
        y = self.model_func(x, y, Ay, By, Cy, Dy, Ey)

        return (x, y)

    def reverse(self, point, params_x, params_y):
        '''
        Obtain the TP point.

        point: array-like
            Target point.
        '''

        (x, y) = point

        Ax, Bx, Cx, Dx, Ex = self.to_args(params_x)
        Ay, By, Cy, Dy, Ey = self.to_args(params_y)

        x_tp = ( (x-Cx)*(By-Ey*y)-(y-Cy)*(Bx-Ex*x) ) / ( (Ax-Dx*x)*(By-Ey*y)-(Ay-Dy*y)*(Bx-Ex*x) )
        y_tp = ( (x-Cx)*(Ay-Dy*y)-(y-Cy)*(Ax-Dx*x) ) / ( (Bx-Ex*x)*(Ay-Dy*y)-(By-Ey*y)*(Ax-Dx*x) )

        return (x_tp, y_tp)

    def to_target_points(self, points, params_x, params_y):
        # Mapping into target points


        target_points = []
        for point in points:
            out = self.forward(point, params_x, params_y)
            target_points.append(out)

        target_points = np.array(target_points)

        return target_points

    def to_tp_points(self, points, params_x, params_y):
        # Mapping into TP points

        tp_points = []
        for point in points:
            out = self.reverse(point, params_x, params_y)
            tp_points.append(out)

        tp_points = np.array(tp_points)

        return tp_points

    def get_init_solutions(self):
 
        # Ax, ..., Ex
        params_x = Parameters()
        params_x.add('A', value=self.ratio_x)
        params_x.add('B', value=0.0)
        params_x.add('C', value=0.0)
        params_x.add('D', value=0.0)
        params_x.add('E', value=0.0)

        # Ay, ..., Ey
        params_y = Parameters()
        params_y.add('A', value=0.0)
        params_y.add('B', value=self.ratio_y)
        params_y.add('C', value=0.0)
        params_y.add('D', value=0.0)
        params_y.add('E', value=0.0)

        return params_x, params_y

    def fit(self, tp_points, target_points, method='leastsq'):
        '''
        Fit the model.

        Parameters
        ----------
        target_points: 2d array
            Target point array in shape of (num_points, 2).
        tp_points: 2d array
            Target point array in shape of (num_points, 2).
        method: str
            Method used for fitting, available options: 'leastsq', 'lm', 'trf' and 'dogbox'
        '''

        # Initial solutions
        params_x, params_y = self.get_init_solutions()

        # Models
        model = Model(self.model_func, independent_vars=['x', 'y'])

        # Fitting
        result_x = model.fit(target_points[:,0], params_x, x=tp_points[:,0], y=tp_points[:,1],
                             method=method)
        result_y = model.fit(target_points[:,1], params_y, x=tp_points[:,0], y=tp_points[:,1],
                             method=method)

        popt_x = result_x.best_values
        popt_y = result_y.best_values

        # Make predictions
        self.pred_points = self.to_target_points(tp_points, popt_x, popt_y)

        # Test score (R2 score). In perfect situation, the score is 1.
        err_x = result_x.redchi
        err_y = result_y.redchi
        self.score = 1 - err_x - err_y

        # self.score = r2_score(target_points, self.pred_points)

        return self.score, popt_x, popt_y


