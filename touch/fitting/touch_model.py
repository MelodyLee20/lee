import numpy as np
from scipy.optimize import minimize
from sklearn.metrics import r2_score


class TouchModel:
    def __init__(self):

        """
        Simple fitting.
        """
        self.result = None # Fitting result

    def forward(self, point, params):
        """
        Obtain the target coordinate.

        x' = (Ax*x+Bx*y+Cx) / (Dx*x+Ex*y+1)
        y' = (Ay*x+By*y+Cy) / (Dy*x+Ey*y+1)

        Parameters
        ----------
        point: array-like
            touch point.
        """

        x, y = point
        Ax, Bx, Cx, Ay, By, Cy, R1, R2, R3, R4 = params

        x_out = (Ax*x+Bx*y+Cx)/(1.0+R1*x+R2*y)
        y_out = (Ay*x+By*y+Cy)/(1.0+R3*x+R4*y)

        point_out = np.array([x_out, y_out])

        return point_out

    def reverse(self, point, params):
        '''
        Obtain the input point.

        point: array-like
            Target point.
        '''

        x, y = point
        Ax, Bx, Cx, Ay, By, Cy, R1, R2 = params

        x_out = ( (x-Cx)*(By-R2*y)-(y-Cy)*(Bx-R2*x) ) / ( (Ax-R1*x)*(By-R2*y)-(Ay-R1*y)*(Bx-R2*x) )
        y_out = ( (x-Cx)*(Ay-R1*y)-(y-Cy)*(Ax-R1*x) ) / ( (Bx-R2*x)*(Ay-R1*y)-(By-R2*y)*(Ax-R1*x) )

        point_out = np.array([x_out, y_out])

        return point_out

    def to_target_points(self, points, params):
        # Mapping into target points

        target_points = []
        for point in points:
            out = self.forward(point, params)
            target_points.append(out)

        target_points = np.array(target_points)

        return target_points

    def to_input_points(self, points, params):
        # Mapping into input points

        input_points = []
        for point in points:
            out = self.reverse(point, params)
            input_points.append(out)

        input_points = np.array(input_points)

        return input_points

    def to_dict(self, a):
        # Convert the array into a dict.

        pdict = {'Ax': a[0], 'Bx': a[1], 'Cx': a[2], 'Ay': a[3], 'By': a[4], 'Cy': a[5], 'R1': a[6],'R2': a[7]}

        return pdict

    def to_array(self, d):
        # Convert the dict into an array.

        popt = np.array([d['Ax'], d['Bx'], d['Cx'], d['Ay'], d['By'], d['Cy'], d['R1'], d['R2']])

        return popt

    def find_distance(self, point0, point1):

        x0, y0 = point0
        x1, y1 = point1

        d = np.sqrt((x1-x0)**2 + (y1-y0)**2)

        return d

    def cost_func(self, params, input_points, target_points):
        '''
        Cost function.

        Parameters
        ----------
        params: array-like
            The independent parameters.

        input_points: 2d array
            Touch point array in shape of (num_points, 2).

        target_points: 2d array
            Target point array in shape of (num_points, 2).
        '''

        num_points = len(target_points)

        cost = 0.0
        for i in range(num_points):
            input_point = input_points[i]
            target_point = target_points[i]

            pred_point = self.forward(input_point, params)
            d = self.find_distance(pred_point, target_point)
            cost += d

        return cost


    def fit(self, input_points, target_points, options=None):
        '''
        Fit the model.

        Parameters
        ----------
        input_points: 2d array
            Touch point array in shape of (num_points, 2).

        target_points: 2d array
            Target point array in shape of (num_points, 2).

        method: str
            Method used for fitting, available options: 'BFGS', 'Powell', 'Nelder-Mead'.
        '''

        # Guessed solution
        params0 = (1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        # Cost function
        def func(params, input_points, target_points):
            return self.cost_func(params, input_points, target_points)


        # Method: 'BFGS', 'Powell', 'Nelder-Mead'
        method = 'BFGS'

        # Fitting
        self.result = minimize(func, params0, args=(input_points, target_points), method = method, options=options)

        # Optimal solution
        popt = self.result.x

        # Make predictions
        pred_points = self.to_target_points(input_points, popt)

        # Test score (R2 score). In perfect situation, the score is 1.
        score = r2_score(target_points, pred_points)

        return score, popt


