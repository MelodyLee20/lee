
import struct
import time
import sys
import os


def list_devices():

    root_dir = '/dev/input'
    items = os.listdir(root_dir)
    paths = []
    for item in sorted(items):
        if 'event' in item:
            device_name = os.path.basename(item)
            path = root_dir + '/' + device_name
            paths.append(path)

    return paths

def get_devices():

    devices = [InputDevice(path) for path in list_devices()]
    return devices

def get_device_list():

    items = []
    for device in get_devices():
        item = "{}, {}, {}".format(device.path, device.name, device.phys)
        items.append(item)

    return items


class InputDevice():

    def __init__(self, path):

        self.sys_root_dir = '/sys/class/input'
        self.dev_root_dir = '/dev/input'

        self.path = path
        self.name = self.read_attribute('name')
        self.phys = self.read_attribute('phys')



    def read_attribute(self, target):
        # Read the attribute.
        # target: 'name' or 'phys'

        node_name = os.path.basename(self.path)
        path = os.path.join(self.sys_root_dir, node_name, 'device', target)

        with open(path, 'r') as f:
            content = f.read().rstrip() # Read and strip the trailing newline symbol

        return content


    def read_loop(self):

        # Format: long int, long int, unsigned short, unsigned short, int
        FORMAT = 'llHHi'
        EVENT_SIZE = struct.calcsize(FORMAT)

        # Open the file in binary mode
        with open(self.path, "rb") as f:

            raw = f.read(EVENT_SIZE)
            while raw:
                (tv_sec, tv_usec, type, code, value) = struct.unpack(FORMAT, raw)

                event = {'sec': tv_sec, 'usec': tv_usec, 'type': type, 'code': code, 'value': value}
                yield event

                raw = f.read(EVENT_SIZE)


