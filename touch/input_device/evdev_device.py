import evdev

def list_devices():

    devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
    for device in devices:
        print(device.path, device.name, device.phys)

def get_device_list():

    devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
    items = []
    for device in devices:
        item = "{} {} {}".format(device.path, device.name, device.phys)
        items.append(item)

    return items

def get_touch_spec(device):
    '''
    Returns the touch specification.
    '''

    cap = device.capabilities(verbose=True)

    root = cap[('EV_ABS', 3)]

    x_min = root[3][1].min
    x_max = root[3][1].max

    y_min = root[4][1].min
    y_max = root[4][1].max

    tracking_id_min = root[5][1].min
    tracking_id_max = root[5][1].max

    spec = {'x_min': x_min, 'x_max': x_max, 'y_min': y_min, 'y_max': y_max,
            'tracking_id_min': tracking_id_min, 'tracking_id_max': tracking_id_max}

    return spec

def get_pen_spec(device):
    '''
    Returns the pen specification.
    '''

    cap = device.capabilities(verbose=True)

    root = cap[('EV_ABS', 3)]

    x_min = root[0][1].min
    x_max = root[0][1].max

    y_min = root[1][1].min
    y_max = root[1][1].max

    pressure_min = root[2][1].min
    pressure_max = root[2][1].max

    tilt_x_min = root[3][1].min
    tilt_x_max = root[3][1].max

    tilt_y_min = root[4][1].min
    tilt_y_max = root[4][1].max


    spec = {'x_min': x_min, 'x_max': x_max, 'y_min': y_min, 'y_max': y_max,
            'tilt_x_min': tilt_x_min, 'tilt_x_max': tilt_x_max, 'tilt_y_min': tilt_y_min, 'tilt_y_max': tilt_y_max}

    return spec

