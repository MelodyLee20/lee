
class PenPoint(object):
    # Pen point.

    def __init__(self):

        self.timestamp = None
        self.x = None
        self.y = None
        self.pressure = None
        self.tilt_x = None
        self.tilt_y = None


    def refresh(self):

        self.timestamp = None
        self.x = None
        self.y = None
        self.pressure = None
        self.tilt_x = None
        self.tilt_y = None

    def is_valid(self):
        # Is it a valid point or not.

        out = True
        if self.timestamp and self.x and self.y:
            out = True
        else:
            out = False

        return out

    def to_dict(self):

        data = {
            'TIMESTAMP': self.timestamp, 'X': self.x, 'Y': self.y,
            'PRESSURE': self.pressure,
            'TILT_X': self.tilt_x, 'TILT_Y': self.tilt_y
        }

        return data

def get_pen_point(event, point):
    '''
    Returns the point data of the pen.

    For pen:
        type 0         => SYN_REPORT
        type 3 code 0  => ABS_X
        type 3 code 1  => ABS_Y
        type 3 code 24 => ABS_PRESSURE

    '''

    point_data = None

    sec = event['sec']
    usec = event['usec']
    code = event['code']
    type = event['type']
    value = event['value']

    timestamp = str(sec) + '.' + str(usec)
    # Update timestamp
    if point.timestamp is None:
        point.timestamp = timestamp

    # Update the point data when it is valid
    if type == 0: # SYN_REPORT
        # Save data
        if point.is_valid():
            point_data = point.to_dict()
        # refresh the point
        point.refresh()

    if type == 3 and code == 0: # ABS_X
        point.x = value
    if type == 3 and code == 1: # ABS_Y
        point.y = value
    if type == 3 and code == 24: # ABS_PRESSURE
        point.pressure = value

    return point_data
