
import cv2 as cv

def flip_image(image, flag=0):
    """
    Flip the image.

    Parameters
    ----------
    image: ndarray
        Image.
    flag: int
        Flip flag. 0 -> vertically and others -> horizontally.
    """
    if flag == 0: # Vertially
        return cv.flip(image, 0)
    else: # Horizontally
        return cv.flip(image, 1)
