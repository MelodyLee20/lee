import cv2 as cv

def get_contour_center(c):
    M = cv.moments(c)
    xc = int(M["m10"] / M["m00"])
    yc = int(M["m01"] / M["m00"])

    return xc, yc