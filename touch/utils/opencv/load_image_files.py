import os
import re

import cv2 as cv

import face_recognition

def tryint(s):
    try:
        return int(s)
    except:
        return s

def alphanum_key(s):
    """ Turn a string into a list of string and number chunks.
        "z23a" -> ["z", 23, "a"]
    """
    return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def natural_sorted(l):
    """ Sort the given list in the way that humans expect.
    """
    return sorted(l, key=alphanum_key)


def get_filenames(dir_path, ext="jpg"):
    # Return the filenames with specific extention.

    filenames = []
    for f in os.listdir(dir_path):
        if f.endswith(ext):
            filenames.append(f)

    filenames = natural_sorted(filenames)

    return filenames

def load_image_files(dir_path, ext="jpg"):
    # Load images.

    filenames = get_filenames(dir_path, ext=ext)

    images = []
    for filename in filenames:
        filepath = os.path.join(dir_path, filename)
        image = cv.imread(filepath)
        images.append(image)

    return images

