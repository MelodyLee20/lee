import numpy as np
from ..filters import KalmanFilter


class KalmanTracer2d(object):
    # Kalman tracer 2d.


    def __init__(self, x0, y0, dt, P_var=1.0, Q_var=1.0, R_var=1.0):

        # Kalman filter
        vx = 0.0 # Inital velocity in x-axis.
        vy = 0.0 # Inital velocity in y-axis.
        self.state_x = np.array([x0, vx]).T
        self.state_y = np.array([y0, vy]).T

        F = np.array([[1.0, dt],[0.0, 1.0]]) # State transition.
        H = np.array([[1.0, 0.0]]) # Observe
        P = np.array([[P_var, 0.0], [0.0, P_var]])
        Q = np.array([[Q_var, 0.0], [0.0, Q_var]])
        R = np.array([[R_var]])

        self.kf_x = KalmanFilter(state=self.state_x, F=F, P=P, Q=Q, H=H, R=R)
        self.kf_y = KalmanFilter(state=self.state_y, F=F, P=P, Q=Q, H=H, R=R)

    def update(self, x, y):

        self.kf_x.predict()
        self.kf_x.update(x)
        self.state_x = self.kf_x.get_state()

        self.kf_y.predict()
        self.kf_y.update(y)
        self.state_y = self.kf_y.get_state()

    def get_pos(self):
        # Get the position.

        x = self.state_x[0]
        y = self.state_y[0]

        return (x, y)

    def get_vel(self):
        # Get the velocity.

        vx = self.state_x[1]
        vy = self.state_y[1]

        return (vx, vy)
