# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 16:12:27 2019

@author: carey
"""

import os
import sys
import time
import logging
sys.path.append("../../../")
import touch.fwio2 as fwio
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.httpserver
NET_PORT = 8050

def callback(ws, status, msg, data=[]):
    print("get callback")
    print([status, msg, [hex(item).upper() for item in data[1:]]])
    ws_data = {"type":"devStatus","data":msg}
    ws.send_updates(ws_data)

if __name__ == '__main__':
    touch_dev = fwio.core(logging.DEBUG)
    if touch_dev.search() == True:
    
        def handler(data):
            print("careytest {}".format(data))
            if data['cmd'] == "devOperation":
                if data['value'] == "cdcOn":
                    print("switch to CDC device")
                    touch_dev.enter_cdc_mode()
                    print(touch_dev.get_hidraw())
            if data['cmd'] == "requestFinalization":
                # Finalize the run.
                print('close touch device.')
                touch_dev.close()
        touch_dev.open()
        touch_dev.set_callback(callback)
        touch_dev.start_server(handler)

    touch_dev.close()

