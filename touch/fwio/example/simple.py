# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 16:12:27 2019

@author: carey
"""

import os
import sys
import time
import logging
sys.path.append("../../../")
import touch.fwio2 as fwio

def callback(ws, status, id, msg, data=[]):
    print("get callback, id:{}".format(id))
    if type(data) == 'list':
        print("\033[41m get data {} \033[0m".format([status, msg, [hex(item).upper() for item in data[1:]]]))
    else:
        print("\033[41m get data {} \033[0m".format([status, msg, data]))
    if ws:
        ws.send_updates(msg)

if __name__ == '__main__':
    touch_dev = fwio.core(logging.DEBUG, ui="none")
    if touch_dev.search() == True:
        touch_dev.start_server(cb=callback)
        touch_dev.open()
        ret = touch_dev.get_slave_num()
        time.sleep(1)
        print(touch_dev.master_chip_info)
        for i in range(touch_dev.master_chip_info["slave_num"] + 1):
            ret = touch_dev.get_tx_rx_size(chip_address=i)
        ret = touch_dev.get_nvb()
        time.sleep(1)
        rawdata = []
        counter = 0
        def rawdata_callback(ws, status, id, msg, data=[]):
            global counter, rawdata
            if status == 0:
                rawdata.append(data.copy())
                counter += 1
                print("\033[43m rawdata counter: {}\033[0m".format(counter))
            if counter == touch_dev.master_chip_info["slave_num"] + 1:
                print("\033[42m col:{}, row:\033[0m".format(len(data)))
        for i in range(touch_dev.master_chip_info["slave_num"] + 1):
            ret = touch_dev.get_rawdata(cb=rawdata_callback,chip_address=i)
        ret = touch_dev.enter_cmd_mode()
        ret = touch_dev.leave_cmd_mode()
    time.sleep(10)
    touch_dev.close()

