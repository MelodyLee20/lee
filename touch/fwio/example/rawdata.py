# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 16:12:27 2019

@author: carey
"""

import os
import sys
import time
import logging
sys.path.append("../../../")
import touch.fwio2 as fwio
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.httpserver

def callback(ws, status, msg, data=[]):
    print("get callback")
    print([status, msg, [hex(item).upper() for item in data[1:]]])
    ws_data = {"type":"devStatus","data":msg}

if __name__ == '__main__':
    touch_dev = fwio.core(logging.DEBUG, ui="pyqt")
    if touch_dev.search() == True:
        counter = 0 
        touch_dev.open()
        touch_dev.set_callback(callback)
        touch_dev.online_train_mode(train_type=2, total_negative_wt=128)
        time.sleep(3)
    touch_dev.close()

