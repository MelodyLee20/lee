# -*- encoding: utf-8 -*-
import threading
import time, sys
import traceback
import logging
import collections

class job_thread(threading.Thread):
    def __init__(self, queue_name, sleep_time=0.01, fifo=True, timeout=30):
        threading.Thread.__init__(self)
        self.queue_name = queue_name
        self.sleep_time = sleep_time
        self.command_cb = None 
        self.timeout = timeout
        self.fifo = fifo # True is fifo, False is Lifo 
        self._stop = False
        self.start_job_time = 0
        self.job_queue = collections.deque(maxlen=2048)
        self.io_buf = [] 
        self.ui = "web" 
        self.io_loop = None
        self.dev = None 

    def set_dev(self, dev, io_loop):
        self.dev = dev
        self.desc = dev.get_desc()
        self.io_loop = io_loop 


    def set_callback(self, cb):
        print("set core cb {}".format(cb))
        self.command_cb = cb

    def add_job(self, job):
        self.job_queue.append(job)

    def run(self):
        print("run job_thread")
        while not self._stop:
            if len(self.job_queue) > 0:
                if self.fifo == True:
                    self.current_job = self.job_queue.popleft()
                else:
                    self.current_job = self.job_queue.pop()
                self.start_job(self.current_job)

            time.sleep(self.sleep_time)

    def command_maker(self, cmd_id, addr, data, chip_address, device="sis"):
        buf = [cmd_id]
        print("chip_address:{}, buf:{}".format(chip_address,buf))
        buf.extend(addr)
        if device =="usi":
           data = [] 
        elif self.desc.bus == "HID_OVER_I2C" or self.desc.chip == 817 or chip_address > 0:
            buf.extend(data) 
            print("chip_address:{}, buf:{}".format(chip_address,buf))
            crc = self.crc16(buf)
            if chip_address > 0:
                tmp = [0xb0, crc, 0x10+chip_address, 0] 
                tmp.extend(buf)
                buf = tmp
            else:
                buf.insert(1, crc)
            data = []
        return buf,data

    def start_job(self, job):
        self.start_job_time = int(time.time())
        logging.debug("\033[44;33m [do job] {}\033[0m ".format(self.current_job))
        ret = False
        if self.dev != None:
            job = self.current_job
            cmd_id = job["cmd_id"]
            report_id = job["report_id"]
            read_id = job["read_id"]
            if read_id == 0:
                read_id = report_id
            addr = job["addr"]
            data = job["data"]
            chip_address = job["chip_address"]
            read_size = job["read_size"]
            device = job["device"]
            self.io_buf.clear()
            addr, data = self.command_maker(cmd_id, addr, data, chip_address, device)
            ret = self.dev.write(addr, data, report_id=report_id)
            if ret == True:
                while bool(self.current_job):
                    ret, buf = self.dev.read(report_id=read_id)
                    #work around for usi
                    if device =="usi" and buf[1]==0 and buf[2] == 0 and read_size < 64: 
                        ret, buf = self.dev.read(report_id=read_id)
                    logging.debug("dev read ret:{}, data:{}".format(ret,buf))
                    if ret == True:
                        self.job_result(ret, buf)
                    if self.timeout + self.start_job_time < int(time.time()):
                        ret = False
                        self.current_job = {}
                        break
        else:
            self.current_job = {}
        if ret == False:
            self.return_cb(-1, id, job)
 
        self.start_job_time = 0

    def job_result(self, status, buf):
        if self.dev != None:
            job = self.current_job.copy()
            report_id = job["report_id"]
            device = job["device"]
            read_id = job["read_id"]
            read_size = job["read_size"]
            id = job["id"]
            if read_id == 0:
                read_id = report_id
            logging.debug("len:{}, job:{}".format(len(buf), job))
            #print("buf {}".format(buf))
            head_size = 1

            if status == True and len(buf) >= head_size:
                ack = 0
                size = head_size
                if device =="usi":
                    if buf[0] == read_id:
                        ack = 1
                        if read_size > 0:
                            size = read_size + head_size
                elif self.desc.bus == "USB":
                    if (self.desc.chip == 819 and buf[4] == 0xbe and buf[5] == 0xef) or (self.desc.chip == 817 and buf[5] == 0xbe and buf[4] == 0xef) :
                        ack = 1
                        if self.desc.chip == 819:
                            head_size = 8
                            size = (buf[1] << 8) + buf[2]
                        else: 
                            size = buf[1]
                elif self.desc.bus == "HID_OVER_I2C":
                    head_size = 7
                    if  buf[4] == 0xbe and buf[3] == 0xef:
                        ack = 1
                        size = buf[1]
                if ack == 1:
                    self.io_buf.extend(buf[head_size:size])
                    logging.debug("device read len:{}, size:{}, buf:{}".format(len(self.io_buf), size, buf[head_size:size]))
                    if len(self.io_buf) >= read_size or read_size == 0:
                        self.current_job = {}
                        status = 0
                        self.return_cb(0, id, job)
                    return
                self.current_job = {}
            self.return_cb(-1, id, job)

    def return_cb(self, status, id, job, msg=None, data=None):
        success_str = "successful"
        fail_str = "failed"
        read_size = job["read_size"]
        parser = job["parser"]
        callback = job["callback"]
        if callback is None:
            callback = self.command_cb
        if parser is None:
            parser = self.dummy_parser
        def web_cb(callback, status, id, msg, data=None):
            print(callback)
            callback(status, id, msg, data)
        if status == 0: # status -1: false, 0: success, 1: pending
            data, status = parser(self.io_buf[:read_size], job)
            if status == 0: 
                if self.ui == "web":
                    self.io_loop.add_callback(web_cb, callback, status, id, success_str, data)
                else:
                    callback(status, id, success_str, data)
        if status == -1: # status -1: false, 0: success, 1: pending
            if self.ui == "web":
                self.io_loop.add_callback(web_cb, callback, status, id, fail_str)
            else:
                callback(status, id, fail_str)

    def dummy_parser(self, data, job):
        return data, 0

    def stop(self):
        self._stop = True

    def crc16(self, datas, crc_old=0):
        tab= [ 0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
                0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
                0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
                0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
                0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
                0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
                0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
                0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
                0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
                0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
                0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
                0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
                0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
                0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
                0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
                0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
                0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
                0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
                0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
                0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
                0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
                0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
                0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
                0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
                0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
                0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
                0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
                0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
                0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
                0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
                0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
                0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0]
        crc = crc_old
        for data in datas:
            tmp = ((crc >> 8) ^ data) & 0xff
            crc = (crc << 8) ^ tab[tmp]
            crc = crc & 0xffff
        return crc & 0xff



