# -*- coding: utf-8 -*-
import logging
from .command import command 
import sys
import platform
import collections
import threading, time

def common_cmd_handler(data):
    pass

class core:
    def __init__(self, debug=logging.ERROR, ui="web"):
        self.dev = None
        self.desc = None
        self.sis_vid = 0x457
        self.ui = ui
        self.master_chip_info = {"tx":0,"rx":0,"nvb":0,"diff_nvb":0,"slave_num":0}
        self.slave_chip_info = [] 
        self.debug = debug
        self.command_builder = command.command_builder(debug)
        logging.basicConfig(level=debug, format='%(asctime)s -(%(filename)s)%(levelname)s : %(message)s')

    def open(self, enable_read_thread=True, io_loop=None, cb=None):
        if self.dev != None:
            self.dev.open(enable_read_thread=enable_read_thread)
            self.command_builder.setup(self.dev, self.desc, io_loop, cb)
            print("open device")
            return True
        return False

    def search(self, dev_path=None):
        dev = None
        ret = False
        logging.debug(platform.system())
        if platform.system() == "Windows":
            from .device_io import windows
            #from .device_io import pywinusb_io
            #dev = pywinusb_io.io(self.debug)
            dev = windows.io(self.debug)
        elif platform.system() == "Darwin":
            from .device_io import pyhidapi_io
            dev = pyhidapi_io.io(self.debug)
        else:
            from .device_io import linux 
            dev = linux.io(self.debug)
        if dev_path is not None:
            ret = dev.search(dev_path=dev_path)
        else:
            ret = dev.search(vid=self.sis_vid)
        if ret == True:
            self.dev = dev
            self.desc = dev.get_desc()
        return ret 

    def close(self):
        if self.dev != None:
            self.dev.close()
            self.dev = None
            self.command_builder.close()

    def get_hidraw(self):
        if len(self.dev.hidraw_buf) > 0:
            buf = self.dev.hidraw_buf.copy()
            self.dev.hidraw_buf.clear()
            return buf

    def execute(self, id, method, command="", property="", action=None, value=[], callback=None):
        self.command_builder.build(id, method, command, property, action, value, callback)


