"""Implements raw HID device communication on Windows."""

import ctypes
from ctypes import wintypes

import platform

import struct
import win32api
import win32file
import pywintypes
import logging
import threading
from .win32_hidraw import *

# Load relevant DLLs
hid = ctypes.windll.Hid
setupapi = ctypes.windll.SetupAPI
kernel32 = ctypes.windll.Kernel32

class HidError(Exception):
    """in the hid usb transport protocol."""
    pass

class OVERLAPPED(ctypes.Structure):
    class OFFSET_OR_HANDLE(ctypes.Union):
        class OFFSET(ctypes.Structure):
            _fields_ = [
                ("offset",      wintypes.DWORD),
                ("offset_high", wintypes.DWORD) ]

        _fields_ = [
                ("offset",      OFFSET),
                ("pointer",     ctypes.c_void_p) ]
    _fields_ = [
        ("internal",        ctypes.POINTER(ctypes.c_ulong)),
        ("internal_high",   ctypes.POINTER(ctypes.c_ulong)),
        ("u",               OFFSET_OR_HANDLE),
        ("h_event",         wintypes.HANDLE)
    ]

# Various structs that are used in the Windows APIs we call
class GUID(ctypes.Structure):
    _fields_ = [("Data1", ctypes.c_ulong),
                            ("Data2", ctypes.c_ushort),
                            ("Data3", ctypes.c_ushort),
                            ("Data4", ctypes.c_ubyte * 8)]

# On Windows, SetupAPI.h packs structures differently in 64bit and
# 32bit mode.    In 64bit mode, thestructures are packed on 8 byte
# boundaries, while in 32bit mode, they are packed on 1 byte boundaries.
# This is important to get right for some API calls that fill out these
# structures.
if platform.architecture()[0] == "64bit":
    SETUPAPI_PACK = 8
elif platform.architecture()[0] == "32bit":
    SETUPAPI_PACK = 1
else:
    raise HidError("Unknown architecture: %s" % platform.architecture()[0])


class DeviceInterfaceData(ctypes.Structure):
    _fields_ = [("cbSize", wintypes.DWORD),
                            ("InterfaceClassGuid", GUID),
                            ("Flags", wintypes.DWORD),
                            ("Reserved", ctypes.POINTER(ctypes.c_ulong))]
    _pack_ = SETUPAPI_PACK


class DeviceInterfaceDetailData(ctypes.Structure):
    _fields_ = [("cbSize", wintypes.DWORD),
                            ("DevicePath", ctypes.c_byte * 1)]
    _pack_ = SETUPAPI_PACK


class HidAttributes(ctypes.Structure):
    _fields_ = [("Size", ctypes.c_ulong),
                            ("VendorID", ctypes.c_ushort),
                            ("ProductID", ctypes.c_ushort),
                            ("VersionNumber", ctypes.c_ushort)]


class HidCapabilities(ctypes.Structure):
    _fields_ = [("Usage", ctypes.c_ushort),
                            ("UsagePage", ctypes.c_ushort),
                            ("InputReportByteLength", ctypes.c_ushort),
                            ("OutputReportByteLength", ctypes.c_ushort),
                            ("FeatureReportByteLength", ctypes.c_ushort),
                            ("Reserved", ctypes.c_ushort * 17),
                            ("NotUsed", ctypes.c_ushort * 10)]

# Various void* aliases for readability.
HDEVINFO = ctypes.c_void_p
HANDLE = ctypes.c_void_p
PHIDP_PREPARSED_DATA = ctypes.c_void_p    # pylint: disable=invalid-name

# This is a HANDLE.
INVALID_HANDLE_VALUE = 0xffffffff

# Status codes
NTSTATUS = ctypes.c_long
HIDP_STATUS_SUCCESS = 0x00110000
FILE_SHARE_READ = 0x00000001
FILE_SHARE_WRITE = 0x00000002
OPEN_EXISTING = 0x03
ERROR_ACCESS_DENIED = 0x05

# CreateFile Flags
GENERIC_WRITE = 0x40000000
GENERIC_READ = 0x80000000

# Function signatures
hid.HidD_GetHidGuid.restype = None
hid.HidD_GetHidGuid.argtypes = [ctypes.POINTER(GUID)]
hid.HidD_GetAttributes.restype = wintypes.BOOLEAN
hid.HidD_GetAttributes.argtypes = [HANDLE, ctypes.POINTER(HidAttributes)]
hid.HidD_GetPreparsedData.restype = wintypes.BOOLEAN
hid.HidD_GetPreparsedData.argtypes = [HANDLE,
                                                                            ctypes.POINTER(PHIDP_PREPARSED_DATA)]
hid.HidD_FreePreparsedData.restype = wintypes.BOOLEAN
hid.HidD_FreePreparsedData.argtypes = [PHIDP_PREPARSED_DATA]
hid.HidD_GetProductString.restype = wintypes.BOOLEAN
hid.HidD_GetProductString.argtypes = [HANDLE, ctypes.c_void_p, ctypes.c_ulong]
hid.HidP_GetCaps.restype = NTSTATUS
hid.HidP_GetCaps.argtypes = [PHIDP_PREPARSED_DATA,
                                                         ctypes.POINTER(HidCapabilities)]
hid.HidD_GetFeature.restype = wintypes.BOOLEAN
hid.HidD_GetFeature.argtypes = [HANDLE, ctypes.c_void_p, ctypes.c_ulong]

#hid.HidD_SetFeature.restype = wintypes.BOOL
#hid.HidD_SetFeature.argtypes = [HANDLE, ctypes.POINTER(wintypes.DWORD), ctypes.c_ulong]
setupapi.SetupDiGetClassDevsA.argtypes = [ctypes.POINTER(GUID), ctypes.c_char_p,
                                                                                    wintypes.HWND, wintypes.DWORD]
setupapi.SetupDiGetClassDevsA.restype = HDEVINFO
setupapi.SetupDiEnumDeviceInterfaces.restype = wintypes.BOOL
setupapi.SetupDiEnumDeviceInterfaces.argtypes = [
        HDEVINFO, ctypes.c_void_p, ctypes.POINTER(GUID), wintypes.DWORD,
        ctypes.POINTER(DeviceInterfaceData)]
setupapi.SetupDiGetDeviceInterfaceDetailA.restype = wintypes.BOOL
setupapi.SetupDiGetDeviceInterfaceDetailA.argtypes = [
        HDEVINFO, ctypes.POINTER(DeviceInterfaceData),
        ctypes.POINTER(DeviceInterfaceDetailData), wintypes.DWORD,
        ctypes.POINTER(wintypes.DWORD), ctypes.c_void_p]

kernel32.CreateFileA.restype = HANDLE
kernel32.CreateFileA.argtypes = [
        ctypes.c_char_p, wintypes.DWORD, wintypes.DWORD, ctypes.c_void_p,
        wintypes.DWORD, wintypes.DWORD, HANDLE]
kernel32.CloseHandle.restype = wintypes.BOOL
kernel32.CloseHandle.argtypes = [HANDLE]
kernel32.ReadFile.restype = wintypes.BOOL
kernel32.ReadFile.argtypes = [
        HANDLE, ctypes.c_void_p, wintypes.DWORD,
        ctypes.POINTER(wintypes.DWORD), ctypes.c_void_p]
kernel32.WriteFile.restype = wintypes.BOOL
kernel32.WriteFile.argtypes = [
        HANDLE, ctypes.c_void_p, wintypes.DWORD,
        ctypes.POINTER(wintypes.DWORD), ctypes.c_void_p]


def FillDeviceAttributes(device, descriptor):
    """Fill out the attributes of the device.

    Fills the devices HidAttributes and product string
    into the descriptor.

    Args:
        device: A handle to the open device
        descriptor: The DeviceDescriptor to populate with the
            attributes.

    Returns:
        None

    Raises:
        WindowsError when unable to obtain attributes or product
            string.
    """
    attributes = HidAttributes()
    result = hid.HidD_GetAttributes(device, ctypes.byref(attributes))
    if not result:
        raise ctypes.WinError()

    buf = ctypes.create_string_buffer(1024)
    result = hid.HidD_GetProductString(device, buf, 1024)

    if not result:
        raise ctypes.WinError()

    descriptor.vendor_id = attributes.VendorID
    descriptor.product_id = attributes.ProductID
    descriptor.product_string = ctypes.wstring_at(buf)
    print("found device, vid={}".format(descriptor.vendor_id))


def FillDeviceCapabilities(device, descriptor):
    """Fill out device capabilities.

    Fills the HidCapabilitites of the device into descriptor.

    Args:
        device: A handle to the open device
        descriptor: DeviceDescriptor to populate with the
            capabilities

    Returns:
        none

    Raises:
        WindowsError when unable to obtain capabilitites.
    """
    preparsed_data = PHIDP_PREPARSED_DATA(0)
    ret = hid.HidD_GetPreparsedData(device, ctypes.byref(preparsed_data))
    if not ret:
        raise ctypes.WinError()

    try:
        caps = HidCapabilities()
        ret = hid.HidP_GetCaps(preparsed_data, ctypes.byref(caps))

        if ret != HIDP_STATUS_SUCCESS:
            raise ctypes.WinError()
        print("feature size: {}".format(caps.FeatureReportByteLength))
        print("out size: {}".format(caps.OutputReportByteLength))
        descriptor.usage = caps.Usage
        descriptor.usage_page = caps.UsagePage
        descriptor.internal_max_in_report_len = caps.InputReportByteLength
        descriptor.internal_max_out_report_len = caps.OutputReportByteLength
        descriptor.internal_max_feature_report_len = caps.FeatureReportByteLength

    finally:
        hid.HidD_FreePreparsedData(preparsed_data)


# The python os.open() implementation uses the windows libc
# open() function, which writes CreateFile but does so in a way
# that doesn't let us open the device with the right set of permissions.
# Therefore, we have to directly use the Windows API calls.
# We could use PyWin32, which provides simple wrappers.    However, to avoid
# requiring a PyWin32 dependency for clients, we simply also implement it
# using ctypes.
def OpenDevice(path, enum=False):
    """Open the device and return a handle to it."""
    desired_access = GENERIC_WRITE | GENERIC_READ
    share_mode = FILE_SHARE_READ | FILE_SHARE_WRITE
    if enum:
        desired_access = 0

    h = kernel32.CreateFileA(path,
                                                     desired_access,
                                                     share_mode,
                                                     None, OPEN_EXISTING, 0, None)
    if h == INVALID_HANDLE_VALUE:
        raise ctypes.WinError()
    return h

class DeviceDescriptor(object):
  """Descriptor for basic attributes of the device."""

  usage_page = None
  usage = None
  vendor_id = None
  product_id = None
  product_string = None
  path = None
  rd_path = "" 
  chip = 817 
  bus = "USB"

  internal_max_in_report_len = 0
  internal_max_out_report_len = 0
  internal_max_feature_report_len = 0

  def ToPublicDict(self):
    out = {}
    for k, v in list(self.__dict__.items()):
      out[k] = v
    return out

class io:
    """Implementation of raw HID interface on Windows."""

    def __init__(self, debug=logging.ERROR):
        print(debug)
        logging.basicConfig(level=debug, format='%(asctime)s - %(levelname)s : %(message)s')
        logging.info("Init windows device.")
        self.io_dev = None 
        self.feature_dev = None 
        self.desc = None 
        self.input_thread = None
        self.hidraw_reciver = None
        self.hidraw_buf = []
        self.buf_size = 1024

    def Enumerate(self, vid=None, dev_path=None):
        hid_guid = GUID()
        hid.HidD_GetHidGuid(ctypes.byref(hid_guid))

        devices = setupapi.SetupDiGetClassDevsA(
                ctypes.byref(hid_guid), None, None, 0x12)
        index = 0
        interface_info = DeviceInterfaceData()
        interface_info.cbSize = ctypes.sizeof(DeviceInterfaceData)    # pylint: disable=invalid-name

        out = []
        while True:
            result = setupapi.SetupDiEnumDeviceInterfaces(
                    devices, 0, ctypes.byref(hid_guid), index,
                    ctypes.byref(interface_info))
            index += 1
            if not result:
                break
            detail_len = wintypes.DWORD()
            result = setupapi.SetupDiGetDeviceInterfaceDetailA(
                    devices, ctypes.byref(interface_info), None, 0,
                    ctypes.byref(detail_len), None)

            detail_len = detail_len.value
            if detail_len == 0:
                # skip this device, some kind of error
                continue

            buf = ctypes.create_string_buffer(detail_len)
            interface_detail = DeviceInterfaceDetailData.from_buffer(buf)
            interface_detail.cbSize = ctypes.sizeof(DeviceInterfaceDetailData)

            result = setupapi.SetupDiGetDeviceInterfaceDetailA(
                    devices, ctypes.byref(interface_info),
                    ctypes.byref(interface_detail), detail_len, None, None)

            if not result:
                raise ctypes.WinError()

            descriptor = DeviceDescriptor() 
            # This is a bit of a hack to work around a limitation of ctypes and
            # "header" structures that are common in windows.    DevicePath is a
            # ctypes array of length 1, but it is backed with a buffer that is much
            # longer and contains a null terminated string.    So, we read the null
            # terminated string off DevicePath here.    Per the comment above, the
            # alignment of this struct varies depending on architecture, but
            # in all cases the path string starts 1 DWORD into the structure.
            #
            # The path length is:
            #     length of detail buffer - header length (1 DWORD)
            path_len = detail_len - ctypes.sizeof(wintypes.DWORD)
            descriptor.path = ctypes.string_at(
                    ctypes.addressof(interface_detail.DevicePath), path_len)
            #print(dir(descriptor))
            device = None
            try:
                device = OpenDevice(descriptor.path, False)
            except WindowsError as e:    # pylint: disable=undefined-variable
                if e.winerror == ERROR_ACCESS_DENIED:    # Access Denied, e.g. a keyboard
                    continue
                else:
                    raise e
            try:
                FillDeviceAttributes(device, descriptor)
                FillDeviceCapabilities(device, descriptor)
                out.append(descriptor.ToPublicDict())
            except:
                continue    
            finally:
                if descriptor.vendor_id == vid and descriptor.internal_max_out_report_len > 0:
                    self.io_dev = device
                    self.desc = descriptor
                    print()
                    print("get io device {}".format(descriptor.ToPublicDict()))
                elif descriptor.vendor_id == vid and descriptor.internal_max_feature_report_len > 0:
                    self.feature_dev = device
                    print("get featur device {}".format(descriptor.ToPublicDict()))
                else:
                    kernel32.CloseHandle(device)

    def search(self, vid=0, dev_path=None):
        self.Enumerate(vid=vid, dev_path=dev_path)
        if self.feature_dev is not None:
            self.desc.chip = 819
        elif self.io_dev is not None:
            self.desc.chip = 817
        else:
            return False
        return True

    def open(self, enable_read_thread=False):
        if self.hidraw_reciver is not None:
            return
        else:
            if enable_read_thread == True:
                self.hidraw_reciver = win32_hidraw(self.read_cb) 
                self.hidraw_reciver.thread_run() 

    def read_cb(self, buf):
        if buf[0] == 0xa: # 817 report_id is 0xa
            logging.debug("read_cb buf:{}".format(buf))
            self.result_buf.append(buf)
        else: # hidraw report_id is 0x61
            logging.debug("read_cb buf:{}".format(buf))
            if len(self.hidraw_buf) > self.buf_size:
                self.hidraw_buf.clear()
            self.hidraw_buf.append(buf)

    def get_desc(self):
        return self.desc

    def set_raw_data_handler(self, funct):
        "Set external raw data handler, set to None to restore default"
        self.__raw_handler = funct

    def get_in_report_data_length(self):
        return self.desc.internal_max_in_report_len

    def get_out_report_data_length(self):
        return self.desc.internal_max_out_report_len

    def feature_write(self, packet):
        if self.feature_dev is not None:
            report_id = packet[0]
            size = 64 * (report_id & 0xf)
            if len(packet) != size:
                raise HidError("Packet length{} must match report data length{}.".format(len(packet), size))

            raw_data_type = ctypes.c_ubyte * size 
            raw_data = raw_data_type()
            for index in range(size):
                raw_data[index] = packet[index]
            print(len(raw_data))
            ret = hid.HidD_SetFeature(self.feature_dev, ctypes.byref(raw_data), len(raw_data))
            if not ret:
                raise ctypes.WinError()

    def feature_read(self, report_id=0x21):
        if self.feature_dev is not None:
            #ToDo, read different report_id and size
            size = 64 * (report_id & 0xf)
            raw_data_type = ctypes.c_ubyte * size
            raw_data = raw_data_type()
            raw_data[0] = report_id
            ret = hid.HidD_GetFeature(self.feature_dev, ctypes.byref(raw_data), len(raw_data))

            if not ret:
                raise ctypes.WinError()

            # Convert the string buffer to a list of numbers.    Throw away the first
            # byte, which is the report id (which we don't care about).
            return list(bytearray(raw_data))


    def output_write (self, packet):
        if len(packet) != self.get_out_report_data_length():
            raise HidError("Packet length{} must match report data length{}.".format(len(packet), self.get_out_report_data_length()))

        packet_data = packet    # Prepend the zero-byte (report ID)
        out = bytes(bytearray(packet_data))
        num_written = wintypes.DWORD()
        ret = kernel32.WriteFile(
                        self.io_dev, out, len(out),
                        ctypes.byref(num_written), None)
        if num_written.value != len(out):
            raise HidError("Failed to write complete packet. " + "Expected %d, but got %d" %
                    (len(out), num_written.value))
        if not ret:
            raise ctypes.WinError()
        else:
            print("output report write success")

    def input_read(self):
        buf = ctypes.create_string_buffer(self.get_in_report_data_length())
        num_read = wintypes.DWORD()
        ret = kernel32.ReadFile(
                self.io_dev, buf, len(buf), ctypes.byref(num_read), None)

        if num_read.value != self.get_in_report_data_length():
            raise HidError("Failed to read full length report from device.")

        if not ret:
            raise ctypes.WinError()
        # Convert the string buffer to a list of numbers.    Throw away the first
        # byte, which is the report id (which we don't care about).
        return list(bytearray(buf))

    def read(self, report_id=0x2d):
        ret = False
        buf = []
        if self.desc.chip == 817:
            buf = self.input_read()
        else:
            buf = self.feature_read(report_id)
        if len(buf) > 0:
            logging.debug('device read:[{}]'.format(', '.join(hex(x) for x in buf)))
            ret = True
        print(buf)
        return ret,buf

    def write(self, addr, data=[], report_id=0x2d):
        buf = []
        size = 0
        if report_id < 0x60:
            if self.desc.bus == "USB":
                if self.desc.chip == 819:
                    size = 64 * (report_id & 0xf)
                    length = 3 + len(addr) + len(data)
                    buf = [report_id, length>>8, length & 0xff]
                elif self.desc.chip == 817:
                    size = 64
                    length = 1 + len(addr) + len(data)
                    buf = [0x9, length & 0xff]
            elif self.desc.bus == "HID_OVER_I2C":
                if self.desc.chip == 819:
                    length = 2 + len(addr) + len(data)
                    size = length
                    buf = [0x9, 0]
        else:
            buf = [report_id]
            size = len(addr) + 1

        buf.extend(addr)
        if len(data) > 0:
            buf.extend(data)
        buf.extend([0] * (size - len(buf)))
        logging.debug('device write:[{}]'.format(', '.join(hex(x) for x in buf)))
 
        if self.desc.chip == 819:
            if self.feature_dev:
                self.feature_write(buf)
                return True

        elif self.desc.chip == 817:
            if self.io_dev:
                self.output_write(buf)
                return True
        logging.info("cmd[{}] fail".format(addr));
        return False


    def __del__(self):
        """Closes the file handle when object is GC-ed."""
        if hasattr(self, 'io_dev'):
            print("close")
            kernel32.CloseHandle(self.io_dev)
            if self.feature_dev is not None:
                kernel32.CloseHandle(self.feature_dev)

    def close(self):
        self.__del__()

class input_report_thread(threading.Thread):
    "Helper to receive input reports"
    def __init__(self, dev, cb):
        threading.Thread.__init__(self)
        self.__abort = False
        self.__active = False
        self.cb = cb
        self.dev = dev
        self.hid_handle = int( dev.io_dev )
        self.raw_report_size = dev.get_in_report_data_length()
        self.__abort_lock = threading.RLock()
        if dev and self.hid_handle and self.raw_report_size:
            #only if input reports are available
            self.daemon = True
            self.start()
            print("input thread staring")

    def abort(self):
        """Stop collectiong reports."""
        with self.__abort_lock:
            if not self.__abort:
                # The abort variable must be set to true
                # before sending the event, otherwise
                # the reader thread might skip
                # CancelIo
                self.__abort = True

    def is_active(self):
        "main reading loop is running (bool)"
        return bool(self.__active)

    def run(self):
        if not self.raw_report_size:
            # don't raise any error as the hid object can still be used
            # for writing reports
            raise HIDError("Attempting to read input reports on non "\
                "capable HID device")

        try:
            bytes_read = ctypes.c_ulong()
            dev = self.dev
            report_len = self.raw_report_size
            #main loop active
            self.__active = True
            while not self.__abort:
                #get storage
                buf_report = ctypes.create_string_buffer(self.raw_report_size)
                if not buf_report or self.__abort:
                    break
                bytes_read.value = 0

                with self.__abort_lock:
                    # Call to ReadFile must only be done if
                    # abort isn't set.
                    if self.__abort:
                        break
                    # async read from device
                    buf = ctypes.create_string_buffer(self.raw_report_size)
                    num_read = wintypes.DWORD()
                    ret = kernel32.ReadFile(
                            self.hid_handle, buf, len(buf), ctypes.byref(num_read), None)

                    if num_read.value != self.get_in_report_data_length():
                        raise HidError("Failed to read full length report from device.")

                    if not ret:
                        raise ctypes.WinError()

                # signal raw data already read
                self.cb( buf )
        finally:
            #clean up
            self.__active = False
            self.__abort = True


