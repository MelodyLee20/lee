# -*- coding: utf-8 -*-
import usb
import os
import sys
import logging
import hid
import threading, time


class io:
    def __init__(self, debug=logging.ERROR):
        logging.basicConfig(level=debug, format='%(asctime)s - %(levelname)s : %(message)s')
        logging.info("Init PyHidapi device.")
        self.vendor_id = 0x0457
        self.product_id = 0
        self.reattach = False
        self.dev = None
        self.chip = 819
        self.ep_in = None  # type: usb.core.Endpoint
        self.ep_out = None  # type: usb.core.Endpoint
        self.read_thread = None
        self.result_buf = []
        self.hidraw_buf = []
        self.buf_size = 1024

    def set_callback(self, cb):
        pass

    def search(self):
        for dev in hid.enumerate():
            keys = list(dev.keys())
            keys.sort()
            if dev["vendor_id"] == self.vendor_id:
                self.product_id = dev["product_id"]
                logging.debug("get pid:{}".format(self.product_id))
            
        if self.product_id == 0:
            logging.error("PyHidapi_io: SIS TP(X)");
            return False,self.chip
        self.dev = hid.device()
        self.dev.open(self.vendor_id, self.product_id)
        self.dev.set_nonblocking(1)
        logging.info("PyHidapi_io: SIS TP (O)");
        try:
            self.dev.send_feature_report([0x21,0])
            while True:
                d = self.dev.get_feature_report(0x21, 64)
                if d[0] != 0x21:
                    print("it's 817")
                    self.chip = 817
                    break
                else:
                    break
        except:
            print("it's 817")
            self.chip = 817
        return True,self.chip

    def open(self):
        # start read thread
        self.read_thread = threading.Thread(target=self.read_handler)
        self.read_thread.daemon = True
        self.read_thread.start()


    # handler for reading incoming raw HID packets via PyHidapi (thread started in local open() method)
    def read_handler(self):
        while self.dev:
            try:
                buf = self.dev.read(64)
                if len(buf) > 0 and buf[0] > 0:
                    logging.debug("PyHidapi get ep_in data {}".format(bytes(buf).hex()))
                    if buf[0] == 0xa:
                        self.result_buf.append(buf)
                        logging.debug("PyHidapi get result")
                    elif buf[0] == 0x61: # hidraw report_id is 0x61
                        if len(self.hidraw_buf) > self.buf_size:
                            self.hidraw_buf.clear()
                        self.hidraw_buf.append(buf)
            except:
                pass
                #logging.debug("PyHidapi read thread error")

        logging.debug("PyHidapi read thread exited")

    def read(self, report_id=0x21):
        ret = False
        buf = self.dev.get_feature_report(0x21, 64)
        if len(buf) > 0:
            print(buf)
            ret = True
        return ret,buf

    def write(self, chip, addr, data=[], report_id=0x21):
        if chip == 819:
            size = 64 * (report_id & 0xf)
            length = 3 + len(addr) + len(data)
            buf = [report_id, length>>8, length & 0xff]
        elif chip == 817:
            size = 64
            length = 1 + len(addr) + len(data)
            buf = [0x9, length & 0xff]
        buf.extend(addr)
        if len(data) > 0:
            buf.extend(data)
        buf.extend([0] * (size - len(buf)))
        logging.debug(buf)
        if self.dev:
            if chip == 819:
                ret = self.dev.send_feature_report(buf)
                if ret >= 0:
                    logging.debug("819 cmd[{}] success".format(buf));
                    return True
            elif chip == 817:
                ret =self.dev.write(buf)
                print(ret)
                if ret >= 0:
                    logging.debug("817 cmd[{}] success".format(buf));
                    return True
            
        logging.debug("cmd[{}] fail".format(addr));
        return False

    def close(self):
        self.dev.close()

    def deactive(self):
        pass

    def active(self):
        pass


