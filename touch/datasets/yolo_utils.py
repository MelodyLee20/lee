import os
from shutil import copyfile

from .voc_datasets import VOCDetection

def voc_to_yolo_files(filepath, yolo_dir, image_dir, voc_dir, classes=None):
    # Convert VOC to YOLO files.
    dataset = VOCDetection(image_dir, voc_dir, classes=classes)
    anns = dataset.anns

    print('Generate the {}'.format(filepath))

    with open(filepath, 'w') as f:
        for i, sample in enumerate(dataset):
            image_filename = anns[i]['filename']
            ext = os.path.splitext(image_filename)[1]

            image_path_src = os.path.join(image_dir, image_filename)
            image_path = os.path.join(yolo_dir, image_filename)

            # Copy the image file
            copyfile(image_path_src, image_path)

            # Record the image path
            line = '{}\n'.format(image_path)
            f.write(line)

            yolo_filename = image_filename.replace(ext, '.txt')
            yolo_filepath = os.path.join(yolo_dir, yolo_filename)

            with open(yolo_filepath, 'w') as yolo_f:
                target = sample['target']
                for obj in target:
                    line = '{} {} {} {} {}\n'.format(obj[0], obj[1], obj[2], obj[3], obj[4])
                    yolo_f.write(line)

