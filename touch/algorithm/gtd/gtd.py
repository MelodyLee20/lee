import numpy as np

class GTD:
    # Ghost Detection.

    def __init__(self, row_border=3, col_border=2):

        # Input data
        self.data = None
        self.local_data = None

        # Variance
        self.var = None
        self.rows = None
        self.cols = None


        # Border
        self.row_border = row_border
        self.col_border = col_border

        # Global bounds
        self.ia = None
        self.iz = None
        self.ja = None
        self.jz = None


    def set_data(self, data):
        # Set the input data.

        self.data = data
        self.var = np.zeros_like(data, dtype=np.float64)
        self.rows, self.cols = data.shape

        # Global bounds
        self.ia = 0
        self.iz = self.rows-1
        self.ja = 0
        self.jz = self.cols-1

    def get_local_bounds(self, i, j):

        ia = i - self.row_border
        iz = i + self.row_border
        ja = j - self.col_border
        jz = j + self.col_border

        if i <= self.ia + self.row_border:
            ia = self.ia

        if i >= self.iz - self.row_border:
            iz = self.iz

        if j <= self.ja + self.col_border:
            ja = self.ja

        if j >= self.jz - self.col_border:
            jz = self.jz


        return (ia, iz, ja, jz)

    def get_local_data(self, i,j):

        ia, iz, ja, jz = self.get_local_bounds(i, j)
        self.local_data = self.data[ia:iz+1, ja:jz+1]

        return self.local_data


    def get_variance(self):
        # Get the 2d array of variance.
        # var = mean(abs(x - x.mean())**2).

        for i in range(self.rows):
            for j in range(self.cols):
                local_data = self.get_local_data(i,j)
                self.var[i, j] = np.var(local_data)

        return self.var
