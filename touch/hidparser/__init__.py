import  sys
#import .hidparser 
from .Item import Item, ItemType
from .ItemMain import *
from .ItemGlobal import *
from .ItemLocal import *

from .DeviceBuilder import DeviceBuilder
from .Device import Device, Collection, ReportGroup, Report

from .UsagePage import UsagePage, Usage, UsageRange, UsageType
from .UsagePages import *

def parse(data):
    items = get_items(data)

    descriptor_builder = DeviceBuilder()
    for item in items:
        #print("New item:{}".format(item))
        item.visit(descriptor_builder)

    return descriptor_builder.build()


def get_items(data):
    import array

    if isinstance(data, bytes):
        data = array.array('B', data)
    # grab the next len bytes and return an array from the iterator
    get_bytes = lambda it, len: [next(it) for x in range(len)]

    byte_iter = iter(data)
    skip = 0
    while True:
        try:
            item = next(byte_iter)
            # Check if the item is "Long"
            if item is 0xFE:
                size = next(byte_iter)
                tag = next(byte_iter)
                if tag not in range(0xF0, 0xFF):
                    raise ValueError("Long Items are only supported by Vender defined tags as of Version 1.11")
                # Yield a long item, There are no tags defined in HID as of Version 1.11
                yield Item(tag=tag, data=get_bytes(byte_iter, size), long=True)
            # Short item's size is the first two bits (eitehr 0,1,2 or 4)
            size = item & 0x03
            #print("item:{}, size:{}".format(hex(item),size))
            if size is 3:
                size = 4

            # Get the item tag type from bits 3 - 2
            item_type = ItemType(item & 0x0C)
            if item_type is ItemType.RESERVED:
                raise ValueError("Invalid bType in short item")

            yield Item.create(tag=item & 0xFC, data=get_bytes(byte_iter, size))

        except StopIteration:
            break
    pass

def parse_data(data, parser):
    if parser is not None:
        points = [] 
        while len(data) > 0:
            got_data = 0
            x_max = 0
            y_max = 0
            report = {}
            finger = {}
            buf = bytearray(data.popleft())
            #print(buf.hex())
            report_id = buf[0]
            parser.deserialize(buf)
            tp = parser.reports[report_id].inputs
            for key in tp._attrs.keys():
                #print(key)
                if key == "touch_screen":
                    ts = tp.touch_screen
                    if ts.finger[0]:
                        for i in ts.finger[0].items:
                            if i.usages:
                                if i.usages[0]._name_ == "X":
                                    x_max = i.physical_range.maximum
                                    #print("xmin:{}, max:{}".format(i.physical_range.minimum,i.physical_range.maximum))
                                elif i.usages[0]._name_ == "Y":
                                    y_max = i.physical_range.maximum
                                    #print("ymin:{}, max:{}".format(i.physical_range.minimum,i.physical_range.maximum))
                        
                    for i in range(len(ts.finger)):
                        finger = ts.finger[i]
                        for key in finger._attrs.keys():
                            value = finger.__getattr__(key)
                            got_data = 1
                            report["%s_%d" % (key,i)] = value 
                elif key == "pen":
                    pen = tp.pen.stylus
                    for item in pen.items:
                        try:
                            if item.usages:
                                if item.usages[0]._name_ == "X":
                                    x_max = item.physical_range.maximum
                                elif item.usages[0]._name_ == "Y":
                                    y_max = item.physical_range.maximum
                                #print(item.usages[0]._name_)
                                #print("mitem.:{}, max:{}".format(item.physical_range.minimum,item.physical_range.maximum))
                        except:
                            pass
                    for key in pen._attrs.keys():
                        value = pen.__getattr__(key)
                        #print("key:{}, value:{}".format(key, value))
                        got_data = 1
                        if isinstance(value, Collection):
                            for sub_key in value._attrs.keys():
                                sub_value = value.__getattr__(sub_key)
                                report[sub_key] = sub_value 
                        elif isinstance(value, list):
                            for j in range(len(value)):
                                sub_value = value[j].fget()
                                sub_key = "{}_{}".format(key,j)
                                report[sub_key] = sub_value 
                        else:
                            if key == "transducer_serial_number" or key[:6] =="vendor":
                                report[key] = hex(value)
                            else:
                                report[key] = value 
                    
                else:
                    print("!!!!! parse error, {}".format(tp.__getattr__(key)))
            if got_data == 1:
                #print(report)
                points.append(report)
        return points


