from .GenericDesktop import GenericDesktop
from .VirtualReality import VirtualReality
from .GenericDeviceControls import GenericDeviceControls
from .Button import Button
from .Digitizers import Digitizers

from .Sensor import Sensor
from .Power import Power
from .Battery import Battery
from .VendorDefined import VendorDefined
