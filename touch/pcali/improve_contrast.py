import cv2 as cv

def improve_contrast(img):

    lab= cv.cvtColor(img, cv.COLOR_BGR2LAB)

    #-----Splitting the LAB image to different channels-------------------------
    l, a, b = cv.split(lab)

    #-----Applying CLAHE to L-channel-------------------------------------------
    clahe = cv.createCLAHE(clipLimit=3.0, tileGridSize=(8,8))
    cl = clahe.apply(l)

    #-----Merge the CLAHE enhanced L-channel with the a and b channel-----------
    limg = cv.merge((cl,a,b))

    #-----Converting image from LAB Color model to BGR model--------------------
    out = cv.cvtColor(limg, cv.COLOR_LAB2BGR)

    return out
