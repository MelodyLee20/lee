import cv2

def draw_contours(image, contours):

    image = image.copy()

    # Draw all contours
    image = cv2.drawContours(image, contours, -1, (0,255,0), 3)

    return image
