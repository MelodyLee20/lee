from .locator import Locator
from .target_factory import TargetFactory
from .find_distance import find_distance
from ..fitting import SimpleModel, TouchModel
import numpy as np

def RMSE(targets, preds):
    # Return the RMSE.

    num_points = len(targets)

    err = 0.0
    for i in range(num_points):
        err += find_distance(targets[i], preds[i])

    err = err / num_points

    return err

class Calibrator:

    def __init__(self):

        self.locator = None
        self.target_factory = None
        self.model = SimpleModel()
        #self.model = TouchModel()

        # Optimal parameters
        self.popt = None

        # Points in the TP coordinates
        self.input_points = None
        self.target_points = None
        self.pred_points = None

    def to_tp_coordinates(self, input_points, target_points, nx_tp, ny_tp):
        # Convert to the TP coordinates.

        input_points[:, 0] *= nx_tp
        input_points[:, 1] *= ny_tp
        target_points[:, 0] *= nx_tp
        target_points[:, 1] *= ny_tp

        return input_points, target_points

    def get_cali_data(self, image, nx=14, ny=14, nx_tp=4096, ny_tp=4096):
        # Get the data for calibration.

        locator = Locator(image)
        tp_markers, sc_markers = locator.detect()

        # Find the points in the screen coordinates
        sc_points = locator.get_sc_points()

        tf = TargetFactory(sc_points, nx=nx, ny=ny)
        input_points = tf.get_input_points()
        target_points = tf.get_target_points()

        # Scale to TP coordinates
        input_points, target_points = self.to_tp_coordinates(input_points, target_points, nx_tp, ny_tp)

        # Save
        self.locator = locator
        self.target_factory = tf
        self.input_points = input_points
        self.target_points = target_points

        return input_points, target_points


    def fit(self):

        score, popt = self.model.fit(self.input_points, self.target_points)

        # Save
        self.popt = popt

        return score, popt

    def predict(self, point):

        out = self.model.forward(point, self.popt)

        return out

    def get_rmse(self):
        # Returns the RMSE.

        targets = []
        preds = []

        for i, p in enumerate(self.input_points):
            target = self.target_points[i]
            pred = self.predict(p)

            targets.append(target)
            preds.append(pred)

        rmse = RMSE(targets, preds)

        return rmse
