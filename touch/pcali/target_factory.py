from .linspace_points import linspace_points
import numpy as np

class TargetFactory:

    def __init__(self, sc_points, nx=8, ny=8):

        self.nx = nx
        self.ny = ny
        self.num_target_points = nx*ny
        self.sc_points = sc_points


    def get_target_points(self):
        # Get the target points with respaect to the screen.

        # Target points
        points = np.zeros((self.num_target_points, 2), dtype=np.float64)

        x_arr = np.linspace(0, 1.0, num=self.nx, endpoint=True, dtype=np.float64)
        y_arr = np.linspace(0, 1.0, num=self.ny, endpoint=True, dtype=np.float64)

        i = 0
        for x in x_arr:
            for y in y_arr:
                points[i, 0] = x
                points[i, 1] = y
                i += 1

        return points

    def get_input_points_old(self):
        # Get the input points with respect to the TP.

        # Reference points
        x0 = self.sc_points[0, 0]
        y0 = self.sc_points[0, 1]
        x1 = self.sc_points[1, 0]
        y1 = self.sc_points[1, 1]
        x2 = self.sc_points[2, 0]
        y2 = self.sc_points[2, 1]
        x3 = self.sc_points[3, 0]
        y3 = self.sc_points[3, 1]


        # Target points
        points = np.zeros((self.num_target_points, 2), dtype=np.float64)

        # p0
        x = x0
        y = y0
        points[0, 0] = x
        points[0, 1] = y

        # p1
        x = 0.5*(x0+x1)
        y = 0.5*(y0+y1)
        points[1, 0] = x
        points[1, 1] = y

        # p2
        x = x1
        y = y1
        points[2, 0] = x
        points[2, 1] = y


        # p3
        x = 0.5*(x0+x3)
        y = 0.5*(y0+y3)
        points[3, 0] = x
        points[3, 1] = y

        # p4
        x = 0.25*(x0+x1+x2+x3)
        y = 0.25*(y0+y1+y2+y3)
        points[4, 0] = x
        points[4, 1] = y

        # p5
        x = 0.5*(x1+x2)
        y = 0.5*(y1+y2)
        points[5, 0] = x
        points[5, 1] = y

        # p6
        x = x3
        y = y3
        points[6, 0] = x
        points[6, 1] = y

        # p7
        x = 0.5*(x2+x3)
        y = 0.5*(y2+y3)
        points[7, 0] = x
        points[7, 1] = y

        # p8
        x = x2
        y = y2
        points[8, 0] = x
        points[8, 1] = y

        return points

    def get_input_points(self):
        # Get the input points with respect to the TP.

        # Reference points
        p0 = self.sc_points[0]
        p1 = self.sc_points[1]
        p2 = self.sc_points[2]
        p3 = self.sc_points[3]

        nx = self.nx
        ny = self.ny
        points_top= linspace_points(p0, p3, num=nx)
        points_bottom = linspace_points(p1, p2, num=nx)

        points = np.zeros((self.num_target_points, 2), dtype=np.float64)

        for i in range(nx):

            pa = points_top[i]
            pz = points_bottom[i]
            col_points = linspace_points(pa, pz, num=ny)

            ipa = i*ny
            ipz = (i+1)*ny
            points[ipa:ipz] = col_points[:]

        return points
