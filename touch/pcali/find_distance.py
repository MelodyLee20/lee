import numpy as np

def find_distance(p1, p2):

    dx = p2[0] - p1[0]
    dy = p2[1] - p1[1]

    d = np.sqrt(dx*dx + dy*dy)

    return d

