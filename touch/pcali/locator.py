from touch.pcali.marker import Marker
from touch.pcali.point_and_line import get_distance_from_two_points, \
    get_slope_from_two_points,  get_perp_foot_from_three_points
from .detect_color import detect_color
from .grab_contours import grab_contours
from .find_distance import find_distance
from touch.utils.opencv import  perspective_transform

import numpy as np
import cv2 as cv
import sys

def mask_rect_area(image, ix_a, ix_z, iy_a, iy_z):
    # Mask a rectange area.

    image = image.copy()
    # Set the mask)
    mask = np.ones(image.shape[0:2], dtype=np.uint8)
    mask[iy_a:iy_z+1, ix_a:ix_z+1] = 0

    masked = cv.bitwise_and(image, image, mask=mask)

    return masked



def get_binary_image(image):
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    blurred = cv.GaussianBlur(gray, (5, 5), 0)
    #binary = cv.adaptiveThreshold(blurred, 255, cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY_INV,11,5)
    ret, binary = cv.threshold(blurred, 10, 255, cv.THRESH_BINARY)

    return binary

def find_contours(binary):

    height, width = binary.shape
    image_area = height*width

    # All contours
    cnts = cv.findContours(binary, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    cnts = grab_contours(cnts)

    # Save the possible contours
    contours = []
    for c in cnts:

        area = cv.contourArea(c)
        if area < image_area*0.0001:
            continue

        x, y, w, h = cv.boundingRect(c)
        ratio = 1.0*w/h

        if ratio > 3.0 or ratio < 0.5:
            continue

        epsilon = 0.1*cv.arcLength(c, True)
        c = cv.approxPolyDP(c, epsilon, True)
        c= c[:, 0]

        contours.append(c)

    return contours

def detect_contours(image):

    colored, mask = detect_color(image, color='blue')
    binary = get_binary_image(colored)
    contours = find_contours(binary)

    return contours

def to_markers(contours, image):

    h, w, c = image.shape
    image_area = h*w

    markers = []
    for c in contours:

        area = cv.contourArea(c)
        if area > image_area*0.1:
            continue

        x, y, w, h = cv.boundingRect(c)
        ratio = 1.0*w/h

        if ratio > 2.0 or ratio < 0.5:
            continue

        marker = Marker(c)
        markers.append(marker)

    return markers


def draw_contours(image, contours, line_width=10):

    image = image.copy()
    h, w, c = image.shape

    cv.drawContours(image, contours, -1, thickness=line_width, color=(0, 255, 0))

    return image


def draw_markers(image, markers, line_width=10):

    image = image.copy()
    h, w, c = image.shape

    for marker in markers:
        p1 = (marker.x, marker.y)
        p2 = (marker.x+marker.w, marker.y+marker.h)
        cv.rectangle(image, p1, p2, (0, 255, 0), line_width)

    return image

def draw_points(image, points, radius=10):

    image = image.copy()
    h, w, c = image.shape

    for p in points:
        x = int(p[0]+0.5)
        y = int(p[1]+0.5)
        cv.circle(image, (x, y), radius, (0, 0, 255), -1)

    return image


def sort_points(points):

    assert len(points) == 4

    corners = np.array(points)

    xc = np.mean(corners[:, 0])
    yc = np.mean(corners[:, 1])

    points = np.zeros((4, 2), dtype=np.float64)
    for p in corners:

        # P0
        if p[0] < xc and p[1] < yc:
            points[0] = p

        # P1
        if p[0] < xc and p[1] > yc:
            points[1] = p

        # P2
        if p[0] > xc and p[1] > yc:
            points[2] = p

        # P3
        if p[0] > xc and p[1] < yc:
            points[3] = p

    return points


def get_closest_one(markers, p_ref):

    target = markers[0]
    d_min = find_distance((target.x, target.y), p_ref)
    for marker in markers:
        d = find_distance((marker.x, marker.y), p_ref)
        if d < d_min:
            d_min = d
            target = marker

    return target


def save_image(filepath, image):
    cv.imwrite(filepath, image)


class Locator:
    # Locator.

    def __init__(self, image):

        self.image = image
        self.image_h, self.image_w, self.image_c = image.shape
        self.image_xc = 0.5*self.image_w
        self.image_yc = 0.5*self.image_h

        self.image_p = None # Perspective image

        self.tp_contours = None
        self.sc_contour = None

        self.num_tp_points = 4
        self.num_sc_points = 4

        # In pixel coordicates
        self.tp_points_px = None
        self.sc_points_px = None

        # In physical coordinates (Normalised)
        self.tp_points = self.get_tp_points()
        self.sc_points = np.zeros((self.num_sc_points, 2), dtype=np.float64)


    def mask_area(self, image):
        # Mask the center area.

        image = image.copy()
        h, w, c = image.shape

        # Add the vertival mask.
        ix_a = int(0.3*w)
        ix_z = int(0.7*w)
        iy_a = 0
        iy_z = h-1
        masked = mask_rect_area(image, ix_a, ix_z, iy_a, iy_z)

        # Add the horizontal mask.
        ix_a = 0
        ix_z = w-1
        iy_a = int(0.4*h)
        iy_z = int(0.6*h)
        masked = mask_rect_area(masked, ix_a, ix_z, iy_a, iy_z)


        return masked

    def detect(self):

        image = self.image

        # Detect color
        detected, mask = detect_color(image, color='blue')

        # Binary image
        binary = get_binary_image(detected)

        # Contours on the original image
        contours = find_contours(binary)

        # Save image of all contours
        tmp = draw_contours(image, contours)
        save_image('detect_contours_ori.jpg', tmp)

        # TP region
        tp_contours = self.find_tp_contours(contours)
        tp_points_px = self.find_tp_points_px(tp_contours)

        # Save the image of TP
        tmp = draw_contours(image, tp_contours)
        tmp = draw_points(tmp, tp_points_px)
        save_image('detect_tp_ori.jpg', tmp)

        # Perspective image
        image_p = self.get_perspective_image()

        # Contours on the perspective image
        contours = detect_contours(image_p)

        # Save image of contours
        tmp = draw_contours(image_p, contours)
        save_image('detect_contours.jpg', tmp)

        # Screen region
        sc_contour = self.find_sc_contour(contours)
        sc_points_px = self.find_sc_points_px(sc_contour)

        # Save image of screen
        tmp = draw_contours(image_p, [sc_contour])
        tmp = draw_points(tmp, sc_points_px)
        save_image('detect_sc.jpg', tmp)

        return tp_points_px, sc_points_px


    def sort_markers(self, markers_in):
        # Sort markerscounter-clockwise

        cx, cy = self.image_xc, self.image_yc

        groups = [[] for i in range(4)] # Counter-clockwise
        for marker in markers_in:

            if marker.x < cx and marker.y < cy:
                groups[0].append(marker)

            if marker.x < cx and marker.y > cy:
                groups[1].append(marker)

            if marker.x > cx and marker.y > cy:
                groups[2].append(marker)

            if marker.x > cx and marker.y < cy:
                groups[3].append(marker)


        # Markers sorted
        markers_out = []
        for group in groups:
            markers_out.append(group[0])

        return markers_out

    def find_sc_contour(self, contours):

        target = contours[0]
        target_area = cv.contourArea(target)
        for c in contours:
            area = cv.contourArea(c)
            if target_area < area:
                target = c
                target_area = area

        # Save
        self.sc_contour = target

        return target

    def find_sc_points_px(self, contour):
        self.sc_points_px = sort_points(contour)
        return self.sc_points_px


    def find_tp_contours(self, contours):

        image_xc = self.image_xc
        image_yc = self.image_yc

        candidates = []
        distances = []
        for c in contours:
            area = cv.contourArea(c)
            x, y, w, h = cv.boundingRect(c)
            d = get_distance_from_two_points(x, y, image_xc, image_yc)
            slope = get_slope_from_two_points(x, y, image_xc, image_yc)

            if area < 100 or area > 10000:
                continue

            if abs(slope) > 2.0:
                continue

            distances.append(d)
            candidates.append(c)

        # Collect the four contours which is closest to the image center.
        d_indexes = np.argsort(distances)
        tp_contours = []
        for i in d_indexes[:]:
            c = candidates[i]
            tp_contours.append(c)

        # Save
        self.tp_contours = tp_contours

        return tp_contours

    def find_tp_points_px(self, contours):

        image_xc = self.image_xc
        image_yc = self.image_yc

        for c in contours:
            marker = Marker(c)

            if marker.x < image_xc and marker.y < image_yc:
                m0 = marker

            if marker.x < image_xc and marker.y > image_yc:
                m1 = marker

            if marker.x > image_xc and marker.y > image_yc:
                m2 = marker

            if marker.x > image_xc and marker.y < image_yc:
                m3 = marker

        markers = [m0, m1, m2, m3]

        points = np.zeros((4, 2), dtype=np.float64)

        # p0
        x = markers[0].x + markers[0].w
        y = markers[0].y + markers[0].h
        points[0, 0] = x
        points[0, 1] = y

        # p1
        x = markers[1].x + markers[1].w
        y = markers[1].y
        points[1, 0] = x
        points[1, 1] = y

        # p2
        x = markers[2].x
        y = markers[2].y
        points[2, 0] = x
        points[2, 1] = y

        # p3
        x = markers[3].x
        y = markers[3].y + markers[3].h
        points[3, 0] = x
        points[3, 1] = y

        # save
        self.tp_points_px = points

        return points

    def draw_image(self, line_width=10):

        image = self.image_p.copy()
        h, w, c = image.shape

        image = draw_contours(image, [self.sc_contour])
        image = draw_points(image, self.sc_points_px)

        return image

    def get_tp_points(self):

        xa = 0.0
        xz = 1.0
        ya = 0.0
        yz = 1.0

        points = np.zeros((self.num_tp_points, 2), dtype=np.float64)

        points[0, 0] = xa
        points[0, 1] = ya

        points[1, 0] = xa
        points[1, 1] = yz

        points[2, 0] = xz
        points[2, 1] = yz

        points[3, 0] = xz
        points[3, 1] = ya

        # save
        self.tp_points = points

        return points

    def get_sc_point_component(self, p0, p1, p2):

        x0 = p0[0]
        y0 = p0[1]
        x1 = p1[0]
        y1 = p1[1]
        x2 = p2[0]
        y2 = p2[1]

        d = get_distance_from_two_points(x1, y1, x2, y2)
        xp, yp = get_perp_foot_from_three_points(x0, y0, x1, y1, x2, y2)
        d1 = get_distance_from_two_points(x1, y1, xp, yp)

        out = d1/d * 1.0

        return out

    def get_sc_points(self):

        image_h, image_w, image_c = self.image_p.shape

        # Find the sc points in pixel space
        sc_points_px = self.sc_points_px
        sc_points = np.zeros((self.num_sc_points, 2), dtype=np.float64)

        for i in range(self.num_sc_points):
            sc_points[i][0] = (sc_points_px[i][0]+1) / image_w
            sc_points[i][1] = (sc_points_px[i][1]+1) / image_h

        # Save points
        self.sc_points = sc_points

        return sc_points

    def get_perspective_image(self):

        pts = np.float32(self.tp_points_px)
        image_p = perspective_transform(self.image, pts)

        # Save
        self.image_p = image_p

        return image_p
