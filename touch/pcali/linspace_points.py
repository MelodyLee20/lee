import numpy as np

def linspace_points(p1, p2, num, endpoint=True):

    x1 = p1[0]
    y1 = p1[1]

    x2 = p2[0]
    y2 = p2[1]

    x_arr = np.linspace(x1, x2, num=num, endpoint=endpoint, dtype=np.float64)
    y_arr = np.linspace(y1, y2, num=num, endpoint=endpoint, dtype=np.float64)

    points = np.column_stack((x_arr, y_arr))

    return points
